package android.mobile.lms.aware.com.atsleave.fragmentview;

import org.junit.Assert;
import org.junit.Test;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class HomePageFragmentTest {
    @Test
    public void runGetDates(){

        Calendar cal1 = Calendar.getInstance();
        Date date1 = cal1.getTime();
        date1.setHours(0);
        date1.setMinutes(0);
        date1.setSeconds(0);

        DateFormat df1 = new SimpleDateFormat("yyyy-MM-dd");
        String formattedDate = df1.format(date1);

        List<Date> expect = new ArrayList<>();
        expect.add(date1);

        List<Date> result = HomePageFragment.getDates(formattedDate, formattedDate, "yyyy-MM-dd");

        Assert.assertEquals(expect.toArray().length, result.toArray().length);

        Assert.assertEquals(expect.isEmpty(), result.isEmpty());

        Assert.assertEquals(expect.toString().trim(), result.toString().trim());

        Assert.assertEquals(expect.get(0).toString().trim(), result.get(0).toString().trim());

    }
}
