package android.mobile.lms.aware.com.atsleave.fragmentview;

import org.junit.Assert;
import org.junit.Test;

public class ApproveFragmentTest {

    @Test
    public void runSetTagDate(){
        String result = ApproveFragment.setTagDate(2019,7,17).toString();
        Assert.assertEquals("20190817", result);

        result = ApproveFragment.setTagDate(0,0,0).toString();
        Assert.assertEquals("0010", result);
    }
}
