package android.mobile.lms.aware.com.atsleave.models;

import java.util.List;

/**
 * Created by apinun.w on 1/9/2558.
 */
public class BadgeResponse extends BaseResponse {

    private List<DataEntity> data;

    public void setData(List<DataEntity> data) {
        this.data = data;
    }

    public List<DataEntity> getData() {
        return data;
    }

    public static class DataEntity {

        private String STF_ID;
        private String LEAVE_MODULE;
        private String BADGE_COUNT;

        public void setSTF_ID(String STF_ID) {
            this.STF_ID = STF_ID;
        }

        public void setLEAVE_MODULE(String LEAVE_MODULE) {
            this.LEAVE_MODULE = LEAVE_MODULE;
        }

        public void setBADGE_COUNT(String BADGE_COUNT) {
            this.BADGE_COUNT = BADGE_COUNT;
        }

        public String getSTF_ID() {
            return STF_ID;
        }

        public String getLEAVE_MODULE() {
            return LEAVE_MODULE;
        }

        public String getBADGE_COUNT() {
            return BADGE_COUNT;
        }
    }
}
