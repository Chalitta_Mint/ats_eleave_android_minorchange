package android.mobile.lms.aware.com.atsleave.callback;

public interface RequestDialogListener {
    void responseSubmit();

    void responseCancel();
}
