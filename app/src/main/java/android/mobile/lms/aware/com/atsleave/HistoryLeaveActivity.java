package android.mobile.lms.aware.com.atsleave;

import android.app.Dialog;
import android.mobile.lms.aware.com.atsleave.adapter.ListLeaveHistoryAdapter;
import android.mobile.lms.aware.com.atsleave.callback.OnListenerFromWSManager;
import android.mobile.lms.aware.com.atsleave.models.StaffSummaryResponse;
import android.mobile.lms.aware.com.atsleave.models.TeamLeaveResponse;
import android.mobile.lms.aware.com.atsleave.utils.AwareUtils;
import android.mobile.lms.aware.com.atsleave.utils.DialogUtils;
import android.mobile.lms.aware.com.atsleave.webservice.LMSGeneralConstant;
import android.mobile.lms.aware.com.atsleave.webservice.LMSWSManager;
import android.os.Bundle;
import androidx.annotation.Nullable;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;

/**
 * Modify 20/08/2019 by Chalitta.k
 */

public class HistoryLeaveActivity extends CustomActivity {

    private String TAG = "HistoryLeaveActivity";
    private String empCode;
    private LMSApplication application;
    private ListLeaveHistoryAdapter adapter;
    private ListView listViewTeamHistory;
    private Dialog dialogWaiting;
    private ImageView btnClose;
    private TextView tv_no_history;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_leave_history);
        dialogWaiting = DialogUtils.createDialog(this);
        getExtraStaffID();
        initView();
        setCloseListener();
        getTeamLeave();

    }

    private void getExtraStaffID() {
        empCode = getIntent().getStringExtra("staffID");
    }

    private void initView() {
        application = (LMSApplication) getApplication();
        listViewTeamHistory = (ListView) findViewById(R.id.lv_team_history);
        btnClose = (ImageView) findViewById(R.id.img_close_btn);
        tv_no_history = (TextView) findViewById(R.id.tv_no_history);
        adapter = new ListLeaveHistoryAdapter(this, new ArrayList<>());
    }

    private void showAlertDialog() {
        if (!dialogWaiting.isShowing()) {
            dialogWaiting.show();
        }
    }

    private void dismissDialog() {
        if (dialogWaiting.isShowing()) {
            dialogWaiting.dismiss();
        }
    }

    private void setCloseListener() {

        btnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }

    private HashMap<String, Integer> analysisSummary(List<StaffSummaryResponse.DataEntity> liststaff) {
        HashMap<String, Integer> staffs = new HashMap<>();
        for (StaffSummaryResponse.DataEntity model : liststaff) {
            int minute = 0;
            if (model.getALLTIMEUSED() != null) {
                minute = Integer.parseInt(model.getALLTIMEUSED());
                staffs.put(model.getLEAVENAME(), minute);
            }
        }
        return staffs;
    }

    private void getTeamLeave() {
        int year = Calendar.getInstance().get(Calendar.YEAR);
        String startDay = year + "0101";
        String endDay = year + "1231";
        String leaveStatus = LMSGeneralConstant.LEAVE_STATUS.Pending + "," + LMSGeneralConstant.LEAVE_STATUS.Approved + "," + LMSGeneralConstant.LEAVE_STATUS.RejectedCancel;
        showAlertDialog();
        LMSWSManager.getTeamLeaveByDate(HistoryLeaveActivity.this, application.getCurProfile().getSTAFF_TOKEN(), empCode, "", "", leaveStatus, "", startDay, endDay, teamLeaveListener);
    }

    /*response from pending request*/
    OnListenerFromWSManager teamLeaveListener = new OnListenerFromWSManager() {
        @Override
        public void onComplete(String statusCode, String message) {
            Log.i("Responce", "Responce_TeamLeave: " + message);
            dismissDialog();
            Gson gson = new Gson();
            final TeamLeaveResponse members = gson.fromJson(message, TeamLeaveResponse.class);
            final List<TeamLeaveResponse.DataEntity> dataEntity = members.getData();

            //sort by group id
            Comparator<TeamLeaveResponse.DataEntity> comparator = new Comparator<TeamLeaveResponse.DataEntity>() {
                @Override
                public int compare(TeamLeaveResponse.DataEntity left, TeamLeaveResponse.DataEntity right) {
                    return left.getLEAVETYPEID().compareTo(right.getLEAVETYPEID());  //sort by group
                }
            };

            Collections.sort(dataEntity, comparator);

            //set by type
            HashMap<Integer, String> listTimeGroup = new HashMap<>();
            HashMap<Integer, Integer> listHeaderGroup = new HashMap<>();
            List<Object> objs = new ArrayList<Object>();
            if(dataEntity.isEmpty()){
                tv_no_history.setVisibility(View.VISIBLE);
            }else{
                tv_no_history.setVisibility(View.GONE);
                for (int i = 0; i < dataEntity.size(); i++) {
                    Log.d(TAG, "LEAVETYPEID : " + dataEntity.get(i).getLEAVETYPEID());
                    objs.add(dataEntity.get(i));

                    int leave_type_id = Integer.parseInt(dataEntity.get(i).getLEAVETYPEID());
                    if(!listTimeGroup.containsKey(leave_type_id))
                    {
                        String total = dataEntity.get(i).getRLD_ALLTIME();
                        listTimeGroup.put(leave_type_id, total);
                        listHeaderGroup.put(leave_type_id, i);
                    }
                    else
                    {
                        String time = listTimeGroup.get(leave_type_id);
                        String allTime = String.valueOf((Integer.parseInt(time) + Integer.parseInt(dataEntity.get(i).getRLD_ALLTIME())));
                        listTimeGroup.put(leave_type_id, allTime);
                    }
                }
                Log.d(TAG, "listTimeGroup size : " + listTimeGroup.size());
                adapter.setLeaveTeams(objs, listTimeGroup, listHeaderGroup);
                listViewTeamHistory.setAdapter(adapter);
                adapter.notifyDataSetChanged();
            }

        }

        @Override
        public void onFailed(String statusCode, String message) {
            dismissDialog();
        }

        @Override
        public void onFailed(String message) {
            dismissDialog();
        }

        @Override
        public void onFailedAndForceLogout() {
            dismissDialog();
            if (!AwareUtils.isForceLogout) {
                AwareUtils.forceLogOut(HistoryLeaveActivity.this);
            }
        }
    };

}
