package android.mobile.lms.aware.com.atsleave.models;

/**
 * Created by apinun.w on 18/5/2558.
 */
public class ResponseEntity {

    private String server_time;
    private String msg_desc;
    private String msg_type;
    private String msg_id;
    private String android_version;
    private Boolean android_force_update;
    private String ios_version;
    private String ios_force_update;


    public void setServer_time(String server_time) {
        this.server_time = server_time;
    }

    public void setMsg_desc(String msg_desc) {
        this.msg_desc = msg_desc;
    }

    public void setMsg_type(String msg_type) {
        this.msg_type = msg_type;
    }

    public void setMsg_id(String msg_id) {
        this.msg_id = msg_id;
    }

    public String getServer_time() {
        return server_time;
    }

    public String getMsg_desc() {
        return msg_desc;
    }

    public String getMsg_type() {
        return msg_type;
    }

    public String getMsg_id() {
        return msg_id;
    }

    public String getAndroid_version() {
        return android_version;
    }

    public Boolean getAndroid_force_update() {
        return android_force_update;
    }

    public void setAndroid_version(String android_version) {
        this.android_version = android_version;
    }

    public void setAndroid_force_update(Boolean android_force_update) {
        this.android_force_update = android_force_update;
    }

    public String getIos_version() {
        return ios_version;
    }

    public void setIos_version(String ios_version) {
        this.ios_version = ios_version;
    }

    public String getIos_force_update() {
        return ios_force_update;
    }

    public void setIos_force_update(String ios_force_update) {
        this.ios_force_update = ios_force_update;
    }
}
