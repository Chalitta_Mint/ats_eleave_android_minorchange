package android.mobile.lms.aware.com.atsleave.utils;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.mobile.lms.aware.com.atsleave.R;
import android.mobile.lms.aware.com.atsleave.callback.RequestDialogListener;
import android.mobile.lms.aware.com.atsleave.customview.ProgressHUD;
import android.util.TypedValue;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

/**
 * Created by apinun.w on 24/8/2558.
 */
public class DialogUtils {

    public static void showAlertDialog(Context context, String message, boolean isCancelTouchOutSide) {
        final Dialog cdd = new Dialog(context);
        cdd.getWindow().setBackgroundDrawable(new ColorDrawable(Color.argb(0, 0, 0, 0)));
        cdd.requestWindowFeature(Window.FEATURE_NO_TITLE);
        cdd.setCanceledOnTouchOutside(isCancelTouchOutSide);
        cdd.setContentView(R.layout.custom_alert_login);
        TextView detailAlert = (TextView) cdd.findViewById(R.id.alertText_login);
        detailAlert.setTextSize(TypedValue.COMPLEX_UNIT_PX, context.getResources().getDimension(R.dimen.text_normal));
        detailAlert.setText(message);
        cdd.setCanceledOnTouchOutside(false);
        Button yes = (Button) cdd.findViewById(R.id.acceptBtn_login);
        yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cdd.dismiss();
            }
        });
        cdd.show();
    }

    public static void showAlertDialog(Context context, String message, boolean isCancelOnTouchOutSide, final RequestDialogListener requestDialog) {
        final Dialog cdd = new Dialog(context);
        cdd.getWindow().setBackgroundDrawable(new ColorDrawable(Color.argb(0, 0, 0, 0)));
        cdd.requestWindowFeature(Window.FEATURE_NO_TITLE);
        cdd.setContentView(R.layout.custom_alert_login);
        TextView detailAlert = (TextView) cdd.findViewById(R.id.alertText_login);
        detailAlert.setTextSize(TypedValue.COMPLEX_UNIT_PX, context.getResources().getDimension(R.dimen.text_normal));
        detailAlert.setText(message);
        cdd.setCanceledOnTouchOutside(false);
        Button yes = (Button) cdd.findViewById(R.id.acceptBtn_login);
        yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (requestDialog != null) {
                    requestDialog.responseSubmit();
                }
                cdd.dismiss();
            }
        });
        cdd.show();
    }

    public static void showAlertDialog(Context context, String message, final RequestDialogListener requestDialog) {
        final Dialog cdd = new Dialog(context);
        cdd.getWindow().setBackgroundDrawable(new ColorDrawable(Color.argb(0, 0, 0, 0)));
        cdd.requestWindowFeature(Window.FEATURE_NO_TITLE);
        cdd.setContentView(R.layout.custom_alert_login);
        TextView detailAlert = (TextView) cdd.findViewById(R.id.alertText_login);
        detailAlert.setTextSize(TypedValue.COMPLEX_UNIT_PX, context.getResources().getDimension(R.dimen.text_normal));
        detailAlert.setText(message);
        cdd.setCanceledOnTouchOutside(false);
        Button yes = (Button) cdd.findViewById(R.id.acceptBtn_login);
        yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                requestDialog.responseSubmit();
                cdd.dismiss();
            }
        });
        cdd.show();
    }

    public static void showAlertDialog(Context context, String message) {
        final Dialog cdd = new Dialog(context);
        cdd.getWindow().setBackgroundDrawable(new ColorDrawable(Color.argb(0, 0, 0, 0)));
        cdd.requestWindowFeature(Window.FEATURE_NO_TITLE);
        cdd.setContentView(R.layout.custom_alert_login);
        TextView detailAlert = (TextView) cdd.findViewById(R.id.alertText_login);
        detailAlert.setTextSize(TypedValue.COMPLEX_UNIT_PX, context.getResources().getDimension(R.dimen.text_normal));
        detailAlert.setText(message);
        cdd.setCanceledOnTouchOutside(false);
        Button yes = (Button) cdd.findViewById(R.id.acceptBtn_login);
        yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cdd.dismiss();
            }
        });
        cdd.show();
    }

    public static void showAlertDialogYesandNo(final Context context, String message, final RequestDialogListener dialogListener) {
        final Dialog cdd = new Dialog(context);
        cdd.getWindow().setBackgroundDrawable(new ColorDrawable(Color.argb(0, 0, 0, 0)));
        cdd.requestWindowFeature(Window.FEATURE_NO_TITLE);
        cdd.setContentView(R.layout.custom_alert);
        TextView detailAlert = (TextView) cdd.findViewById(R.id.alertText);
        detailAlert.setTextSize(TypedValue.COMPLEX_UNIT_PX, context.getResources().getDimension(R.dimen.text_normal));
        detailAlert.setText(message);
        cdd.setCanceledOnTouchOutside(false);
        Button yes = (Button) cdd.findViewById(R.id.acceptBtn);
        Button no = (Button) cdd.findViewById(R.id.cancelBtn);

        yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogListener.responseSubmit();
                cdd.dismiss();
            }
        });
        no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cdd.dismiss();
            }
        });
        cdd.show();
    }

    public static void showAlertDismissofRetry(final Context context, String message, final RequestDialogListener dialogListener) {
        final Dialog cdd = new Dialog(context);
        cdd.getWindow().setBackgroundDrawable(new ColorDrawable(Color.argb(0, 0, 0, 0)));
        cdd.requestWindowFeature(Window.FEATURE_NO_TITLE);
        cdd.setContentView(R.layout.custom_alert);
        TextView detailAlert = (TextView) cdd.findViewById(R.id.alertText);

        detailAlert.setTextSize(TypedValue.COMPLEX_UNIT_PX, context.getResources().getDimension(R.dimen.text_normal));
        detailAlert.setText(message);
        cdd.setCanceledOnTouchOutside(false);
        Button yes = (Button) cdd.findViewById(R.id.acceptBtn);
        Button no = (Button) cdd.findViewById(R.id.cancelBtn);
        yes.setText("Retry");
        no.setText("Dismiss");

        yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cdd.dismiss();
                dialogListener.responseSubmit();
            }
        });
        no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cdd.dismiss();
            }
        });
        cdd.show();
    }

    public static Dialog createDialog(Activity context) {
        return ProgressHUD.show(context, "Loading...", true, null);
    }

    public static void showAlertDialogYesandNo(final Context context, String message, String positiveText, String negativeText, final RequestDialogListener dialoglistener) {
        final Dialog cdd = new Dialog(context);
        cdd.getWindow().setBackgroundDrawable(new ColorDrawable(Color.argb(0, 0, 0, 0)));
        cdd.requestWindowFeature(Window.FEATURE_NO_TITLE);
        cdd.setContentView(R.layout.custom_alert);
        TextView detailAlert = (TextView) cdd.findViewById(R.id.alertText);

        detailAlert.setTextSize(TypedValue.COMPLEX_UNIT_PX, context.getResources().getDimension(R.dimen.text_normal));
        detailAlert.setText(message);
        cdd.setCanceledOnTouchOutside(false);
        Button yes = (Button) cdd.findViewById(R.id.acceptBtn);
        yes.setText(positiveText);
        Button no = (Button) cdd.findViewById(R.id.cancelBtn);
        no.setText(negativeText);

        yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialoglistener.responseSubmit();
                cdd.dismiss();
            }
        });
        no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cdd.dismiss();
            }
        });
        cdd.show();
    }
}
