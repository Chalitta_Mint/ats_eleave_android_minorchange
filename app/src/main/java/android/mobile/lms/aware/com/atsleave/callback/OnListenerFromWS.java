package android.mobile.lms.aware.com.atsleave.callback;

/**
 * Created by apinun.w on 14/5/2558.
 */
public interface OnListenerFromWS {
    void onComplete(String response);

    void onFailed(String message);
}
