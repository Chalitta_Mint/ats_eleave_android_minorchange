package android.mobile.lms.aware.com.atsleave.adapter;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.mobile.lms.aware.com.atsleave.R;
import android.mobile.lms.aware.com.atsleave.webservice.LMSGeneralConstant;
import android.mobile.lms.aware.com.atsleave.models.HolidayResponse;
import android.mobile.lms.aware.com.atsleave.models.LeaveHistoryResponse;
import android.mobile.lms.aware.com.atsleave.utils.AwareUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

/**
 * Created by apinun.w on 23/7/2558.
 */
public class SickLeaveAdapter extends BaseAdapter {
//    private SimpleDateFormat inputSDF = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");       //2015-05-22 15:00:49
    private SimpleDateFormat inputSDF = new SimpleDateFormat("yyyyMMddHHmm",Locale.ENGLISH);       //    201509010830
    private SimpleDateFormat outputSDF = new SimpleDateFormat("EEE dd-MMM-yy", Locale.ENGLISH);
    List<HolidayResponse.DataEntity> holidays;
    private Context context;

    public void setLeaveModels(List<LeaveHistoryResponse.DataEntity> leaveModels) {
        this.leaveModels = leaveModels;
    }

    public List<HolidayResponse.DataEntity> getHolidays() {
        return holidays;
    }

    public void setHolidays(List<HolidayResponse.DataEntity> holidays) {
        this.holidays = holidays;
    }

    public List<LeaveHistoryResponse.DataEntity> getLeaveModels() {
        return leaveModels;
    }

    private List<LeaveHistoryResponse.DataEntity> leaveModels;

    public SickLeaveAdapter(Context context, List<LeaveHistoryResponse.DataEntity> leaveModels, List<HolidayResponse.DataEntity> holidays){
        this.context = context;
        this.leaveModels = leaveModels;
        this.holidays = holidays;
    }

    @Override
    public int getCount() {
        int count = leaveModels.size() > 5? 5 : leaveModels.size();
        return count;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        String str_startDate = leaveModels.get(position).getRLD_DATE_START();
        boolean isExpireLimited = AwareUtils.checkExpireDate((Activity) context, str_startDate, "yyyyMMddHHmm"); //201508210830

        if(isExpireLimited){
            return 7;
        }

        return Integer.parseInt(leaveModels.get(position).getRL_STATUSID());
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Holder holder = null;
        if(convertView == null){
            holder = new Holder();
            convertView = LayoutInflater.from(context).inflate(R.layout.layout_leave_listitem,parent,false);
            holder.img_sick_leave = (ImageView) convertView.findViewById(R.id.img_sick_leave);
            holder.txt_sick_date = (TextView) convertView.findViewById(R.id.txt_sick_date);
            holder.txt_sick_time = (TextView) convertView.findViewById(R.id.txt_sick_time);
            holder.txt_sick_status = (TextView) convertView.findViewById(R.id.txt_sick_status);
            holder.txt_sick_my_reason = (TextView) convertView.findViewById(R.id.txt_sick_my_reason);
            holder.txt_sick_sup_reson = (TextView) convertView.findViewById(R.id.txt_sick_sup_reson);
            convertView.setTag(holder);
        }else{
            holder = (Holder) convertView.getTag();
        }
        int imageId = R.drawable.denied;
        int status_id = Integer.parseInt(leaveModels.get(position).getRL_STATUSID());
        switch (status_id){
            case LMSGeneralConstant.LEAVE_STATUS.Approved :
                imageId = R.drawable.approved;
                break;
            case LMSGeneralConstant.LEAVE_STATUS.Rejected :
                imageId = R.drawable.denied;
                break;
            case LMSGeneralConstant.LEAVE_STATUS.PendingCancel :
                imageId = R.drawable.pending_cancel;
                break;
            case LMSGeneralConstant.LEAVE_STATUS.ApprovedCancel :
                imageId = R.drawable.approved_cancel;
                break;
            case LMSGeneralConstant.LEAVE_STATUS.Canceled:
                imageId = R.drawable.denied_cancel;
                break;
            case LMSGeneralConstant.LEAVE_STATUS.Pending :
                imageId = R.drawable.pending;
                break;
            case LMSGeneralConstant.LEAVE_STATUS.RejectedCancel :
                imageId = R.drawable.denied_cancel;
                break;
        }
        Bitmap bitmap = BitmapFactory.decodeResource(context.getResources(),imageId);
        holder.img_sick_leave.setImageBitmap(bitmap);

//        holder.img_sick_leave.setImageBitmap();
        String startDate = leaveModels.get(position).getRLD_DATE_START();
        String endDate = leaveModels.get(position).getRLD_DATE_END();
        Date parse_end_date = new Date();
        Date parse_start_date = new Date();
        Date startDateCompare = null;
        Date endDateCompate = null;
        if(startDate != null) {
            try {
                parse_start_date = inputSDF.parse(startDate);
                startDateCompare = inputSDF.parse(startDate);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }

        if(endDate != null) {
            try {
                parse_end_date = inputSDF.parse(endDate);
                endDateCompate = inputSDF.parse(endDate);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }

        String toDateText = "";


        startDateCompare.setHours(0);
        startDateCompare.setMinutes(0);
        endDateCompate.setHours(0);
        endDateCompate.setMinutes(0);

        if(startDateCompare.compareTo(endDateCompate)==0) {
            toDateText = outputSDF.format(parse_start_date);
        }else{
            toDateText = outputSDF.format(parse_start_date) + "-" +outputSDF.format(parse_end_date);
        }


        String remark = "";
        if(leaveModels.get(position).getRL_STATUSID().equalsIgnoreCase(LMSGeneralConstant.LEAVE_STATUS.Pending+"")){
            remark = "N/A";
        }else{
            if(leaveModels.get(position).getREMARK() != null) {
                if (leaveModels.get(position).getREMARK().equalsIgnoreCase(""))
                    remark = "N/A";
                else
                    remark = leaveModels.get(position).getREMARK();
            }else{
                remark = "N/A";
            }
        }


        String sickTime = "";
        String minuteTotal = leaveModels.get(position).getRLD_ALLTIME();
        int i_minute = Integer.parseInt(minuteTotal);
        int int_date = i_minute / 480;
        float f_hour = (i_minute%480.0f) / 60.0f;

        if(int_date > 0) {
            sickTime += int_date;
            sickTime += int_date <= 1 ? " Day " : " Days ";
        }

        if(f_hour > 0){
            sickTime += (f_hour % 1 == 0? (int)f_hour+"": f_hour+"") + (f_hour <= 1? " Hour ":" Hours ");
        }



//        Log.e("", "date : "+int_date+" : "+"hour : "+f_hour);

        holder.txt_sick_date.setText(toDateText);
        holder.txt_sick_time.setText(sickTime);
        holder.txt_sick_status.setText(context.getString(R.string.title_sick_status) + getLeaveStatus(Integer.parseInt(leaveModels.get(position).getRL_STATUSID())));
        holder.txt_sick_my_reason.setText(leaveModels.get(position).getREASON());

        holder.txt_sick_sup_reson.setText(remark);

        return convertView;
    }

    private class Holder{
        ImageView img_sick_leave;
        TextView txt_sick_date;
        TextView txt_sick_time;
        TextView txt_sick_status;
        TextView txt_sick_my_reason;
        TextView txt_sick_sup_reson;
    }

    private String getLeaveStatus(int type){
        switch (type){
            case LMSGeneralConstant.LEAVE_STATUS.Pending :
                return  "Pending";
            case LMSGeneralConstant.LEAVE_STATUS.Approved :
                return  "Approved";
            case LMSGeneralConstant.LEAVE_STATUS.ApprovedCancel :
                return  "Approved Cancellation";
            case LMSGeneralConstant.LEAVE_STATUS.Canceled :
                return  "";
            case LMSGeneralConstant.LEAVE_STATUS.Rejected :
                return  "Rejected";
            case LMSGeneralConstant.LEAVE_STATUS.PendingCancel :
                return  "Pending Cancellation";
            case LMSGeneralConstant.LEAVE_STATUS.RejectedCancel :
                return  "Rejected Cancellation";
            default:
                return "";
        }
    }

//    private String calculateWorkingDate(Date start_date,Date end_date){
//        String result = "";
//        long diff_date = AwareUtils.getDifferenceMinute(start_date, end_date);
//        int result_minute = 0;
//        for(int i = 0 ; i < diff_date ; i++){
////            if(start_date.get)
//        }
//        return result;
//    }
//Check holiday and weekend
private static List<Date> getDates(String dateString1, String dateString2,String formats) {
    ArrayList<Date> dates = new ArrayList<Date>();
    DateFormat df1 = new SimpleDateFormat(formats,Locale.ENGLISH);

    Date date1 = null;
    Date date2 = null;

    try {
        date1 = df1 .parse(dateString1);
        date2 = df1 .parse(dateString2);
    } catch (ParseException e) {
        e.printStackTrace();
    }

    Calendar cal1 = Calendar.getInstance();
    cal1.setTime(date1);


    Calendar cal2 = Calendar.getInstance();
    cal2.setTime(date2);

    while(!cal1.after(cal2))
    {
        dates.add(cal1.getTime());
        cal1.add(Calendar.DATE, 1);
    }
    return dates;
}

    private String checkWeekEnd(Date day) {
        SimpleDateFormat dateFormatReturn = new SimpleDateFormat("EEE", Locale.ENGLISH);
        String strDay = "";
        strDay = dateFormatReturn.format(day);

        if (strDay.equals("Sat") || strDay.equals("Sun")) {
            return "Y";
        } else {
            return "N";
        }
    }


    private String checkHistory(Date day) {

        SimpleDateFormat dateFormatReturn = new SimpleDateFormat("yyyyMMdd",Locale.ENGLISH);
        String strDay = "";
        String status = "";
        strDay = dateFormatReturn.format(day);
        int i=0;

        for(i=0;i<holidays.size();i++) {

            if(holidays.get(i).equals(strDay)){
                status="Y";
            }

        }

        return  status;
    }
}
