package android.mobile.lms.aware.com.atsleave.constant;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by apinun.w on 22/8/2558.
 */
public class SaveSharedPreference {

    private final String MyPREFERENCES_KEY = "MyPREFERENCES_KEY";
    private final String KEY_TOKEN = "TOKEN_AWARE";
    private final String KEY_DEVICE_TOKEN = "KEY_DEVICE_TOKEN";
    private final String KEY_STATUS_HBD = "KEY_STATUS_HBD";
    private final String KEY_LOGIN_DATA = "KEY_LOGIN_DATA";
    private final String KEY_STATUS_REQUEST_HBD = "KEY_STATUS_REQUEST_HBD";
    private final String KEY_STATUS_MESSAGE_DAYOFF = "KEY_STATUS_MESSAGE_DAYOFF";
    private final String KEY_STATUS_MESSAGE_HBD = "KEY_STATUS_MESSAGE_HBD";
    private final String KEY_STATUS_FORCE_UPDATE = "KEY_STATUS_FORCE_UPDATE";
    private final String KEY_VERSION_UPDATE = "KEY_VERSION_UPDATE";
    private final String KEY_VIEW_TUTORIALS = "KEY_VIEW_TUTORIALS";
    private final String KEY_USER = "KEY_USER";
    private final String KEY_IMAGE = "KEY_IMAGE";
    Context context;
    SharedPreferences sharedpreferences;

    public SaveSharedPreference(Context context) {
        this.context = context;
        sharedpreferences = context.getSharedPreferences(MyPREFERENCES_KEY, Context.MODE_PRIVATE);
    }

    public void saveToken(String token) {
        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.putString(KEY_TOKEN, token);
        editor.apply();
    }

    public void saveUSER(String user) {
        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.putString(KEY_USER, user);
        editor.apply();
    }

    public void saveDeviceToken(String device_token) {
        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.putString(KEY_DEVICE_TOKEN, device_token);
        editor.apply();
    }

    public String getDeviceToken() {
        return sharedpreferences.getString(KEY_DEVICE_TOKEN, "");
    }

    public Boolean getHBDStatus() {
        return sharedpreferences.getBoolean(KEY_STATUS_HBD, true);
    }

    public void saveHBDStatus(Boolean status) {
        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.putBoolean(KEY_STATUS_HBD, status);
        editor.apply();
    }

    public Boolean getHBDStatusRequest() {
        return sharedpreferences.getBoolean(KEY_STATUS_REQUEST_HBD, true);
    }

    public void saveHBDStatusRequest(Boolean status) {
        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.putBoolean(KEY_STATUS_REQUEST_HBD, status);
        editor.apply();
    }

    public Boolean getDayoffStatusMessage() {
        return sharedpreferences.getBoolean(KEY_STATUS_MESSAGE_DAYOFF, true);
    }

    public void saveDayoffStatusMessage(Boolean status) {
        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.putBoolean(KEY_STATUS_MESSAGE_DAYOFF, status);
        editor.apply();
    }


    public Boolean getHBDStatusMessage() {
        return sharedpreferences.getBoolean(KEY_STATUS_MESSAGE_HBD, true);
    }

    public void saveHBDStatusMessage(Boolean status) {
        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.putBoolean(KEY_STATUS_MESSAGE_HBD, status);
        editor.apply();
    }

    public String getToken() {
        return sharedpreferences.getString(KEY_TOKEN, "");
    }

    public String getUser() {
        return sharedpreferences.getString(KEY_USER, "");
    }

    public void saveJSON_LOGIN(String json_data) {
        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.putString(KEY_LOGIN_DATA, json_data);
        editor.apply();
    }

    public String getLOGIN_JSON() {
        return sharedpreferences.getString(KEY_LOGIN_DATA, "");
    }

    public Boolean getStatusForceUpdate() {
        return sharedpreferences.getBoolean(KEY_STATUS_FORCE_UPDATE, false);
    }

    public void saveStatusForceUpdate(Boolean status) {
        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.putBoolean(KEY_STATUS_FORCE_UPDATE, status);
        editor.apply();
    }

    public String getVersionUpdate() {
        return sharedpreferences.getString(KEY_VERSION_UPDATE, "");
    }

    public void saveVersionUpdate(String status) {
        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.putString(KEY_VERSION_UPDATE, status);
        editor.apply();
    }

    public Boolean getViewTutorials() {
        return sharedpreferences.getBoolean(KEY_VIEW_TUTORIALS, true);
    }

    public void saveViewTutorials(Boolean status) {
        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.putBoolean(KEY_VIEW_TUTORIALS, status);
        editor.apply();
    }

    public void saveIMAGE(String image) {
        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.putString(KEY_IMAGE, image);
        editor.apply();
    }

    public String getIMAGE() {
        return sharedpreferences.getString(KEY_IMAGE, "");
    }

}
