package android.mobile.lms.aware.com.atsleave.utils;

import android.util.Log;

/**
 * Created by apinun.w on 13/5/2558.
 */
public class LogUtils {
    public static final String TAG = "LOGUTILS";

    public static void showLog(String className, String function, String parameter) {
        Log.i(TAG, "CLASSNAME: " + className);
        Log.i(TAG, "FUNCTION: " + function);
        Log.i(TAG, "PARAMETERS: " + parameter);
    }
}
