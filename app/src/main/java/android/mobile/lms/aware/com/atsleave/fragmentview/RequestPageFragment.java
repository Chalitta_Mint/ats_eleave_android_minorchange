package android.mobile.lms.aware.com.atsleave.fragmentview;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.mobile.lms.aware.com.atsleave.LMSApplication;
import android.mobile.lms.aware.com.atsleave.MainActivity;
import android.mobile.lms.aware.com.atsleave.R;
import android.mobile.lms.aware.com.atsleave.RequestReviewActivity;
import android.mobile.lms.aware.com.atsleave.callback.BackPressed;
import android.mobile.lms.aware.com.atsleave.callback.OnListenerFromWSManager;
import android.mobile.lms.aware.com.atsleave.models.Holiday;
import android.mobile.lms.aware.com.atsleave.models.HolidayResponse;
import android.mobile.lms.aware.com.atsleave.models.LeaveHistoryResponse;
import android.mobile.lms.aware.com.atsleave.models.LeaveType;
import android.mobile.lms.aware.com.atsleave.models.ResponseEntity;
import android.mobile.lms.aware.com.atsleave.models.StaffSummaryResponse;
import android.mobile.lms.aware.com.atsleave.utils.AwareUtils;
import android.mobile.lms.aware.com.atsleave.utils.DateView;
import android.mobile.lms.aware.com.atsleave.utils.DialogUtils;
import android.mobile.lms.aware.com.atsleave.utils.OnChangePageView;
import android.mobile.lms.aware.com.atsleave.webservice.LMSGeneralConstant;
import android.mobile.lms.aware.com.atsleave.webservice.LMSWSManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.crashlytics.android.Crashlytics;
import com.google.gson.Gson;
import com.prolificinteractive.materialcalendarview.CalendarDay;
import com.prolificinteractive.materialcalendarview.MaterialCalendarView;
import com.prolificinteractive.materialcalendarview.OnDateChangedListener;
import com.prolificinteractive.materialcalendarview.OnMonthChangedListener;
import com.prolificinteractive.materialcalendarview.decorators.ClickDecorator;
import com.prolificinteractive.materialcalendarview.decorators.EventDecorator;
import com.prolificinteractive.materialcalendarview.decorators.MarkDecorator;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.Executors;

/**
 * Created by apinun.w on 15/7/2558.
 */
public class RequestPageFragment extends Fragment implements OnDateChangedListener, OnMonthChangedListener, BackPressed {
    private Button btn_submit_request;
    private OnChangePageView onChangePageView;
    private String TAG = "RequestPageFragment";
    private MaterialCalendarView widget;
    private LMSApplication application;
    private Dialog boxAddLeave;
    private Spinner dropdown1;
    private String start_date, end_date;
    private String statusExtra;
    private Dialog dialogWaiting;
    private static int countAlert = 0;
    //-----------
    private LinearLayout.LayoutParams layoutParams1;
    private LinearLayout.LayoutParams layoutParams2;

    private int remeanTime;
    private EditText text_reason;
    private String statusAllday;
    private String startD, endD;
    private int totalDay;
    private DateView dateView;
    private String totalTime, showStartDate, showEndDate;
    // Date Picker
    private Date tempDate;
    private LinearLayout linear1, linear2;
    private TextView btnDateStart, btnDateEnd, DateStartTime, DateEndTime, value_totalday;
    private TextView month1, month2, month3, month4, month5;
    private EditText comments;
    private int timeStart = 0, timeEnd, minuteStart, minuteEnd;
    private String fixMonth1, fixMonth2, fixMonth3, fixMonth4, fixMonth5;
    private Button btnGetdate1, btnGetdate2;
    private ArrayList<LeaveType> list_leavetype = new ArrayList<LeaveType>();
    private ArrayList<String> selected_leavetype = new ArrayList<String>();
    private ArrayList<String> selected_leavetype_name = new ArrayList<String>();
    private ArrayList<String> selected_leavetype_salulation = new ArrayList<String>();

    private ArrayList<String> monthBirthday = new ArrayList<String>();
    private ArrayList<String> selected_startdate = new ArrayList<String>();
    private ArrayList<String> selected_enddate = new ArrayList<String>();

    private ArrayList<String> list_history = new ArrayList<String>();
    private ArrayList<Holiday> list_holiday = new ArrayList<Holiday>();
    private ArrayList<String> btnApply = new ArrayList<String>();
    private ArrayList<String> switcher = new ArrayList<String>();
    private ArrayList<CalendarDay> dates = new ArrayList<>();//new
    private ArrayList<CalendarDay> holidayDates = new ArrayList<CalendarDay>();//new
    private ArrayList<CalendarDay> historyDates = new ArrayList<CalendarDay>();//new
    private boolean isVisibleView = true;
    private boolean INCLUDING_SAT_SUN = false;
    private boolean beforeBirthdayLeave = false;
    private String serverTime = "";
    private ISO8601 callServeTime;
    private Date serverDate;
    private Date monthDate;
    private Typeface font;
    private String totalMinute;
    private int realDay;

    private int[] leaveTypesRequest = null;
    private ClickDecorator decorator;
    private Date curDate;
    private LinearLayout lyHoliday;

    private Boolean change_spinner_start = true;
    private Boolean change_spinner_end = true;
    private Boolean thisMonthBirthday = false;
    private String notice = "";
    private int remainDayLeave = 0;
    private Boolean BtnBirthDay, BtnDayoff;

    public static RequestPageFragment initRequestPageFragment(String fragment, OnChangePageView onchangePageView) {
        RequestPageFragment requestPageFragment = new RequestPageFragment();
        requestPageFragment.setOnChangePageView(onchangePageView);
        return requestPageFragment;
    }

    public void setOnChangePageView(OnChangePageView onChangePageView) {
        this.onChangePageView = onChangePageView;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.layout_leave_request, container, false);
        application = LMSApplication.getInstance();
        dialogWaiting = DialogUtils.createDialog(getActivity());
        leaveTypesRequest = new int[]{1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11};
        //Condition this for Gifware Only
        statusExtra = application.getCurProfile().getIS_EXTRA();//if extra==1 mean this gift ware
        //Call Leave Type

        month1 = (TextView) view.findViewById(R.id.month1);
        month2 = (TextView) view.findViewById(R.id.month2);
        month3 = (TextView) view.findViewById(R.id.month3);
        month4 = (TextView) view.findViewById(R.id.month4);
        month5 = (TextView) view.findViewById(R.id.month5);
        btn_submit_request = (Button) view.findViewById(R.id.btn_submit_request);
        Button btn_last_month = (Button) view.findViewById(R.id.btn_last_month);
        Button btn_next_month = (Button) view.findViewById(R.id.btn_next_month);
        ImageView btn_refresh = (ImageView) view.findViewById(R.id.btn_refresh);
        ImageView btnToday = (ImageView) view.findViewById(R.id.btn_today);
        lyHoliday = (LinearLayout) view.findViewById(R.id.lyHoliday);
        widget = (MaterialCalendarView) view.findViewById(R.id.calendarView);

        try {
            serverDate = AwareUtils.getCurTimeFromServer(getActivity());
        } catch (Exception e) {
            e.printStackTrace();
            Crashlytics.logException(e);
        }

        widget.setOnDateChangedListener(this);
        widget.setOnMonthChangedListener(this);
        widget.setShowOtherDates(true);//new
        widget.setSelectionColor(getResources().getColor(R.color.bg_red));//new
        widget.addDecorators();
        widget.setBackgroundResource(R.color.bg_gray);
        widget.getBackground().setAlpha(200);
        widget.setCurrentDate(serverDate);
        widget.setSwipeLocked(true);

        //confog time server
        font = Typeface.createFromAsset(getActivity().getAssets(), "HelveticaNeue.ttf");
        btn_submit_request.setTypeface(font);

        btn_last_month.setTypeface(font);
        btn_next_month.setTypeface(font);

        month1.setTypeface(font);
        month2.setTypeface(font);
        month3.setTypeface(font);
        month4.setTypeface(font);
        month5.setTypeface(font);

        DateFormat format = new SimpleDateFormat("yyyy-MM-dd", Locale.US);

        if (serverDate == null) {
            serverDate = new Date();
        }

        try {
            String currentDate = format.format(serverDate);
            curDate = format.parse(currentDate);
        } catch (Exception e) {
            e.printStackTrace();
            curDate = new Date();
        }

        //-----------------------------------------------------------------
        selected_startdate.add("0");
        selected_enddate.add("0");
        //----------------------------------------------------------------------------
        btn_refresh.setOnClickListener(new View.OnClickListener() {//refresh
            @Override
            public void onClick(View v) {
                clearData();
            }
        });

        btn_last_month.setOnClickListener(new View.OnClickListener() {//last month
            @Override
            public void onClick(View v) {
                widget.lastAllow();
            }
        });

        month1.setOnClickListener(new View.OnClickListener() {//last month
            @Override
            public void onClick(View v) {
                widget.twoPrevAllow();
            }
        });

        month2.setOnClickListener(new View.OnClickListener() {//last month
            @Override
            public void onClick(View v) {
                widget.lastAllow();
            }
        });

        month4.setOnClickListener(new View.OnClickListener() {//last month
            @Override
            public void onClick(View v) {
                widget.nextAllow();
            }
        });

        month5.setOnClickListener(new View.OnClickListener() {//last month
            @Override
            public void onClick(View v) {
                widget.twoNextAllow();
            }
        });

        btn_next_month.setOnClickListener(new View.OnClickListener() {//next month
            @Override
            public void onClick(View v) {
                widget.nextAllow();
            }
        });

        totalMinute = "";

        btn_submit_request.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                showBoxDetails(view, "");
            }
        });

        btnToday.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                widget.setCurrentDate(serverDate);
            }
        });

        monthDate = serverDate;
        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        onChangePageView = (OnChangePageView) context;
    }

    public void showBoxDetails(View view, String btnLeave) {

        if (dates.size() != 0 || btnApply.size() != 0) {//check select date

            String date_start = AwareUtils.getDateFormat(dates.get(0));
            String date_end = AwareUtils.getDateFormat(dates.get(dates.size() - 1));

            Log.i(TAG, "date_start: " + date_start);
            Log.i(TAG, "date_end: " + date_end);

            final Calendar saveStartCal = Calendar.getInstance(Locale.ENGLISH);
            final Calendar saveEndCal = Calendar.getInstance(Locale.ENGLISH);

            DateFormat format = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
            Date date = null;
            Date dateend = null;
            String StWeekDay = "";
            String StHoliday = "";
            String EdWeekDay = "";
            String EdHoliday = "";
            try {
                //check for start date
                date = format.parse(date_start);
                StWeekDay = checkWeekEnd(date);
                StHoliday = checkHoliday(date);

                //check for end date
                dateend = format.parse(date_end);
                EdWeekDay = checkWeekEnd(dateend);
                EdHoliday = checkHoliday(dateend);

                if (statusExtra.equals("1")) {//for Giffware
                    StHoliday = "N";
                    EdHoliday = "N";
                }
            } catch (ParseException e) {
                e.printStackTrace();
            }

            if (StWeekDay.equals("Y") && btnApply.size() == 0) {
                DialogUtils.showAlertDialog(getActivity(), getString(R.string.alert_text_select_weekend), false, null);
            } else if (StHoliday.equals("Y") && btnApply.size() == 0) {
                DialogUtils.showAlertDialog(getActivity(), getString(R.string.alert_text_select_holiday), false, null);
            } else if (EdWeekDay.equals("Y") && btnApply.size() == 0) {
                DialogUtils.showAlertDialog(getActivity(), getString(R.string.alert_text_selectend_weekend), false, null);
            } else if (EdHoliday.equals("Y") && btnApply.size() == 0) {
                DialogUtils.showAlertDialog(getActivity(), getString(R.string.alert_text_selectend_holiday), false, null);
            } else { //Check - weekend,holiday

                boxAddLeave = new Dialog(getActivity());
                boxAddLeave.setCanceledOnTouchOutside(false);
                boxAddLeave.getWindow().setBackgroundDrawable(new ColorDrawable(Color.argb(0, 0, 0, 0)));
                boxAddLeave.requestWindowFeature(Window.FEATURE_NO_TITLE);
                boxAddLeave.setContentView(R.layout.custom_box_add_leave);
                RelativeLayout boxCloseButton = (RelativeLayout) boxAddLeave.findViewById(R.id.boxCloseButton);
                Button btn_confirm = (Button) boxAddLeave.findViewById(R.id.btn_confirm);
                btnGetdate1 = (Button) boxAddLeave.findViewById(R.id.btnGetdate1);
                btnGetdate2 = (Button) boxAddLeave.findViewById(R.id.btnGetdate2);
                btnDateStart = (TextView) boxAddLeave.findViewById(R.id.val_starts_date);
                btnDateEnd = (TextView) boxAddLeave.findViewById(R.id.val_end_date);
                DateStartTime = (TextView) boxAddLeave.findViewById(R.id.val_starts_time);
                DateEndTime = (TextView) boxAddLeave.findViewById(R.id.val_end_time);
                comments = (EditText) boxAddLeave.findViewById(R.id.comments);
                comments.setTypeface(font);
                btn_confirm.setTypeface(font);
                linear1 = (LinearLayout) boxAddLeave.findViewById(R.id.boxdateStart);
                linear2 = (LinearLayout) boxAddLeave.findViewById(R.id.boxdateEnd);

                RelativeLayout box_start = (RelativeLayout) boxAddLeave.findViewById(R.id.box_start);
                RelativeLayout box_end = (RelativeLayout) boxAddLeave.findViewById(R.id.box_end);
                ImageView btnSwitcher = (ImageView) boxAddLeave.findViewById(R.id.btnSwitcher);

                btnGetdate1.setVisibility(View.GONE);
                btnGetdate2.setVisibility(View.GONE);
                LeaveTypeItem(boxAddLeave);//Call Spinner Leave Type

                if (btnLeave.equals("B")) {
                    comments.setText("Birthday Leave!");
                } else if (btnLeave.equals("D")) {
                    comments.setText("Applying for my dayoff");
                }

                //------------------------- Form Select date Calendra ----------------------------
                String show_date_start = "";
                String show_date_end = "";
                String show_datestart = "";
                String show_dateend = "";
                Date startDateTime = new Date();
                Date endDateTime = new Date();

                show_datestart = AwareUtils.getDateFormat(dates.get(0));
                show_dateend = AwareUtils.getDateFormat(dates.get(dates.size() - 1));
                show_date_start = dateFormat4(show_datestart, "yyyy-MM-dd", "-");
                show_date_end = dateFormat4(show_dateend, "yyyy-MM-dd", "-");

                SimpleDateFormat sdfDay = new SimpleDateFormat("EEE dd-MMM-yy", Locale.ENGLISH);
                try {
                    startDateTime = sdfDay.parse(show_date_start);
                    endDateTime = sdfDay.parse(show_date_end);
                    startDateTime.setHours(8);
                    startDateTime.setMinutes(30);
                    endDateTime.setHours(17);
                    endDateTime.setMinutes(30);
                } catch (ParseException e) {
                    e.printStackTrace();
                }

                saveStartCal.setTime(startDateTime);
                saveEndCal.setTime(endDateTime);

                btnDateStart.setText(show_date_start);
                DateStartTime.setText(" 08:30");
                btnDateEnd.setText(show_date_end);
                DateEndTime.setText(" 17:30");

                showStartDate = dateFormat4(show_datestart, "yyyy-MM-dd", "/");
                showEndDate = dateFormat4(show_dateend, "yyyy-MM-dd", "/");

                String startDate = dateFormat(0, 0, 0, "yyyy-MM-dd", show_datestart, "ALL");
                String endDate = dateFormat(0, 0, 0, "yyyy-MM-dd", show_dateend, "ALL");
                selected_startdate.clear();
                selected_enddate.clear();
                selected_startdate.add(startDate + "0830");
                selected_enddate.add(endDate + "1730");

                startD = show_datestart;
                endD = show_dateend;

                //--------------------------------------------------------------------------

                statusAllday = "ALL";

                btnSwitcher.setOnClickListener(new View.OnClickListener() {

                    @Override
                    public void onClick(View v) {

                        if (switcher.contains("1")) {
                            v.setBackgroundResource(R.drawable.btn_close_switch);
                            switcher.remove("1");
                            switcher.add("0");
                            DateStartTime.setVisibility(View.VISIBLE);
                            DateEndTime.setVisibility(View.VISIBLE);
                            dateView.showAllDay("SHOW");//show
                            //linear1.removeAllViews();
                            if (change_spinner_start || change_spinner_end) {
                                btnGetdate1.setVisibility(View.GONE);
                                btnGetdate2.setVisibility(View.GONE);
                            }
                            statusAllday = "NONE";

                            //start hour and minute
                            String startTime = DateStartTime.getText().toString().trim();
                            String endTime = DateEndTime.getText().toString().trim();

                            SimpleDateFormat sdfTime = new SimpleDateFormat("HH:mmaa", Locale.ENGLISH);  // 08:30AM

                            Date stTimeDate = null;//new Date();
                            try {
                                stTimeDate = AwareUtils.getCurTimeFromServer(getActivity());
                            } catch (Exception e) {
                                stTimeDate = new Date();
                                e.printStackTrace();
                            }
                            Date edTimeDate = null;//new Date();
                            try {
                                edTimeDate = AwareUtils.getCurTimeFromServer(getActivity());
                            } catch (Exception e) {
                                edTimeDate = new Date();
                                e.printStackTrace();
                            }
                            int stHours = Integer.parseInt(startTime.substring(0, 2));
                            int stMinute = Integer.parseInt(startTime.substring(3, 5));
                            int edHours = Integer.parseInt(endTime.substring(0, 2));
                            int edMinute = Integer.parseInt(endTime.substring(3, 5));
                            stTimeDate.setHours(stHours);
                            stTimeDate.setMinutes(stMinute);
                            edTimeDate.setHours(edHours);
                            edTimeDate.setMinutes(edMinute);

                            Calendar stTimeCal = Calendar.getInstance(Locale.ENGLISH);
                            Calendar edTimeCal = Calendar.getInstance(Locale.ENGLISH);
                            stTimeCal.setTime(stTimeDate);
                            edTimeCal.setTime(edTimeDate);

                            saveStartCal.set(Calendar.HOUR_OF_DAY, stTimeCal.get(Calendar.HOUR_OF_DAY));
                            saveStartCal.set(Calendar.MINUTE, stTimeCal.get(Calendar.MINUTE));
                            saveEndCal.set(Calendar.HOUR_OF_DAY, edTimeCal.get(Calendar.HOUR_OF_DAY));
                            saveEndCal.set(Calendar.MINUTE, edTimeCal.get(Calendar.MINUTE));
                        } else {
                            v.setBackgroundResource(R.drawable.btn_open_switch);
                            switcher.remove("0");
                            switcher.add("1");
                            DateStartTime.setVisibility(View.GONE);
                            DateEndTime.setVisibility(View.GONE);
                            dateView.showAllDay("CLOSE");//show
                            // linear1.removeAllViews();
                            if (change_spinner_start || change_spinner_end) {
                                btnGetdate1.setVisibility(View.GONE);
                                btnGetdate2.setVisibility(View.GONE);
                            }
                            statusAllday = "ALL";
                        }

                    }
                });

                //----------------------------------------------------
                SimpleDateFormat dateOutput = new SimpleDateFormat("dd/MM/yyyy hh:mm");
                SimpleDateFormat dateYearOutput = new SimpleDateFormat("yyyy");
                /*if (!(view instanceof Button)) {
                    return;
                }*/

                Button dateBtn = (Button) view;
                String dateText = dateBtn.getText().toString();
                String standard_date_format = getResources().getString(R.string.date_null_format);
                if (dateText.equalsIgnoreCase(standard_date_format)) {
                    //text null
                    tempDate = Calendar.getInstance().getTime();
                    tempDate.setMinutes(0);
                } else {
                    //set text
                    try {
                        tempDate = dateOutput.parse(dateText);
                    } catch (ParseException e) {
                        e.printStackTrace();
                        tempDate = Calendar.getInstance().getTime();
                        tempDate.setMinutes(0);
                    }
                }

                dateView = new DateView(getActivity());

                dateView.setDate(tempDate.getDate(),
                        tempDate.getMonth(),
                        Integer.parseInt(dateYearOutput.format(tempDate)),
                        tempDate.getHours(),
                        tempDate.getMinutes());

                //----DEFAULT LEAVE DETAIL---
                switcher.add("1");
                btnSwitcher.setBackgroundResource(R.drawable.btn_open_switch);
                DateStartTime.setVisibility(View.GONE);
                DateEndTime.setVisibility(View.GONE);
                dateView.showAllDay("CLOSE");//show
                //----------------------------------------------------
                final String finalShow_date_start = show_date_start;
                box_start.setOnClickListener(new View.OnClickListener() {

                    @Override
                    public void onClick(View viewIn) {
                        if (change_spinner_start) {

                            String dateFrom = btnDateStart.getText().toString();
                            Date stTimeDate = null;//new Date();
                            try {
                                stTimeDate = AwareUtils.getCurTimeFromServer(getActivity());
                            } catch (Exception e) {
                                stTimeDate = new Date();
                                e.printStackTrace();
                            }
                            Date edTimeDate = null;//new Date();
                            try {
                                edTimeDate = AwareUtils.getCurTimeFromServer(getActivity());
                            } catch (Exception e) {
                                edTimeDate = new Date();
                                e.printStackTrace();
                            }
                            String pattern = "-";
                            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("EEE " + "dd" + pattern + "MMM" + pattern + "yy", Locale.ENGLISH);
                            try {
                                stTimeDate = simpleDateFormat.parse(dateFrom);
                            } catch (ParseException e) {
                                e.printStackTrace();
                            }
                            String startTime = DateStartTime.getText().toString().trim();
                            String endTime = DateEndTime.getText().toString().trim();

                            int stHours = Integer.parseInt(startTime.substring(0, 2));
                            int stMinute = Integer.parseInt(startTime.substring(3, 5));
                            int edHours = Integer.parseInt(endTime.substring(0, 2));
                            int edMinute = Integer.parseInt(endTime.substring(3, 5));
                            stTimeDate.setHours(stHours);
                            stTimeDate.setMinutes(stMinute);
                            edTimeDate.setHours(edHours);
                            edTimeDate.setMinutes(edMinute);

                            Calendar stTimeCal = Calendar.getInstance(Locale.ENGLISH);
                            Calendar edTimeCal = Calendar.getInstance(Locale.ENGLISH);
                            stTimeCal.setTime(stTimeDate);
                            edTimeCal.setTime(edTimeDate);

                            saveStartCal.set(Calendar.DAY_OF_MONTH, stTimeCal.get(Calendar.DAY_OF_MONTH));
                            saveStartCal.set(Calendar.MONTH, stTimeCal.get(Calendar.MONTH));
                            saveStartCal.set(Calendar.HOUR_OF_DAY, stTimeCal.get(Calendar.HOUR_OF_DAY));
                            saveStartCal.set(Calendar.MINUTE, stTimeCal.get(Calendar.MINUTE));
                            saveEndCal.set(Calendar.DAY_OF_MONTH, stTimeCal.get(Calendar.DAY_OF_MONTH));
                            saveEndCal.set(Calendar.MONTH, stTimeCal.get(Calendar.MONTH));
                            saveEndCal.set(Calendar.HOUR_OF_DAY, edTimeCal.get(Calendar.HOUR_OF_DAY));
                            saveEndCal.set(Calendar.MINUTE, edTimeCal.get(Calendar.MINUTE));

                            ShowDatePicker(boxAddLeave, "S", saveStartCal);
                            linear1.setVisibility(View.VISIBLE);
                            change_spinner_start = false;

                        }
                    }
                });

                box_end.setOnClickListener(new View.OnClickListener() {

                    @Override
                    public void onClick(View viewIn) {
                        if (change_spinner_end) {
                            String startTime = DateStartTime.getText().toString().trim();
                            String endTime = DateEndTime.getText().toString().trim();
                            String dateFrom = btnDateEnd.getText().toString();
                            Date stTimeDate = null;//new Date();
                            try {
                                stTimeDate = AwareUtils.getCurTimeFromServer(getActivity());
                            } catch (Exception e) {
                                stTimeDate = new Date();
                                e.printStackTrace();
                            }
                            Date edTimeDate = null;//new Date();
                            try {
                                edTimeDate = AwareUtils.getCurTimeFromServer(getActivity());
                            } catch (Exception e) {
                                edTimeDate = new Date();
                                e.printStackTrace();
                            }
                            if (switcher.contains("1")) {
                                String pattern = "-";
                                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("EEE " + "dd" + pattern + "MMM" + pattern + "yy", Locale.ENGLISH);
                                try {
                                    stTimeDate = simpleDateFormat.parse(dateFrom);
                                } catch (ParseException e) {
                                    e.printStackTrace();
                                }
                            } else {
                                String pattern = "-";
                                SimpleDateFormat simpleDateFormat2 = new SimpleDateFormat("EEE " + "dd" + pattern + "MMM" + pattern + "yy " + "HH:mm", Locale.ENGLISH);
                                try {
                                    stTimeDate = simpleDateFormat2.parse(dateFrom);
                                } catch (ParseException e) {
                                    e.printStackTrace();
                                }
                            }
                            int stHours = Integer.parseInt(startTime.substring(0, 2));
                            int stMinute = Integer.parseInt(startTime.substring(3, 5));
                            int edHours = Integer.parseInt(endTime.substring(0, 2));
                            int edMinute = Integer.parseInt(endTime.substring(3, 5));
                            stTimeDate.setHours(stHours);
                            stTimeDate.setMinutes(stMinute);
                            edTimeDate.setHours(edHours);
                            edTimeDate.setMinutes(edMinute);

                            Calendar stTimeCal = Calendar.getInstance(Locale.ENGLISH);
                            Calendar edTimeCal = Calendar.getInstance(Locale.ENGLISH);
                            stTimeCal.setTime(stTimeDate);
                            edTimeCal.setTime(edTimeDate);

                            saveStartCal.set(Calendar.DAY_OF_MONTH, stTimeCal.get(Calendar.DAY_OF_MONTH));
                            saveStartCal.set(Calendar.MONTH, stTimeCal.get(Calendar.MONTH));
                            saveStartCal.set(Calendar.HOUR_OF_DAY, stTimeCal.get(Calendar.HOUR_OF_DAY));
                            saveStartCal.set(Calendar.MINUTE, stTimeCal.get(Calendar.MINUTE));
                            saveEndCal.set(Calendar.DAY_OF_MONTH, stTimeCal.get(Calendar.DAY_OF_MONTH));
                            saveEndCal.set(Calendar.MONTH, stTimeCal.get(Calendar.MONTH));
                            saveEndCal.set(Calendar.HOUR_OF_DAY, edTimeCal.get(Calendar.HOUR_OF_DAY));
                            saveEndCal.set(Calendar.MINUTE, edTimeCal.get(Calendar.MINUTE));

                            ShowDatePicker(boxAddLeave, "E", saveEndCal);
                            linear2.setVisibility(View.VISIBLE);
                            change_spinner_end = false;


                        }
                    }
                });

                btnGetdate1.setOnClickListener(new View.OnClickListener() {//Date start
                    @Override
                    public void onClick(View v) {
                        change_spinner_start = true;
                        change_spinner_end = true;
                        linear1.removeAllViews();
                        linear1.setVisibility(View.GONE);
                        btnGetdate1.setVisibility(View.GONE);
                        //---------------------for View ---------------------------------------------------------------------
                        String show_date = dateFormat2(dateView.getDate(), dateView.getMonth() + 1, dateView.getYear(), "dd/MM/yyyy", "-");
                        btnDateStart.setText(show_date);
                        DateStartTime.setText(" " + timeFromString(dateView.getHour(), dateView.getMinute()));// + dateView.getAMPM()

                        showStartDate = dateFormat2(dateView.getDate(), dateView.getMonth() + 1, dateView.getYear(), "dd/MM/yyyy", "/");
                        //----------------------For send Api ----------------------------------------------------------------------
                        start_date = dateFormat(dateView.getDate(), (dateView.getMonth() + 1), dateView.getYear(), "dd/MM/yyyy", "", "");
                        selected_startdate.clear();
                        if (statusAllday.equals("ALL")) {
                            selected_startdate.add(start_date + "0830");
                        } else {
                            selected_startdate.add(start_date + timeFromString2(dateView.getHour(), dateView.getMinute()));
                        }
                        //---------------------Totall Minute-&-TotalDay---------------------------------------------------------------------------
                        startD = dateFormat3(dateView.getDate(), (dateView.getMonth() + 1), dateView.getYear());

                    }
                });

                btnGetdate2.setOnClickListener(new View.OnClickListener() {//Date end
                    @Override
                    public void onClick(View v) {
                        change_spinner_start = true;
                        change_spinner_end = true;
                        linear2.removeAllViews();
                        linear2.setVisibility(View.GONE);
                        btnGetdate2.setVisibility(View.GONE);
                        String show_date = dateFormat2(dateView.getDate(), dateView.getMonth() + 1, dateView.getYear(), "dd/MM/yyyy", "-");
                        btnDateEnd.setText(show_date);
                        DateEndTime.setText(" " + timeFromString(dateView.getHour(), dateView.getMinute()));// "" + dateView.getAMPM()
                        showEndDate = dateFormat2(dateView.getDate(), dateView.getMonth() + 1, dateView.getYear(), "dd/MM/yyyy", "/");

                        //---------------------- For send Api ----------------------------------------------------------------------
                        end_date = dateFormat(dateView.getDate(), (dateView.getMonth() + 1), dateView.getYear(), "dd/MM/yyyy", "", "");
                        selected_enddate.clear();
                        if (statusAllday.equals("ALL")) {
                            selected_enddate.add(end_date + "1730");
                        } else {
                            selected_enddate.add(end_date + timeFromString2(dateView.getHour(), dateView.getMinute()));
                        }
                        endD = dateFormat3(dateView.getDate(), (dateView.getMonth() + 1), dateView.getYear());

                    }
                });

                btn_confirm.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        String val_reason = comments.getText().toString();
                        Date today = null;//new Date();
                        try {
                            today = AwareUtils.getCurTimeFromServer(getActivity());
                        } catch (Exception e) {
                            today = new Date();
                            e.printStackTrace();
                        }
                        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmm");//all time
                        String strDateToday = sdf.format(today);

                        SimpleDateFormat sdf2 = new SimpleDateFormat("yyyyMMdd");// date not time
                        String strDateTodayNotTime = sdf2.format(today);

                        String stDateStart = new String(selected_startdate.get(0));
                        String date_start = stDateStart.substring(0, 8);
                        String stDateEnd = new String(selected_enddate.get(0));
                        String date_end = stDateEnd.substring(0, 8);

                        String yearStart = stDateStart.substring(0, 4);
                        String monthStart = stDateStart.substring(4, 6);
                        String dayStart = stDateStart.substring(6, 8);

                        String yearEnd = stDateEnd.substring(0, 4);
                        String monthEnd = stDateEnd.substring(4, 6);
                        String dayEnd = stDateEnd.substring(6, 8);

                        String stStart = new String(selected_startdate.get(0));
                        String time_start = stStart.substring(8, 12);
                        String time_start_month = stStart.substring(4, 6);

                        String time_start_hour = stStart.substring(8, 10);//cal time
                        String time_start_minute = stStart.substring(10, 12);//cal time

                        String stEnd = new String(selected_enddate.get(0).toString());
                        String time_end = stEnd.substring(8, 12);
                        String time_end_hour = stEnd.substring(8, 10);//cal time
                        String time_end_minute = stEnd.substring(10, 12);//cal time

                        int timeStart = 0;
                        int timeEnd = 0;
                        timeStart = Integer.parseInt(time_start);
                        timeEnd = Integer.parseInt(time_end);

                        //--------------------- CHECK Holiday startDate And endDate -----------------------

                        DateFormat format = new SimpleDateFormat("yyyy-MM-dd");
                        Date strDateStart = null;
                        Date strDateEnd = null;
                        try {
                            strDateStart = format.parse(yearStart + "-" + monthStart + "-" + dayStart);
                            strDateEnd = format.parse(yearEnd + "-" + monthEnd + "-" + dayEnd);
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }
                        String WeekDayByStartDate = checkWeekEnd(strDateStart);
                        String HolidayByStartDate = checkHoliday(strDateStart);
                        String WeekDayByEndDate = checkWeekEnd(strDateEnd);
                        String HolidayByEndDate = checkHoliday(strDateEnd);

                        if (statusExtra.equals("1")) {//for Giffware
                            HolidayByStartDate = "N";
                            HolidayByEndDate = "N";
                        }

                        //----------------------------------------------------------------------------------
                        if (selected_leavetype.get(0).equals("0")) {//check blank select type leave
                            DialogUtils.showAlertDialog(getActivity(), getString(R.string.alert_text_leave_type));
                        } else if (val_reason.length() == 0) {//check blank input Comment
                            DialogUtils.showAlertDialog(getActivity(), getString(R.string.alert_text_comment));
                        } else if (selected_startdate.get(0).equals("0")) {//check select start date
                            DialogUtils.showAlertDialog(getActivity(), getString(R.string.alert_text_time_statdate));
                        } else if (selected_enddate.get(0).equals("0")) {//check select end date
                            DialogUtils.showAlertDialog(getActivity(), getString(R.string.alert_text_time_enddate));
                        } else if (Long.parseLong(date_start) < Long.parseLong(strDateTodayNotTime) || Long.parseLong(date_end) < Long.parseLong(strDateTodayNotTime)) {
                            DialogUtils.showAlertDialog(getActivity(), getString(R.string.alert_text_morethan_today));
                        } else if (WeekDayByStartDate.equals("Y") || HolidayByStartDate.equals("Y")) {
                            DialogUtils.showAlertDialog(getActivity(), getString(R.string.alert_text_select_holiday));
                        } else if (WeekDayByEndDate.equals("Y") || HolidayByEndDate.equals("Y")) {
                            DialogUtils.showAlertDialog(getActivity(), getString(R.string.alert_text_selectend_holiday));
                        } else if (timeStart < 830 || timeStart > 1730) {
                            DialogUtils.showAlertDialog(getActivity(), getString(R.string.alert_text_select_time));
                        } else if (timeEnd < 830 || timeEnd > 1730) {
                            DialogUtils.showAlertDialog(getActivity(), getString(R.string.alert_text_select_time));
                        } else if (timeStart == 1730) {
                            DialogUtils.showAlertDialog(getActivity(), getString(R.string.alert_text_startdate_1730));
                        } else if (timeEnd == 830) {
                            DialogUtils.showAlertDialog(getActivity(), getString(R.string.alert_text_enddate_0830));
                        } else if (Long.parseLong(selected_startdate.get(0)) == Long.parseLong(selected_enddate.get(0))) {//start date same  end date
                            DialogUtils.showAlertDialog(getActivity(), getString(R.string.alert_text_start_same_end));
                        } else if (Long.parseLong(selected_startdate.get(0)) > Long.parseLong(selected_enddate.get(0))) {
                            DialogUtils.showAlertDialog(getActivity(), getString(R.string.alert_text_start_morethan_end));
                        } else if (selected_leavetype.get(0).equals("2") && !time_start_month.equals(monthBirthday.get(0))) {
                            DialogUtils.showAlertDialog(getActivity(), getString(R.string.alert_text_select_birthday));
                        } else {
                            //----------------------------------------------------------------------------
                            String StWeekDay = "";
                            String StHoliday = "";
                            List<Date> dates = getDates(startD, endD, "yyyy-MM-dd");//check between date
                            int allDay = dates.size();
                            realDay = 0;
                            totalDay = 0;
                            for (Date date : dates) {

                                StWeekDay = checkWeekEnd(date);
                                StHoliday = checkHoliday(date);
                                if (statusExtra.equals("1")) {//for Giffware
                                    StHoliday = "N";
                                }
                                if (!StWeekDay.equals("Y") && !StHoliday.equals("Y")) {
                                    realDay++;
                                }

                            }

                            //-----------------Cal Time--------------------------------------------
                            remeanTime = 0;
                            remeanTime = Integer.parseInt(time_end_hour) - Integer.parseInt(time_start_hour);
                            minuteStart = Integer.parseInt(time_start_minute);
                            minuteEnd = Integer.parseInt(time_end_minute);

                            if (minuteEnd == 30 && minuteStart == 30 || minuteEnd == 0 && minuteStart == 0) {
                                remeanTime = (remeanTime * 60);
                            } else if (minuteStart == 30 && minuteEnd == 0) {
                                remeanTime = (remeanTime * 60) - 30;
                            } else if (minuteStart == 0 && minuteEnd == 30) {
                                remeanTime = (remeanTime * 60) + 30;
                            }

                            //---  Check status leave to day -----
                            String statusToday = "";
                            if (Long.parseLong(date_start) == Long.parseLong(strDateTodayNotTime) || Long.parseLong(date_end) == Long.parseLong(strDateTodayNotTime)) {
                                statusToday = "Y";
                            } else {
                                statusToday = "N";
                            }

                            if (INCLUDING_SAT_SUN) {//-	Military leave  -	Maternity leave -	Leave Without Pay
                                realDay = allDay;
                            }

                            //------------------------- Cal time------------------------------------------
                            String startDate = selected_startdate.get(0);
                            String endDate = selected_enddate.get(0);
                            String val_time_start = "08";
                            String val_time_end = "17";

                            totalMinute = String.valueOf(realDay * 480);
                            if (statusAllday.equals("ALL")) {//Case All Day
                                totalDay = realDay;
                                totalMinute = String.valueOf(Integer.parseInt(totalMinute));
                                startDate = date_start + "0830";
                                endDate = date_end + "1730";
                            } else {//Case General
                                if (timeStart == 830 && timeEnd == 1730 && startD.equals(endD)) {
                                    totalMinute = "480";
                                } else if (timeStart != 830 && timeEnd != 1730 && startD.equals(endD)) {
                                    totalDay = realDay - 1;
                                    totalMinute = String.valueOf(((realDay - 1) * 480));

                                    //Call lunch ----------------------------------------
                                    String st_start = new String(startDate);
                                    val_time_start = st_start.substring(8, 10);
                                    String st_end = new String(endDate);
                                    val_time_end = st_end.substring(8, 10);
                                    //-----------------------------------------------------------------------------
                                    if (Integer.parseInt(val_time_start) <= 12 && Integer.parseInt(val_time_end) >= 13) {
                                        totalMinute = String.valueOf(Integer.parseInt(totalMinute) - 60);
                                    }
                                    totalMinute = String.valueOf(Integer.parseInt(totalMinute) + remeanTime);
                                } else {//Case special start date != end date and  start datetime != end date

                                    totalDay = realDay - 1;

                                    totalMinute = String.valueOf(((realDay - 1) * 480));

                                    String st_start = new String(startDate);
                                    val_time_start = st_start.substring(8, 10);
                                    String st_end = new String(endDate);
                                    val_time_end = st_end.substring(8, 10);

                                    int remeanTimeBeforeLunchStart = 0;
                                    int remeanTimeAfterLunchStart = 0;
                                    int remeanTimeBeforeLunchEnd = 0;
                                    int remeanTimeAfterLunchEnd = 0;
                                    //Call time Start Date
                                    if (Integer.parseInt(val_time_start) <= 12) {//Before Lunch
                                        remeanTimeBeforeLunchStart = 17 - Integer.parseInt(val_time_start);
                                        minuteStart = Integer.parseInt(time_start_minute);
                                        minuteEnd = 30;//00 in 12:00
                                        if (minuteEnd == 30 && minuteStart == 30 || minuteEnd == 0 && minuteStart == 0) {//After Lunch
                                            remeanTimeBeforeLunchStart = (remeanTimeBeforeLunchStart * 60);
                                        } else if (minuteStart == 30 && minuteEnd == 0) {
                                            remeanTimeBeforeLunchStart = (remeanTimeBeforeLunchStart * 60) - 30;
                                        } else if (minuteStart == 0 && minuteEnd == 30) {
                                            remeanTimeBeforeLunchStart = (remeanTimeBeforeLunchStart * 60) + 30;
                                        }
                                        remeanTimeBeforeLunchStart = remeanTimeBeforeLunchStart - 60;// - Lench 60  minute
                                    } else if (Integer.parseInt(val_time_start) >= 13) {
                                        remeanTimeAfterLunchStart = 17 - Integer.parseInt(val_time_start);
                                        minuteStart = Integer.parseInt(time_start_minute);
                                        minuteEnd = 30;//30 in 17:30
                                        if (minuteEnd == 30 && minuteStart == 30 || minuteEnd == 0 && minuteStart == 0) {//After Lunch
                                            remeanTimeAfterLunchStart = (remeanTimeAfterLunchStart * 60);
                                        } else if (minuteStart == 30 && minuteEnd == 0) {
                                            remeanTimeAfterLunchStart = (remeanTimeAfterLunchStart * 60) - 30;
                                        } else if (minuteStart == 0 && minuteEnd == 30) {
                                            remeanTimeAfterLunchStart = (remeanTimeAfterLunchStart * 60) + 30;
                                        }
                                    }

                                    if (Integer.parseInt(val_time_end) <= 12) {//Before Lunch
                                        remeanTimeBeforeLunchEnd = Integer.parseInt(val_time_end) - 8;
                                        minuteEnd = Integer.parseInt(time_end_minute);
                                        minuteStart = 30;//00 in 12:00
                                        if (minuteEnd == 30 && minuteStart == 30 || minuteEnd == 0 && minuteStart == 0) {//After Lunch
                                            remeanTimeBeforeLunchEnd = (remeanTimeBeforeLunchEnd * 60);
                                        } else if (minuteStart == 30 && minuteEnd == 0) {
                                            remeanTimeBeforeLunchEnd = (remeanTimeBeforeLunchEnd * 60) - 30;
                                        } else if (minuteStart == 0 && minuteEnd == 30) {
                                            remeanTimeBeforeLunchEnd = (remeanTimeBeforeLunchEnd * 60) + 30;
                                        }
                                    } else if (Integer.parseInt(val_time_end) >= 13) {
                                        remeanTimeAfterLunchEnd = Integer.parseInt(val_time_end) - 8;
                                        minuteEnd = Integer.parseInt(time_end_minute);
                                        minuteStart = 30;//30 in 17:30
                                        if (minuteEnd == 30 && minuteStart == 30 || minuteEnd == 0 && minuteStart == 0) {//After Lunch
                                            remeanTimeAfterLunchEnd = (remeanTimeAfterLunchEnd * 60);
                                        } else if (minuteStart == 30 && minuteEnd == 0) {
                                            remeanTimeAfterLunchEnd = (remeanTimeAfterLunchEnd * 60) - 30;
                                        } else if (minuteStart == 0 && minuteEnd == 30) {
                                            remeanTimeAfterLunchEnd = (remeanTimeAfterLunchEnd * 60) + 30;
                                        }
                                        remeanTimeAfterLunchEnd = remeanTimeAfterLunchEnd - 60;// - Lunch 60  minute
                                    }

                                    int AllMinuteStartDate = remeanTimeAfterLunchStart + remeanTimeBeforeLunchStart;
                                    int AllMinuteEndDate = remeanTimeAfterLunchEnd + remeanTimeBeforeLunchEnd;

                                    if (AllMinuteStartDate == 480) {
                                        totalMinute = String.valueOf((Integer.parseInt(totalMinute)) + AllMinuteEndDate);
                                    } else {
                                        totalMinute = String.valueOf((Integer.parseInt(totalMinute)) - 480 + AllMinuteStartDate + AllMinuteEndDate);
                                    }
                                }
                            }

                            //Check Birthday > 1 day (480Minute)-------------------------------------------------
                            if (selected_leavetype.get(0).equals("2") && Integer.parseInt(totalMinute) > 480) {
                                DialogUtils.showAlertDialog(getActivity(), getString(R.string.alert_text_select_birthday));
                            } else if (selected_leavetype.get(0).equals("2") && Integer.parseInt(totalMinute) > remainDayLeave) {
                                String showTime = "";
                                int totalTime = remainDayLeave;

                                int IntTotalDay = totalTime / 480;
                                float FloatTotalHour = (totalTime % 480.0f) / 60.0f;

                                if (IntTotalDay != 0) {
                                    showTime += IntTotalDay;
                                    showTime += IntTotalDay <= 1 ? " day " : " days ";
                                }

                                if (FloatTotalHour > 0) {
                                    showTime += (FloatTotalHour % 1 == 0 ? (int) FloatTotalHour + "" : FloatTotalHour + "") + (FloatTotalHour <= 1 ? " Hour " : " Hours ");
                                }

                                DialogUtils.showAlertDialog(getActivity(), "Sorry, but you only have  " + showTime + " left to use for your birthday leave.");
//                            } else if (selected_leavetype.get(0).equals("1") && Integer.parseInt(totalMinute) > remainDayLeave) {
//                                DialogUtils.showAlertDialog(getActivity(), getString(R.string.alert_text_annualleave_over));
                            } else if (selected_leavetype.get(0).equals("3") && Integer.parseInt(totalMinute) > remainDayLeave) {
                                DialogUtils.showAlertDialog(getActivity(), getString(R.string.alert_text_leave_over));
                            } else if (!startDate.substring(0, 4).equals(endDate.substring(0, 4))) {
                                DialogUtils.showAlertDialog(getActivity(), getString(R.string.alert_text_select_year));
                            } else {
                                Intent goSecond = new Intent(getActivity(), RequestReviewActivity.class);
                                goSecond.putExtra("token", application.getCurProfile().getSTAFF_TOKEN());
                                goSecond.putExtra("Api_startDate", startDate);
                                goSecond.putExtra("Api_endDate", endDate);
                                goSecond.putExtra("startDate", showStartDate);
                                goSecond.putExtra("endDate", showEndDate);
                                goSecond.putExtra("Api_totalTime", totalMinute);
                                goSecond.putExtra("leaveType", selected_leavetype.get(0));
                                goSecond.putExtra("leaveTypeName", selected_leavetype_name.get(0));
                                goSecond.putExtra("leaveStatus", "1");
                                goSecond.putExtra("reason", val_reason);
                                goSecond.putExtra("salulation", selected_leavetype_salulation.get(0));
                                goSecond.putExtra("noticeDay", notice);
                                goSecond.putExtra("serverTime", serverTime);
                                goSecond.putExtra("statusToday", statusToday);
                                startActivityForResult(goSecond, 0);
                            }
                        }
                    }
                });

                boxCloseButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        ((MainActivity) getActivity()).setIsApplayDayoff(false);
                        ((MainActivity) getActivity()).setIsApplyBirthDay(false);
                        change_spinner_start = true;
                        change_spinner_end = true;
                        boxAddLeave.dismiss();

                    }
                });

                boxAddLeave.show();
            }
        } else {
            DialogUtils.showAlertDialog(getActivity(), getString(R.string.alert_text_noselect_date));
        }
    }


    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {//Start api
        super.setUserVisibleHint(isVisibleToUser);
        if (this.isVisible()) {
            if (!isVisibleToUser) {
                isVisibleView = false;
            } else {
                isVisibleView = true;
                thisMonthBirthday = false;

                ((MainActivity) getActivity()).enableViewPager();
                getStaffSummary();// Call Staff Summary
                clearData();//clear data

                //Birthday
                Date toDay = null;///new Date();
                try {
                    toDay = AwareUtils.getCurTimeFromServer(getActivity());
                } catch (Exception e) {
                    toDay = new Date();
                    e.printStackTrace();
                }

                SimpleDateFormat sdf = new SimpleDateFormat("MM", Locale.ENGLISH);
                String strDateToday = sdf.format(toDay);
                String st_birthday = application.getCurProfile().getBIRTH_DATE();
                String month_birthday = st_birthday.substring(5, 7);

                thisMonthBirthday = strDateToday.equals(month_birthday);

                BtnBirthDay = ((MainActivity) getActivity()).isApplyBirthDay();
                BtnDayoff = ((MainActivity) getActivity()).isApplyDayoff();

                if (BtnBirthDay) { // ref. btn applyBirthday
                    Date today = null;
                    try {
                        today = AwareUtils.getCurTimeFromServer(getActivity());
                    } catch (Exception e) {
                        today = new Date();
                        e.printStackTrace();
                    }

                    SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMdd");
                    String dayToday = formatter.format(today);
                    String stEnd = dayToday;
                    String timeYear = stEnd.substring(0, 4);
                    String timeMonth = stEnd.substring(4, 6);//
                    String timeDay = stEnd.substring(6, 8);

                    if (!thisMonthBirthday) {
                        String StrMonth = "";
                        if (timeMonth.equals("12")) {//calendarDay.getMonth
                            StrMonth = "01";
                        } else {
                            StrMonth = String.valueOf(Integer.parseInt(timeMonth) + 1);//calendarDay.getMonth
                        }

                        int num = StrMonth.length();
                        if (num == 1) {

                            StrMonth = "0" + StrMonth;
                        }
                        dayToday = timeYear + StrMonth + "01";
                    }
                    //--------------------------------------------------------
                    DateFormat format = new SimpleDateFormat("yyyyMMdd");
                    Date strDate = null;
                    try {
                        strDate = format.parse(dayToday);
                    } catch (ParseException e) {
                        strDate = new Date();
                        e.printStackTrace();
                    }

                    String sthistory = checkHistory(strDate);
                    if (!sthistory.equals("Y")) {//check history
                        dates.add(CalendarDay.from(strDate));
                        new ShowEventClickDate().executeOnExecutor(Executors.newSingleThreadExecutor());
                    }

                    btnApply.clear();
                    btnApply.add("B");

                    Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            showBoxDetails(btn_submit_request, "B");
                        }
                    }, 1000);

                } else if (BtnDayoff) { // ref. btn applyBirthday

                    Date today = null;
                    try {
                        today = AwareUtils.getCurTimeFromServer(getActivity());
                    } catch (Exception e) {
                        today = new Date();
                        e.printStackTrace();
                    }
                    SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMdd");
                    String dayToday = formatter.format(today);
                    DateFormat format = new SimpleDateFormat("yyyyMMdd");

                    Date strDate = null;
                    try {
                        strDate = format.parse(dayToday);
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }

                    String sthistory = checkHistory(strDate);
                    if (!sthistory.equals("Y")) {//check history
                        dates.add(CalendarDay.from(strDate));
                        new ShowEventClickDate().executeOnExecutor(Executors.newSingleThreadExecutor());
                    }

                    btnApply.clear();
                    btnApply.add("D");

                    Handler handler = new Handler();

                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            showBoxDetails(btn_submit_request, "D");
                        }
                    }, 1000);
                }
            }
        }
    }

    private static List<Date> getDates(String dateString1, String dateString2, String formats) {
        ArrayList<Date> dates = new ArrayList<Date>();
        DateFormat df1 = new SimpleDateFormat(formats);

        Date date1 = null;
        Date date2 = null;

        try {
            date1 = df1.parse(dateString1);
            date2 = df1.parse(dateString2);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        Calendar cal1 = Calendar.getInstance();
        cal1.setTime(date1);

        Calendar cal2 = Calendar.getInstance();
        cal2.setTime(date2);

        while (!cal1.after(cal2)) {
            dates.add(cal1.getTime());
            cal1.add(Calendar.DATE, 1);
        }
        return dates;
    }

    private void ShowDatePicker(Dialog boxAddLeave, String btnClick, Calendar tempCal) {

        linear1 = (LinearLayout) boxAddLeave.findViewById(R.id.boxdateStart);
        linear2 = (LinearLayout) boxAddLeave.findViewById(R.id.boxdateEnd);

        layoutParams1 = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT);

        layoutParams2 = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT);

        if (btnClick.equals("S")) {
            linear2.removeAllViews();
            dateView.setDate(tempCal.get(Calendar.DAY_OF_MONTH),
                    tempCal.get(Calendar.MONTH),
                    tempCal.get(Calendar.YEAR),
                    tempCal.get(Calendar.HOUR_OF_DAY),
                    tempCal.get(Calendar.MINUTE));
            linear1.addView(dateView.getView(), layoutParams1);
            btnGetdate1.setVisibility(View.VISIBLE);
            btnGetdate2.setVisibility(View.GONE);
        } else {
            dateView.setDate(tempCal.get(Calendar.DAY_OF_MONTH),
                    tempCal.get(Calendar.MONTH),
                    tempCal.get(Calendar.YEAR),
                    tempCal.get(Calendar.HOUR_OF_DAY),
                    tempCal.get(Calendar.MINUTE));
            linear1.removeAllViews();
            linear2.addView(dateView.getView(), layoutParams2);
            btnGetdate2.setVisibility(View.VISIBLE);
            btnGetdate1.setVisibility(View.GONE);
        }
    }

    //---------------------------------------------- SPINNER LEAVE TYPE -----------------------------------------------------
    private void LeaveTypeItem(Dialog boxAddLeave) {

        final ArrayAdapter<LeaveType> adapter = new ArrayAdapter<LeaveType>(getActivity(), android.R.layout.simple_spinner_dropdown_item, list_leavetype);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        Spinner dropdown1 = (Spinner) boxAddLeave.findViewById(R.id.sp_leavetype);
        final TextView leaveRemark = (TextView) boxAddLeave.findViewById(R.id.text_leave_remark);
        dropdown1.setAdapter(adapter);

        dropdown1.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> adapterView, final View view, final int position, long id) {

                LeaveType leavetype = adapter.getItem(position);
                selected_leavetype.clear();
                selected_leavetype_name.clear();
                selected_leavetype_salulation.clear();
                selected_leavetype.add(leavetype.getID());
                selected_leavetype_name.add(leavetype.getName());
                selected_leavetype_salulation.add(leavetype.getSalutation());
                leaveRemark.setVisibility(View.VISIBLE);
                leaveRemark.setText(leavetype.getLeaveremark());
                notice = "";
                notice = leavetype.getNOTICEREQUEST();

                INCLUDING_SAT_SUN = leavetype.getINCLUDING_SAT_SUN().equals("1");
                //----- check remain day leave--*Annual and Sick

                remainDayLeave = 0;
                int allTimeUsed = 0;
                if (leavetype.getID().equals("1") || leavetype.getID().equals("2") || leavetype.getID().equals("3")) {
                    if (leavetype.getAlltimeused() != null || !leavetype.getAlltimeused().equals("0")) {
                        allTimeUsed = Integer.parseInt(leavetype.getAlltimeused());
                    }
                    remainDayLeave = Integer.parseInt(leavetype.getAlldayforleave()) - allTimeUsed;
                }

                //leavetype.
                ((TextView) view).setGravity(Gravity.CENTER);
                ((TextView) view).setTextSize(16);
                ((TextView) view).setTypeface(font);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapter) {
            }
        });

        if (btnApply.size() != 0) {
            String key = "";
            if (btnApply.get(0).equals("B")) {
                key = "2";
            } else {
                key = "11";
            }
            for (int i = 0; i < list_leavetype.size(); i++) {

                if (list_leavetype.get(i).getID().equals(key)) {
                    dropdown1.setSelection(i);
                }
            }
        }
    }

    //--------------------------------------------------   SERVICE ----------------------------------------------------------
    private OnListenerFromWSManager onListenerFromWSManagerHistory = new OnListenerFromWSManager() {
        @Override
        public void onComplete(String statusCode, String message) {

            Gson gson = new Gson();
            LeaveHistoryResponse leaveHistoryFromWS = gson.fromJson(message, LeaveHistoryResponse.class);
            List<LeaveHistoryResponse.DataEntity> models = leaveHistoryFromWS.getData();

            ResponseEntity resEntity = leaveHistoryFromWS.getResponse();

            //serverTime
            String StrServerTime = resEntity.getServer_time();
            callServeTime = new ISO8601();

            try {
                Calendar calendarDay = callServeTime.toCalendar(StrServerTime);
                Date time = calendarDay.getTime();
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-ddHHmm");//all time
                serverTime = sdf.format(time);

            } catch (ParseException e) {
                e.printStackTrace();
            }

            for (LeaveHistoryResponse.DataEntity model : models) {

                if (model.getRL_STATUSID().equals("1") || model.getRL_STATUSID().equals("3") || model.getRL_STATUSID().equals("5") || model.getRL_STATUSID().equals("7")) {

                    String dayStart = model.getRLD_DATE_START();
                    String date_start = dayStart.substring(0, 8);
                    String dayEnd = model.getRLD_DATE_END();
                    String date_end = dayEnd.substring(0, 8);
                    List<Date> dates = getDates(date_start, date_end, "yyyyMMdd");//check betweendate
                    for (Date date : dates) {
                        SimpleDateFormat dateFormatReturn = new SimpleDateFormat("yyyyMMdd");
                        String strDay = "";
                        strDay = dateFormatReturn.format(date);
                        list_history.add(strDay);
                    }
                    //-------------------------------------------------------------------------------
                    String yearStart = date_start.substring(0, 4);
                    String monthStart = date_start.substring(4, 6);
                    String daysStart = date_start.substring(6, 8);

                    String yearEnd = date_end.substring(0, 4);
                    String monthEnd = date_end.substring(4, 6);
                    String daysEnd = date_end.substring(6, 8);

                    String StartDate = yearStart + "-" + monthStart + "-" + daysStart;
                    String EndDate = yearEnd + "-" + monthEnd + "-" + daysEnd;

                    callEventHistory(StartDate, EndDate, "yyyy-MM-dd");//Old "yyyyMMddHHmm"
                }
            }
            new ShowEventHistory().execute();
            dismissDialog();
        }

        @Override
        public void onFailed(String statusCode, String message) {
            dismissDialog();
        }

        @Override
        public void onFailed(String message) {
            dismissDialog();
        }

        @Override
        public void onFailedAndForceLogout() {
            dismissDialog();
            if (!AwareUtils.isForceLogout) {
                AwareUtils.forceLogOut(getActivity());
            }
        }
    };

    private OnListenerFromWSManager onListenerFromWSManagerHoliday = new OnListenerFromWSManager() {//Holiday
        @Override
        public void onComplete(String statusCode, String message) {

            Gson gson = new Gson();
            HolidayResponse holidayFromWS = gson.fromJson(message, HolidayResponse.class);
            List<HolidayResponse.DataEntity> models = holidayFromWS.getData();
            list_holiday.clear();
            for (HolidayResponse.DataEntity model : models) {
                list_holiday.add(new Holiday(model.getCLDD_DATECOUNT(), model.getCLDD_STARTDATE(), model.getCLDD_ENDDATE(), model.getCLDD_DESCRIPTION(), model.getCLDD_DAY_NAMETH()));
                callEventHoliday(model.getCLDD_STARTDATE(), model.getCLDD_ENDDATE());
                countAlert = 1;
            }
            new ShowEventHoliday().executeOnExecutor(Executors.newSingleThreadExecutor());
            setMonth(monthDate);
            dismissDialog();
        }

        @Override
        public void onFailed(String statusCode, final String message) {
            dismissDialog();
        }

        @Override
        public void onFailed(final String message) {
            dismissDialog();
        }

        @Override
        public void onFailedAndForceLogout() {
            dismissDialog();
            if (!AwareUtils.isForceLogout) {
                AwareUtils.forceLogOut(getActivity());
            }
        }
    };

    private OnListenerFromWSManager onResManager = new OnListenerFromWSManager() {//Leave type
        @Override
        public void onComplete(String statusCode, String message) {
            Date today = null;//
            try {
                today = AwareUtils.getCurTimeFromServer(getActivity());
            } catch (Exception e) {
                today = new Date();
                e.printStackTrace();
            }
            SimpleDateFormat sdf = new SimpleDateFormat("MM", Locale.ENGLISH);
            SimpleDateFormat sdf2 = new SimpleDateFormat("dd", Locale.ENGLISH);
            String strMonthToday = sdf.format(today);
            String strDateToday = sdf2.format(today);
            String st_birthday = application.getCurProfile().getBIRTH_DATE();
            String month_birthday = st_birthday.substring(5, 7);
            if (month_birthday.equals("01") && strMonthToday.equals("12")) {
                strMonthToday = "0";
            }

            //date before month birthday
            beforeBirthdayLeave = Integer.parseInt(month_birthday) - Integer.parseInt(strMonthToday) == 1 && Integer.parseInt(strDateToday) >= 25;

            monthBirthday.clear();
            monthBirthday.add(month_birthday);
            Gson gson = new Gson();
            StaffSummaryResponse staffSummaryFromWS = gson.fromJson(message, StaffSummaryResponse.class);
            List<StaffSummaryResponse.DataEntity> models = staffSummaryFromWS.getData();

            list_leavetype.clear();
            list_leavetype.add(new LeaveType("0", "Select Leave type", "", "", "", "", "", ""));

            String remark = "";
            String ALLTIMEUSED = "";
            for (StaffSummaryResponse.DataEntity model : models) {
                if (model.getACTIVETOLIST().equals("1")) {
                    String id = checkLeaveTypeID(model.getLEAVENAME());
                    if (model.getLEAVEREMARK() == null) {
                        remark = "";
                    } else {
                        remark = model.getLEAVEREMARK();
                    }

                    if (model.getALLTIMEUSED() == null) {
                        ALLTIMEUSED = "0";
                    } else {
                        ALLTIMEUSED = model.getALLTIMEUSED();
                    }

                    switch (id) {
                        case "2": //check Birthday  && BtnBirthDay
                            if (strMonthToday.equals(month_birthday) && !model.getALLDAYFORLEAVE().equals(ALLTIMEUSED) && !model.getALLDAYFORLEAVE().equals("0")) {
                                list_leavetype.add(new LeaveType(id, model.getLEAVENAME(), remark, model.getLEAVESALUTATION(), model.getALLDAYFORLEAVE(), ALLTIMEUSED, model.getINCLUDING_SAT_SUN(), model.getNOTICEREQUEST()));
                            } else if (beforeBirthdayLeave) {//date before month birthday
                                list_leavetype.add(new LeaveType(id, model.getLEAVENAME(), remark, model.getLEAVESALUTATION(), model.getALLDAYFORLEAVE(), ALLTIMEUSED, model.getINCLUDING_SAT_SUN(), model.getNOTICEREQUEST()));
                            }
                            break;
                        case "3": //check annual && sick leave
                            if (!model.getALLDAYFORLEAVE().equals(ALLTIMEUSED)) {
                                list_leavetype.add(new LeaveType(id, model.getLEAVENAME(), remark, model.getLEAVESALUTATION(), model.getALLDAYFORLEAVE(), ALLTIMEUSED, model.getINCLUDING_SAT_SUN(), model.getNOTICEREQUEST()));
                            }
                            break;
                        case "11": //check day off
                            if (model.getHASDAYOFF().equals("1")) {//1 = show 2 //no show
                                list_leavetype.add(new LeaveType(id, model.getLEAVENAME(), remark, model.getLEAVESALUTATION(), model.getALLDAYFORLEAVE(), ALLTIMEUSED, model.getINCLUDING_SAT_SUN(), model.getNOTICEREQUEST()));
                            }
                            break;
                        default:
                            list_leavetype.add(new LeaveType(id, model.getLEAVENAME(), remark, model.getLEAVESALUTATION(), model.getALLDAYFORLEAVE(), ALLTIMEUSED, model.getINCLUDING_SAT_SUN(), model.getNOTICEREQUEST()));
                            break;
                    }
                }
            }
            dismissDialog();
        }

        @Override
        public void onFailed(String statusCode, final String message) {
            dismissDialog();
        }

        @Override
        public void onFailed(final String message) {
            dismissDialog();
        }

        @Override
        public void onFailedAndForceLogout() {
            dismissDialog();
            if (!AwareUtils.isForceLogout) {
                AwareUtils.forceLogOut(getActivity());
            }
        }
    };

    private String checkLeaveTypeID(String Key) {

        switch (Key) {
            case "Annual Leave":
                return "1";
            case "Birthday Leave":
                return "2";
            case "Sick Leave":
                return "3";
            case "Military Leave":
                return "4";
            case "Sterilization Leave":
                return "5";
            case "Ordination Leave":
                return "6";
            case "Maternity Leave":
                return "7";
            case "Paternity Leave":
                return "8";
            case "Bereavement Leave":
                return "9";
            case "Leave Without Pay":
                return "10";
            case "Day Off":
                return "11";
            default:
                return "0";
        }

    }

    ///----------------------------  Format Date ---------------------------------------------

    private String timeFromString(int hour, int minute) {
        return ((hour > 9) ? "" : "0") + hour + ":" + ((minute > 9) ? "" : "0") + minute;
    }

    private String timeFromString2(int hour, int minute) {
        return ((hour > 9) ? "" : "0") + hour + "" + ((minute > 9) ? "" : "0") + minute;
    }

    //----------------------------------------------------------------------------------------------
    private String dateFormat(int date, int month, int year, String format, String strDate, String mode) {
        SimpleDateFormat dateFormatSource = new SimpleDateFormat(format, Locale.US);
        SimpleDateFormat dateFormatReturn = new SimpleDateFormat("yyyyMMdd", Locale.US);
        if (!mode.equals("ALL")) {
            strDate = date + "/" + month + "/" + year;
        }
        Date dDate;
        String strReturn = "";

        try {
            dDate = dateFormatSource.parse(strDate);
            strReturn = dateFormatReturn.format(dDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return strReturn;
    }

    private String dateFormat2(int date, int month, int year, String from_format, String pattern) {

        SimpleDateFormat dateFormatSource = new SimpleDateFormat(from_format, Locale.US);
        SimpleDateFormat dateFormatReturn = new SimpleDateFormat("EEE " + "dd" + pattern + "MMM" + pattern + "yy", Locale.US);

        String strDate = date + "/" + month + "/" + year;
        Date dDate;
        String strReturn = "";

        try {
            dDate = dateFormatSource.parse(strDate);
            strReturn = dateFormatReturn.format(dDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return strReturn;
    }

    private String dateFormat3(int date, int month, int year) {
        SimpleDateFormat dateFormatSource = new SimpleDateFormat("dd/MM/yyyy", Locale.US);
        SimpleDateFormat dateFormatReturn = new SimpleDateFormat("yyyy-MM-dd", Locale.US);

        String strDate = date + "/" + month + "/" + year;
        Date dDate;
        String strReturn = "";

        try {
            dDate = dateFormatSource.parse(strDate);
            strReturn = dateFormatReturn.format(dDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return strReturn;
    }

    private String dateFormat4(String strDate, String from_format, String pattern) {

        SimpleDateFormat dateFormatSource = new SimpleDateFormat(from_format, Locale.US);
        SimpleDateFormat dateFormatReturn = new SimpleDateFormat("EEE " + "dd" + pattern + "MMM" + pattern + "yy", Locale.US);
        Date dDate;
        String strReturn = "";

        try {
            dDate = dateFormatSource.parse(strDate);
            strReturn = dateFormatReturn.format(dDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return strReturn;
    }

    private String checkWeekEnd(Date day) {
        SimpleDateFormat dateFormatReturn = new SimpleDateFormat("EEE", Locale.ENGLISH);
        String strDay = "";
        strDay = dateFormatReturn.format(day);

        if (strDay.equals("Sat") || strDay.equals("Sun")) {
            return "Y";
        } else {
            return "N";
        }
    }

    private String checkHistory(Date day) {
        SimpleDateFormat dateFormatReturn = new SimpleDateFormat("yyyyMMdd", Locale.ENGLISH);
        String strDay = "";
        String status = "";
        strDay = dateFormatReturn.format(day);

        for (int i = 0; i < list_history.size(); i++) {
            if (list_history.get(i).equals(strDay)) {
                return "Y";
            }
        }
        return status;
    }

    private String checkHoliday(Date day) {
        SimpleDateFormat dateFormatReturn = new SimpleDateFormat("yyyyMMdd", Locale.ENGLISH);
        String strDay = "";
        String status = "";
        strDay = dateFormatReturn.format(day);

        for (Holiday holiday : list_holiday) {

            if (!holiday.getCLDD_DATECOUNT().equals("1")) {//between holiday

                if (Integer.parseInt(strDay) >= Integer.parseInt(holiday.getCLDD_STARTDATE()) && Integer.parseInt(strDay) <= Integer.parseInt(holiday.getCLDD_ENDDATE())) {
                    return "Y";
                }

            } else {//normal holiday

                if (holiday.getCLDD_STARTDATE().equals(strDay)) {
                    return "Y";
                }
            }
        }
        return status;
    }
    //----------------------------------------------------------------------------------------------

    @Override
    public void onDateChanged(@NonNull MaterialCalendarView widget, @Nullable CalendarDay date) {
        if (date == null)
            return;

        if (date.getDate().getTime() < curDate.getTime())
            return;

        if (dates.size() > 0) {
            Date lastDate = dates.get(dates.size() - 1).getDate();
            Date first = dates.get(0).getDate();
            if (dates.get(0).equals(date) || dates.get(dates.size() - 1).equals(date)) {
                Log.i("onDateChanged", "onDateChanged equals");
                dates.remove(date);
            } else if (date.getDate().getTime() == lastDate.getTime() + 86400000) {
                dates.add(date);
            } else if (date.getDate().getTime() == first.getTime() - 86400000) {
                ArrayList<CalendarDay> temp = new ArrayList<>();
                temp.addAll(dates);
                dates.clear();
                dates.add(date);
                dates.addAll(temp);
            }
            new ShowEventClickDate().executeOnExecutor(Executors.newSingleThreadExecutor());
        } else {
            dates.add(date);
            new ShowEventClickDate().executeOnExecutor(Executors.newSingleThreadExecutor());
        }
    }

    @Override
    public void onMonthChanged(MaterialCalendarView widget, CalendarDay date) {
        monthDate = date.getDate();
        setMonth(monthDate);
    }

    private void setMonth(Date date) {
        fixMonth1 = "";
        fixMonth2 = "";
        fixMonth3 = "";
        fixMonth4 = "";
        fixMonth5 = "";

        SimpleDateFormat formatter = new SimpleDateFormat("MM", Locale.US);
        String formattedDate = formatter.format(date);
        String[] array_month = {"Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"};

        switch (formattedDate) {
            case "01":
                fixMonth1 = array_month[10];
                fixMonth2 = array_month[11];
                fixMonth3 = array_month[0];
                fixMonth4 = array_month[1];
                fixMonth5 = array_month[2];
                break;
            case "02":
                fixMonth1 = array_month[11];
                fixMonth2 = array_month[0];
                fixMonth3 = array_month[1];
                fixMonth4 = array_month[2];
                fixMonth5 = array_month[3];
                break;
            case "03":
                fixMonth1 = array_month[0];
                fixMonth2 = array_month[1];
                fixMonth3 = array_month[2];
                fixMonth4 = array_month[3];
                fixMonth5 = array_month[4];
                break;
            case "04":
                fixMonth1 = array_month[1];
                fixMonth2 = array_month[2];
                fixMonth3 = array_month[3];
                fixMonth4 = array_month[4];
                fixMonth5 = array_month[5];
                break;
            case "05":
                fixMonth1 = array_month[2];
                fixMonth2 = array_month[3];
                fixMonth3 = array_month[4];
                fixMonth4 = array_month[5];
                fixMonth5 = array_month[6];
                break;
            case "06":
                fixMonth1 = array_month[3];
                fixMonth2 = array_month[4];
                fixMonth3 = array_month[5];
                fixMonth4 = array_month[6];
                fixMonth5 = array_month[7];
                break;
            case "07":
                fixMonth1 = array_month[4];
                fixMonth2 = array_month[5];
                fixMonth3 = array_month[6];
                fixMonth4 = array_month[7];
                fixMonth5 = array_month[8];
                break;
            case "08":
                fixMonth1 = array_month[5];
                fixMonth2 = array_month[6];
                fixMonth3 = array_month[7];
                fixMonth4 = array_month[8];
                fixMonth5 = array_month[9];
                break;
            case "09":
                fixMonth1 = array_month[6];
                fixMonth2 = array_month[7];
                fixMonth3 = array_month[8];
                fixMonth4 = array_month[9];
                fixMonth5 = array_month[10];
                break;
            case "10":
                fixMonth1 = array_month[7];
                fixMonth2 = array_month[8];
                fixMonth3 = array_month[9];
                fixMonth4 = array_month[10];
                fixMonth5 = array_month[11];
                break;
            case "11":
                fixMonth1 = array_month[8];
                fixMonth2 = array_month[9];
                fixMonth3 = array_month[10];
                fixMonth4 = array_month[11];
                fixMonth5 = array_month[0];
                break;
            case "12":
                fixMonth1 = array_month[9];
                fixMonth2 = array_month[10];
                fixMonth3 = array_month[11];
                fixMonth4 = array_month[0];
                fixMonth5 = array_month[1];
                break;
        }

        month1.setText(fixMonth1);
        month2.setText(fixMonth2);
        month3.setText(fixMonth3);
        month4.setText(fixMonth4);
        month5.setText(fixMonth5);

        formatter = new SimpleDateFormat("yyyy", Locale.US);
        String formatYear = formatter.format(date);
        lyHoliday.removeAllViews();

        for (Holiday holiday : list_holiday) {
            String month = holiday.getCLDD_STARTDATE().substring(4, 6);
            String year = holiday.getCLDD_STARTDATE().substring(0, 4);
            if (month.equals(formattedDate) && year.equals(formatYear)) {
                TextView tv = new TextView(getContext());
                tv.setTextColor(Color.RED);
                String ss = "";
                if (Integer.parseInt(holiday.getCLDD_DATECOUNT()) == 1) {
                    ss = holiday.getCLDD_STARTDATE().substring(6, 8) + " " + holiday.getCLDD_DAY_NAMETH();
                } else {
                    ss = holiday.getCLDD_STARTDATE().substring(6, 8) + " - " + holiday.getCLDD_ENDDATE().substring(6, 8) + " " + holiday.getCLDD_DAY_NAMETH();
                }
                Log.i("Holiday", "Holiday: " + ss);

                tv.setText(ss);
                lyHoliday.addView(tv);
            }
        }
    }

    private void clearData() {

        list_history.clear();
        dates.clear();
        holidayDates.clear();
        historyDates.clear();
        widget.clearSelection();
        widget.removeDecorators();
        btnApply.clear();

        countAlert = 1;
        showAlertDialog();
        //Call History
        LMSWSManager.getLeaveHistoryByType(getActivity(), application.getCurProfile().getSTAFF_TOKEN(), leaveTypesRequest, onListenerFromWSManagerHistory);
        //Call Holidays
        if (!statusExtra.equals("1")) {//for Giffware
            LMSWSManager.getHolidays(getActivity(), application.getCurProfile().getSTAFF_TOKEN(), "Thailand", onListenerFromWSManagerHoliday);
        }
    }

    private void callEventHistory(String startH, String endH, String format) {
        List<Date> dates = getDates(startH, endH, format);//check betweendate
        for (Date date : dates) {
            historyDates.add(CalendarDay.from(date));
        }
    }

    private void callEventHoliday(String startH, String endH) {
        List<Date> dates = getDates(startH, endH, "yyyyMMdd");//check betweendate
        for (Date date : dates) {
            holidayDates.add(CalendarDay.from(date));
        }
    }

    private class ShowEventHistory extends AsyncTask<Void, Void, List<CalendarDay>> {
        @Override
        protected List<CalendarDay> doInBackground(Void... voids) {
            return historyDates;
        }

        @Override
        protected void onPostExecute(List<CalendarDay> calendarDays) {
            widget.addDecorator(new MarkDecorator(getActivity(), getResources().getColor(R.color.mark_blue), calendarDays));

        }
    }

    private class ShowEventClickDate extends AsyncTask<Void, Void, List<CalendarDay>> {

        @Override
        protected List<CalendarDay> doInBackground(Void... voids) {
            return dates;
        }

        @Override
        protected void onPostExecute(List<CalendarDay> calendarDays) {
            if (decorator != null) {
                widget.removeDecorator(decorator);
            }
            if (calendarDays.size() > 0) {
                decorator = new ClickDecorator(getActivity(), getResources().getColor(R.color.bg_red), calendarDays, "CLICK");
                widget.addDecorator(decorator);
            }
        }
    }

    private class ShowEventHoliday extends AsyncTask<Void, Void, List<CalendarDay>> {

        @Override
        protected List<CalendarDay> doInBackground(Void... voids) {
            return holidayDates;
        }

        @Override
        protected void onPostExecute(List<CalendarDay> calendarDays) {
            widget.addDecorator(new EventDecorator(getActivity(), getResources().getColor(R.color.bg_red), calendarDays));
        }
    }

    //-------------------------------------------------------------

    private void showAlertDialog() {
        if (dialogWaiting == null)
            dialogWaiting = DialogUtils.createDialog(getActivity());
//                ++countAlert;
        if (!dialogWaiting.isShowing() && isVisibleView) {
            dialogWaiting.show();
        }
    }

    private void dismissDialog() {
        if (dialogWaiting.isShowing() && --countAlert <= 0) {
            dialogWaiting.dismiss();
            countAlert = 0;
        }
    }

    private void getStaffSummary() {
        showAlertDialog();
        LMSWSManager.getStaffSummary(getActivity(), application.getCurProfile().getSTAFF_TOKEN(), onResManager);
    }

    private final class ISO8601 {
        /**
         * Transform Calendar to ISO 8601 string.
         */
        public String fromCalendar(final Calendar calendar) {
            Date date = calendar.getTime();
            String formatted = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ").format(date);
            return formatted.substring(0, 22) + ":" + formatted.substring(22);
        }

        /**
         * Get current date and time formatted as ISO 8601 string.
         */
        public String now() {
            return fromCalendar(GregorianCalendar.getInstance());
        }

        /**
         * Transform ISO 8601 string to Calendar.
         */
        public Calendar toCalendar(final String iso8601string) throws ParseException {
            Calendar calendar = GregorianCalendar.getInstance();
            String s = iso8601string.replace("Z", "+00:00");
            try {
                s = s.substring(0, 22) + s.substring(23);  // to get rid of the ":"
            } catch (IndexOutOfBoundsException e) {
                throw new ParseException("Invalid length", 0);
            }
            Date date = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ").parse(s);
            calendar.setTime(date);
            return calendar;
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        getView().setFocusableInTouchMode(true);
        getView().requestFocus();
        getView().setOnKeyListener(onKeyDown);
    }

    /*back of fragment*/
    private View.OnKeyListener onKeyDown = new View.OnKeyListener() {
        @Override
        public boolean onKey(View v, int keyCode, KeyEvent event) {
            if (isVisibleView) {
                if (KeyEvent.ACTION_UP == event.getAction()) {
                    if (keyCode == KeyEvent.KEYCODE_BACK) {
                        onChangePageView.setOnChangePager(LMSGeneralConstant.FIRST_PAGE, LMSGeneralConstant.HOME_PAGE);
                    }
                }
                return true;
            }
            return false;
        }
    };

    @Override
    public void onBack() {
        if (isVisibleView) {
            onChangePageView.setOnChangePager(LMSGeneralConstant.FIRST_PAGE, LMSGeneralConstant.HOME_PAGE);
        }
    }
}
