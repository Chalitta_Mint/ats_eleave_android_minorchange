package android.mobile.lms.aware.com.atsleave;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.mobile.lms.aware.com.atsleave.constant.SaveSharedPreference;
import android.mobile.lms.aware.com.atsleave.callback.OnListenerFromWSManager;
import android.mobile.lms.aware.com.atsleave.models.BaseResponse;
import android.mobile.lms.aware.com.atsleave.models.ResponseEntity;
import android.mobile.lms.aware.com.atsleave.webservice.LMSWSManager;
import android.os.Bundle;
import android.os.Handler;
import android.view.Window;
import android.view.WindowManager;

import android.content.pm.PackageManager.NameNotFoundException;
import android.widget.TextView;

import com.google.gson.Gson;

public class SplashScreen extends Activity {

    private Handler myHandler;
    private String versionApp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        this.setContentView(R.layout.splash_screen);

        PackageInfo pInfo = null;
        try {
            pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
        } catch (NameNotFoundException e) {
            e.printStackTrace();
        }
        versionApp = pInfo.versionName;
        TextView version_number = (TextView) findViewById(R.id.version_number);
        version_number.setText("Version: " + versionApp);

        checkUpdate();
    }


    private void callPage(final String versionServer, final Boolean forceUpdate) {

        myHandler = new Handler();
        myHandler.postDelayed(new Runnable() {

            @Override
            public void run() {

                float versionAppF = Float.parseFloat(versionApp);
                float versionServerF = Float.parseFloat(versionServer);
                //check_version
                if (versionAppF >= versionServerF) {
                    Intent goSecond = new Intent(getApplicationContext(), LoginActivity.class);
                    startActivityForResult(goSecond, 0);
                    finish();
                    overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                } else {
                    if (forceUpdate) {
                        Intent goSecond = new Intent(getApplicationContext(), UpdateVersionActivity.class);
                        startActivityForResult(goSecond, 0);
                        finish();
                        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);

                    } else {
                        Intent goSecond = new Intent(getApplicationContext(), LoginActivity.class);
                        startActivityForResult(goSecond, 0);
                        finish();
                        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                    }
                }

            }

        }, 3000);
    }


    private void checkUpdate() {

        LMSWSManager.updateVersion(SplashScreen.this, "", "", versionApp, new OnListenerFromWSManager() {
            @Override
            public void onComplete(String statusCode, String message) {

            }

            @Override
            public void onFailed(String statusCode, String message) {

                Gson gson = new Gson();
                BaseResponse badgecount = gson.fromJson(message, BaseResponse.class);
                ResponseEntity resEntity = badgecount.getResponse();

                SaveSharedPreference sharedPreferences = new SaveSharedPreference(getApplicationContext());
                sharedPreferences.saveVersionUpdate("");
                sharedPreferences.saveVersionUpdate(resEntity.getAndroid_version());
                sharedPreferences.saveStatusForceUpdate(resEntity.getAndroid_force_update());

                callPage(resEntity.getAndroid_version(), resEntity.getAndroid_force_update());

            }

            @Override
            public void onFailed(String message) {

            }

            @Override
            public void onFailedAndForceLogout() {

            }
        });

    }
}
