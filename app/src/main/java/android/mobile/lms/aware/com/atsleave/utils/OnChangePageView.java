package android.mobile.lms.aware.com.atsleave.utils;

/**
 * Created by apinun.w on 15/7/2558.
 */
public interface OnChangePageView {
    void setOnChangePager(int page, int position);
}
