package android.mobile.lms.aware.com.atsleave.service;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.media.RingtoneManager;
import android.mobile.lms.aware.com.atsleave.LMSApplication;
import android.mobile.lms.aware.com.atsleave.R;
import android.mobile.lms.aware.com.atsleave.SplashScreen;
import android.mobile.lms.aware.com.atsleave.constant.LMSGeneralConstant;
import android.mobile.lms.aware.com.atsleave.constant.SaveSharedPreference;
import android.net.Uri;
import android.os.Build;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.core.app.NotificationCompat;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

public class AppFirebaseMessagingService extends FirebaseMessagingService {

    @Override
    public void onMessageReceived(@NonNull RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);

        String message = remoteMessage.getData().get("message");
        String badge = remoteMessage.getData().get("badge");
        Log.e("notificationNumber", ":" + badge);
        int int_badge_count = Integer.parseInt(badge);

        Intent pushReceivedIntent = new Intent(LMSGeneralConstant.ACTION);
        pushReceivedIntent.putExtra("message", message);
        if (((LMSApplication) getApplication()).isInterestingActivityVisible()) {
            getApplicationContext().sendBroadcast(pushReceivedIntent);
        } else {
            sendNotification(this, message, int_badge_count);
        }
    }

    private void sendNotification(Context c, String message, int badge) {
        createNotificationChannel();

        Intent intent = new Intent(this, SplashScreen.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_ONE_SHOT);

        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(getApplicationContext(),getString(R.string.channel_id))
                .setSmallIcon(R.drawable.ic_stat_aware)
                .setLargeIcon(BitmapFactory.decodeResource(c.getResources(), R.drawable.icon_app))
                .setContentTitle(getString(R.string.app_name))
                .setContentText(message)
                .setStyle(new NotificationCompat.BigTextStyle().bigText(message))
                .setAutoCancel(true)
                .setPriority(Notification.PRIORITY_HIGH)
                .setNumber(badge)
                .setBadgeIconType(NotificationCompat.BADGE_ICON_SMALL)
                .setSound(defaultSoundUri)
                .setContentIntent(pendingIntent);

        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(NotificationID.getID(), notificationBuilder.build());
    }

    @Override
    public void onNewToken(@NonNull String s) {
        super.onNewToken(s);
        sendRegistrationToServer(s);
    }

    private void sendRegistrationToServer(String token) {
        SaveSharedPreference saveSharedPreference = new SaveSharedPreference(getApplicationContext());
        saveSharedPreference.saveDeviceToken(token);
    }

    private void createNotificationChannel() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            CharSequence name = "ats-eleave";
            int importance = NotificationManager.IMPORTANCE_DEFAULT;
            NotificationChannel channel = new NotificationChannel(getString(R.string.channel_id), name, importance);
            channel.setShowBadge(true);
            NotificationManager notificationManager = getSystemService(NotificationManager.class);
            notificationManager.createNotificationChannel(channel);
        }
    }
}
