package android.mobile.lms.aware.com.atsleave.adapter;

import android.content.Context;
import android.mobile.lms.aware.com.atsleave.R;
import android.mobile.lms.aware.com.atsleave.constant.LMSGeneralConstant;
import android.mobile.lms.aware.com.atsleave.models.TeamLeaveResponse;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

public class ListLeaveHistoryAdapter extends BaseAdapter {
    private String TAG = "ListLeaveHistoryAdapter";
    private Context context;
    private List<Object> leaveTeams;
    private HashMap<Integer, String> listTimeGroup;
    private HashMap<Integer, Integer> listHeaderGroup;
    private SimpleDateFormat inputSDF = new SimpleDateFormat("yyyyMMddHHmm");
    private SimpleDateFormat outputSDF = new SimpleDateFormat("EEE dd MMM yy", Locale.ENGLISH);

    public ListLeaveHistoryAdapter(Context context, List<Object> leaveTeams) {
        this.context = context;
        this.leaveTeams = leaveTeams;
    }

    public void setLeaveTeams(List<Object> leaveTeams, HashMap<Integer, String> listTimeGroup, HashMap<Integer, Integer> listHeaderGroup) {
        this.listHeaderGroup = listHeaderGroup;
        this.leaveTeams = leaveTeams;
        this.listTimeGroup = listTimeGroup;
    }

    @Override
    public int getCount() {
        Log.d(TAG, "leaveTeams size : "  + leaveTeams.size());
        return leaveTeams.size();
    }

    @Override
    public Object getItem(int position) {
        return leaveTeams.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        Holder holder = null;
        if (convertView == null) {

            holder = new Holder();
            convertView = LayoutInflater.from(context).inflate(R.layout.layout_pending_history, parent, false);
            holder.rl_header = (RelativeLayout) convertView.findViewById(R.id.rl_header);
            holder.iv_icon_title = (ImageView) convertView.findViewById(R.id.iv_icon_title);
            holder.tv_title = (TextView) convertView.findViewById(R.id.tv_title);
            holder.tv_value_leave = (TextView) convertView.findViewById(R.id.tv_value_leave);
            holder.img_pending = (ImageView) convertView.findViewById(R.id.img_pending);
            holder.txt_pending_name = (TextView) convertView.findViewById(R.id.txt_pending_name);
            holder.relative_main = (RelativeLayout) convertView.findViewById(R.id.relative_main);
            holder.txt_img_title = (TextView) convertView.findViewById(R.id.txt_img_title);
            holder.linear_main_right = (RelativeLayout) convertView.findViewById(R.id.linear_main_right);
            holder.txt_pending_date = (TextView) convertView.findViewById(R.id.txt_pending_date);
            holder.txt_pending_time = (TextView) convertView.findViewById(R.id.txt_pending_time);
            holder.txt_pending_status = (TextView) convertView.findViewById(R.id.txt_pending_status);
            holder.txt_pending_my_reason = (TextView) convertView.findViewById(R.id.txt_pending_my_reason);
            holder.txt_pending_sup_reson = (TextView) convertView.findViewById(R.id.txt_pending_sup_reson);
            holder.img_arrow_pending = (ImageView) convertView.findViewById(R.id.img_arrow_pending);
            convertView.setTag(holder);

        } else {
            holder = (Holder) convertView.getTag();
        }

        TeamLeaveResponse.DataEntity data = (TeamLeaveResponse.DataEntity) leaveTeams.get(position);

        int leaveType_id = Integer.parseInt(data.getLEAVETYPEID());
        String first_name = data.getNAME_ENG();
        String sur_name = data.getSURNAME_ENG();
        int status_id = Integer.parseInt(data.getRL_STATUSID());

        String startDate = data.getRLD_DATE_START();
        String endDate = data.getRLD_DATE_END();
        Date parse_end_date = new Date();
        Date parse_start_date = new Date();
        Date startDateCompare = null;
        Date endDateCompare = null;

        if(listHeaderGroup.get(leaveType_id) == position){
            holder.rl_header.setVisibility(View.VISIBLE);

            int icon_title = android.mobile.lms.aware.com.atsleave.webservice.LMSGeneralConstant.getIconLeaveTypeById(leaveType_id);
            holder.iv_icon_title.setImageDrawable(context.getResources().getDrawable(icon_title));

            String name_title = android.mobile.lms.aware.com.atsleave.webservice.LMSGeneralConstant.getNameLeaveTypeById(leaveType_id);
            holder.tv_title.setText(name_title);

            holder.tv_value_leave.setText(calulateSickTimeGroup(listTimeGroup.get(leaveType_id)));

        }else{

            holder.rl_header.setVisibility(View.GONE);
        }

        if (startDate != null) {
            try {
                parse_start_date = inputSDF.parse(startDate);
                startDateCompare = inputSDF.parse(startDate);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }

        if (endDate != null) {
            try {
                parse_end_date = inputSDF.parse(endDate);
                endDateCompare = inputSDF.parse(endDate);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }

        String toDateText = "";
        startDateCompare.setHours(0);
        startDateCompare.setMinutes(0);
        endDateCompare.setHours(0);
        endDateCompare.setMinutes(0);
        if (startDateCompare.compareTo(endDateCompare) == 0) {
            toDateText = outputSDF.format(parse_start_date);
        } else {
            toDateText = outputSDF.format(parse_start_date) + " - " + outputSDF.format(parse_end_date);
        }

        String minuteTotal = data.getRLD_ALLTIME();

        holder.txt_pending_date.setText(toDateText);
        holder.txt_pending_time.setText(calulateSickTime(minuteTotal));
        holder.txt_pending_my_reason.setText(data.getREASON());

        if (data.getRL_STATUSID().equalsIgnoreCase(LMSGeneralConstant.LEAVE_STATUS.Pending + "")) {
            holder.txt_pending_sup_reson.setText("");
        } else {
            if (data.getREMARK() != null) {
                if (data.getREMARK().equalsIgnoreCase(""))
                    holder.txt_pending_sup_reson.setText("");
                else
                    holder.txt_pending_sup_reson.setText(data.getREMARK());
            } else {
                holder.txt_pending_sup_reson.setText("");
            }
        }

        //set background
        holder.relative_main.setBackgroundColor(context.getResources().getColor(R.color.bg_gray_alpha2));
        String title_status = context.getString(R.string.title_status);
        holder.txt_pending_status.setText(title_status + "" + getLeaveStatus(Integer.parseInt(data.getRL_STATUSID())));

        String status_type_txt_title = "";

        if (status_id == LMSGeneralConstant.LEAVE_STATUS.PendingCancel) {
            holder.relative_main.setBackgroundColor(context.getResources().getColor(R.color.bg_blur_alpha_pendingcancel));
        }

        int draw_title = android.mobile.lms.aware.com.atsleave.webservice.LMSGeneralConstant.getIconNormalLeaveTypeById(leaveType_id);
        holder.txt_img_title.setText(status_type_txt_title);
        holder.img_pending.setImageDrawable(context.getResources().getDrawable(draw_title));
        holder.txt_pending_name.setText(first_name + " " + sur_name);

        return convertView;
    }

    private String calulateSickTimeGroup(String minuteTotal) {

        String total_time_leave = "";
        int i_minute = Integer.parseInt(minuteTotal);

        int int_date = i_minute / 480;
        float f_hour = (float) (i_minute % 480) / 60.0f;

        Log.e("", "i_minute : " + i_minute);
        if (f_hour == 0) {
            total_time_leave = int_date + "D";
        } else {
            if (int_date == 0) {
                if (f_hour % 1 == 0) {
                    total_time_leave = (int) f_hour + "H";
                } else {
                    total_time_leave = f_hour + "H";
                }
            } else {
                if (f_hour % 1 == 0) {
                    total_time_leave = int_date + "D " + (int) f_hour + "H";
                } else {
                    total_time_leave = int_date + "D " + f_hour + "H";
                }
            }
        }

        return total_time_leave;
    }

    private String calulateSickTime(String minuteTotal) {
        String sickTime = "";
        int i_minute = Integer.parseInt(minuteTotal);
        int int_date = i_minute / 480;
        float f_hour = (float) (i_minute % 480 / 60.0f);

        if (int_date > 0) {
            sickTime += (int) int_date;
            sickTime += (int) int_date <= 1 ? " Day " : " Days ";
        }

        if (f_hour > 0) {
            sickTime += (f_hour % 1 == 0 ? (int) f_hour + "" : f_hour + "") + (f_hour <= 1 ? " Hour " : " Hours ");
        }
        return sickTime;
    }


    private class Holder {
        ImageView img_pending;
        TextView txt_img_title;
        TextView txt_pending_name;
        TextView txt_pending_date;
        TextView txt_pending_time;
        TextView txt_pending_status;
        TextView txt_pending_my_reason;
        TextView txt_pending_sup_reson;
        ImageView img_arrow_pending;
        RelativeLayout linear_main_right;
        RelativeLayout relative_main;
        RelativeLayout rl_header;
        ImageView iv_icon_title;
        TextView tv_title;
        TextView tv_value_leave;

    }

    private String getLeaveStatus(int type) {
        switch (type) {
            case LMSGeneralConstant.LEAVE_STATUS.Pending:
                return "Pending";
            case LMSGeneralConstant.LEAVE_STATUS.Approved:
                return "Approved";
            case LMSGeneralConstant.LEAVE_STATUS.ApprovedCancel:
                return "Approved Cancelation";
            case LMSGeneralConstant.LEAVE_STATUS.Canceled:
                return "";
            case LMSGeneralConstant.LEAVE_STATUS.Rejected:
                return "Rejected";
            case LMSGeneralConstant.LEAVE_STATUS.PendingCancel:
                return "Pending Cancelation";
            case LMSGeneralConstant.LEAVE_STATUS.RejectedCancel:
                return "Rejected Cancelation";
            default:
                return "";
        }
    }
}
