package android.mobile.lms.aware.com.atsleave.models;

import java.util.List;

/**
 * Created by apinun.w on 3/8/2558.
 */
public class StaffSummaryResponse extends BaseResponse {

    private List<DataEntity> data;

    public void setData(List<DataEntity> data) {
        this.data = data;
    }

    public List<DataEntity> getData() {
        return data;
    }

    public static class DataEntity {

        private String LEAVESALUTATION;
        private String LEAVEGENDER;
        private String LEAVEREMARK;
        private String ACTIVETOLIST;
        private String ALLDAYFORLEAVE;
        private String NOTICEREQUEST;
        private String ALLTIMEUSED;
        private String LEAVENAME;
        private String HASDAYOFF;
        private String INCLUDING_SAT_SUN;

        public void setLEAVESALUTATION(String LEAVESALUTATION) {
            this.LEAVESALUTATION = LEAVESALUTATION;
        }

        public void setLEAVEGENDER(String LEAVEGENDER) {
            this.LEAVEGENDER = LEAVEGENDER;
        }

        public void setLEAVEREMARK(String LEAVEREMARK) {
            this.LEAVEREMARK = LEAVEREMARK;
        }

        public void setACTIVETOLIST(String ACTIVETOLIST) {
            this.ACTIVETOLIST = ACTIVETOLIST;
        }

        public void setALLDAYFORLEAVE(String ALLDAYFORLEAVE) {
            this.ALLDAYFORLEAVE = ALLDAYFORLEAVE;
        }

        public void setNOTICEREQUEST(String NOTICEREQUEST) {
            this.NOTICEREQUEST = NOTICEREQUEST;
        }

        public void setALLTIMEUSED(String ALLTIMEUSED) {
            this.ALLTIMEUSED = ALLTIMEUSED;
        }

        public void setLEAVENAME(String LEAVENAME) {
            this.LEAVENAME = LEAVENAME;
        }

        public void setHASDAYOFF(String HASDAYOFF) {
            this.HASDAYOFF = HASDAYOFF;
        }

        public String getLEAVESALUTATION() {
            return LEAVESALUTATION;
        }

        public String getLEAVEGENDER() {
            return LEAVEGENDER;
        }

        public String getLEAVEREMARK() {
            return LEAVEREMARK;
        }

        public String getACTIVETOLIST() {
            return ACTIVETOLIST;
        }

        public String getALLDAYFORLEAVE() {
            return ALLDAYFORLEAVE;
        }

        public String getNOTICEREQUEST() {
            return NOTICEREQUEST;
        }

        public String getALLTIMEUSED() {
            return ALLTIMEUSED;
        }

        public String getLEAVENAME() {
            return LEAVENAME;
        }

        public String getHASDAYOFF() {
            return HASDAYOFF;
        }

        public String getINCLUDING_SAT_SUN() {
            return INCLUDING_SAT_SUN;
        }

    }
}
