package android.mobile.lms.aware.com.atsleave.models;

public class LeaveType {

    private String id;
    private String name;
    private String leaveremark;
    private String salutation;
    private String alldayforleave;
    private String alltimeused;
    private String INCLUDING_SAT_SUN;
    private String NOTICEREQUEST;

    public LeaveType(String id, String name, String leaveremark, String salutation, String alldayforleave, String alltimeused, String INCLUDING_SAT_SUN, String NOTICEREQUEST) {
        // TODO Auto-generated constructor stub
        this.id = id;
        this.name = name;
        this.leaveremark = leaveremark;
        this.salutation = salutation;
        this.alldayforleave = alldayforleave;
        this.alltimeused = alltimeused;
        this.INCLUDING_SAT_SUN = INCLUDING_SAT_SUN;
        this.NOTICEREQUEST = NOTICEREQUEST;
    }

    public String getID() {
        return this.id;
    }

    public String getName() {

        return this.name;
    }

    public String getLeaveremark() {

        return this.leaveremark;
    }

    public String getSalutation() {

        return this.salutation;
    }


    public String getAlldayforleave() {

        return this.alldayforleave;
    }

    public String getAlltimeused() {

        return this.alltimeused;
    }

    public String getINCLUDING_SAT_SUN() {

        return this.INCLUDING_SAT_SUN;
    }

    public String getNOTICEREQUEST() {

        return this.NOTICEREQUEST;
    }

    @Override
    public String toString() {
        return this.name;
    }

}
