package android.mobile.lms.aware.com.atsleave.models;

import java.util.List;

/**
 * Created by apinun.w on 7/9/2558.
 */
public class CheckForDayOffResponse extends BaseResponse {

    private List<DataEntity> data;

    public void setData(List<DataEntity> data) {
        this.data = data;
    }

    public List<DataEntity> getData() {
        return data;
    }

    public static class DataEntity {

        private String RD_TIME;
        private String RD_REQUESTTO;
        private String RD_APPROVER;
        private String RD_STATUSID;
        private String ROW_NUM;
        private String RD_REQUESTBY;
        private String RD_TRANSDATE;
        private String TOTAL_USED;
        private String RD_ID;
        private String RD_REQUESTDATE;
        private String RD_EXPIREDATE;
        private String BALANCE;

        public void setRD_TIME(String RD_TIME) {
            this.RD_TIME = RD_TIME;
        }

        public void setRD_REQUESTTO(String RD_REQUESTTO) {
            this.RD_REQUESTTO = RD_REQUESTTO;
        }

        public void setRD_APPROVER(String RD_APPROVER) {
            this.RD_APPROVER = RD_APPROVER;
        }

        public void setRD_STATUSID(String RD_STATUSID) {
            this.RD_STATUSID = RD_STATUSID;
        }

        public void setROW_NUM(String ROW_NUM) {
            this.ROW_NUM = ROW_NUM;
        }

        public void setRD_REQUESTBY(String RD_REQUESTBY) {
            this.RD_REQUESTBY = RD_REQUESTBY;
        }

        public void setRD_TRANSDATE(String RD_TRANSDATE) {
            this.RD_TRANSDATE = RD_TRANSDATE;
        }

        public void setTOTAL_USED(String TOTAL_USED) {
            this.TOTAL_USED = TOTAL_USED;
        }

        public void setRD_ID(String RD_ID) {
            this.RD_ID = RD_ID;
        }

        public void setRD_REQUESTDATE(String RD_REQUESTDATE) {
            this.RD_REQUESTDATE = RD_REQUESTDATE;
        }

        public void setRD_EXPIREDATE(String RD_EXPIREDATE) {
            this.RD_EXPIREDATE = RD_EXPIREDATE;
        }

        public void setBALANCE(String BALANCE) {
            this.BALANCE = BALANCE;
        }

        public String getRD_TIME() {
            return RD_TIME;
        }

        public String getRD_REQUESTTO() {
            return RD_REQUESTTO;
        }

        public String getRD_APPROVER() {
            return RD_APPROVER;
        }

        public String getRD_STATUSID() {
            return RD_STATUSID;
        }

        public String getROW_NUM() {
            return ROW_NUM;
        }

        public String getRD_REQUESTBY() {
            return RD_REQUESTBY;
        }

        public String getRD_TRANSDATE() {
            return RD_TRANSDATE;
        }

        public String getTOTAL_USED() {
            return TOTAL_USED;
        }

        public String getRD_ID() {
            return RD_ID;
        }

        public String getRD_REQUESTDATE() {
            return RD_REQUESTDATE;
        }

        public String getRD_EXPIREDATE() {
            return RD_EXPIREDATE;
        }

        public String getBALANCE() {
            return BALANCE;
        }
    }
}
