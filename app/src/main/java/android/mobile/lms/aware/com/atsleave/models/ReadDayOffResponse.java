package android.mobile.lms.aware.com.atsleave.models;

import java.util.List;

/**
 * Created by apinun.w on 20/8/2558.
 */
public class ReadDayOffResponse extends BaseResponse {

    private List<DataEntity> data;

    public void setData(List<DataEntity> data) {
        this.data = data;
    }

    public List<DataEntity> getData() {
        return data;
    }

    public static class DataEntity {

        private String RD_REQUESTTO;
        private String RD_APPROVER;
        private String RD_REQUESTBY;
        private String SUP_NAME;
        private String RD_ID;
        private String RD_EXPIREDATE;
        private String RD_TIME;
        private String RD_STATUSID;
        private String DR_STATUSID;
        private String RD_TRANSDATE;
        private String RD_REQUESTDATE;
        private String STAFF_NAME;
        private String DR_STATUSNAME;

        public void setRD_REQUESTTO(String RD_REQUESTTO) {
            this.RD_REQUESTTO = RD_REQUESTTO;
        }

        public void setRD_APPROVER(String RD_APPROVER) {
            this.RD_APPROVER = RD_APPROVER;
        }

        public void setRD_REQUESTBY(String RD_REQUESTBY) {
            this.RD_REQUESTBY = RD_REQUESTBY;
        }

        public void setSUP_NAME(String SUP_NAME) {
            this.SUP_NAME = SUP_NAME;
        }

        public void setRD_ID(String RD_ID) {
            this.RD_ID = RD_ID;
        }

        public void setRD_EXPIREDATE(String RD_EXPIREDATE) {
            this.RD_EXPIREDATE = RD_EXPIREDATE;
        }

        public void setRD_TIME(String RD_TIME) {
            this.RD_TIME = RD_TIME;
        }

        public void setRD_STATUSID(String RD_STATUSID) {
            this.RD_STATUSID = RD_STATUSID;
        }

        public void setDR_STATUSID(String DR_STATUSID) {
            this.DR_STATUSID = DR_STATUSID;
        }

        public void setRD_TRANSDATE(String RD_TRANSDATE) {
            this.RD_TRANSDATE = RD_TRANSDATE;
        }

        public void setRD_REQUESTDATE(String RD_REQUESTDATE) {
            this.RD_REQUESTDATE = RD_REQUESTDATE;
        }

        public void setSTAFF_NAME(String STAFF_NAME) {
            this.STAFF_NAME = STAFF_NAME;
        }

        public void setDR_STATUSNAME(String DR_STATUSNAME) {
            this.DR_STATUSNAME = DR_STATUSNAME;
        }

        public String getRD_REQUESTTO() {
            return RD_REQUESTTO;
        }

        public String getRD_APPROVER() {
            return RD_APPROVER;
        }

        public String getRD_REQUESTBY() {
            return RD_REQUESTBY;
        }

        public String getSUP_NAME() {
            return SUP_NAME;
        }

        public String getRD_ID() {
            return RD_ID;
        }

        public String getRD_EXPIREDATE() {
            return RD_EXPIREDATE;
        }

        public String getRD_TIME() {
            return RD_TIME;
        }

        public String getRD_STATUSID() {
            return RD_STATUSID;
        }

        public String getDR_STATUSID() {
            return DR_STATUSID;
        }

        public String getRD_TRANSDATE() {
            return RD_TRANSDATE;
        }

        public String getRD_REQUESTDATE() {
            return RD_REQUESTDATE;
        }

        public String getSTAFF_NAME() {
            return STAFF_NAME;
        }

        public String getDR_STATUSNAME() {
            return DR_STATUSNAME;
        }
    }
}
