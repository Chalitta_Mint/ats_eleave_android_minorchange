package android.mobile.lms.aware.com.atsleave.service;

import java.util.concurrent.atomic.AtomicInteger;

/**
 * Created by worawit.b on 4/11/2016.
 */
public class NotificationID {
    private final static AtomicInteger c = new AtomicInteger(100);

    public static int getID() {
        return c.incrementAndGet();
    }
}
