package android.mobile.lms.aware.com.atsleave;

import android.content.Intent;
import android.graphics.Typeface;
import android.mobile.lms.aware.com.atsleave.constant.SaveSharedPreference;
import android.mobile.lms.aware.com.atsleave.callback.OnListenerFromWSManager;
import android.mobile.lms.aware.com.atsleave.utils.AlertDialogWaiting;
import android.mobile.lms.aware.com.atsleave.utils.DialogUtils;
import android.mobile.lms.aware.com.atsleave.webservice.LMSGeneralConstant;
import android.mobile.lms.aware.com.atsleave.webservice.LMSWSManager;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class RequestReviewActivity extends CustomActivity {
    private AlertDialogWaiting alertdialog;
    private String TAG = "RequestPageFragment";
    private String token;
    private String startDate;
    private String endDate;
    private String Api_totalTime;
    private String leaveType;
    private String leaveTypeName;
    private String leaveStatus;
    private String reason;
    private String Api_startDate, Api_endDate;
    private String totalDay;
    private String salulation;
    private String totalHour;
    private Typeface font;
    private String status_allday;
    private String noticeDay;
    private String serverTime;
    private String statusToday;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_request_review);
        application = (LMSApplication) getApplication();
        Button btn_submit = (Button) findViewById(R.id.btn_submit);
        Button btn_edit = (Button) findViewById(R.id.btn_edit);
        TextView text_comment = (TextView) findViewById(R.id.text_comment);
        TextView text_detailrequest_leave = (TextView) findViewById(R.id.text_detailrequest_leave);
        TextView text_head_request_leave = (TextView) findViewById(R.id.text_head_request_leave);
        TextView text_notice = (TextView) findViewById(R.id.text_notice);

        alertdialog = new AlertDialogWaiting();
        token = getIntent().getStringExtra("token");//Get
        startDate = getIntent().getStringExtra("startDate");//Get
        endDate = getIntent().getStringExtra("endDate");//Get
        Api_startDate = getIntent().getStringExtra("Api_startDate");//Get
        Api_endDate = getIntent().getStringExtra("Api_endDate");//Get
        Api_totalTime = getIntent().getStringExtra("Api_totalTime");//Get
        leaveType = getIntent().getStringExtra("leaveType");//Get
        leaveTypeName = getIntent().getStringExtra("leaveTypeName");//Get
        leaveStatus = getIntent().getStringExtra("leaveStatus");//Get
        reason = getIntent().getStringExtra("reason");//Get
        salulation = getIntent().getStringExtra("salulation");//Get
        noticeDay = getIntent().getStringExtra("noticeDay");//Get
        serverTime = getIntent().getStringExtra("serverTime");//Get
        statusToday = getIntent().getStringExtra("statusToday");//Get
        font = Typeface.createFromAsset(getAssets(), "HelveticaNeue.ttf");

        text_comment.setTypeface(font);
        btn_submit.setTypeface(font);
        text_comment.setTypeface(font);
        text_detailrequest_leave.setTypeface(font);
        text_head_request_leave.setTypeface(font);

        String emp_role = application.getCurProfile().getEMP_ROLE();

        if (emp_role.equalsIgnoreCase(LMSGeneralConstant.EMP_ROLE.DIRECTOR_TEXT)) {
            leaveStatus = "3";
        } else {
            leaveStatus = "1";
        }

        text_comment.setText("Comments:" + reason);
        String stStart = serverTime;
        serverTime = stStart.substring(0, 10);
        String strTime = stStart.substring(10, 14);

        String stDate = Api_startDate;
        String Year = stDate.substring(0, 4);
        String month = stDate.substring(4, 6);
        String day = stDate.substring(6, 8);
        String startDay = Year + "-" + month + "-" + day;

        int resultDay = checkNotice(serverTime, startDay);

        if (noticeDay != null || !noticeDay.equals("") || !noticeDay.equals("0")) {
            if (resultDay >= Integer.parseInt(noticeDay)) {
                text_notice.setVisibility(View.GONE);
            } else {
                text_notice.setVisibility(View.VISIBLE);
                text_notice.setText(String.format(getString(R.string.text_notice), noticeDay));
            }

        } else if (statusToday.equals("Y") && leaveType.equals("3") && Integer.parseInt(strTime) > 900) {
            text_notice.setVisibility(View.VISIBLE);
            text_notice.setText(getString(R.string.text_leave_today));
        } else {
            text_notice.setVisibility(View.GONE);
        }

        String showTime = "";
        int totalTime = Integer.parseInt(Api_totalTime);

        int IntTotalDay = totalTime / 480;
        float FloatTotalHour = (totalTime % 480.0f) / 60.0f;

        if (IntTotalDay != 0) {
            showTime += IntTotalDay;
            showTime += IntTotalDay <= 1 ? " day " : " days ";
        }

        if (FloatTotalHour > 0) {
            showTime += (FloatTotalHour % 1 == 0 ? (int) FloatTotalHour + "" : FloatTotalHour + "") + (FloatTotalHour <= 1 ? " hour " : " hours ");
        }

        if (leaveTypeName != null || !leaveTypeName.equals("")) {
            text_detailrequest_leave.setText(leaveTypeName + " - " + "total: " + showTime + "\n" + startDate + " - " + endDate);
        }

        btn_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                doRequestLeave(token, Api_startDate, Api_endDate, Api_totalTime, leaveType, leaveStatus, reason);
            }
        });

        btn_edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    public void doRequestLeave(String token, String startDate, String endDate, String totalTime, String leaveType, String leaveStatus, String reason) {
        alertdialog.show(getSupportFragmentManager(), TAG);
        LMSWSManager.requestLeave(this, token, startDate, endDate, totalTime, leaveType, leaveStatus, reason, onResManager);
    }

    OnListenerFromWSManager onResManager = new OnListenerFromWSManager() {
        @Override
        public void onComplete(String statusCode, String message) {
            SaveSharedPreference sharedPreferences = new SaveSharedPreference(getApplicationContext());
            if (leaveType.equalsIgnoreCase("2") && Api_totalTime.equals("480")) {//HBD use
                sharedPreferences.saveHBDStatusRequest(false);
            }
            Intent goSecond = new Intent(getApplicationContext(), ThankActivity.class);
            goSecond.putExtra("salulation", salulation);
            startActivityForResult(goSecond, 0);
            alertdialog.dismiss();
        }

        @Override
        public void onFailed(String statusCode, final String message) {
            DialogUtils.showAlertDialog(RequestReviewActivity.this, message, false);
            alertdialog.dismiss();
        }

        @Override
        public void onFailed(final String message) {
            alertdialog.dismiss();
            DialogUtils.showAlertDialog(RequestReviewActivity.this, message, false);
        }

        @Override
        public void onFailedAndForceLogout() {

        }
    };

    private int checkNotice(String serverTime, String startDate) {
        int numDay = 0;
        List<Date> dates = getDates(serverTime, startDate, "yyyy-MM-dd");//check between date
        for (Date date : dates) {
            numDay++;
        }
        return numDay;
    }

    private static List<Date> getDates(String dateString1, String dateString2, String formats) {
        ArrayList<Date> dates = new ArrayList<Date>();
        DateFormat df1 = new SimpleDateFormat(formats);

        Date date1 = null;
        Date date2 = null;

        try {
            date1 = df1.parse(dateString1);
            date2 = df1.parse(dateString2);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        Calendar cal1 = Calendar.getInstance();
        cal1.setTime(date1);

        Calendar cal2 = Calendar.getInstance();
        cal2.setTime(date2);

        while (!cal1.after(cal2)) {
            dates.add(cal1.getTime());
            cal1.add(Calendar.DATE, 1);
        }
        return dates;
    }

}