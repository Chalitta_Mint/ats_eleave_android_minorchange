package android.mobile.lms.aware.com.atsleave.customview;

import android.content.Context;
import androidx.viewpager.widget.PagerAdapter;
import android.view.View;
import android.view.ViewGroup;

import java.util.Vector;

public class CustomPagerAdapter extends PagerAdapter {

	 private Context mContext;
	 private Vector<View> pages;

	 public CustomPagerAdapter(Context context, Vector<View> pages) {
	  this.mContext=context;
	  this.pages=pages;
	 }

	 @Override
	 public Object instantiateItem(ViewGroup container, int position) {
	  View page = pages.get(position);

	  container.addView(page);

	  return page;


	 }
	@Override
	public int getItemPosition(Object object) {


		return PagerAdapter.POSITION_NONE;
	}
	 @Override
	 public int getCount() {
	  return pages.size();
	 }

	 @Override
	 public boolean isViewFromObject(View view, Object object) {
	  return view.equals(object);
	 }
	 
	 @Override
	 public void destroyItem(ViewGroup container, int position, Object object) {

	  container.removeView((View) object);
	 }

	}