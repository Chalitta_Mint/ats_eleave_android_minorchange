package android.mobile.lms.aware.com.atsleave.models;

import android.content.Context;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by apinun.w on 13/5/2558.
 */
public class LMSServiceModel {
    private Map<String, String> params;
    private String message;
    private String response;
    private String realImagePath;

    public LMSServiceModel(Context context) {
        params = new HashMap<>();
    }

    public LMSServiceModel() {
        params = new HashMap<>();
    }
    public String getRealImagePath() {
        return realImagePath;
    }

    public void setRealImagePath(String realImagePath) {
        this.realImagePath = realImagePath;
    }

    public Map<String, String> getParams() {
        return params;
    }

    public void setParams(Map<String, String> params) {
        this.params = params;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }

    public void addParams(String name, String value) {
        params.put(name, value);
    }
}
