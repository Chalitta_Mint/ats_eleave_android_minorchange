package android.mobile.lms.aware.com.atsleave.models;

public class Holiday {

    private String CLDD_STARTDATE;
    private String CLDD_ENDDATE;
    private String CLDD_DATECOUNT;
    private String CLDD_DESCRIPTION;
    private String CLDD_DAY_NAMETH;

    public Holiday(String CLDD_DATECOUNT, String CLDD_STARTDATE, String CLDD_ENDDATE, String CLDD_DESCRIPTION, String CLDD_DAY_NAMETH) {
        this.CLDD_DATECOUNT = CLDD_DATECOUNT;
        this.CLDD_STARTDATE = CLDD_STARTDATE;
        this.CLDD_ENDDATE = CLDD_ENDDATE;
        this.CLDD_DESCRIPTION = CLDD_DESCRIPTION;
        this.CLDD_DAY_NAMETH = CLDD_DAY_NAMETH;
    }

    public String getCLDD_DATECOUNT() {
        return CLDD_DATECOUNT;
    }

    public String getCLDD_STARTDATE() {
        return CLDD_STARTDATE;
    }

    public String getCLDD_ENDDATE() {
        return CLDD_ENDDATE;
    }

    public String getCLDD_DESCRIPTION() {
        return CLDD_DESCRIPTION;
    }

    public void setCLDD_DESCRIPTION(String CLDD_DESCRIPTION) {
        this.CLDD_DESCRIPTION = CLDD_DESCRIPTION;
    }

    public String getCLDD_DAY_NAMETH() {
        return CLDD_DAY_NAMETH;
    }
}
