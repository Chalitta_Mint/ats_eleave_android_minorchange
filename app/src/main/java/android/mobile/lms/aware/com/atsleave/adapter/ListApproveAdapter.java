package android.mobile.lms.aware.com.atsleave.adapter;

import android.content.Context;
import android.mobile.lms.aware.com.atsleave.R;
import android.mobile.lms.aware.com.atsleave.models.HolidayResponse;
import android.mobile.lms.aware.com.atsleave.models.TeamLeaveResponse;
import android.mobile.lms.aware.com.atsleave.webservice.LMSGeneralConstant;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

/**
 * Created by apinun.w on 17/7/2558.
 */
public class ListApproveAdapter extends BaseAdapter {
    Context context;
    List<Object> leaveTeams;
    String USER_EMP_ROLE;
    public static final int STATUS_PENDING_NORMAL = 111;
    private SimpleDateFormat inputSDF = new SimpleDateFormat("yyyyMMddHHmm", Locale.getDefault());
    private SimpleDateFormat outputSDF = new SimpleDateFormat("EEE dd MMM yy", Locale.ENGLISH);
    List<HolidayResponse.DataEntity> holidays;
    private List<Integer> listType;

    public ListApproveAdapter(Context context, List<Object> leaveTeams, List<Integer> listType, List<HolidayResponse.DataEntity> holidays) {
        this.context = context;
        this.leaveTeams = leaveTeams;
        this.listType = listType;
        this.holidays = holidays;
    }

    public void setListType(List<Integer> listType) {
        this.listType = listType;
        notifyDataSetChanged();
    }

    public void setLeaveTeams(List<Object> leaveTeams) {
        this.leaveTeams = leaveTeams;
        notifyDataSetChanged();
    }

    public void clearList() {
        this.leaveTeams.clear();
        notifyDataSetChanged();
    }

    public String getEMP_ROLE() {
        return USER_EMP_ROLE;
    }

    public void setEMP_ROLE(String EMP_ROLE) {
        this.USER_EMP_ROLE = EMP_ROLE;
    }

    @Override
    public int getCount() {
        return leaveTeams.size();
    }

    @Override
    public Object getItem(int position) {
        return leaveTeams.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        Holder holder = null;
        if (convertView == null) {
            holder = new Holder();
            convertView = LayoutInflater.from(context).inflate(R.layout.layout_approve, parent, false);
            holder.iv_item_approve = (ImageView) convertView.findViewById(R.id.iv_item_approve);
            holder.tv_approve_name = (TextView) convertView.findViewById(R.id.tv_approve_name);
            holder.relative_main = (RelativeLayout) convertView.findViewById(R.id.relative_main);
            holder.tv_approve_title = (TextView) convertView.findViewById(R.id.tv_approve_title);
            holder.linear_main_right = (RelativeLayout) convertView.findViewById(R.id.linear_main_right);
            holder.tv_approve_date = (TextView) convertView.findViewById(R.id.tv_approve_date);
            holder.tv_approve_time = (TextView) convertView.findViewById(R.id.tv_approve_time);
            holder.tv_approve_status = (TextView) convertView.findViewById(R.id.tv_approve_status);
            holder.tv_approve_my_reason = (TextView) convertView.findViewById(R.id.tv_approve_my_reason);
            holder.tv_approve_sup_reson = (TextView) convertView.findViewById(R.id.tv_approve_sup_reson);
            convertView.setTag(holder);
        } else {
            holder = (Holder) convertView.getTag();
        }

        int leaveType_id = 0;
        String first_name = "";
        String nick_name = "";
        String sur_name = "";
        int status_id = LMSGeneralConstant.LEAVE_STATUS.Pending;
        if (listType.get(position) == STATUS_PENDING_NORMAL) {
            TeamLeaveResponse.DataEntity data = (TeamLeaveResponse.DataEntity) leaveTeams.get(position);

            //Log.d(TAG, "data TeamLeaveResponse :  " + new Gson().toJson(data));

            leaveType_id = Integer.parseInt(data.getLEAVETYPEID());
            first_name = data.getNAME_ENG();
            sur_name = data.getSURNAME_ENG();
            nick_name = data.getNICKNAME_ENG();
            status_id = Integer.parseInt(data.getRL_STATUSID());

            String startDate = data.getRLD_DATE_START();
            String endDate = data.getRLD_DATE_END();
            Date parse_end_date = new Date();
            Date parse_start_date = new Date();
            Date startDateCompare = null;
            Date endDateCompare = null;


            if (startDate != null) {
                try {
                    parse_start_date = inputSDF.parse(startDate);
                    startDateCompare = inputSDF.parse(startDate);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }

            if (endDate != null) {
                try {
                    parse_end_date = inputSDF.parse(endDate);
                    endDateCompare = inputSDF.parse(endDate);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }

            String toDateText = "";
            startDateCompare.setHours(0);
            startDateCompare.setMinutes(0);
            endDateCompare.setHours(0);
            endDateCompare.setMinutes(0);
            if (startDateCompare.compareTo(endDateCompare) == 0) {
                toDateText = outputSDF.format(parse_start_date);
            } else {
                toDateText = outputSDF.format(parse_start_date) + " - " + outputSDF.format(parse_end_date);
            }

            String sickTime = "";
            String minuteTotal = data.getRLD_ALLTIME();
            int i_minute = Integer.parseInt(minuteTotal);
            int int_date = i_minute / 480;
            float f_hour = (i_minute % 480 / 60.0f);

            if (int_date > 0) {
                sickTime += (int) int_date;
                sickTime += (int) int_date <= 1 ? " Day " : " Days ";
            }

            if (f_hour > 0) {
                sickTime += (f_hour % 1 == 0 ? (int) f_hour + "" : f_hour + "") + (f_hour <= 1 ? " Hour " : " Hours ");
            }

            holder.tv_approve_date.setText(toDateText);
            holder.tv_approve_time.setText(sickTime);
            holder.tv_approve_my_reason.setText(data.getREASON());

            if (data.getRL_STATUSID().equalsIgnoreCase(LMSGeneralConstant.LEAVE_STATUS.Pending + "")) {
                holder.tv_approve_sup_reson.setText("");
            } else {
                if (data.getREMARK() != null) {
                    if (data.getREMARK().equalsIgnoreCase(""))
                        holder.tv_approve_sup_reson.setText("");
                    else
                        holder.tv_approve_sup_reson.setText(data.getREMARK());
                } else {
                    holder.tv_approve_sup_reson.setText("");
                }

            }

            //set background
            holder.relative_main.setBackgroundColor(context.getResources().getColor(R.color.bg_gray_alpha2));
            String title_status = context.getString(R.string.title_status);
            holder.tv_approve_status.setText(title_status + " " + getLeaveStatus(Integer.parseInt(data.getRL_STATUSID())));
        }

        String status_type_txt_title = "";
        int draw_title = R.drawable.star_small;

        if (status_id == LMSGeneralConstant.LEAVE_STATUS.PendingCancel) {
            holder.relative_main.setBackgroundColor(context.getResources().getColor(R.color.bg_blur_alpha_pendingcancel));
        }

        /*public static final int AnnualLeave = 1;
        public static final int BirthdayLeave = 2;
        public static final int SickLeave = 3;
        public static final int MilitaryLeave = 4;
        public static final int SterilizationLeave = 5;
        public static final int OrdinationLeave = 6;
        public static final int MaternityLeave = 7;
        public static final int PaternityLeave = 8;
        public static final int BereavementLeave = 9;
        public static final int LeaveWithoutPay = 10;
        public static final int DayOff = 11;*/

        if (leaveType_id == LMSGeneralConstant.LEAVE_TYPE.AnnualLeave) {
            status_type_txt_title = "Annual Leave";
            draw_title = R.drawable.palm_small;
        } else if (leaveType_id == LMSGeneralConstant.LEAVE_TYPE.BirthdayLeave) {
            status_type_txt_title = "Birthday Leave";
            draw_title = R.drawable.bday;
        } else if (leaveType_id == LMSGeneralConstant.LEAVE_TYPE.SickLeave) {
            status_type_txt_title = "Sick Leave";
            draw_title = R.drawable.heart_small;
        } else if (leaveType_id == LMSGeneralConstant.LEAVE_TYPE.MilitaryLeave) {
            status_type_txt_title = "Military Leave";
            draw_title = R.drawable.military;
        } else if (leaveType_id == LMSGeneralConstant.LEAVE_TYPE.SterilizationLeave) {
            status_type_txt_title = "Sterilization Leave";
            draw_title = R.drawable.sterilization;
        } else if (leaveType_id == LMSGeneralConstant.LEAVE_TYPE.OrdinationLeave) {
            status_type_txt_title = "Ordination Leave";
            draw_title = R.drawable.ordination;
        } else if (leaveType_id == LMSGeneralConstant.LEAVE_TYPE.MaternityLeave) {
            status_type_txt_title = "Maternity Leave";
            draw_title = R.drawable.maternity;
        } else if (leaveType_id == LMSGeneralConstant.LEAVE_TYPE.PaternityLeave) {
            status_type_txt_title = "Paternity Leave";
            draw_title = R.drawable.paternity;
        } else if (leaveType_id == LMSGeneralConstant.LEAVE_TYPE.BereavementLeave) {
            status_type_txt_title = "Bereavement Leave";
            draw_title = R.drawable.bereavment;
        } else if (leaveType_id == LMSGeneralConstant.LEAVE_TYPE.LeaveWithoutPay) {
            status_type_txt_title = "LeaveWithoutPay";
            draw_title = R.drawable.leave_without_pay;
        } else if (leaveType_id == LMSGeneralConstant.LEAVE_TYPE.DayOff) {
            status_type_txt_title = "DayOff";
            draw_title = R.drawable.star_small;
        }

        holder.tv_approve_title.setText(status_type_txt_title);
        holder.iv_item_approve.setImageDrawable(context.getResources().getDrawable(draw_title));
        holder.tv_approve_name.setText(first_name + " (" + nick_name + ") " + sur_name);

        return convertView;
    }

    private class Holder {
        ImageView iv_item_approve;
        TextView tv_approve_name;
        RelativeLayout relative_main;
        TextView tv_approve_title;
        RelativeLayout linear_main_right;
        TextView tv_approve_date;
        TextView tv_approve_time;
        TextView tv_approve_status;
        TextView tv_approve_my_reason;
        TextView tv_approve_sup_reson;
    }

    @Override
    public int getItemViewType(int position) {
        return listType.get(position);
    }

    private String getLeaveStatus(int type) {
        switch (type) {
            case LMSGeneralConstant.LEAVE_STATUS.Pending:
                return "Pending";
            case LMSGeneralConstant.LEAVE_STATUS.Approved:
                return "Approved";
            case LMSGeneralConstant.LEAVE_STATUS.ApprovedCancel:
                return "Approved Cancelation";
            case LMSGeneralConstant.LEAVE_STATUS.Canceled:
                return "";
            case LMSGeneralConstant.LEAVE_STATUS.Rejected:
                return "Rejected";
            case LMSGeneralConstant.LEAVE_STATUS.PendingCancel:
                return "Pending Cancelation";
            case LMSGeneralConstant.LEAVE_STATUS.RejectedCancel:
                return "Rejected Cancelation";
            default:
                return "";
        }
    }
}

