package android.mobile.lms.aware.com.atsleave.adapter;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import java.util.List;

/**
 * Created by apinun.w on 9/7/2558.
 */
public class FragmentAdapters extends FragmentPagerAdapter {
    protected static final String[] CONTENT = new String[]{"List1", "List2", "List3", "List4",};
    private List<Fragment> fragments;

    public FragmentAdapters(FragmentManager fm, List<Fragment> fragments) {
        super(fm);
        this.fragments = fragments;
    }

    @Override
    public Fragment getItem(int position) {
        return fragments.get(position);
    }

    @Override
    public int getCount() {
        return fragments.size();
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return FragmentAdapters.CONTENT[position % CONTENT.length];
    }
}
