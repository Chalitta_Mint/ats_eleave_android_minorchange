package android.mobile.lms.aware.com.atsleave.utils;

import com.squareup.otto.Bus;

/**
 * Created by worawit.b on 3/3/2016.
 */
public class EventBus {
    private static Bus ourInstance = new Bus();
    public static Bus getInstance() {
        return ourInstance;
    }

    private EventBus() {
    }
}
