package android.mobile.lms.aware.com.atsleave.fragmentview;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.mobile.lms.aware.com.atsleave.FullHistoryActivity;
import android.mobile.lms.aware.com.atsleave.LMSApplication;
import android.mobile.lms.aware.com.atsleave.R;
import android.mobile.lms.aware.com.atsleave.adapter.SickLeaveAdapter;
import android.mobile.lms.aware.com.atsleave.callback.OnListenerFromWSManager;
import android.mobile.lms.aware.com.atsleave.callback.RequestDialogListener;
import android.mobile.lms.aware.com.atsleave.models.HolidayResponse;
import android.mobile.lms.aware.com.atsleave.models.LeaveHistoryResponse;
import android.mobile.lms.aware.com.atsleave.models.StaffSummaryResponse;
import android.mobile.lms.aware.com.atsleave.utils.AwareUtils;
import android.mobile.lms.aware.com.atsleave.utils.DialogUtils;
import android.mobile.lms.aware.com.atsleave.utils.OnChangePageView;
import android.mobile.lms.aware.com.atsleave.webservice.LMSGeneralConstant;
import android.mobile.lms.aware.com.atsleave.webservice.LMSWSManager;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.baoyz.swipemenulistview.SwipeMenu;
import com.baoyz.swipemenulistview.SwipeMenuCreator;
import com.baoyz.swipemenulistview.SwipeMenuItem;
import com.baoyz.swipemenulistview.SwipeMenuListView;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.utils.ColorTemplate;
import com.google.gson.Gson;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

/**
 * Created by apinun.w on 15/7/2558.
 */
public class PersonalHistoryFragment extends Fragment {
    private static final int DetailId = 111;
    private static final int ApproveId = 222;
    private static final int CancelId = 333;
    private OnChangePageView onchangePageView;
    private SwipeMenuListView listView;
    private SickLeaveAdapter sickLeaveAdapter;
    private LMSApplication application;
    private String TAG = "SickLeaveHistoryFragment";
    private Dialog dialogWaiting;
    private ImageView img_home_img;
    //    private ImageView image_bg;
    private RelativeLayout relative_show_null;
    private RelativeLayout relative_main;
    private RelativeLayout linear_second_title;
    private Button btn_full_history;
    private int[] leaveTypesRequest = new int[]{LMSGeneralConstant.LEAVE_TYPE.AnnualLeave};
    private LinearLayout layout_third;
    private LinearLayout linear_fourth;
    private ScrollView scrollview;
    private RelativeLayout linear_first_title;
    private SwipeRefreshLayout swipeView;
    private PieChart mChart1, mChart2;//chart
    private TextView text_dayleft;
    private TextView text_daytaken;
    private static boolean isFirst = true;
    private LinearLayout box_title;
    private TextView text_day_taken;
    private LinearLayout linear_chart;
    private int updatePosition = 0;
    private boolean isVisibleView = true;
    private static int countAlert = 0;
    private static int top_editText = -1;
    private static int bottom_editText = -1;
    private String reason;

    final SwipeMenuCreator creator = new SwipeMenuCreator() {
        @Override
        public void create(SwipeMenu menu) {
            SwipeMenuItem menuItem = null;

            switch (menu.getViewType()) {
                case LMSGeneralConstant.LEAVE_STATUS.Pending:
                    menuItem = createMenuSwapItem("Detail", R.drawable.swipe_icon_detail, R.color.bg_gray, R.color.bg_white);
                    menuItem.setId(DetailId);
                    menu.addMenuItem(menuItem);
                    menuItem = createMenuSwapItem("Cancel", R.drawable.swipe_menu_cancel, R.color.bg_white, R.color.bg_black);
                    menuItem.setId(CancelId);
                    menu.addMenuItem(menuItem);
                    break;
                case LMSGeneralConstant.LEAVE_STATUS.Canceled:
                    menuItem = createMenuSwapItem("Cancel", R.drawable.btn_close, R.color.bg_white, R.color.bg_black);
                    menuItem.setId(CancelId);
                    menu.addMenuItem(menuItem);
                    break;
                case LMSGeneralConstant.LEAVE_STATUS.Approved:
                    menuItem = createMenuSwapItem("Detail", R.drawable.swipe_icon_detail, R.color.bg_gray, R.color.bg_white);
                    menuItem.setId(DetailId);
                    menu.addMenuItem(menuItem);
                    menuItem = createMenuSwapItem("Request to Cancel", R.drawable.swipe_close_white, R.color.bg_black, R.color.bg_white);
                    menuItem.setId(CancelId);
                    menu.addMenuItem(menuItem);
                    break;
                case LMSGeneralConstant.LEAVE_STATUS.Rejected:
                    menuItem = createMenuSwapItem("Detail", R.drawable.swipe_icon_detail, R.color.bg_gray, R.color.bg_white);
                    menuItem.setId(DetailId);
                    menu.addMenuItem(menuItem);
                    break;
                case LMSGeneralConstant.LEAVE_STATUS.PendingCancel:
                    menuItem = createMenuSwapItem("Detail", R.drawable.swipe_icon_detail, R.color.bg_gray, R.color.bg_white);
                    menuItem.setId(DetailId);
                    menu.addMenuItem(menuItem);
                    break;
                case LMSGeneralConstant.LEAVE_STATUS.ApprovedCancel:
                    menuItem = createMenuSwapItem("Detail", R.drawable.swipe_icon_detail, R.color.bg_gray, R.color.bg_white);
                    menuItem.setId(DetailId);
                    menu.addMenuItem(menuItem);
                    break;
                case LMSGeneralConstant.LEAVE_STATUS.RejectedCancel:
                    menuItem = createMenuSwapItem("Detail", R.drawable.swipe_icon_detail, R.color.bg_gray, R.color.bg_white);
                    menuItem.setId(DetailId);
                    menu.addMenuItem(menuItem);
                    break;
            }
        }
    };


    public static PersonalHistoryFragment initPersonalFragment(String fragment, OnChangePageView onchangePageView) {
        PersonalHistoryFragment personalHistoryFragment = new PersonalHistoryFragment();
        personalHistoryFragment.onchangePageView = onchangePageView;
        return personalHistoryFragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        application = (LMSApplication) getActivity().getApplication();
        dialogWaiting = DialogUtils.createDialog(getActivity());
        View view = inflater.inflate(R.layout.layout_personal_history_page, container, false);
        listView = (SwipeMenuListView) view.findViewById(R.id.listview);
        relative_main = (RelativeLayout) view.findViewById(R.id.relative_main);
        linear_first_title = (RelativeLayout) view.findViewById(R.id.linear_first_title);
        layout_third = (LinearLayout) view.findViewById(R.id.layout_third);
        scrollview = (ScrollView) view.findViewById(R.id.scrollview);
        linear_second_title = (RelativeLayout) view.findViewById(R.id.linear_second_title);
        img_home_img = (ImageView) view.findViewById(R.id.img_home_img);
        box_title = (LinearLayout) view.findViewById(R.id.box_title);
//        image_bg = (ImageView) view.findViewById(R.id.image_bg);
        text_day_taken = (TextView) view.findViewById(R.id.textView8);
        swipeView = (SwipeRefreshLayout) view.findViewById(R.id.swipeRefreshLayout);
        btn_full_history = (Button) view.findViewById(R.id.btn_full_history);
        relative_show_null = (RelativeLayout) view.findViewById(R.id.relative_show_null);
        linear_fourth = (LinearLayout) view.findViewById(R.id.linear_fourth);
        text_daytaken = (TextView) view.findViewById(R.id.text_daytaken);
        text_dayleft = (TextView) view.findViewById(R.id.text_dayleft);
        linear_chart = (LinearLayout) view.findViewById(R.id.linear_chart);

        sickLeaveAdapter = new SickLeaveAdapter(getActivity(), new ArrayList<LeaveHistoryResponse.DataEntity>(), new ArrayList<HolidayResponse.DataEntity>());
        listView.setAdapter(sickLeaveAdapter);
        listView.setMenuCreator(creator);


        listView.requestLayout();
        setListViewHeightBasedOnChildren(listView);

        updateBadege();

        relative_show_null.setLayoutParams(new RelativeLayout.LayoutParams(
                application.getWidth_screen(), application.getHeight_screen()
                - linear_first_title.getMeasuredHeight()
                - AwareUtils.getNevigationBar(getActivity())
        ));

        img_home_img.setOnClickListener(onBtnClick);
        btn_full_history.setOnClickListener(onBtnClick);

        listView.setOnItemClickListener(listItemClick);
        listView.setOnTouchListener(listViewOnTouch);
        listView.setOnMenuItemClickListener(onMenuItemClick);
        swipeView.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                swipeView.setRefreshing(true);

                countAlert = 2;
                updatePersonalLeave();
                getStaffSummary();

            }
        });

        btn_full_history.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), FullHistoryActivity.class);
                intent.putExtra("status_type", new int[]{1});
                startActivity(intent);
            }
        });


        //------------------  Pie chart -------------
        mChart1 = (PieChart) view.findViewById(R.id.chart1);
        mChart1.setUsePercentValues(true);
        mChart1.setDescription("");
        // mChart1.setDragDecelerationFrictionCoef(0.95f);
        mChart1.setDrawHoleEnabled(true);
        mChart1.setHoleColorTransparent(true);
        //mChart1.setTransparentCircleColor(getResources().getColor(R.color.bg_white));
        mChart1.setHoleRadius(70f);
        mChart1.setTransparentCircleRadius(61f);
        mChart1.setDrawCenterText(true);
        mChart1.setRotationEnabled(false);
        mChart1.setNoDataText("");
        mChart1.setTransparentCircleRadius(0.1f);
        //mChart1.setRotationAngle(90);
        // mChart1.setCenterText("1 Day off");

        Legend l1 = mChart1.getLegend();
        l1.setEnabled(false);

        scrollview.getViewTreeObserver().addOnScrollChangedListener(new ViewTreeObserver.OnScrollChangedListener() {

            @Override
            public void onScrollChanged() {
//                int scrollY = scrollview.getScrollY();
//                if (scrollY == 0)
//                    swipeView.setEnabled(true);
//                else
//                    swipeView.setEnabled(false);

            }
        });
        //--------------------------------------------------------------------

        return view;
    }

    View.OnClickListener onBtnClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.img_home_img:
//                    onchangePageView.setOnChangePager(LMSGeneralConstant.FIRST_PAGE,LMSGeneralConstant.HOME_PAGE);
                    getActivity().finish();
                    break;
                case R.id.btn_full_history:
                    //go to full history
                    break;
            }
        }
    };

    private SwipeMenuListView.OnMenuItemClickListener onMenuItemClick = new SwipeMenuListView.OnMenuItemClickListener() {
        @Override
        public boolean onMenuItemClick(int position, SwipeMenu menu, int index) {
            switch (menu.getMenuItem(index).getId()) {
                case DetailId:
                    showDetailDialog(position);
                    break;
                case CancelId:
                    changeChildListView(ApproveId, position);
                    break;
            }

            return true;
        }
    };

    private void showDetailDialog(int position) {
        SimpleDateFormat inputSDF = new SimpleDateFormat("yyyyMMddHHmm");                            //201509140830
        SimpleDateFormat outputSDF = new SimpleDateFormat("EEE dd-MMM-yy", Locale.ENGLISH);

        final Dialog boxAddLeave = new Dialog(getActivity());
        boxAddLeave.setCanceledOnTouchOutside(false);
        boxAddLeave.getWindow().setBackgroundDrawable(new ColorDrawable(Color.argb(0, 0, 0, 0)));
        boxAddLeave.requestWindowFeature(Window.FEATURE_NO_TITLE);
        boxAddLeave.setContentView(R.layout.custom_box_sick_history);

        TextView text_sup_comment = (TextView) boxAddLeave.findViewById(R.id.text_supervisor);
        TextView text_leave_type = (TextView) boxAddLeave.findViewById(R.id.text_leave_type);
        TextView text_sent = (TextView) boxAddLeave.findViewById(R.id.text_sent);
        TextView text_comments = (TextView) boxAddLeave.findViewById(R.id.text_comments);
        TextView text_form = (TextView) boxAddLeave.findViewById(R.id.text_form);
        TextView text_to = (TextView) boxAddLeave.findViewById(R.id.text_to);
        TextView text_time_used = (TextView) boxAddLeave.findViewById(R.id.text_time_used);
        RelativeLayout box_btn_comment = (RelativeLayout) boxAddLeave.findViewById(R.id.box_btn_comment);

        //get Data
        List<LeaveHistoryResponse.DataEntity> leaveModels = sickLeaveAdapter.getLeaveModels();

        String startDate = leaveModels.get(position).getRLD_DATE_START();
        String endDate = leaveModels.get(position).getRLD_DATE_END();
        Date parse_end_date = new Date();
        Date parse_start_date = new Date();
        Date parse_send_date = new Date();
        Date startDateCompare = null;
        Date endDateCompate = null;

        if (startDate != null) {
            try {
                parse_start_date = inputSDF.parse(startDate);
                parse_send_date = inputSDF.parse(leaveModels.get(position).getRL_DATE());
                startDateCompare = inputSDF.parse(startDate);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }

        if (endDate != null) {
            try {
                parse_end_date = inputSDF.parse(endDate);
                endDateCompate = inputSDF.parse(endDate);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }

        String sup_comment = "";

        if (leaveModels.get(position).getRL_STATUSID().equalsIgnoreCase(LMSGeneralConstant.LEAVE_STATUS.Pending + "")) {
            sup_comment = "N/A";
        } else {
            if (leaveModels.get(position).getREMARK() != null) {
                if (leaveModels.get(position).getREMARK().equalsIgnoreCase(""))
                    sup_comment = "N/A";
                else
                    sup_comment = leaveModels.get(position).getREMARK();
            } else {
                sup_comment = "N/A";
            }
        }

        text_sup_comment.setText(sup_comment);

        int typeId = -1;
        try {
            typeId = Integer.parseInt(leaveModels.get(position).getLEAVETYPEID());
        } catch (Exception e) {
        }
        text_leave_type.setText(getLeaveType(typeId));
        text_comments.setText(leaveModels.get(position).getREASON());
        text_sent.setText(outputSDF.format(parse_send_date));
        String sickTime = "";
        String minuteTotal = leaveModels.get(position).getRLD_ALLTIME();
        int i_minute = Integer.parseInt(minuteTotal);
        int int_date = i_minute / 480;
        float f_hour = (i_minute % 480.0f) / 60.0f;

        if (int_date > 0) {
            sickTime += int_date;
            sickTime += int_date <= 1 ? " Day " : " Days ";
        }

        if (f_hour > 0) {
            sickTime += (f_hour % 1 == 0 ? (int) f_hour + "" : f_hour + "") + (f_hour <= 1 ? " Hour " : " Hours ");
        }

        SimpleDateFormat outDate = new SimpleDateFormat("EEE dd-MMM-yy HH:mm", Locale.ENGLISH);

        text_form.setText(outDate.format(parse_start_date));
        text_to.setText(outDate.format(parse_end_date));
        text_time_used.setText(sickTime);

        box_btn_comment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boxAddLeave.dismiss();
            }
        });

        boxAddLeave.show();
    }

    private void getStaffSummary() {
        showAlertDialog();
        String token = application.getCurProfile().getSTAFF_TOKEN();
        LMSWSManager.getStaffSummary(getActivity(), token, onResponseStaffSummary);
    }

    private OnListenerFromWSManager onResponseStaffSummary = new OnListenerFromWSManager() {
        @Override
        public void onComplete(String statusCode, String message) {
            dismissDialog();
            dismissRefresh();
            Gson gson = new Gson();
            StaffSummaryResponse staffs = gson.fromJson(message, StaffSummaryResponse.class);
            List<StaffSummaryResponse.DataEntity> dataEntity = staffs.getData();
            String[] timeAllDayForLeave = null;
            for (StaffSummaryResponse.DataEntity data : dataEntity) {
                String leaveName = data.getLEAVENAME();
                String allTimeUsed = data.getALLTIMEUSED();
                String AllDayForLeave = data.getALLDAYFORLEAVE();

                if (allTimeUsed == null) allTimeUsed = "0";
                if (AllDayForLeave == null) AllDayForLeave = "0";


                if (leaveName.equalsIgnoreCase("Annual Leave")) {
                    final int ALLTIMEUSED = Integer.parseInt(allTimeUsed);
                    int ALLDAYFORLEAVE = Integer.parseInt(AllDayForLeave);
                    final int DAYLEFT = ALLDAYFORLEAVE - ALLTIMEUSED;
                    int daysToken = DAYLEFT / 480;
                    final int f_daysToken = ALLTIMEUSED / 480;
                    float hoursToken = (float) (DAYLEFT % 480 / 60.0f);
                    final float daysLeft = DAYLEFT / 480;
                    String textTaken = "";
                    if (hoursToken == 0) {
                        textTaken = daysToken + " days ";
                    } else {
                        if (hoursToken % 1 == 0) {
                            textTaken = daysToken + " days " + (int) hoursToken + " hours";
                        } else {
                            textTaken = daysToken + " days " + hoursToken + " hours";
                        }
                    }
                    final String finalTextTaken = textTaken;

                    text_day_taken.setText(finalTextTaken);
                    setData(2, ALLTIMEUSED, DAYLEFT, null);

                    break;
                }
            }
        }

        @Override
        public void onFailed(String statusCode, String message) {
            dismissDialog();
            dismissRefresh();

            DialogUtils.showAlertDismissofRetry(getActivity(), "", new RequestDialogListener() {
                @Override
                public void responseSubmit() {
                    getStaffSummary();
                }

                @Override
                public void responseCancel() {

                }
            });

            if (swipeView.isRefreshing())
                swipeView.setRefreshing(false);

        }

        @Override
        public void onFailed(String message) {
            dismissDialog();
            dismissRefresh();

            DialogUtils.showAlertDismissofRetry(getActivity(), "", new RequestDialogListener() {
                @Override
                public void responseSubmit() {
                    getStaffSummary();
                }

                @Override
                public void responseCancel() {

                }
            });
        }


        @Override
        public void onFailedAndForceLogout() {
            dismissDialog();
            dismissRefresh();

            if (!AwareUtils.isForceLogout) {
                AwareUtils.forceLogOut(getActivity());
            }
        }
    };

    private String getLeaveType(int leaveType_id) {
        String status_type_txt_title = "";
        int draw_title = R.drawable.star_small;
        if (leaveType_id == LMSGeneralConstant.LEAVE_TYPE.AnnualLeave) {
            status_type_txt_title = "Annual Leave";
            draw_title = R.drawable.palm_small;
        } else if (leaveType_id == LMSGeneralConstant.LEAVE_TYPE.BirthdayLeave) {
            status_type_txt_title = "Birthday Leave";
            draw_title = R.drawable.bday;
        } else if (leaveType_id == LMSGeneralConstant.LEAVE_TYPE.SickLeave) {
            status_type_txt_title = "Sick Leave";
            draw_title = R.drawable.heart_small;
        } else if (leaveType_id == LMSGeneralConstant.LEAVE_TYPE.MilitaryLeave) {
            status_type_txt_title = "Military Leave";
            draw_title = R.drawable.military;
        } else if (leaveType_id == LMSGeneralConstant.LEAVE_TYPE.SterilizationLeave) {
            status_type_txt_title = "Sterilization Leave";
            draw_title = R.drawable.sterilization;
        } else if (leaveType_id == LMSGeneralConstant.LEAVE_TYPE.OrdinationLeave) {
            status_type_txt_title = "Ordination Leave";
            draw_title = R.drawable.ordination;
        } else if (leaveType_id == LMSGeneralConstant.LEAVE_TYPE.MaternityLeave) {
            status_type_txt_title = "Maternity Leave";
            draw_title = R.drawable.maternity;
        } else if (leaveType_id == LMSGeneralConstant.LEAVE_TYPE.PaternityLeave) {
            status_type_txt_title = "Paternity Leave";
            draw_title = R.drawable.paternity;
        } else if (leaveType_id == LMSGeneralConstant.LEAVE_TYPE.BereavementLeave) {
            status_type_txt_title = "Bereavement Leave";
            draw_title = R.drawable.bereavment;
        } else if (leaveType_id == LMSGeneralConstant.LEAVE_TYPE.LeaveWithoutPay) {
            status_type_txt_title = "LeaveWithoutPay";
            draw_title = R.drawable.leave_without_pay;
        } else if (leaveType_id == LMSGeneralConstant.LEAVE_TYPE.DayOff) {
            status_type_txt_title = "DayOff";
            draw_title = R.drawable.star_small;
        }

        return status_type_txt_title;
    }

    private void updateStatus(int position, String comment) {
        String toChangeStatus = "";
        int RL_StatusId = Integer.parseInt(sickLeaveAdapter.getLeaveModels().get(position).getRL_STATUSID());
        if (!application.getCurProfile().getEMP_ROLE().equalsIgnoreCase(LMSGeneralConstant.EMP_ROLE.DIRECTOR_TEXT)) {
            if (RL_StatusId == LMSGeneralConstant.LEAVE_STATUS.Pending) {               //Pending(1) -> Cancel(2)
                toChangeStatus = "2";
            } else if (RL_StatusId == LMSGeneralConstant.LEAVE_STATUS.Approved) {       //Approve(3) -> Pending Cancel(5)
                toChangeStatus = "5";
            }
        } else {
            if (RL_StatusId == LMSGeneralConstant.LEAVE_STATUS.Pending) {               //Pending(1) -> Cancel(2)
                toChangeStatus = "2";
            } else if (RL_StatusId == LMSGeneralConstant.LEAVE_STATUS.Approved) {       //Directer auto approve to cancel(6)
                toChangeStatus = "6";                                                   //Approve -> Approve Cancel
            }
        }

        LMSWSManager.updateLeaveStatus(getActivity(), application.getCurProfile().getSTAFF_TOKEN(),
                sickLeaveAdapter.getLeaveModels().get(position).getRL_ID(),
                toChangeStatus,
                comment,
                onResponseUpdateStatus
        );
    }

    //------------------- chart ---------------------------------------------
    //setData(2, ALLTIMEUSED,DAYTAKEN,null);
    private void setData(int count, float ALLTIMEUSED, float DAYLEFT, String number_set) {
        //set Piechar
        float startValue = 0;
        float endValue = 0;

        if (ALLTIMEUSED != 0) {
            startValue = (float) (ALLTIMEUSED * 100) / DAYLEFT;
            endValue = (float) 100 - startValue;
        } else {

            startValue = 100;
            endValue = 0;
        }

        ArrayList<Entry> yVals1 = new ArrayList<Entry>();
        yVals1.add(new Entry((float) DAYLEFT, 0));
        yVals1.add(new Entry((float) ALLTIMEUSED, 1));

        ArrayList<String> xVals = new ArrayList<String>();

        for (int i = 0; i < count + 1; i++)
            //xVals.add(mParties[i % mParties.length]);
            xVals.add("");
        PieDataSet dataSet = new PieDataSet(yVals1, "");
        dataSet.setSliceSpace(0f);
        dataSet.setSelectionShift(5f);
        ArrayList<Integer> colors = new ArrayList<Integer>();

        for (int c : ColorTemplate.CUSTOM_COLORS_LEAVE)
            colors.add(c);
        dataSet.setColors(colors);
        PieData data = new PieData(xVals, dataSet);
        data.setDrawValues(false);
        mChart1.setData(data);
        mChart1.invalidate();

        //set TextShow

        if (ALLTIMEUSED == 0 && DAYLEFT == 0) {
            linear_chart.setVisibility(View.GONE);
        } else {
            linear_chart.setVisibility(View.VISIBLE);
        }

        int dayUsed = (int) (ALLTIMEUSED / 480);
        float hourUsed = (float) (ALLTIMEUSED % 480.0f) / 60.0f;

        int dayTaken = (int) (DAYLEFT / 480);
        float hourTaken = (float) (DAYLEFT % 480.0f) / 60.0f;

        String str_dayleft = "";
        String str_dayUsed = "";
        if (hourUsed % 1 == 0) {
            str_dayUsed += dayUsed + " days ";
            str_dayUsed += hourUsed == 0 ? "" : (int) hourUsed + " hours";
        } else {
            str_dayUsed += dayUsed + " days ";
            str_dayUsed += hourUsed + " hours";
        }
        if (hourTaken % 1 == 0) {
            str_dayleft += (dayTaken == 0) ? "" : dayTaken + " days ";
            str_dayleft += hourTaken == 0 ? "" : (dayTaken != 0) ? Math.abs((int) hourTaken) + " hours" : hourTaken + " hours";
        } else {
            str_dayleft += (dayTaken == 0) ? "" : dayTaken + " days ";
            str_dayleft += (dayTaken != 0) ? Math.abs(hourTaken) + " hours" : hourTaken + " hours";
        }

        text_dayleft.setText(str_dayleft);
        text_daytaken.setText(str_dayUsed);

    }
    //----------------------------------------------------------------

    private OnListenerFromWSManager onResponseUpdateStatus = new OnListenerFromWSManager() {
        @Override
        public void onComplete(String statusCode, String message) {
            dismissDialog();
            dismissRefresh();

            AwareUtils.keyboardHide(getActivity());

            countAlert = 2;
            updatePersonalLeave();
            getStaffSummary();
        }

        @Override

        public void onFailed(String statusCode, final String message) {
            dismissDialog();

            if (swipeView.isRefreshing())
                swipeView.setRefreshing(false);
            else {
                DialogUtils.showAlertDismissofRetry(getActivity(), "", new RequestDialogListener() {
                    @Override
                    public void responseSubmit() {
                        updateStatus(updatePosition, reason);
                    }

                    @Override
                    public void responseCancel() {

                    }
                });
            }
        }

        @Override
        public void onFailed(final String message) {
            dismissDialog();

            if (swipeView.isRefreshing())
                swipeView.setRefreshing(false);
            else {
                DialogUtils.showAlertDismissofRetry(getActivity(), "", new RequestDialogListener() {
                    @Override
                    public void responseSubmit() {
                        updateStatus(updatePosition, reason);
                    }

                    @Override
                    public void responseCancel() {

                    }
                });
            }

        }

        @Override
        public void onFailedAndForceLogout() {
            dismissDialog();
            dismissRefresh();
            if (!AwareUtils.isForceLogout) {
                AwareUtils.forceLogOut(getActivity());
            }
        }
    };

    View.OnTouchListener listViewOnTouch = new View.OnTouchListener() {
        @Override
        public boolean onTouch(View v, MotionEvent event) {
            if (event.getAction() == MotionEvent.ACTION_DOWN) {
                listView.getParent().requestDisallowInterceptTouchEvent(false);
            }
            return false;
        }
    };

    OnListenerFromWSManager onListenerFromWSManager = new OnListenerFromWSManager() {
        @Override
        public void onComplete(String statusCode, String message) {
            dismissDialog();
            dismissRefresh();
            Gson gson = new Gson();
            LeaveHistoryResponse leaveHistoryFromWS = gson.fromJson(message, LeaveHistoryResponse.class);
            List<LeaveHistoryResponse.DataEntity> models = leaveHistoryFromWS.getData();

            final SimpleDateFormat outFormat = new SimpleDateFormat("yyyyMMddHHss");        //201510290830

//            //sort data from start date
//            Collections.sort(models, new Comparator<LeaveHistoryResponse.DataEntity>() {
//                @Override
//                public int compare(LeaveHistoryResponse.DataEntity lhs, LeaveHistoryResponse.DataEntity rhs) {
//                    Date lhsDate = null;
//                    Date rhsDate = null;
//                    try {
//                        lhsDate = outFormat.parse(lhs.getRLD_DATE_START());
//                        rhsDate = outFormat.parse(rhs.getRLD_DATE_START());
//                    } catch (ParseException e) {
//                        e.printStackTrace();
//                    }
//
//                    if (lhsDate == null || rhsDate == null)
//                        return 0;
//
//                    return lhsDate.compareTo(rhsDate);
//                }
//            });

            try {
                setLookView(models);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        public void onFailed(String statusCode, String message) {
            dismissDialog();

            if (swipeView.isRefreshing())
                swipeView.setRefreshing(false);
            else {
                DialogUtils.showAlertDismissofRetry(getActivity(), "", new RequestDialogListener() {
                    @Override
                    public void responseSubmit() {
                        updatePersonalLeave();
                    }

                    @Override
                    public void responseCancel() {

                    }
                });
            }
        }

        @Override
        public void onFailed(String message) {
            dismissDialog();

            if (swipeView.isRefreshing())
                swipeView.setRefreshing(false);
            else {
                DialogUtils.showAlertDismissofRetry(getActivity(), "", new RequestDialogListener() {
                    @Override
                    public void responseSubmit() {
                        updatePersonalLeave();
                    }

                    @Override
                    public void responseCancel() {

                    }
                });
            }
        }

        @Override
        public void onFailedAndForceLogout() {
            dismissDialog();
            dismissRefresh();
            if (!AwareUtils.isForceLogout) {
                AwareUtils.forceLogOut(getActivity());
            }
        }
    };

    //open -> approve or deny button
    //close -> cancel
    //change child view in adapter view
    private void changeChildListView(final int id, final int position) {
        updatePosition = position;
        int visiblePosition = listView.getFirstVisiblePosition();
        View view = listView.getChildAt(position - visiblePosition);
        listView.getAdapter().getView(position, view, listView);
        RelativeLayout linear_main = (RelativeLayout) view.findViewById(R.id.linear_main);
        LinearLayout linear_sub_main = (LinearLayout) view.findViewById(R.id.linear_sub_main);

        LinearLayout cancel = (LinearLayout) view.findViewById(R.id.btn_cancel);
        LinearLayout btn_confirm = (LinearLayout) view.findViewById(R.id.btn_approve);
        ImageView img_approve = (ImageView) view.findViewById(R.id.img_approve);
        final TextView title_text_confirm = (TextView) view.findViewById(R.id.title_text_confirm);
        final EditText editComment = (EditText) view.findViewById(R.id.edit_comment);

        View listItem = sickLeaveAdapter.getView(position, null, listView);
        listItem.measure(0, 0);
        int m_height = view.getHeight();
        int s_height = linear_sub_main.getHeight();
        if (top_editText == -1)
            top_editText = (int) AwareUtils.dpAsPixel(getActivity(), (float) ((m_height - s_height) / 2.0f) + editComment.getPaddingBottom());

        if (bottom_editText == -1)
            bottom_editText = (int) AwareUtils.dpAsPixel(getActivity(), (float) ((m_height - s_height) / 2.0f) + editComment.getPaddingTop());

        editComment.setHeight(m_height);

        if (id == CancelId) {
            btn_confirm.setBackgroundColor(getResources().getColor(R.color.bg_red));
            img_approve.setImageDrawable(getResources().getDrawable(R.drawable.denied));
            title_text_confirm.setText("Deny");
        }

        //case opened
        if (linear_sub_main.getVisibility() == View.VISIBLE) {
            linear_sub_main.setVisibility(View.GONE);
            linear_main.setVisibility(View.VISIBLE);
        } else {
            linear_sub_main.setVisibility(View.VISIBLE);
            linear_main.setVisibility(View.INVISIBLE);
            AwareUtils.keyboardHide(getActivity());
        }

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                changeChildListView(id, position);
            }
        });

        btn_confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String commentText = editComment.getText().toString();
                if (id == ApproveId) {
                    if (!commentText.trim().equalsIgnoreCase("")) {
                        AwareUtils.keyboardHide(getActivity());
                        reason = commentText;
                        updateStatus(position, commentText);
                    } else {
                        Toast.makeText(getActivity(), "no comment in edit text", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });

    }

    public void setLookView(final List<LeaveHistoryResponse.DataEntity> models) throws Exception {
        if (models == null)
            return;

        for (int i = 0; i < models.size(); i++) {
            LeaveHistoryResponse.DataEntity leaveModel = models.get(i);
            int status_id = Integer.parseInt(leaveModel.getRL_STATUSID());
            if (status_id == LMSGeneralConstant.LEAVE_STATUS.Canceled) {
                models.remove(i);
                i--;
            }
        }

        if (models.size() == 0) {
            listView.setVisibility(View.GONE);
            linear_fourth.setVisibility(View.GONE);
            relative_show_null.setVisibility(View.VISIBLE);
            box_title.setVisibility(View.VISIBLE);
            text_daytaken.setText("0");
        } else {
            listView.setVisibility(View.VISIBLE);
            linear_second_title.setVisibility(View.VISIBLE);
            box_title.setVisibility(View.VISIBLE);
            linear_fourth.setVisibility(View.VISIBLE);
            relative_show_null.setVisibility(View.GONE);

            if (models.size() > 5) {
                btn_full_history.setVisibility(View.VISIBLE);
            } else {
                btn_full_history.setVisibility(View.GONE);
            }
        }

        sickLeaveAdapter.setLeaveModels(models);
        listView.setAdapter(sickLeaveAdapter);
        sickLeaveAdapter.notifyDataSetChanged();
        setListViewHeightBasedOnChildren2(listView);

    }

    private OnListenerFromWSManager onResponseBadgeUpdate = new OnListenerFromWSManager() {
        @Override
        public void onComplete(String statusCode, String message) {
            dismissDialog();
            dismissRefresh();
            application.setIntAnnualShowBadge(0);
        }

        @Override
        public void onFailed(String statusCode, String message) {
            dismissDialog();
            dismissRefresh();
        }

        @Override
        public void onFailed(String message) {
            dismissDialog();
            dismissRefresh();
        }

        @Override
        public void onFailedAndForceLogout() {
            dismissDialog();
            dismissRefresh();
            if (!AwareUtils.isForceLogout) {
                AwareUtils.forceLogOut(getActivity());
            }
        }
    };

    private void updatePersonalLeave() {
        showAlertDialog();
        leaveTypesRequest = new int[]{LMSGeneralConstant.LEAVE_TYPE.AnnualLeave};
        LMSWSManager.getLeaveHistoryByType(getActivity(), application.getCurProfile().getSTAFF_TOKEN(), leaveTypesRequest, onListenerFromWSManager);
    }

    private SwipeMenuItem createMenuSwapItem(String title, int drawable, int color, int txt_color) {
        SwipeMenuItem itemMenu = new SwipeMenuItem(getActivity());
        itemMenu.setBackground(new ColorDrawable(getResources().getColor(color)));
        // set item widthe
        itemMenu.setWidth(AwareUtils.dp2px(getActivity(), 90));
        // set a icon
        itemMenu.setIcon(drawable);
        itemMenu.setTitle(title);
        itemMenu.setTitleSize(12);
        itemMenu.setTitleColor(getResources().getColor(txt_color));

        return itemMenu;
    }

    public static void setListViewHeightBasedOnChildren(ListView listView) {
        ListAdapter listAdapter = listView.getAdapter();
        if (listAdapter == null)
            return;

        int desiredWidth = View.MeasureSpec.makeMeasureSpec(listView.getWidth(), View.MeasureSpec.UNSPECIFIED);
        int totalHeight = 0;
        View view = null;
        for (int i = 0; i < listAdapter.getCount(); i++) {
            view = listAdapter.getView(i, view, listView);
            if (i == 0)
                view.setLayoutParams(new ViewGroup.LayoutParams(desiredWidth, LinearLayout.LayoutParams.WRAP_CONTENT));

            view.measure(desiredWidth, View.MeasureSpec.UNSPECIFIED);
            totalHeight += view.getMeasuredHeight();
        }
        ViewGroup.LayoutParams params = listView.getLayoutParams();
        params.height = totalHeight + (listView.getDividerHeight() * (listAdapter.getCount()));
        listView.setLayoutParams(params);
        listView.requestLayout();
    }

    public static void setListViewHeightBasedOnChildren2(ListView listView) {

        ListAdapter listAdapter = listView.getAdapter();
        if (listAdapter == null) {
            return;
        }

        int totalHeight = listView.getPaddingTop() + listView.getPaddingBottom();
        int desiredWidth = View.MeasureSpec.makeMeasureSpec(listView.getWidth(), View.MeasureSpec.AT_MOST);
        for (int i = 0; i < listAdapter.getCount(); i++) {
            View listItem = listAdapter.getView(i, null, listView);

            if (listItem != null) {
                // This next line is needed before you call measure or else you won't get measured height at all. The listitem needs to be drawn first to know the height.
                listItem.setLayoutParams(new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT));
                listItem.measure(desiredWidth, View.MeasureSpec.UNSPECIFIED);
                int hh = listItem.getMeasuredHeight();
                totalHeight += listItem.getMeasuredHeight();

            }
        }

        ViewGroup.LayoutParams params = listView.getLayoutParams();
        params.height = totalHeight + (listView.getDividerHeight() * (listAdapter.getCount() - 1));
        listView.setLayoutParams(params);
        listView.requestLayout();
    }

    AdapterView.OnItemClickListener listItemClick = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            listView.setTag(position);
            listView.smoothOpenMenu(position);
        }
    };

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
//        super.setUserVisibleHint(isVisibleToUser);
        if (this.isVisible()) {
            if (!isVisibleToUser) {
                Log.e("", "isVisibleToUser : " + isVisibleToUser);
                isVisibleView = false;
            } else {
                isVisibleView = true;
                if (isFirst) {
                    isFirst = false;
                }
            }
        }
    }

    private void updateBadege() {
        String token = application.getCurProfile().getSTAFF_TOKEN();
        String staff_id = application.getCurProfile().getSTF_ID();
        LMSWSManager.badgeCountUpdate(getActivity(), token, staff_id, "ANNUAL", "0", onResponseBadgeUpdate);
    }

    private void showAlertDialog() {
        if (dialogWaiting != null) {
            if (!dialogWaiting.isShowing() && isVisibleView) {
                dialogWaiting.show();
            }
        }
    }

    private void dismissDialog() {
        if (dialogWaiting != null) {
            if (dialogWaiting.isShowing() && --countAlert <= 0) {
                dialogWaiting.dismiss();
                countAlert = 0;
            }
        }
    }

    private void dismissRefresh() {
        if (swipeView != null) {
            if (swipeView.isRefreshing())
                swipeView.setRefreshing(false);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        countAlert = 2;
        updatePersonalLeave();
        getStaffSummary();
    }
}
