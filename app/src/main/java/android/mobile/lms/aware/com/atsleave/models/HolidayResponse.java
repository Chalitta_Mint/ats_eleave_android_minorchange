package android.mobile.lms.aware.com.atsleave.models;

import java.util.List;

/**
 * Created by apinun.w on 18/8/2558.
 */
public class HolidayResponse extends BaseResponse {

    private List<DataEntity> data;

    public void setData(List<DataEntity> data) {
        this.data = data;
    }

    public List<DataEntity> getData() {
        return data;
    }

    public static class DataEntity {

        private String CLDD_ID;
        private String CREATEDATE;
        private String CLDD_STARTDATE;
        private String CLDD_DATECOUNT;
        private String CREATEBY;
        private String CLDD_DAY_NAMETH;
        private String CLD_YEAR;
        private String CLD_COUNTRY;
        private String UPDATEDATE;
        private String CLD_USEFOREXTRA;
        private String UPDATEBY;
        private String CLDD_DESCRIPTION;
        private String CLDD_ENDDATE;

        public void setCLDD_ID(String CLDD_ID) {
            this.CLDD_ID = CLDD_ID;
        }

        public void setCREATEDATE(String CREATEDATE) {
            this.CREATEDATE = CREATEDATE;
        }

        public void setCLDD_STARTDATE(String CLDD_STARTDATE) {
            this.CLDD_STARTDATE = CLDD_STARTDATE;
        }

        public void setCLDD_DATECOUNT(String CLDD_DATECOUNT) {
            this.CLDD_DATECOUNT = CLDD_DATECOUNT;
        }

        public void setCREATEBY(String CREATEBY) {
            this.CREATEBY = CREATEBY;
        }

        public void setCLDD_DAY_NAMETH(String CLDD_DAY_NAMETH) {
            this.CLDD_DAY_NAMETH = CLDD_DAY_NAMETH;
        }

        public void setCLD_YEAR(String CLD_YEAR) {
            this.CLD_YEAR = CLD_YEAR;
        }

        public void setCLD_COUNTRY(String CLD_COUNTRY) {
            this.CLD_COUNTRY = CLD_COUNTRY;
        }

        public void setUPDATEDATE(String UPDATEDATE) {
            this.UPDATEDATE = UPDATEDATE;
        }

        public void setCLD_USEFOREXTRA(String CLD_USEFOREXTRA) {
            this.CLD_USEFOREXTRA = CLD_USEFOREXTRA;
        }

        public void setUPDATEBY(String UPDATEBY) {
            this.UPDATEBY = UPDATEBY;
        }

        public void setCLDD_DESCRIPTION(String CLDD_DESCRIPTION) {
            this.CLDD_DESCRIPTION = CLDD_DESCRIPTION;
        }

        public void setCLDD_ENDDATE(String CLDD_ENDDATE) {
            this.CLDD_ENDDATE = CLDD_ENDDATE;
        }

        public String getCLDD_ID() {
            return CLDD_ID;
        }

        public String getCREATEDATE() {
            return CREATEDATE;
        }

        public String getCLDD_STARTDATE() {
            return CLDD_STARTDATE;
        }

        public String getCLDD_DATECOUNT() {
            return CLDD_DATECOUNT;
        }

        public String getCREATEBY() {
            return CREATEBY;
        }

        public String getCLDD_DAY_NAMETH() {
            return CLDD_DAY_NAMETH;
        }

        public String getCLD_YEAR() {
            return CLD_YEAR;
        }

        public String getCLD_COUNTRY() {
            return CLD_COUNTRY;
        }

        public String getUPDATEDATE() {
            return UPDATEDATE;
        }

        public String getCLD_USEFOREXTRA() {
            return CLD_USEFOREXTRA;
        }

        public String getUPDATEBY() {
            return UPDATEBY;
        }

        public String getCLDD_DESCRIPTION() {
            return CLDD_DESCRIPTION;
        }

        public String getCLDD_ENDDATE() {
            return CLDD_ENDDATE;
        }
    }
}
