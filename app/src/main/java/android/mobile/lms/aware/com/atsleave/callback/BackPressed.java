package android.mobile.lms.aware.com.atsleave.callback;

/**
 * Created by apinun.w on 3/9/2558.
 */
public interface BackPressed {
    void onBack();
}
