package android.mobile.lms.aware.com.atsleave.webservice;

import android.app.Activity;
import android.mobile.lms.aware.com.atsleave.LMSApplication;
import android.mobile.lms.aware.com.atsleave.callback.OnListenerFromWS;
import android.mobile.lms.aware.com.atsleave.callback.OnListenerFromWSManager;
import android.mobile.lms.aware.com.atsleave.constant.RequestMethod;
import android.mobile.lms.aware.com.atsleave.constant.SaveSharedPreference;
import android.mobile.lms.aware.com.atsleave.models.BadgeResponse;
import android.mobile.lms.aware.com.atsleave.models.BaseResponse;
import android.mobile.lms.aware.com.atsleave.models.LMSServiceModel;
import android.mobile.lms.aware.com.atsleave.models.LoginResponse;
import android.mobile.lms.aware.com.atsleave.models.ResponseEntity;
import android.mobile.lms.aware.com.atsleave.utils.AwareUtils;
import android.mobile.lms.aware.com.atsleave.utils.LogUtils;

import com.google.gson.Gson;

/**
 * Created by apinun.w on 14/5/2558.
 */

public class LMSWSManager {
    public static int countWS = 0;

    public static void doLogin(final Activity context, String username, String password, String tokenDevice, final OnListenerFromWSManager onResponse) {
        final LMSServiceModel model = new LMSServiceModel(context);
        model.addParams("requestType", "Login");
        model.addParams("deviceToken", tokenDevice);
        model.addParams("deviceType", "android");
        model.addParams("username", username);
        model.addParams("password", password);

        LogUtils.showLog(context.getClass().getName(), "Login", model.getParams().toString());
        countWS++;

        request(context, RequestMethod.POST, model, onResponse);
    }

    public static void doLogin(final Activity context, String username, String deviceToken, final OnListenerFromWSManager onResponse) {
        final LMSServiceModel model = new LMSServiceModel(context);
        model.addParams("requestType", "LoginPassAD");
        model.addParams("deviceToken", deviceToken);
        model.addParams("deviceType", "android");
        model.addParams("username", username);

        LogUtils.showLog(context.getClass().getName(), "Login", model.getParams().toString());
        countWS++;

        request(context, RequestMethod.POST, model, onResponse);
    }

    public static void doLogout(final Activity context, String token, final OnListenerFromWSManager onResponse) {
        final LMSServiceModel model = new LMSServiceModel(context);
        model.addParams("requestType", "Logout");
        model.addParams("deviceType", "android");
        model.addParams("token", token);

        LogUtils.showLog(context.getClass().getName(), "Logout", model.getParams().toString());
        countWS++;

        request(context, RequestMethod.POST, model, onResponse);
    }

    public static void getProfile(final Activity context, String token, final OnListenerFromWSManager onResponse) {
        final LMSServiceModel model = new LMSServiceModel(context);
        model.addParams("requestType", "GetProfile");
        model.addParams("year", AwareUtils.getCurrentYearRequest());
        model.addParams("token", token);
        model.addParams("deviceType", "android");

        LogUtils.showLog(context.getClass().getName(), "GetProfile", model.getParams().toString());
        countWS++;

        request(context, RequestMethod.POST, model, onResponse);
    }

    public static void requestDayOff(final Activity context, String token, String staffID, String requestID, String statusID, String totalTime, String remark, final OnListenerFromWSManager onResponse) {
        final LMSServiceModel model = new LMSServiceModel(context);
        model.addParams("requestType", "requestDayOff");
        model.addParams("staffID", staffID);
        model.addParams("statusID", statusID);
        model.addParams("totalTime", totalTime);
        model.addParams("comments", remark);
        model.addParams("token", token);
        model.addParams("deviceType", "android");

        LogUtils.showLog(context.getClass().getName(), "requestDayOff", model.getParams().toString());
        countWS++;

        request(context, RequestMethod.POST, model, onResponse);
    }

    public static void readDayOff(final Activity context, String token, String staffID, String requestID, String statusID, final OnListenerFromWSManager onResponse) {
        final LMSServiceModel model = new LMSServiceModel(context);
        model.addParams("requestType", "readDayOff");
        model.addParams("staffID", staffID);
        model.addParams("requestID", requestID);
        model.addParams("statusID", statusID);
        model.addParams("token", token);
        model.addParams("deviceType", "android");

        LogUtils.showLog(context.getClass().getName(), "readDayOff", model.getParams().toString());
        countWS++;

        request(context, RequestMethod.POST, model, onResponse);
    }


    public static void getTeamLeave(final Activity context, String token, String staffID, String leaveID, String leaveType, String leaveStatus, String requestDate, final OnListenerFromWSManager onResponse) {
        final LMSServiceModel model = new LMSServiceModel(context);
        model.addParams("requestType", "teamLeave");
        model.addParams("token", token);
        model.addParams("staffID", staffID);
        model.addParams("leaveID", leaveID);
        model.addParams("leaveType", leaveType);
        model.addParams("leaveStatus", leaveStatus);
        model.addParams("requestDate", requestDate);
        model.addParams("deviceType", "android");

        LogUtils.showLog(context.getClass().getName(), "teamLeave", model.getParams().toString());
        countWS++;

        request(context, RequestMethod.POST, model, onResponse);
    }

    public static void getTeamLeaveByDate(final Activity context, String token, String staffID, String leaveID, String leaveType, String leaveStatus, String requestDate, String startLeaveDate, String endLeaveDate, final OnListenerFromWSManager onResponse) {
        final LMSServiceModel model = new LMSServiceModel(context);
        model.addParams("requestType", "teamLeave");
        model.addParams("token", token);
        model.addParams("staffID", staffID);
        model.addParams("leaveID", leaveID);
        model.addParams("leaveType", leaveType);
        model.addParams("leaveStatus", leaveStatus);
        model.addParams("requestDate", requestDate);
        model.addParams("deviceType", "android");
        model.addParams("startLeaveDate", startLeaveDate);
        model.addParams("endLeaveDate", endLeaveDate);

        LogUtils.showLog(context.getClass().getName(), "teamLeaveByDate", model.getParams().toString());
        countWS++;

        request(context, RequestMethod.POST, model, onResponse);
    }

    public static void getStaffSummary(final Activity context, String token, String staffID, final OnListenerFromWSManager onResponse) {
        final LMSServiceModel model = new LMSServiceModel(context);
        model.addParams("requestType", "StaffSummary");
        model.addParams("token", token);
        model.addParams("year", AwareUtils.getCurrentYearRequest());
        model.addParams("staffID", staffID);
        model.addParams("deviceType", "android");

        LogUtils.showLog(context.getClass().getName(), "StaffSummary", model.getParams().toString());
        countWS++;

        request(context, RequestMethod.POST, model, onResponse);
    }

    public static void getStaffSummary(final Activity context, String token, final OnListenerFromWSManager onResponse) {
        final LMSServiceModel model = new LMSServiceModel(context);
        model.addParams("requestType", "StaffSummary");
        model.addParams("year", AwareUtils.getCurrentYearRequest());
        model.addParams("token", token);
        model.addParams("deviceType", "android");

        LogUtils.showLog(context.getClass().getName(), "StaffSummary", model.getParams().toString());
        countWS++;

        request(context, RequestMethod.POST, model, onResponse);
    }

    public static void getMember(final Activity context, String EMP_ROLE, String token, final OnListenerFromWSManager onResponse) {
        final LMSServiceModel model = new LMSServiceModel(context);
        if (EMP_ROLE.equalsIgnoreCase(LMSGeneralConstant.EMP_ROLE.SUPERVISOR_TEXT) || EMP_ROLE.equalsIgnoreCase(LMSGeneralConstant.EMP_ROLE.DIRECTOR_TEXT))
            model.addParams("requestType", "GetSubordinates");
        else
            model.addParams("requestType", "GetTeamMembers");
        model.addParams("token", token);
        model.addParams("year", AwareUtils.getCurrentYearRequest());
        model.addParams("deviceType", "android");

        LogUtils.showLog(context.getClass().getName(), "GetTeamMembers", model.getParams().toString());
        countWS++;

        request(context, RequestMethod.POST, model, onResponse);
    }

    public static void sendFeedback(final Activity context, String name, LoginResponse.DataEntity curProfile, String description, final OnListenerFromWSManager onResponse) {
        final LMSServiceModel model = new LMSServiceModel(context);
        model.addParams("requestType", "feedback");
        model.addParams("empCode", curProfile.getEMP_CODE());
        model.addParams("name", name);
        model.addParams("email", curProfile.getEMAIL());
        model.addParams("token", curProfile.getSTAFF_TOKEN());
        model.addParams("description", description);
        model.addParams("deviceType", "android");

        LogUtils.showLog(context.getClass().getName(), "feedback", model.getParams().toString());
        countWS++;

        request(context, RequestMethod.POST, model, onResponse);
    }

    public static void getLeaveHistoryByType(final Activity context, String token, int[] leaveType, final OnListenerFromWSManager onResponse) {
        final LMSServiceModel model = new LMSServiceModel(context);
        String leaveTypesText = "";

        for (int i = 0; i < leaveType.length; i++) {
            int type = leaveType[i];
            leaveTypesText += type + "";
            if (i != leaveType.length - 1) {
                leaveTypesText += ",";
            }
        }

        model.addParams("requestType", "LeaveHistory");
        model.addParams("year", AwareUtils.getCurrentNextYearRequest());
        model.addParams("leaveType", leaveTypesText);
        model.addParams("token", token);
        model.addParams("deviceType", "android");

        LogUtils.showLog(context.getClass().getName(), "LeaveHistory", model.getParams().toString());
        countWS++;

        request(context, RequestMethod.POST, model, onResponse);
    }

    public static void updateLeaveStatus(final Activity context, String token, String requestID, String leaveStatus, String reason, final OnListenerFromWSManager onResponse) {
        final LMSServiceModel model = new LMSServiceModel(context);
        model.addParams("requestType", "updateLeaveStatus");
        model.addParams("requestID", requestID);
        model.addParams("leaveStatus", leaveStatus);
        model.addParams("reason", reason);
        model.addParams("token", token);
        model.addParams("deviceType", "android");

        LogUtils.showLog(context.getClass().getName(), "updateLeaveStatus", model.getParams().toString());
        countWS++;

        request(context, RequestMethod.POST, model, onResponse);
    }


    public static void requestLeave(final Activity context, String token, String startDate, String endDate, String totalTime, String leaveType, String leaveStatus, String reason, final OnListenerFromWSManager onResponse) {
        final LMSServiceModel model = new LMSServiceModel(context);

        model.addParams("requestType", "RequestLeave");
        model.addParams("token", token);
        model.addParams("startDate", startDate);
        model.addParams("endDate", endDate);
        model.addParams("totalTime", totalTime);
        model.addParams("leaveType", leaveType);
        model.addParams("leaveStatus", leaveStatus);
        model.addParams("year", startDate.substring(0, 4));
        model.addParams("reason", reason);
        model.addParams("deviceType", "android");

        LogUtils.showLog(context.getClass().getName(), "Request Leave", model.getParams().toString());
        countWS++;

        request(context, RequestMethod.POST, model, onResponse);
    }

    public static void getHolidays(final Activity context, String token, String country, final OnListenerFromWSManager onResponse) {
        final LMSServiceModel model = new LMSServiceModel(context);
        model.addParams("requestType", "calendarHoldidays");
        model.addParams("token", token);
        model.addParams("year", AwareUtils.getYearRequest());
        model.addParams("countryID", country);
        model.addParams("deviceType", "android");

        LogUtils.showLog(context.getClass().getName(), "Holdidays", model.getParams().toString());
        countWS++;

        request(context, RequestMethod.POST, model, onResponse);
    }

    public static void badgeCountUpdate(final Activity context, String token, String staffID, String leaveModule, String badgeNum, final OnListenerFromWSManager onResponse) {
        final LMSServiceModel model = new LMSServiceModel(context);
        model.addParams("requestType", "badgeCountUpdate");
        model.addParams("token", token);
        model.addParams("staffID", staffID);
        model.addParams("leaveModule", leaveModule);
        model.addParams("badgeNum", badgeNum);
        model.addParams("deviceType", "android");

        LogUtils.showLog(context.getClass().getName(), "badgeCountUpdate", model.getParams().toString());
        countWS++;

        request(context, RequestMethod.POST, model, onResponse);
    }

    public static void checkForDayOff(final Activity context, String token, final OnListenerFromWSManager onResponse) {
        final LMSServiceModel model = new LMSServiceModel(context);
        model.addParams("requestType", "checkForDayOff");
        model.addParams("token", token);
        model.addParams("deviceType", "android");

        LogUtils.showLog(context.getClass().getName(), "checkForDayOff", model.getParams().toString());
        countWS++;

        request(context, RequestMethod.POST, model, onResponse);
    }

    public static void changeDayOffStatus(final Activity context, String token, String requestID, String statusID, final OnListenerFromWSManager onResponse) {

        final LMSServiceModel model = new LMSServiceModel(context);
        model.addParams("requestType", "changeDayOffStatus");
        model.addParams("token", token);
        model.addParams("requestID", requestID);
        model.addParams("statusID", statusID);
        model.addParams("deviceType", "android");

        LogUtils.showLog(context.getClass().getName(), "changeDayOffStatus", model.getParams().toString());
        countWS++;

        request(context, RequestMethod.POST, model, onResponse);
    }

    public static void upLoadImageProfile(final Activity context, String token, String resPath, final OnListenerFromWSManager onResponse) {
        final LMSServiceModel model = new LMSServiceModel(context);
        model.addParams("requestType", "uploadProfileImage");
        model.addParams("token", token);
        model.addParams("deviceType", "android");
        model.setRealImagePath(resPath);

        LogUtils.showLog(context.getClass().getName(), "uploadProfileImage", model.getParams().toString());
        countWS++;

        request(context, RequestMethod.POST, model, onResponse);
    }

    public static void badgeCountRead(final Activity context, String token, String staffID, final OnListenerFromWSManager onResponse) {
        final LMSServiceModel model = new LMSServiceModel(context);
        model.addParams("requestType", "badgeCountRead");
        model.addParams("token", token);
        model.addParams("staffID", staffID);
        model.addParams("device_os", "android");
        model.addParams("deviceType", "android");

        LogUtils.showLog(context.getClass().getName(), "badgeCountRead", model.getParams().toString());
        countWS++;

        final LMSRestClient service = new LMSRestClient(context, RequestMethod.POST, model, new OnListenerFromWS() {

            @Override
            public void onComplete(String response) {
                SaveSharedPreference saveSharedPreference = new SaveSharedPreference(context);
                Gson gson = new Gson();
                BaseResponse badgecount = gson.fromJson(response, BaseResponse.class);
                ResponseEntity responses = badgecount.getResponse();
                if (responses != null) {
                    String type = responses.getMsg_type();
                    String id = responses.getMsg_id();
                    String desc = responses.getMsg_desc();
                    String serverTime = responses.getServer_time();
                    saveSharedPreference.saveVersionUpdate(responses.getAndroid_version());
                    saveSharedPreference.saveStatusForceUpdate(responses.getAndroid_force_update());
                    LMSApplication application = (LMSApplication) context.getApplication();
                    application.setServerTime(serverTime);

                    if (id.equalsIgnoreCase("Success")) {
                        onResponse.onComplete(type, response);
                    } else if (id.equalsIgnoreCase("MobileInvalidToken")) {
                        onResponse.onFailedAndForceLogout();
                        saveSharedPreference.saveToken("");
                        saveSharedPreference.saveJSON_LOGIN("");
                    } else {
                        onResponse.onFailed(type, response);
                    }
                } else {
                    onResponse.onFailed(response);
                }
            }

            @Override
            public void onFailed(String message) {

                onResponse.onFailed(message);
            }
        });

        service.execute();
    }

    public static void updateVersion(final Activity context, String token, String staffID, String versionApp, final OnListenerFromWSManager onResponse) {
        final LMSServiceModel model = new LMSServiceModel(context);
        model.addParams("requestType", "badgeCountRead");
        model.addParams("token", token);
        model.addParams("version", versionApp);
        model.addParams("device_os", "android");
        model.addParams("staffID", staffID);

        LogUtils.showLog(context.getClass().getName(), "updateVersion", model.getParams().toString());
        countWS++;

        final LMSRestClient service = new LMSRestClient(context, RequestMethod.POST, model, new OnListenerFromWS() {
            @Override
            public void onComplete(String response) {
                Gson gson = new Gson();
                BaseResponse badgeCount = gson.fromJson(response, BadgeResponse.class);
                ResponseEntity responses = badgeCount.getResponse();
                if (responses != null) {
                    String type = responses.getMsg_type();
                    String serverTime = responses.getServer_time();
                    LMSApplication application = LMSApplication.getInstance();
                    application.setServerTime(serverTime);
                    onResponse.onFailed(type, response);

                } else {
                    onResponse.onFailed(response);
                }
            }

            @Override
            public void onFailed(String message) {
                onResponse.onFailed(message);
            }
        });

        service.execute();
    }

    private static void request(final Activity activity, RequestMethod method, LMSServiceModel lmsModel, final OnListenerFromWSManager onResponse) {
        final LMSRestClient service = new LMSRestClient(activity, method, lmsModel, new OnListenerFromWS() {
            @Override
            public void onComplete(String response) {
                Gson gson = new Gson();
                BaseResponse responseGSON = gson.fromJson(response, BaseResponse.class);
                ResponseEntity responses = responseGSON.getResponse();
                if (responses != null) {
                    String type = responses.getMsg_type();
                    String id = responses.getMsg_id();
                    String desc = responses.getMsg_desc();
                    String serverTime = responses.getServer_time();
                    LMSApplication application = LMSApplication.getInstance();
                    application.setServerTime(serverTime);
                    if (id.equalsIgnoreCase("Success")) {
                        onResponse.onComplete(type, response);
                    } else if (id.equalsIgnoreCase("MobileInvalidToken")) {
                        onResponse.onFailedAndForceLogout();
                        SaveSharedPreference saveSharedPreference = new SaveSharedPreference(activity);
                        saveSharedPreference.saveToken("");
                        saveSharedPreference.saveJSON_LOGIN("");
                    } else {
                        onResponse.onFailed(type, desc);
                    }
                } else {
                    onResponse.onFailed(response);
                }
            }

            @Override
            public void onFailed(String message) {
                onResponse.onFailed(message);
            }
        });

        service.execute();
    }
}
