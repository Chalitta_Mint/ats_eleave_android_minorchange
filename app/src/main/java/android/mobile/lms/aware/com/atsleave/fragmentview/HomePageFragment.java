package android.mobile.lms.aware.com.atsleave.fragmentview;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.mobile.lms.aware.com.atsleave.LMSApplication;
import android.mobile.lms.aware.com.atsleave.LoginActivity;
import android.mobile.lms.aware.com.atsleave.MainActivity;
import android.mobile.lms.aware.com.atsleave.R;
import android.mobile.lms.aware.com.atsleave.TutorialsActivity;
import android.mobile.lms.aware.com.atsleave.callback.BackPressed;
import android.mobile.lms.aware.com.atsleave.callback.OnListenerFromWSManager;
import android.mobile.lms.aware.com.atsleave.callback.RequestDialogListener;
import android.mobile.lms.aware.com.atsleave.constant.SaveSharedPreference;
import android.mobile.lms.aware.com.atsleave.models.BadgeResponse;
import android.mobile.lms.aware.com.atsleave.models.CheckForDayOffResponse;
import android.mobile.lms.aware.com.atsleave.models.HolidayResponse;
import android.mobile.lms.aware.com.atsleave.models.ImageUploadResponse;
import android.mobile.lms.aware.com.atsleave.models.LoginResponse;
import android.mobile.lms.aware.com.atsleave.models.ProfileResponse;
import android.mobile.lms.aware.com.atsleave.models.ResponseEntity;
import android.mobile.lms.aware.com.atsleave.models.StaffSummaryResponse;
import android.mobile.lms.aware.com.atsleave.utils.AwareUtils;
import android.mobile.lms.aware.com.atsleave.utils.DialogUtils;
import android.mobile.lms.aware.com.atsleave.utils.EventBus;
import android.mobile.lms.aware.com.atsleave.utils.OnChangePageView;
import android.mobile.lms.aware.com.atsleave.webservice.LMSGeneralConstant;
import android.mobile.lms.aware.com.atsleave.webservice.LMSRestClient;
import android.mobile.lms.aware.com.atsleave.webservice.LMSWSManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import androidx.fragment.app.Fragment;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.appcompat.app.AlertDialog;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.readystatesoftware.viewbadger.BadgeView;
import com.squareup.otto.Subscribe;
import com.yalantis.ucrop.UCrop;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import me.leolin.shortcutbadger.ShortcutBadger;

import static android.app.Activity.RESULT_OK;

/**
 * Created by apinun.w on 14/7/2558.
 */
public class HomePageFragment extends Fragment implements View.OnClickListener, BackPressed {

    private final String TAG = "HomePageFragment";
    //view
    public static HomePageFragment instance;
    private WebView webView;
    private View view;
    private Button btn_history_sick;
    private Button btn_history_personal;
    private Button btn_history_other;
    private LinearLayout box_request, box_team_leave;
    private ScrollView scrollView;
    private LinearLayout box_team;
    private TextView title_name;
    private TextView title_position;
    private TextView text_id;
    private TextView text_phone;
    private TextView text_birthday;
    private TextView text_email;
    private TextView text_daySick;
    private TextView text_houSick;
    private TextView text_daysick_personal;
    private TextView text_housick_personal;
    private TextView text_daysick_other;
    private TextView text_housick_other;
    private ImageView round_img_profile;
    private TextView text_message_birthdaty;
    private TextView text_message_dayoff;
    private TextView text_amountbirthday;
    private TextView text_amountdayoff, btn_dismiss, btn_download;
    private ImageView icon_setting, icon_download;
    private LinearLayout linear_popup;
    private LMSApplication application;
    private RelativeLayout box_dayoff;
    private Animation anim_in_from_bottom;
    private Animation anim_out_from_bottom;
    private Button btn_apply, btn_apply_dayoff;
    SimpleDateFormat sdf_output = new SimpleDateFormat("yyyy MM dd", Locale.ENGLISH);
    SimpleDateFormat sdf_input = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH);        //1990-02-09 00:00:00
    private LinearLayout linear_feedback;
    private LinearLayout linear_logout;
    private LinearLayout linear_cancel;
    private LinearLayout linear_video_tutorial;
    private SwipeRefreshLayout swipeView;
    SaveSharedPreference saveSharedPreference;
    private TextView title_assign_role;
    private RelativeLayout relative_hbd, box_update_version, box_alert_first_update;
    private BadgeView badge_sick;
    private BadgeView badge_emp;
    private BadgeView badge_personal;
    private BadgeView badge_other;
    private Dialog dialogWaiting;
    private boolean isVisibleView = true;
    private int statusPro = 0;
    private Button btn_save_image;
    //interface
    private OnChangePageView onChangePageView;
    private static String realPath;
    private Boolean stBirthday = true;
    //general
    public String title;
    private static int countAlert = 0;
    private String versionApp;
    private ProgressDialog pd;
    private String versionUpdate;
    private RelativeLayout relativeSetting;
    private Uri outputFileUri;
    private File fileTemp;
    private File fileCamera;

    public static HomePageFragment initHomePageFragment(String title, OnChangePageView onChangePageView) {
        if (instance == null) {
            instance = new HomePageFragment();
        }
        instance.setOnChangePageView(onChangePageView);
        return instance;
    }

    /*back of fragment*/
    private View.OnKeyListener onKeyDown = new View.OnKeyListener() {
        @Override
        public boolean onKey(View v, int keyCode, KeyEvent event) {

            if (KeyEvent.ACTION_UP == event.getAction()) {
                if (keyCode == KeyEvent.KEYCODE_MENU) {
                    if (isVisibleView) {
                        if (linear_popup.getVisibility() == View.VISIBLE) {
                            hidePopUp();
                        } else {
                            showPopUp();
                        }
                    }
                    return true;
                } else if (keyCode == KeyEvent.KEYCODE_BACK) {
                    if (isVisibleView) {
                        if (linear_popup.getVisibility() == View.VISIBLE) {
                            hidePopUp();
                        } else {
                            getActivity().finish();
                        }
                    }
                    return true;
                }
            }
            return false;
        }
    };

    public void setOnChangePageView(OnChangePageView onChangePageView) {
        this.onChangePageView = onChangePageView;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        saveSharedPreference = new SaveSharedPreference(getActivity());
        application = (LMSApplication) getActivity().getApplication();
        dialogWaiting = DialogUtils.createDialog(getActivity());
        setHasOptionsMenu(true);

        view = inflater.inflate(R.layout.layout_homepagefragment, container, false);
        webView = (WebView) view.findViewById(R.id.mybrowser);

        linear_popup = (LinearLayout) view.findViewById(R.id.linear_popup);
        swipeView = (SwipeRefreshLayout) view.findViewById(R.id.swipeRefreshLayout);
        scrollView = (ScrollView) view.findViewById(R.id.scrollView);
        btn_history_sick = (Button) view.findViewById(R.id.btn_history_sick);
        btn_history_personal = (Button) view.findViewById(R.id.btn_history_personal);
        btn_history_other = (Button) view.findViewById(R.id.btn_history_other);
        title_name = (TextView) view.findViewById(R.id.title_name);
        icon_setting = (ImageView) view.findViewById(R.id.icon_setting);
        title_assign_role = (TextView) view.findViewById(R.id.title_assign_role);
        relative_hbd = (RelativeLayout) view.findViewById(R.id.box_birthday);
        title_position = (TextView) view.findViewById(R.id.title_position);
        text_id = (TextView) view.findViewById(R.id.text_id);
        text_phone = (TextView) view.findViewById(R.id.text_phone);
        btn_apply = (Button) view.findViewById(R.id.btn_apply);
        btn_save_image = (Button) view.findViewById(R.id.btn_save_image);
        text_amountbirthday = (TextView) view.findViewById(R.id.text_amountbirthday);
        text_message_birthdaty = (TextView) view.findViewById(R.id.text_message_birthdaty);
        text_birthday = (TextView) view.findViewById(R.id.text_birthday);

        box_dayoff = (RelativeLayout) view.findViewById(R.id.box_dayoff);
        btn_apply_dayoff = (Button) view.findViewById(R.id.btn_apply_dayoff);
        text_amountdayoff = (TextView) view.findViewById(R.id.text_amountdayoff);
        text_message_dayoff = (TextView) view.findViewById(R.id.text_message_dayoff);

        text_daySick = (TextView) view.findViewById(R.id.text_daysick);
        text_houSick = (TextView) view.findViewById(R.id.text_housick);
        text_daysick_personal = (TextView) view.findViewById(R.id.text_daysick_personal);
        text_housick_personal = (TextView) view.findViewById(R.id.text_housick_personal);
        text_daysick_other = (TextView) view.findViewById(R.id.text_daysick_other);
        text_housick_other = (TextView) view.findViewById(R.id.text_housick_other);
        text_email = (TextView) view.findViewById(R.id.text_email);
        round_img_profile = (ImageView) view.findViewById(R.id.round_img_profile);
        box_request = (LinearLayout) view.findViewById(R.id.box_request);
        box_team = (LinearLayout) view.findViewById(R.id.box_team);
        box_team_leave = (LinearLayout) view.findViewById(R.id.box_team_leave);
        linear_feedback = (LinearLayout) view.findViewById(R.id.linear_feedback);
        linear_logout = (LinearLayout) view.findViewById(R.id.linear_logout);
        linear_cancel = (LinearLayout) view.findViewById(R.id.linear_cancel);
        linear_video_tutorial = (LinearLayout) view.findViewById(R.id.linear_video_tutorial);
//        round_img_profile.setImageBitmap(BitmapFactory.decodeFile(Environment.getExternalStorageDirectory().getPath()+"/"+"IMG201510141354471973880726.jpg"));

        LinearLayout linear_button_sick_his = (LinearLayout) view.findViewById(R.id.linear_button_sick_his);
        LinearLayout linear_button_personal_his = (LinearLayout) view.findViewById(R.id.linear_button_personal_his);
        LinearLayout linear_button_other_his = (LinearLayout) view.findViewById(R.id.linear_button_other_his);
        badge_emp = new BadgeView(getActivity(), round_img_profile);
        badge_sick = new BadgeView(getActivity(), linear_button_sick_his);
        badge_personal = new BadgeView(getActivity(), linear_button_personal_his);
        badge_other = new BadgeView(getActivity(), linear_button_other_his);
        badge_sick.setBadgePosition(BadgeView.POSITION_TOP_RIGHT);
        badge_emp.setBadgePosition(BadgeView.POSITION_TOP_RIGHT);
        badge_personal.setBadgePosition(BadgeView.POSITION_TOP_RIGHT);
        badge_other.setBadgePosition(BadgeView.POSITION_TOP_RIGHT);
        box_dayoff.setVisibility(View.GONE);
        box_update_version = (RelativeLayout) view.findViewById(R.id.box_alert_update);
        box_alert_first_update = (RelativeLayout) view.findViewById(R.id.box_alert_first_update);
        btn_dismiss = (TextView) view.findViewById(R.id.btn_dissmiss);
        btn_download = (TextView) view.findViewById(R.id.btn_download);
        icon_download = (ImageView) view.findViewById(R.id.icon_download);
        relativeSetting = (RelativeLayout) view.findViewById(R.id.relativeSetting);
        // blurring_view = (BlurringView) view.findViewById(R.id.blurring_view);
        // blurring_view.setBlurredView(img_blur);

        if (saveSharedPreference.getViewTutorials()) {//this show Tutorials Video
            Intent goSecond = new Intent(getActivity(), TutorialsActivity.class);
            startActivityForResult(goSecond, 0);
        }

        pd = new ProgressDialog(getActivity());
        pd.setMessage("Loading...");
        pd.setCanceledOnTouchOutside(false);
        //--------------------------------------------------------------
        PackageInfo pInfo = null;
        try {
            pInfo = getActivity().getPackageManager().getPackageInfo(getActivity().getPackageName(), 0);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        versionApp = pInfo.versionName;
        //--------------------------------------------------------------

        box_dayoff.setVisibility(View.GONE);
        relative_hbd.setVisibility(View.GONE);

        //Animation
        anim_in_from_bottom = AnimationUtils.loadAnimation(getActivity(), R.anim.fade_in_on_bottom);
        anim_in_from_bottom.setDuration(250);
        anim_out_from_bottom = AnimationUtils.loadAnimation(getActivity(), R.anim.fade_out_on_bottom);
        anim_out_from_bottom.setDuration(250);

        btn_history_sick.setOnClickListener(this);
        btn_history_personal.setOnClickListener(this);
        btn_history_other.setOnClickListener(this);
        box_team.setOnClickListener(this);
        box_team_leave.setOnClickListener(this);
        //icon_setting.setOnClickListener(this);
        relativeSetting.setOnClickListener(this);
        box_request.setOnClickListener(this);
        btn_apply.setOnClickListener(this);
        btn_apply_dayoff.setOnClickListener(this);
        round_img_profile.setOnClickListener(this);
        btn_save_image.setOnClickListener(this);
        btn_dismiss.setOnClickListener(this);
        icon_download.setOnClickListener(this);
        btn_download.setOnClickListener(this);
        box_alert_first_update.setOnClickListener(this);
        linear_feedback.setOnClickListener(OnClickPopUp);
        linear_logout.setOnClickListener(OnClickPopUp);
        linear_cancel.setOnClickListener(OnClickPopUp);
        linear_video_tutorial.setOnClickListener(OnClickPopUp);

        badge_emp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onChangePageView.setOnChangePager(LMSGeneralConstant.FIRST_PAGE, LMSGeneralConstant.PENDING_REQUEST_PAGE);
            }
        });

        //------------------For Update version-------------------
        checkUpdateVersion(saveSharedPreference.getVersionUpdate(), saveSharedPreference.getStatusForceUpdate());

        scrollView.getViewTreeObserver().addOnScrollChangedListener(new ViewTreeObserver.OnScrollChangedListener() {

            @Override
            public void onScrollChanged() {
                int scrollY = scrollView.getScrollY();
                if (scrollY == 0) swipeView.setEnabled(true);
                else swipeView.setEnabled(false);
            }
        });

        swipeView.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                swipeView.setRefreshing(true);
                readDayoff();
                showAlertDialog();
                countAlert = 3;
                getStaffSumary();
                readBadgeCount();
                getProfile();
                checkUpdateVersion(saveSharedPreference.getVersionUpdate(), saveSharedPreference.getStatusForceUpdate());
            }
        });

        String emp_role = application.getCurProfile().getEMP_ROLE();
        if (emp_role.equalsIgnoreCase(LMSGeneralConstant.EMP_ROLE.SUPERVISOR_TEXT) || emp_role.equalsIgnoreCase(LMSGeneralConstant.EMP_ROLE.DIRECTOR_TEXT)) {
            box_team_leave.setVisibility(View.VISIBLE);
            //final ScrollView scrollView = (ScrollView) view.findViewById(R.id.)
        } else {
            box_team_leave.setVisibility(View.GONE);
        }
        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        onChangePageView = (OnChangePageView) context;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_history_sick:
                onChangePageView.setOnChangePager(LMSGeneralConstant.SECOND_PAGE, LMSGeneralConstant.SICK_HISTORY_PAGE);
                application.setIntSickLeaveShowBadge(0);
                setBadgeView();
                break;
            case R.id.btn_history_personal:
                onChangePageView.setOnChangePager(LMSGeneralConstant.SECOND_PAGE, LMSGeneralConstant.PERSONAL_HISTORY_PAGE);
                application.setIntAnnualShowBadge(0);
                setBadgeView();
                break;
            case R.id.btn_history_other:
                onChangePageView.setOnChangePager(LMSGeneralConstant.SECOND_PAGE, LMSGeneralConstant.OTHER_HISTORY_PAGE);
                application.setIntOtherShowBadge(0);
                setBadgeView();
                break;
            case R.id.box_request:
                onChangePageView.setOnChangePager(LMSGeneralConstant.FIRST_PAGE, LMSGeneralConstant.REQUEST_PAGE);
                break;
            case R.id.box_team:
                onChangePageView.setOnChangePager(LMSGeneralConstant.FIRST_PAGE, LMSGeneralConstant.MY_TEAM_PAGE);
                break;
            case R.id.box_team_leave:
                onChangePageView.setOnChangePager(LMSGeneralConstant.FIRST_PAGE, LMSGeneralConstant.TEAM_LEAVE);
                break;
            case R.id.relativeSetting:
                if (linear_popup.getVisibility() == View.VISIBLE) {
                    hidePopUp();
                } else {
                    showPopUp();
                }
                break;
            case R.id.btn_apply:
                ((MainActivity) getActivity()).setIsApplyBirthDay(true);
                ((MainActivity) getActivity()).setIsApplayDayoff(false);
                onChangePageView.setOnChangePager(LMSGeneralConstant.FIRST_PAGE, LMSGeneralConstant.REQUEST_PAGE);
                break;
            case R.id.btn_apply_dayoff:
                ((MainActivity) getActivity()).setIsApplayDayoff(true);
                ((MainActivity) getActivity()).setIsApplyBirthDay(false);
                onChangePageView.setOnChangePager(LMSGeneralConstant.FIRST_PAGE, LMSGeneralConstant.REQUEST_PAGE);
                break;
            case R.id.round_img_profile:
                selectImage();
                break;
            case R.id.btn_save_image:
                upLoadImage();
                break;
            case R.id.btn_dissmiss:
                box_update_version.setVisibility(View.GONE);
                break;
            case R.id.btn_download:
                CallWebview();
                break;
            case R.id.icon_download:
                CallWebview();
                break;
            case R.id.box_alert_first_update:
                box_update_version.setVisibility(View.VISIBLE);
                break;
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        countAlert = 3;
        setLookUp();
        loadHolidays();
        readDayoff();

        showAlertDialog();

        getStaffSumary();
        readBadgeCount();
        //count alert dialog to show Dialog
        getView().setFocusableInTouchMode(true);
        getView().requestFocus();
        getView().setOnKeyListener(onKeyDown);
        checkUpdateVersion(saveSharedPreference.getVersionUpdate(), saveSharedPreference.getStatusForceUpdate());
        EventBus.getInstance().register(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        EventBus.getInstance().unregister(this);
    }

    @Subscribe
    public void getMessage(String msg) {
        Log.e("getMessage", "getMessage: " + msg);
        readBadgeCount();
    }

    private void getImageFromGallery() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select picture"), 222);
    }

    private void selectImage() {
        final CharSequence[] items = {"Take Photo", "Choose from Library", "Cancel"};
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Select Photo!");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (items[item].equals("Take Photo")) {
                    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    fileCamera = new File(android.os.Environment.getExternalStorageDirectory() + File.separator + "LMS" + File.separator, "camera.jpg");
                    intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(fileCamera));
                    startActivityForResult(intent, 221);
                } else if (items[item].equals("Choose from Library")) {
                    Intent intent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    intent.setType("image/*");
                    startActivityForResult(Intent.createChooser(intent, "Select picture"), 222);
                } else if (items[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    private void upLoadImage() {
        showAlertDialog();
        LMSWSManager.upLoadImageProfile(getActivity(), application.getCurProfile().getSTAFF_TOKEN(), fileTemp.getAbsolutePath(), onResponseUpload);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.i("onActivityResult", "requestCode: " + requestCode + " resultCode: " + resultCode);
        if (resultCode == RESULT_OK && requestCode == UCrop.REQUEST_CROP) {
            Log.i("REQUEST_CROP", "requestCode: " + requestCode + " resultCode: " + resultCode);
            final Uri resultUri = UCrop.getOutput(data);
            round_img_profile.setImageURI(null);
            round_img_profile.setImageURI(resultUri);
            btn_save_image.setVisibility(View.VISIBLE);

        } else if (requestCode == 222 && resultCode == RESULT_OK) {

            if (data == null || data.getData() == null) {
                Toast.makeText(getActivity(), getString(R.string.error_image), Toast.LENGTH_SHORT).show();
                return;
            }

            File root = new File(Environment.getExternalStorageDirectory() + File.separator + "LMS" + File.separator);
            root.mkdir();
            final String fname = "img_temp.jpg";
            fileTemp = new File(root, fname);

            UCrop.of(data.getData(), Uri.fromFile(fileTemp))
                    .withAspectRatio(1, 1)
                    .withMaxResultSize(500, 500)
                    .start(getActivity());
        } else if (requestCode == 221 && resultCode == RESULT_OK) {

            File root = new File(Environment.getExternalStorageDirectory() + File.separator + "LMS" + File.separator);
            root.mkdir();
            final String fname = "img_temp.jpg";
            fileTemp = new File(root, fname);

            UCrop.of(Uri.fromFile(fileCamera), Uri.fromFile(fileTemp))
                    .withAspectRatio(1, 1)
                    .withMaxResultSize(500, 500)
                    .start(getActivity());
        } else if (resultCode == UCrop.RESULT_ERROR) {
            Toast.makeText(getActivity(), getString(R.string.error_image), Toast.LENGTH_SHORT).show();
        } else {
            getProfile();
        }
    }

    private View.OnClickListener OnClickPopUp = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.linear_feedback:
                    //show dialog feedback
                    hidePopUp();
                    showFeedBackDialog();
                    break;
                case R.id.linear_logout:
                    //log out
                    requestLogOut();
                    break;
                case R.id.linear_cancel:
                    if (linear_popup.getVisibility() == View.VISIBLE) {
                        hidePopUp();
                    }
                    break;
                case R.id.linear_video_tutorial:
                    if (linear_popup.getVisibility() == View.VISIBLE) {
                        hidePopUp();
                    }
                    //set action
                    Intent goSecond = new Intent(getActivity(), TutorialsActivity.class);
                    startActivityForResult(goSecond, 0);
                    break;
            }
        }
    };

    private void showFeedBackDialog() {
        final Dialog feedBackDailog = new Dialog(getActivity(), android.R.style.Theme_Translucent_NoTitleBar);
        feedBackDailog.setContentView(R.layout.layout_feedback);

        int width_of_dialog = (int) (application.getWidth_screen() * 0.85f);
        int height_of_dialog = (int) (application.getHeight_screen() * 0.8f);
        final WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(feedBackDailog.getWindow().getAttributes());
        lp.width = width_of_dialog;
        lp.height = height_of_dialog;
        feedBackDailog.getWindow().setAttributes(lp);

        TextView edit_to = (TextView) feedBackDailog.findViewById(R.id.edit_to);
        TextView edit_from = (TextView) feedBackDailog.findViewById(R.id.edit_from);
        TextView text_cancel = (TextView) feedBackDailog.findViewById(R.id.text_cancel);
        TextView text_send = (TextView) feedBackDailog.findViewById(R.id.text_send);
        final EditText edit_detail = (EditText) feedBackDailog.findViewById(R.id.edit_detail);

        final String str_name = application.getCurProfile().getNAME_ENG() + " (" + application.getCurProfile().getNICKNAME_ENG() + ") " + application.getCurProfile().getSURNAME_ENG();
        edit_from.setText(str_name);

        edit_to.setText("E-Leave Management Administrators");

        text_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                feedBackDailog.dismiss();
            }
        });

        text_send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (!edit_detail.getText().toString().trim().equalsIgnoreCase("")) {
                    LMSWSManager.sendFeedback(getActivity(), str_name, application.getCurProfile(), edit_detail.getText().toString(), onResponseFeedback);
                    feedBackDailog.dismiss();
                } else {
                    DialogUtils.showAlertDialog(getActivity(), getString(R.string.alert_text_btn_deny), false);
                }
            }
        });

        feedBackDailog.show();
        feedBackDailog.getWindow().setAttributes(lp);
    }

    private void getStaffSumary() {
        showAlertDialog();
        LMSWSManager.getStaffSummary(getActivity(), application.getCurProfile().getSTAFF_TOKEN(), onListenStaff);
    }

    private void loadHolidays() {
        showAlertDialog();
        LMSWSManager.getHolidays(getActivity(), application.getCurProfile().getSTAFF_TOKEN(), "Thailand", new OnListenerFromWSManager() {
            @Override
            public void onComplete(String statusCode, String message) {
                dismissDialog();
                Gson gson = new Gson();
                HolidayResponse holidayFromWS = gson.fromJson(message, HolidayResponse.class);
                application.setStr_holidays(message);
            }

            @Override
            public void onFailed(String statusCode, final String message) {
                dismissDialog();
            }

            @Override
            public void onFailed(final String message) {
                dismissDialog();
            }

            @Override
            public void onFailedAndForceLogout() {
                dismissDialog();
                dismissRefresh();
                if (!AwareUtils.isForceLogout) {
                    AwareUtils.forceLogOut(getActivity());
                }
            }
        });
    }

    private void readBadgeCount() {
        showAlertDialog();
        LMSWSManager.badgeCountRead(getActivity(), application.getCurProfile().getSTAFF_TOKEN(), application.getCurProfile().getSTF_ID(), onResponseBadge);
    }

    private void checkHDB(String servertime, int statusPro, String allDayforleave, String AllTimeUse) {

        String txtHBD = application.getCurProfile().getBIRTH_DATE();
        Date HBD_date = null;
        try {
            HBD_date = sdf_input.parse(txtHBD);    //1990-02-09 00:00:00
        } catch (ParseException e) {
            e.printStackTrace();
            HBD_date = null;
        }

        if (HBD_date == null) {
            return;
        }

        if (AllTimeUse == null) {
            AllTimeUse = "0";
        }
        ///-----remain date -------------
        int remainDay = Integer.parseInt(allDayforleave) - Integer.parseInt(AllTimeUse);

        Calendar cal_start_date = Calendar.getInstance();
        Calendar cal_end_date = Calendar.getInstance();
        Calendar cal_hbd_date = Calendar.getInstance();
        Calendar cal_current_date = Calendar.getInstance();

        try {
            cal_current_date.setTime(AwareUtils.getCurTimeFromServer(getActivity()));
        } catch (Exception e) {
            e.printStackTrace();
        }

        cal_hbd_date.setTime(HBD_date);
        cal_hbd_date.set(cal_current_date.get(Calendar.YEAR), cal_hbd_date.get(Calendar.MONTH), cal_hbd_date.get(Calendar.DAY_OF_MONTH), 0, 0, 0);

        cal_end_date.set(cal_current_date.get(Calendar.YEAR), cal_hbd_date.get(Calendar.MONTH), cal_hbd_date.getActualMaximum(Calendar.DAY_OF_MONTH), 23, 59, 59);
        cal_start_date.set(cal_current_date.get(Calendar.YEAR), cal_hbd_date.get(Calendar.MONTH) - 1, 25, 0, 0, 0);             //hbd == 1 and other

        Date currentDate = null;
        try {
            currentDate = AwareUtils.getCurTimeFromServer(getActivity());
        } catch (Exception e) {
            currentDate = new Date();
            e.printStackTrace();
        }

        Calendar calCurrentDate = Calendar.getInstance();
        calCurrentDate.setTime(currentDate);
        int maxHBDDay = cal_hbd_date.getActualMaximum(Calendar.DAY_OF_MONTH);

        int hbdMonth = cal_hbd_date.get(Calendar.MONTH);
        int curMonth = calCurrentDate.get(Calendar.MONTH);

        int closeUpDate = 0;
        if (hbdMonth == curMonth) {
            closeUpDate = maxHBDDay - calCurrentDate.get(Calendar.DAY_OF_MONTH);
        } else {
            int maxCurDay = calCurrentDate.getActualMaximum(Calendar.DAY_OF_MONTH);
            closeUpDate = maxHBDDay + (maxCurDay - calCurrentDate.get(Calendar.DAY_OF_MONTH));
        }
        closeUpDate++;
        Log.i("checkHDB", "cal_hbd_date " + sdf_output.format(cal_hbd_date.getTime()));
        Log.i("checkHDB", "cal_end_date " + sdf_output.format(cal_end_date.getTime()));
        Log.i("checkHDB", "cal_start_date " + sdf_output.format(cal_start_date.getTime()));
        Log.i("checkHDB", "cal_current_date " + sdf_output.format(cal_current_date.getTime()));

        if (statusPro == 0) {//check status pass pro
            Log.i("checkHDB", "statusPro ");
            relative_hbd.setVisibility(View.GONE);
        } else {
            Log.i("checkHDB", "remainDay: " + remainDay);
            if (remainDay != 0) {//check remain day leave
                if ((cal_current_date.after(cal_start_date) || cal_current_date.compareTo(cal_start_date) == 0) && ((cal_current_date.before(cal_end_date) || cal_current_date.compareTo(cal_end_date) == 0))) {
                    Log.e("checkHDB", "incase");
                    if (saveSharedPreference.getHBDStatusRequest()) {//Leave Birthday
                        Log.e("checkHDB", "HBD : " + true);
                        if (saveSharedPreference.getHBDStatusMessage()) {//show message
                            DialogUtils.showAlertDialog(getActivity(), getString(R.string.alert_hbd), false, new RequestDialogListener() {
                                @Override
                                public void responseSubmit() {
                                    saveSharedPreference.saveHBDStatusMessage(false);
                                }

                                @Override
                                public void responseCancel() {

                                }
                            });
                        }

                        String strCloseDate = closeUpDate + (closeUpDate <= 1 ? " day" : " days");

                        text_amountbirthday.setText("" + closeUpDate);
                        String strMessage = "";
                        if (closeUpDate == 2) {
                            strMessage = getString(R.string.text_msg_tomorrow);
                            text_amountbirthday.setVisibility(View.VISIBLE);
                        } else if (closeUpDate == 1) {
                            strMessage = getString(R.string.text_msg_today);
                            text_amountbirthday.setVisibility(View.GONE);
                        } else {
                            strMessage = String.format(getString(R.string.textMessageBirthday), strCloseDate);
                            text_amountbirthday.setVisibility(View.VISIBLE);
                        }
                        text_message_birthdaty.setText(strMessage);
                        relative_hbd.setVisibility(View.VISIBLE);
                    } else {
                        relative_hbd.setVisibility(View.GONE);
                    }

                } else {
                    relative_hbd.setVisibility(View.GONE);
                }
            } else {
                relative_hbd.setVisibility(View.GONE);
            }
        }
    }

    private void requestLogOut() {
        DialogUtils.showAlertDialogYesandNo(getActivity(), getString(R.string.alert_text_log_out), new RequestDialogListener() {
            @Override
            public void responseSubmit() {
                String token = application.getCurProfile().getSTAFF_TOKEN();
                showAlertDialog();
                LMSWSManager.doLogout(getActivity(), token, onResponseLogout);
            }

            @Override
            public void responseCancel() {

            }
        });
        if (swipeView.isRefreshing())
            swipeView.setRefreshing(false);
    }

    private void setLookUp() {
        LoginResponse.DataEntity profile = application.getCurProfile();
        String txt_hbd_date = "";

        //hbd date
        try {
            Date hbd_date = LMSGeneralConstant.sdp_input.parse(profile.getBIRTH_DATE());
            txt_hbd_date = LMSGeneralConstant.sdp_output_profile.format(hbd_date);

        } catch (ParseException e) {
            e.printStackTrace();
            txt_hbd_date = profile.getBIRTH_DATE();
        }

        title_name.setText(profile.getNAME_ENG() + " " + profile.getSURNAME_ENG());
        title_position.setText(profile.getPOSITION());
        title_assign_role.setText(profile.getASSIGN_ROLE());
        text_id.setText("ID# " + profile.getSTF_ID());
        text_phone.setText(profile.getMOBILE_NUMBER());
        text_birthday.setText(txt_hbd_date);
        text_email.setText(profile.getEMAIL());

        if (btn_save_image.getVisibility() != View.VISIBLE) {
            ImageLoader.getInstance().displayImage(LMSRestClient.getImageURL() + saveSharedPreference.getIMAGE(), round_img_profile);
        }
    }

    public void showPopUp() {
        //clear animation
        anim_in_from_bottom.reset();
        //linear popup visible
        linear_popup.setVisibility(View.VISIBLE);
        linear_popup.clearAnimation();
        linear_popup.startAnimation(anim_in_from_bottom);

        ((MainActivity) getActivity()).disableViewPager();
        btn_history_sick.setEnabled(false);
        btn_history_personal.setEnabled(false);
        btn_history_other.setEnabled(false);
        box_request.setEnabled(false);
        box_team.setEnabled(false);
        round_img_profile.setEnabled(false);
    }

    public void hidePopUp() {
        //show pop up
        linear_popup.setVisibility(View.GONE);
        //anim_out_from_bottom animation
        anim_out_from_bottom.reset();
        //set listview animation.
        linear_popup.startAnimation(anim_out_from_bottom);

        ((MainActivity) getActivity()).enableViewPager();
        btn_history_sick.setEnabled(true);
        btn_history_personal.setEnabled(true);
        btn_history_other.setEnabled(true);
        box_request.setEnabled(true);
        box_team.setEnabled(true);
        round_img_profile.setEnabled(true);
    }

    private void runonUiToast(final String message) {
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
            }
        });
    }

    private OnListenerFromWSManager onResponseLogout = new OnListenerFromWSManager() {
        @Override
        public void onComplete(String statusCode, String message) {
            dismissDialog();

            saveSharedPreference = new SaveSharedPreference(getActivity());
            saveSharedPreference.saveToken("");
            saveSharedPreference.saveJSON_LOGIN("");
            saveSharedPreference.saveHBDStatus(false);

            saveSharedPreference.saveHBDStatusRequest(true);
            saveSharedPreference.saveHBDStatusMessage(true);
            saveSharedPreference.saveDayoffStatusMessage(true);

            ShortcutBadger.with(getActivity()).count(0);
           // ShortcutBadger.applyCount(getActivity(),0);

            Intent logoutIntent = new Intent(getActivity(), LoginActivity.class);
            startActivity(logoutIntent);
            getActivity().finish();
        }

        @Override
        public void onFailed(String statusCode, String message) {
            dismissDialog();
            runonUiToast(message);
        }

        @Override
        public void onFailed(String message) {
            dismissDialog();
        }

        @Override
        public void onFailedAndForceLogout() {
            dismissDialog();
            if (!AwareUtils.isForceLogout) {
                AwareUtils.forceLogOut(getActivity());
            }
        }
    };

    private OnListenerFromWSManager onResponseBadge = new OnListenerFromWSManager() {
        @Override
        public void onComplete(String statusCode, String message) {
            dismissDialog();
            Gson gson = new Gson();
            BadgeResponse badgecount = gson.fromJson(message, BadgeResponse.class);
            List<BadgeResponse.DataEntity> dataEntity = badgecount.getData();
            int annual_count = 0;
            int dire_count = 0;
            int other_count = 0;
            int sick_count = 0;
            int supv_count = 0;

            for (BadgeResponse.DataEntity data : dataEntity) {
                if (data.getLEAVE_MODULE().equalsIgnoreCase(LMSGeneralConstant.BADGE_COUNT_READ.LEAVE_MODULE_ANNAUL)) {
                    annual_count = Integer.parseInt(data.getBADGE_COUNT());
                } else if (data.getLEAVE_MODULE().equalsIgnoreCase(LMSGeneralConstant.BADGE_COUNT_READ.LEAVE_MODULE_DIRE)) {
                    dire_count = Integer.parseInt(data.getBADGE_COUNT());
                } else if (data.getLEAVE_MODULE().equalsIgnoreCase(LMSGeneralConstant.BADGE_COUNT_READ.LEAVE_MODULE_OTHER)) {
                    other_count = Integer.parseInt(data.getBADGE_COUNT());
                } else if (data.getLEAVE_MODULE().equalsIgnoreCase(LMSGeneralConstant.BADGE_COUNT_READ.LEAVE_MODULE_SICK)) {
                    sick_count = Integer.parseInt(data.getBADGE_COUNT());
                } else if (data.getLEAVE_MODULE().equalsIgnoreCase(LMSGeneralConstant.BADGE_COUNT_READ.LEAVE_MODULE_SUPV)) {
                    supv_count = Integer.parseInt(data.getBADGE_COUNT());
                }
            }
            int emp_count = 0;
            String emp_role = application.getCurProfile().getEMP_ROLE();
            if (emp_role.equalsIgnoreCase(LMSGeneralConstant.EMP_ROLE.DIRECTOR_TEXT)) {
                emp_count = supv_count + dire_count;
            } else if (emp_role.equalsIgnoreCase(LMSGeneralConstant.EMP_ROLE.SUPERVISOR_TEXT)) {
                emp_count = supv_count;
            } else {
                emp_count = 0;
            }

            application.setIntAnnualShowBadge(annual_count);
            application.setIntEmpShowBadge(emp_count);
            application.setIntOtherShowBadge(other_count);
            application.setIntSickLeaveShowBadge(sick_count);

            setBadgeView();
        }

        @Override
        public void onFailed(String statusCode, String message) {
            dismissDialog();
        }

        @Override
        public void onFailed(String message) {
            dismissDialog();
        }

        @Override
        public void onFailedAndForceLogout() {
            dismissDialog();
            dismissRefresh();
            if (!AwareUtils.isForceLogout) {
                AwareUtils.forceLogOut(getActivity());
            }
        }
    };

    private OnListenerFromWSManager onResponseFeedback = new OnListenerFromWSManager() {
        @Override
        public void onComplete(String statusCode, String message) {
            DialogUtils.showAlertDialog(getActivity(), "Send feedback complete.", false);
        }

        @Override
        public void onFailed(String statusCode, String message) {

        }

        @Override
        public void onFailed(String message) {

        }

        @Override
        public void onFailedAndForceLogout() {
            dismissDialog();
            dismissRefresh();
            if (!AwareUtils.isForceLogout) {
                AwareUtils.forceLogOut(getActivity());
            }
        }
    };

    private OnListenerFromWSManager onListenStaff = new OnListenerFromWSManager() {
        @Override
        public void onComplete(String statusCode, String message) {

            dismissDialog();

            Gson gson = new Gson();
            StaffSummaryResponse staffs = gson.fromJson(message, StaffSummaryResponse.class);
            List<StaffSummaryResponse.DataEntity> staffSummaries = staffs.getData();

            ResponseEntity resEntity = staffs.getResponse();
            int sickMinutes = 0;
            int personalMinutes = 0;
            int otherMinute = 0;

            for (final StaffSummaryResponse.DataEntity model : staffSummaries) {

                if (model.getLEAVENAME().equalsIgnoreCase("Birthday Leave") && model.getALLDAYFORLEAVE().equals("0")) {//check pass pro
                    statusPro = 0;
                } else if (model.getLEAVENAME().equalsIgnoreCase("Birthday Leave") && !model.getALLDAYFORLEAVE().equals("0")) {
                    statusPro = 1;
                }

                if (model.getLEAVENAME().equalsIgnoreCase("Birthday Leave")) {
                    checkHDB(resEntity.getServer_time(), statusPro, model.getALLDAYFORLEAVE(), model.getALLTIMEUSED());
                }

                if (model.getLEAVENAME().equalsIgnoreCase("Annual Leave")) {
                    if (model.getALLTIMEUSED() != null) {
                        personalMinutes = Integer.parseInt(model.getALLTIMEUSED());
                    }
                } else if (model.getLEAVENAME().equalsIgnoreCase("Sick Leave")) {
                    if (model.getALLTIMEUSED() != null) {
                        sickMinutes = Integer.parseInt(model.getALLTIMEUSED());
                    }
                } else {
                    if (model.getALLTIMEUSED() != null) {
                        otherMinute += Integer.parseInt(model.getALLTIMEUSED());
                    }
                }
            }

            final int sickDay = sickMinutes / 480;
            final float sickHours = (sickMinutes % 480.0f) / 60.0f;

            String strSickDay = "";
            String strSickHour = "";
            //Day
            if (sickDay == 0 || sickDay == 1) {
                strSickDay = sickDay + " Day";
            } else {
                strSickDay = sickDay + " Days";
            }

            //Hour
            if (sickHours <= 1) {
                strSickHour = (sickHours % 1 == 0 ? (int) sickHours + "" : sickHours + "") + " Hour";
            } else {
                strSickHour = (sickHours % 1 == 0 ? (int) sickHours + "" : sickHours + "") + " Hours";
            }


            final int personalDay = personalMinutes / 480;
            final float personalHour = (personalMinutes % 480.0f) / 60.0f;

            String strPersonalDay = "";
            String strPersonalHour = "";
            //Day
            if (personalDay == 0 || personalDay == 1) {
                strPersonalDay = personalDay + " Day";
            } else {
                strPersonalDay = personalDay + " Days";
            }

            //Hour
            if (personalHour <= 1) {
                strPersonalHour = (personalHour % 1 == 0 ? (int) personalHour + "" : personalHour + "") + " Hour";
            } else {
                strPersonalHour = (personalHour % 1 == 0 ? (int) personalHour + "" : personalHour + "") + " Hours";
            }

            final int otherDay = otherMinute / 480;
            final float otherHour = (otherMinute % 480.0f) / 60.0f;

            String strOtherDay = "";
            String strOtherHour = "";
            //Day
            if (otherDay == 0 || otherDay == 1) {
                strOtherDay = otherDay + " Day";
            } else {
                strOtherDay = otherDay + " Days";
            }

            //Hour
            if (otherHour <= 1) {
                strOtherHour = (otherHour % 1 == 0 ? (int) otherHour + "" : otherHour + "") + " Hour";
            } else {
                strOtherHour = (otherHour % 1 == 0 ? (int) otherHour + "" : otherHour + "") + " Hours";
            }

            final String finalStrSickDay = strSickDay;
            final String finalStrSickHour = strSickHour;
            final String finalStrPersonalDay = strPersonalDay;
            final String finalStrPersonalHour = strPersonalHour;
            final String finalStrOtherDay = strOtherDay;
            final String finalStrOtherHour = strOtherHour;

            text_daySick.setText(finalStrSickDay);
            text_houSick.setText(finalStrSickHour);
            text_daysick_personal.setText(finalStrPersonalDay);
            text_housick_personal.setText(finalStrPersonalHour);
            text_daysick_other.setText(finalStrOtherDay);
            text_housick_other.setText(finalStrOtherHour);

            if (swipeView.isRefreshing())
                swipeView.setRefreshing(false);
        }

        @Override
        public void onFailed(String statusCode, String message) {
            dismissDialog();
            if (swipeView.isRefreshing())
                swipeView.setRefreshing(false);
        }

        @Override
        public void onFailed(String message) {
            dismissDialog();
            if (swipeView.isRefreshing())
                swipeView.setRefreshing(false);
        }

        @Override
        public void onFailedAndForceLogout() {
            dismissDialog();
            dismissRefresh();
            if (!AwareUtils.isForceLogout) {
                AwareUtils.forceLogOut(getActivity());
            }
        }
    };

    private void checkUpdateVersion(final String versionServer, final Boolean forceUpdate) {

        float versionAppF = Float.parseFloat(versionApp);
        float versionServerF = Float.parseFloat(versionServer);

        Log.i("checkUpdateVersion", "versionAppF: " + versionAppF);
        Log.i("checkUpdateVersion", "versionServerF: " + versionServerF);

        if (versionAppF >= versionServerF) {
            box_alert_first_update.setVisibility(View.GONE);
        } else {
            box_alert_first_update.setVisibility(View.VISIBLE);
        }
    }

    private void setBadgeView() {
        if (application.getIntSickLeaveShowBadge() != 0) {
            int badgeInt = application.getIntSickLeaveShowBadge();
            String badgeString = "" + badgeInt;
            if (badgeString.length() == 1) {
                badgeString = " " + badgeString + " ";
            }
            badge_sick.setText(badgeString);
            badge_sick.show();
        } else {
            badge_sick.hide();
        }

        if (application.getIntAnnualShowBadge() != 0) {
            int badgeInt = application.getIntAnnualShowBadge();
            String badgeString = "" + badgeInt;
            if (badgeString.length() == 1) {
                badgeString = " " + badgeString + " ";
            }
            badge_personal.setText(badgeString);
            badge_personal.show();
        } else {
            badge_personal.hide();
        }

        if (application.getIntOtherShowBadge() != 0) {
            int badgeInt = application.getIntOtherShowBadge();
            String badgeString = "" + badgeInt;
            if (badgeString.length() == 1) {
                badgeString = " " + badgeString + " ";
            }
            badge_other.setText(badgeString);
            badge_other.show();
        } else {
            badge_other.hide();
        }

        if (application.getIntEmpShowBadge() != 0) {
            int badgeInt = application.getIntEmpShowBadge();
            String badgeString = "" + badgeInt;
            if (badgeString.length() == 1) {
                badgeString = " " + badgeString + " ";
            }
            badge_emp.setText(badgeString);
            badge_emp.show();
        } else {
            badge_emp.hide();
        }

        int c = application.getIntSickLeaveShowBadge() + application.getIntEmpShowBadge() + application.getIntOtherShowBadge() + application.getIntAnnualShowBadge();
        ShortcutBadger.with(getActivity()).count(c);
       // ShortcutBadger.applyCount(getActivity(),c);
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);

        if (this.isVisible()) {
            if (!isVisibleToUser) {
                Log.e("", "isVisibleToUser : " + isVisibleToUser);
                isVisibleView = false;
            } else {
                setBadgeView();
                ((MainActivity) getActivity()).setIsApplyBirthDay(false);
                isVisibleView = true;
            }
        }
    }

    private void readDayoff() {
        LMSWSManager.checkForDayOff(getActivity(), application.getCurProfile().getSTAFF_TOKEN(), new OnListenerFromWSManager() {//Holiday
            @Override
            public void onComplete(String statusCode, String message) {
                dismissDialog();
                dismissRefresh();
                Gson gson = new Gson();
                CheckForDayOffResponse checkDayoff = gson.fromJson(message, CheckForDayOffResponse.class);
                final List<CheckForDayOffResponse.DataEntity> dataEntity = checkDayoff.getData();
                ResponseEntity resEntity = checkDayoff.getResponse();

                if (dataEntity.size() == 0) {
                    box_dayoff.setVisibility(View.GONE);
                } else {

                    if (saveSharedPreference.getDayoffStatusMessage()) {//show message
                        DialogUtils.showAlertDialog(getActivity(), getString(R.string.alert_dayOff), false, new RequestDialogListener() {
                            @Override
                            public void responseSubmit() {
                                saveSharedPreference.saveDayoffStatusMessage(false);
                            }

                            @Override
                            public void responseCancel() {

                            }
                        });
                    }

                    box_dayoff.setVisibility(View.VISIBLE);
                    int j = 0;
                    double totalHour = 0.00;
                    String timeHaveHour = "";
                    for (CheckForDayOffResponse.DataEntity data : dataEntity) {

                        String balance = data.getBALANCE();
                        int time = Integer.parseInt(balance);
                        if (j == 0) {
                            int timeExp = 0;
                            int numExp = 1;
                            String serverTime = resEntity.getServer_time();
                            String stStart = serverTime;
                            serverTime = stStart.substring(0, 10);

                            String stEnd = data.getRD_EXPIREDATE();
                            String expTime = stEnd.substring(0, 10);

                            List<Date> dates = getDates(serverTime, expTime, "yyyy-MM-dd");
                            for (Date date : dates) {
                                timeExp++;
                            }

                            text_amountdayoff.setText(String.valueOf(timeExp - numExp));

                            int int_balance = Integer.parseInt(balance);
                            int dayBalance = (int) (int_balance / 480.0f);
                            float hourBalance = (int_balance % 480.0f) / 60.0f;

                            String test_balance = "";
                            //s day
                            String textDays = " days ";
                            if (dayBalance <= 1) {
                                textDays = " day ";
                            }
                            if (dayBalance > 0)
                                test_balance += dayBalance + textDays;
                            //hours balance
                            if (hourBalance > 0)
                                test_balance += (hourBalance % 1 == 0 ? (int) hourBalance : hourBalance) + (hourBalance <= 1 ? " hour" : " hours");

                            //expire day
                            int expireDay = timeExp - numExp;
                            String strExpireDay = "";
                            strExpireDay += expireDay;
                            strExpireDay += expireDay <= 1 ? " day" : " days";
                            expireDay++;

                            String msg = "";
                            if (expireDay == 2) {
                                msg = "You have " + test_balance + " left to use and tomorrow is your last day to apply so hurry up!!";
                                text_amountdayoff.setVisibility(View.VISIBLE);
                            } else if (expireDay == 1) {
                                msg = "You have " + test_balance + " left to use and it expires today!";
                                text_amountdayoff.setVisibility(View.GONE);
                            } else {
                                msg = "You have " + test_balance + " left to use and have " + strExpireDay + " left before it expires!";
                                text_amountdayoff.setVisibility(View.VISIBLE);
                            }

                            text_message_dayoff.setText(msg);
                            break;
                        }
                        j++;
                    }
                }
            }

            @Override
            public void onFailed(String statusCode, final String message) {
                dismissDialog();
                dismissRefresh();
            }

            @Override
            public void onFailed(final String message) {
                dismissDialog();
                dismissRefresh();
            }

            @Override
            public void onFailedAndForceLogout() {
                dismissDialog();
                dismissRefresh();
                if (!AwareUtils.isForceLogout) {
                    AwareUtils.forceLogOut(getActivity());
                }
            }
        });
    }

    public static List<Date> getDates(String dateString1, String dateString2, String formats) {
        ArrayList<Date> dates = new ArrayList<Date>();
        DateFormat df1 = new SimpleDateFormat(formats);

        Date date1 = null;
        Date date2 = null;

        try {
            date1 = df1.parse(dateString1);
            date2 = df1.parse(dateString2);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        Calendar cal1 = Calendar.getInstance();
        cal1.setTime(date1);


        Calendar cal2 = Calendar.getInstance();
        cal2.setTime(date2);

        while (!cal1.after(cal2)) {
            dates.add(cal1.getTime());
            cal1.add(Calendar.DATE, 1);
        }
        return dates;
    }

    private void showAlertDialog() {
        if (!dialogWaiting.isShowing() && isVisibleView) {
            dialogWaiting.show();
        }
    }

    private void dismissDialog() {
        if (dialogWaiting != null) {
            if (dialogWaiting.isShowing() && --countAlert <= 0) {
                dialogWaiting.dismiss();
                countAlert = 0;
            }
        }
    }

    private void dismissRefresh() {
        if (swipeView != null) {
            if (swipeView.isRefreshing())
                swipeView.setRefreshing(false);
        }
    }

    OnListenerFromWSManager onResponseUpload = new OnListenerFromWSManager() {
        @Override
        public void onComplete(String statusCode, String message) {
            dismissDialog();
            dismissRefresh();
            Gson gson = new Gson();
            ImageUploadResponse imageUploadResponse = gson.fromJson(message, ImageUploadResponse.class);
            ImageUploadResponse.DataEntity dataEntity = imageUploadResponse.getData();
            updateImageUpload(dataEntity.getImgUrl());
        }

        @Override
        public void onFailed(String statusCode, final String message) {
            dismissDialog();
            dismissRefresh();
            DialogUtils.showAlertDialog(getActivity(), message, false);
        }

        @Override
        public void onFailed(final String message) {
            dismissDialog();
            dismissRefresh();
            DialogUtils.showAlertDialog(getActivity(), message, false);
        }

        @Override
        public void onFailedAndForceLogout() {
            dismissDialog();
            dismissRefresh();
            if (!AwareUtils.isForceLogout) {
                AwareUtils.forceLogOut(getActivity());
            }
        }
    };

    private void updateImageUpload(String imagePath) {
        if (imagePath != null) {
            if (!imagePath.equalsIgnoreCase("")) {
                ImageLoader.getInstance().displayImage(LMSRestClient.getImageURL() + imagePath, round_img_profile);
                saveSharedPreference.saveIMAGE(imagePath);
            }
        }
        btn_save_image.setVisibility(View.INVISIBLE);
    }

    private void getProfile() {
        showAlertDialog();
        LMSWSManager.getProfile(getActivity(), application.getCurProfile().getSTAFF_TOKEN(), new OnListenerFromWSManager() {
            @Override
            public void onComplete(String statusCode, String message) {
                dismissDialog();
                dismissRefresh();
                Gson gson = new Gson();
                ProfileResponse responseGson = gson.fromJson(message, ProfileResponse.class);
                List<ProfileResponse.DataEntity> dataEntity = responseGson.getData();
                updateImageUpload(dataEntity.get(0).getPROFILE_IMAGE_PATH());
            }

            @Override
            public void onFailed(String statusCode, String message) {
                dismissDialog();
                dismissRefresh();
            }

            @Override
            public void onFailed(String message) {
                dismissDialog();
                dismissRefresh();
            }

            @Override
            public void onFailedAndForceLogout() {
                dismissDialog();
                dismissRefresh();
                if (!AwareUtils.isForceLogout) {
                    AwareUtils.forceLogOut(getActivity());
                }
            }
        });
    }

    private void CallWebview() {
        webView.setVisibility(View.VISIBLE);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.setWebViewClient(new MyWebViewClient());//WebViewClient()

        webView.getSettings().setUseWideViewPort(true);
        webView.getSettings().setLoadWithOverviewMode(true);
        webView.loadUrl("https://ats-eleave.aware.co.th/");//production
    }

    @Override
    public void onBack() {
        if (isVisibleView) {
            if (linear_popup.getVisibility() == View.VISIBLE) {
                hidePopUp();
            } else {
                getActivity().finish();
            }
        }
    }

    private class MyWebViewClient extends WebViewClient {

        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
        }

        @Override
        public void onPageFinished(WebView view, String url) {
        }

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.loadUrl(url);

            versionUpdate = saveSharedPreference.getVersionUpdate();
            versionUpdate = versionUpdate.replace(".", "-");

            Log.i("clickDownload", "clickDownload: " + url.contains(".apk"));
            Log.i("url", "url: " + url);

            if (url.contains(".apk")) {
                File file = new File("/sdcard/Download/atseleave_" + versionUpdate + ".apk");
                if (file.isFile()) {
                    file.delete();
                }

                UpdateApp atualizaApp = new UpdateApp(view.getContext());
                atualizaApp.execute(url);
            }
            return false;
        }

    }

    public class UpdateApp extends AsyncTask<String, Void, Void> {
        private Context context;

        public UpdateApp(Context c) {
            this.context = c;
            pd = new ProgressDialog(context);
            pd.setMessage("Loading...");
            pd.setCanceledOnTouchOutside(false);
            pd.show();
        }

        @Override
        protected Void doInBackground(String... arg0) {
            try {
                URL url = new URL(arg0[0]);
                HttpURLConnection c = (HttpURLConnection) url.openConnection();
                c.setRequestMethod("GET");
                c.setDoOutput(true);
                c.connect();

                String PATH = "/mnt/sdcard/Download/";
                File file = new File(PATH);
                file.mkdirs();
                File outputFile = new File(file, "atseleave_" + versionUpdate + ".apk");
                if (outputFile.exists()) {
                    outputFile.delete();
                }
                FileOutputStream fos = new FileOutputStream(outputFile);

                InputStream is = c.getInputStream();

                byte[] buffer = new byte[1024];
                int len1 = 0;
                while ((len1 = is.read(buffer)) != -1) {
                    fos.write(buffer, 0, len1);
                }
                fos.close();
                is.close();

                Intent intent = new Intent(Intent.ACTION_VIEW);
                intent.setDataAndType(Uri.fromFile(new File("/mnt/sdcard/Download/atseleave_" + versionUpdate + ".apk")), "application/vnd.android.package-archive");
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(intent);
            } catch (Exception e) {
                Log.e("UpdateAPP", "Update error! " + e.getMessage());
            }
            pd.dismiss();
            return null;
        }
    }
}
