package android.mobile.lms.aware.com.atsleave;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.mobile.lms.aware.com.atsleave.constant.SaveSharedPreference;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

public class UpdateVersionActivity extends CustomActivity {
    WebView webView;
    private SaveSharedPreference saveSharedPreference;
    private String versionUpdate;
    private Animation fadein;
    private LinearLayout lyNoti;
    private LinearLayout lyDownload;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_version);
        saveSharedPreference = new SaveSharedPreference(this);
        ImageView icon_download = (ImageView) findViewById(R.id.icon_download);
        TextView btn_dowload = (TextView) findViewById(R.id.btn_download);

        lyNoti = (LinearLayout) findViewById(R.id.lyNoti);
        lyDownload = (LinearLayout) findViewById(R.id.lyDownload);

        fadein = AnimationUtils.loadAnimation(UpdateVersionActivity.this, R.anim.fade_in_on_bottom);

        icon_download.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CallWebview();
            }
        });

        btn_dowload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CallWebview();

            }
        });
        //lyNoti.startAnimation(fadein);
        //lyDownload.startAnimation(fadein);
    }

    private void CallWebview() {

        webView = (WebView) findViewById(R.id.mybrowser);
        webView.setVisibility(View.VISIBLE);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.setWebViewClient(new MyWebViewClient());//WebViewClient()
        // webView.setWebChromeClient(new  WebChromeClient() ); //WebChromeClient()
        // webView.getSettings().setBuiltInZoomControls(true);
        webView.getSettings().setUseWideViewPort(true);
        webView.getSettings().setLoadWithOverviewMode(true);
        webView.loadUrl("https://ats-eleave.aware.co.th/");//production

    }

    private class MyWebViewClient extends WebViewClient {

        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
        }

        @Override
        public void onPageFinished(WebView view, String url) {
        }

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.loadUrl(url);
            versionUpdate = saveSharedPreference.getVersionUpdate();
            versionUpdate = versionUpdate.replace(".", "-");

            Log.i("clickDownload", "clickDownload: " + url.contains(".apk"));
            Log.i("url", "url: " + url);
            if (url.contains(".apk")) {
                File file = new File("/sdcard/Download/atseleave_" + versionUpdate + ".apk");
                if (file.isFile()) {
                    file.delete();
                }

                UpdateApp atualizaApp = new UpdateApp(view.getContext());
                atualizaApp.execute(url);

            }
            return false;
        }

    }

    public class UpdateApp extends AsyncTask<String, String, Void> {
        private Context context;
        private ProgressDialog pd;

        public UpdateApp(Context context) {
            this.context = context;
        }

        @Override
        protected void onPreExecute() {
            pd = new ProgressDialog(context);
            pd.setMessage("Loading...");
            //pd.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
            pd.setCanceledOnTouchOutside(false);
            pd.show();
        }

        @Override
        protected Void doInBackground(String... arg0) {
            try {
                URL url = new URL(arg0[0]);
                HttpURLConnection c = (HttpURLConnection) url.openConnection();
                c.setRequestMethod("GET");
                c.setDoOutput(true);
                c.connect();
                long lenghtOfFile = c.getContentLength();
                String PATH = "/mnt/sdcard/Download/";
                File file = new File(PATH);
                file.mkdirs();
                File outputFile = new File(file, "atseleave_" + versionUpdate + ".apk");
                if (outputFile.exists()) {
                    outputFile.delete();
                }
                FileOutputStream fos = new FileOutputStream(outputFile);
                InputStream is = c.getInputStream();

                byte[] buffer = new byte[1024];
                long total = 0;
                int len1 = 0;
                while ((len1 = is.read(buffer)) != -1) {
                    total += len1;
                    publishProgress("" + (int) ((total * 100) / lenghtOfFile));
                    fos.write(buffer, 0, len1);
                }
                fos.close();
                is.close();

                Intent intent = new Intent(Intent.ACTION_VIEW);
                intent.setDataAndType(Uri.fromFile(new File("/mnt/sdcard/Download/atseleave_" + versionUpdate + ".apk")), "application/vnd.android.package-archive");
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(intent);

            } catch (Exception e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onProgressUpdate(String... values) {
            //Log.d("ANDRO_ASYNC", values[0]);
            //pd.setProgress(Integer.parseInt(values[0]));
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            pd.dismiss();
        }
    }

}