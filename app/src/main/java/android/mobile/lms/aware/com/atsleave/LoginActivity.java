package android.mobile.lms.aware.com.atsleave;

import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.mobile.lms.aware.com.atsleave.callback.OnListenerFromWSManager;
import android.mobile.lms.aware.com.atsleave.callback.RequestDialogListener;
import android.mobile.lms.aware.com.atsleave.constant.SaveSharedPreference;
import android.mobile.lms.aware.com.atsleave.models.LoginResponse;
import android.mobile.lms.aware.com.atsleave.models.Username;
import android.mobile.lms.aware.com.atsleave.service.QuickstartPreferences;
import android.mobile.lms.aware.com.atsleave.utils.AwareUtils;
import android.mobile.lms.aware.com.atsleave.utils.DialogUtils;
import android.mobile.lms.aware.com.atsleave.webservice.LMSWSManager;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.google.gson.Gson;

public class LoginActivity extends CustomActivity implements OnClickListener {

    private String TAG = "LoginActivity";
    private EditText edit_user;
    private EditText edit_pass;
    private LinearLayout linear_edit_form;
    private static final int STATE_LOGIN_SHOW = 0;
    private static final int STATE_LOGIN_HIDE = 1;
    private int state_login = STATE_LOGIN_HIDE;
    //Animation
    private Animation slideIn_on_top_and_fadeIin;
    private Animation slideOut_on_top_and_fadeOut;
    private SaveSharedPreference sharedPreference;
    private String versionApp = "";

    //----------------Notification ----------------------
    private static final int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;
    private BroadcastReceiver mRegistrationBroadcastReceiver;
    private Dialog dialogWaiting;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        FirebaseInstanceId.getInstance().getInstanceId()
                .addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
                    @Override
                    public void onComplete(@NonNull Task<InstanceIdResult> task) {
                        if (!task.isSuccessful()) {
                            Log.w(TAG, "getInstanceId failed", task.getException());
                            return;
                        }

                        // Get new Instance ID token
                        String token = task.getResult().getToken();
                        Log.e("DeviceToken",token);
                    }
                });


        dialogWaiting = DialogUtils.createDialog(this);
        sharedPreference = new SaveSharedPreference(this);

        //-----------------------Notification Android-----------------------------
        mRegistrationBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
                boolean sentToken = sharedPreferences.getBoolean(QuickstartPreferences.SENT_TOKEN_TO_SERVER, false);
                if (sentToken) {
                    Log.e(TAG, "sent Complete");
                } else {
                    Log.e(TAG, "sent Failed");
                }
            }
        };

        if (checkPlayServices()) {
//            Intent intent = new Intent(this, RegistrationIntentService.class);
//            startService(intent);
        }

        PackageInfo pInfo = null;
        try {
            pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
            versionApp = pInfo.versionName;
        } catch (Exception e) {
            e.printStackTrace();
        }

        TextView version_number = (TextView) findViewById(R.id.version_number);
        version_number.setText("Version: " + versionApp);
        //-------------------------Notification Android-----------------------------

        application = (LMSApplication) getApplication();
        Button btn_signin = (Button) findViewById(R.id.btn_signin);
        edit_user = (EditText) findViewById(R.id.edit_user);
        edit_pass = (EditText) findViewById(R.id.edit_pass);
        edit_user.setText(sharedPreference.getUser());

        linear_edit_form = (LinearLayout) findViewById(R.id.linear_edit_form);

        slideIn_on_top_and_fadeIin = AnimationUtils.loadAnimation(LoginActivity.this, R.anim.slidein_on_top_and_fade_in);
        slideOut_on_top_and_fadeOut = AnimationUtils.loadAnimation(LoginActivity.this, R.anim.slideout_on_top_and_fade_out);
        slideOut_on_top_and_fadeOut.setAnimationListener(animationOutlistener);
        slideIn_on_top_and_fadeIin.setAnimationListener(animationInListener);

        btn_signin.setOnClickListener(this);

        checkLogin();
    }

    private void checkLogin() {
        if (!AwareUtils.isOnline(LoginActivity.this)) {
            DialogUtils.showAlertDialog(LoginActivity.this, "No Internet Connection.", false);
            return;
        }

        sharedPreference = new SaveSharedPreference(this);
        String jsonData = sharedPreference.getLOGIN_JSON();
        if (!jsonData.equalsIgnoreCase("")) {
            getProfileData(jsonData);
        }
    }

    private void getProfileData(String json_login) {
        Gson gson = new Gson();
        LoginResponse profileData = gson.fromJson(json_login, LoginResponse.class);
        LoginResponse.DataEntity profile = profileData.getData();

        Username username = new Username();
        username.username = edit_user.getText().toString();
        username.password = edit_pass.getText().toString();

        sharedPreference.saveToken(profile.getSTAFF_TOKEN());

        if (profile.getBIRTH_DATE() != null && profile.getEMP_CODE() != null
                && profile.getEMAIL() != null && profile.getOFFICIAL_DATE() != null
                && profile.getJOIN_DATE() != null) {
            Intent goSecond1 = new Intent(LoginActivity.this, MainActivity.class);
            goSecond1.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivityForResult(goSecond1, 0);
            finish();
        }
    }


    private boolean checkInput(String username, String password) {
        boolean isCorrect = true;
        String message = "";

        if (username.equalsIgnoreCase("")) {
            message = getString(R.string.alert_text_login_username_null);
            isCorrect = false;
        } else if (password.equalsIgnoreCase("")) {
            message = getString(R.string.alert_text_login_password_null);
            isCorrect = false;
        }

        if (!isCorrect)
            Toast.makeText(LoginActivity.this, message, Toast.LENGTH_SHORT).show();

        return isCorrect;
    }

    Animation.AnimationListener animationInListener = new Animation.AnimationListener() {
        @Override
        public void onAnimationStart(Animation animation) {

        }

        @Override
        public void onAnimationEnd(Animation animation) {

        }

        @Override
        public void onAnimationRepeat(Animation animation) {

        }
    };

    Animation.AnimationListener animationOutlistener = new Animation.AnimationListener() {
        @Override
        public void onAnimationStart(Animation animation) {

        }

        @Override
        public void onAnimationEnd(Animation animation) {

        }

        @Override
        public void onAnimationRepeat(Animation animation) {

        }
    };

    /**
     * Check the device to make sure it has the Google Play Services APK. If
     * it doesn't, display a dialog that allows users to download the APK from
     * the Google Play Store or enable it in the device's system settings.
     */
    private boolean checkPlayServices() {
        GoogleApiAvailability googleApi = GoogleApiAvailability.getInstance();
        int resultCode = googleApi.isGooglePlayServicesAvailable(this);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (googleApi.isUserResolvableError(resultCode)) {
                googleApi.getErrorDialog(this, resultCode, PLAY_SERVICES_RESOLUTION_REQUEST).show();
            } else {
                Log.i(TAG, "This device is not supported.");
                finish();
            }
            return false;
        }
        return true;
    }

    @Override
    protected void onResume() {
        super.onResume();
        LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver, new IntentFilter(QuickstartPreferences.REGISTRATION_COMPLETE));
    }

    @Override
    protected void onPause() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mRegistrationBroadcastReceiver);
        super.onPause();
    }

    private void doLogin() {
        if (!AwareUtils.isOnline(LoginActivity.this)) {
            DialogUtils.showAlertDialog(LoginActivity.this, "No Internet Connection.", false);
            return;
        }

        String username = edit_user.getText().toString();
        String password = edit_pass.getText().toString();
        SaveSharedPreference saveSharedPreference = new SaveSharedPreference(LoginActivity.this);
        String deviceToken = saveSharedPreference.getDeviceToken();

        if (checkInput(username, password)) {
            saveSharedPreference.saveUSER(username);
            if (!dialogWaiting.isShowing()) {
                dialogWaiting.show();
            }
            if (BuildConfig.ServerID == 0) {
                LMSWSManager.doLogin(LoginActivity.this, username, password, deviceToken, onLoginListener);
            } else {
                LMSWSManager.doLogin(LoginActivity.this, username, deviceToken, onLoginListener);
            }
        }
    }

    private OnListenerFromWSManager onLoginListener = new OnListenerFromWSManager() {
        @Override
        public void onComplete(String statusCode, String message) {

            Gson gson = new Gson();
            LoginResponse profileData = gson.fromJson(message, LoginResponse.class);
            LoginResponse.DataEntity data = profileData.getData();

            sharedPreference.saveJSON_LOGIN(message);

            Username username = new Username();
            username.username = edit_user.getText().toString();
            username.password = edit_pass.getText().toString();

            sharedPreference.saveToken(data.getSTAFF_TOKEN());
            sharedPreference.saveIMAGE(data.getPROFILE_IMAGE_PATH());


            if (dialogWaiting.isShowing()) {
                dialogWaiting.dismiss();
            }

            Intent logoutIntent = new Intent(LoginActivity.this, MainActivity.class);
            logoutIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(logoutIntent);
            LoginActivity.this.finish();
            overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
        }

        @Override
        public void onFailed(String statusCode, final String message) {
            if (dialogWaiting.isShowing()) {
                dialogWaiting.dismiss();
            }

            DialogUtils.showAlertDismissofRetry(LoginActivity.this, message, new RequestDialogListener() {
                @Override
                public void responseSubmit() {
                    doLogin();
                }

                @Override
                public void responseCancel() {

                }
            });

        }

        @Override
        public void onFailed(final String message) {
            if (dialogWaiting.isShowing()) {
                dialogWaiting.dismiss();
            }

            DialogUtils.showAlertDismissofRetry(LoginActivity.this, message, new RequestDialogListener() {
                @Override
                public void responseSubmit() {
                    doLogin();
                }

                @Override
                public void responseCancel() {

                }
            });

        }

        @Override
        public void onFailedAndForceLogout() {
            if (dialogWaiting.isShowing()) {
                dialogWaiting.dismiss();
            }

            DialogUtils.showAlertDialog(LoginActivity.this, getString(R.string.alert_text_log_out), false, new RequestDialogListener() {
                @Override
                public void responseSubmit() {
                    Intent logoutIntent = new Intent(LoginActivity.this, LoginActivity.class);
                    startActivity(logoutIntent);
                    LoginActivity.this.finish();
                }

                @Override
                public void responseCancel() {

                }
            });

        }
    };

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_signin:
                if (state_login == STATE_LOGIN_HIDE) {
                    slideIn_on_top_and_fadeIin.reset();
                    linear_edit_form.clearAnimation();
                    linear_edit_form.startAnimation(slideIn_on_top_and_fadeIin);
                    linear_edit_form.setVisibility(View.VISIBLE);
                    state_login = STATE_LOGIN_SHOW;
                } else {
                    slideOut_on_top_and_fadeOut.reset();
                    linear_edit_form.clearAnimation();
                    doLogin();
                }
                break;
        }
    }
}