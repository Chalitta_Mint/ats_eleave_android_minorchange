package android.mobile.lms.aware.com.atsleave.fragmentview;

import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.mobile.lms.aware.com.atsleave.LMSApplication;
import android.mobile.lms.aware.com.atsleave.R;
import android.mobile.lms.aware.com.atsleave.adapter.DayOffOtherAdapter;
import android.mobile.lms.aware.com.atsleave.adapter.OtherHistoryAdapter;
import android.mobile.lms.aware.com.atsleave.constant.SaveSharedPreference;
import android.mobile.lms.aware.com.atsleave.customview.ExpandableHeightGridView;
import android.mobile.lms.aware.com.atsleave.callback.OnListenerFromWSManager;
import android.mobile.lms.aware.com.atsleave.callback.RequestDialogListener;
import android.mobile.lms.aware.com.atsleave.models.CheckForDayOffResponse;
import android.mobile.lms.aware.com.atsleave.models.LeaveHistoryResponse;
import android.mobile.lms.aware.com.atsleave.models.ResponseEntity;
import android.mobile.lms.aware.com.atsleave.models.StaffSummaryResponse;
import android.mobile.lms.aware.com.atsleave.utils.AwareUtils;
import android.mobile.lms.aware.com.atsleave.utils.DialogUtils;
import android.mobile.lms.aware.com.atsleave.utils.OnChangePageView;
import android.mobile.lms.aware.com.atsleave.webservice.LMSGeneralConstant;
import android.mobile.lms.aware.com.atsleave.webservice.LMSWSManager;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;


import com.allen.expandablelistview.SwipeMenuExpandableCreator;
import com.allen.expandablelistview.SwipeMenuExpandableListView;
import com.baoyz.swipemenulistview2.ContentViewWrapper;
import com.baoyz.swipemenulistview2.SwipeMenu;
import com.baoyz.swipemenulistview2.SwipeMenuItem;
import com.baoyz.swipemenulistview2.SwipeMenuLayout;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.components.Legend;
import com.google.gson.Gson;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;


/**
 * Created by apinun.w on 15/7/2558.
 */
public class OtherHistoryFragment extends Fragment implements View.OnClickListener {
    private static final int DetailId = 111;
    private static final int ApproveId = 222;
    private static final int CancelId = 333;
    private HashMap<Integer, Integer> listSwipeSize = new HashMap<>();
    private OnChangePageView onchangePageView;
    private int groupPosition = 0;
    private int childPosition = 0;
    //-----------------By ton---------------
    List<String> groupList;
    private PieChart mChart1;
    private String leaveType = "";
    //----------------For swap------------
    private LMSApplication application;
    private Dialog alertWaiting;
    private String TAG = "OtherLeaveHistoryFragment";

    private View childView;
    //------------------------------------
    //  private List<ApplicationInfo> mAppList;
    private OtherHistoryAdapter otherHistoryAdapter;
    private SwipeMenuExpandableListView listView;
    private int[] leaveTypesRequest = null;
    private SwipeRefreshLayout swipeView;
    private String reason;

    public static OtherHistoryFragment initOtherFragment(String fragment, OnChangePageView onchangePageView) {
        OtherHistoryFragment otherHistoryFragment = new OtherHistoryFragment();
        otherHistoryFragment.onchangePageView = onchangePageView;
        return otherHistoryFragment;
    }

    private ImageView img_home_img;
    private static int top_editText = -1;
    private static int bottom_editText = -1;

    ExpandableHeightGridView gridView;
    private DayOffOtherAdapter dayOffOtherAdapter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        application = (LMSApplication) getActivity().getApplication();
        alertWaiting = DialogUtils.createDialog(getActivity());
        //set id
        View view = inflater.inflate(R.layout.layout_other_history_page, container, false);
        swipeView = (SwipeRefreshLayout) view.findViewById(R.id.swipeRefreshLayout);
        img_home_img = (ImageView) view.findViewById(R.id.img_home_img);
        gridView = (ExpandableHeightGridView) view.findViewById(R.id.gridView);
        mChart1 = (PieChart) view.findViewById(R.id.chart1);

        leaveTypesRequest = new int[]{2, 4, 5, 6, 7, 8, 9, 10, 11};
        gridView.setExpanded(true);
        dayOffOtherAdapter = new DayOffOtherAdapter(getActivity(), new ArrayList<CheckForDayOffResponse.DataEntity>(), "");
        gridView.setAdapter(dayOffOtherAdapter);

        img_home_img.setOnClickListener(this);

        //set listview and adapter
        listView = (SwipeMenuExpandableListView) view.findViewById(R.id.listView);
        // 7. Create and set a SwipeMenuExpandableCreator
        // define the group and child creator function
        final SwipeMenuExpandableCreator creator = new SwipeMenuExpandableCreator() {

            @Override
            public void createGroup(SwipeMenu menu) {

            }

            @Override
            public void createChild(SwipeMenu menu) {
                SwipeMenuItem menuItem = null;
                switch (menu.getViewType()) {
                    case LMSGeneralConstant.LEAVE_STATUS.Pending:
                        menuItem = createMenuSwapItem("Detail", R.drawable.swipe_icon_detail, R.color.bg_gray, R.color.bg_white);
                        menuItem.setId(DetailId);
                        menu.addMenuItem(menuItem);
                        menuItem = createMenuSwapItem("Cancel", R.drawable.swipe_menu_cancel, R.color.bg_white, R.color.bg_black);
                        menuItem.setId(CancelId);
                        menu.addMenuItem(menuItem);
                        listSwipeSize.put(LMSGeneralConstant.LEAVE_STATUS.Pending, 2);
                        break;
                    case LMSGeneralConstant.LEAVE_STATUS.Canceled:
                        menuItem = createMenuSwapItem("Cancel", R.drawable.btn_close, R.color.bg_white, R.color.bg_black);
                        menuItem.setId(CancelId);
                        menu.addMenuItem(menuItem);
                        listSwipeSize.put(LMSGeneralConstant.LEAVE_STATUS.Canceled, 1);
                        break;
                    case LMSGeneralConstant.LEAVE_STATUS.Approved:
                        menuItem = createMenuSwapItem("Detail", R.drawable.swipe_icon_detail, R.color.bg_gray, R.color.bg_white);
                        menuItem.setId(DetailId);
                        menu.addMenuItem(menuItem);
                        menuItem = createMenuSwapItem("Request to Cancel", R.drawable.swipe_close_white, R.color.bg_black, R.color.bg_white);
                        menuItem.setId(CancelId);
                        menu.addMenuItem(menuItem);
                        listSwipeSize.put(LMSGeneralConstant.LEAVE_STATUS.Approved, 2);
                        break;
                    case LMSGeneralConstant.LEAVE_STATUS.Rejected:
                        menuItem = createMenuSwapItem("Detail", R.drawable.swipe_icon_detail, R.color.bg_gray, R.color.bg_white);
                        menuItem.setId(DetailId);
                        menu.addMenuItem(menuItem);
                        listSwipeSize.put(LMSGeneralConstant.LEAVE_STATUS.Rejected, 1);
                        break;
                    case LMSGeneralConstant.LEAVE_STATUS.PendingCancel:
                        menuItem = createMenuSwapItem("Detail", R.drawable.swipe_icon_detail, R.color.bg_gray, R.color.bg_white);
                        menuItem.setId(DetailId);
                        menu.addMenuItem(menuItem);
                        listSwipeSize.put(LMSGeneralConstant.LEAVE_STATUS.PendingCancel, 1);
                        break;
                    case LMSGeneralConstant.LEAVE_STATUS.ApprovedCancel:
                        menuItem = createMenuSwapItem("Detail", R.drawable.swipe_icon_detail, R.color.bg_gray, R.color.bg_white);
                        menuItem.setId(DetailId);
                        menu.addMenuItem(menuItem);
                        listSwipeSize.put(LMSGeneralConstant.LEAVE_STATUS.ApprovedCancel, 1);
                        break;
                    case LMSGeneralConstant.LEAVE_STATUS.RejectedCancel:
                        menuItem = createMenuSwapItem("Detail", R.drawable.swipe_icon_detail, R.color.bg_gray, R.color.bg_white);
                        menuItem.setId(DetailId);
                        menu.addMenuItem(menuItem);
                        listSwipeSize.put(LMSGeneralConstant.LEAVE_STATUS.RejectedCancel, 1);
                        break;
                    default:
                        menuItem = createMenuSwapItem("Detail", R.drawable.swipe_icon_detail, R.color.bg_gray, R.color.bg_white);
                        menuItem.setId(DetailId);
                        menu.addMenuItem(menuItem);
                        listSwipeSize.put(LMSGeneralConstant.LEAVE_STATUS.RejectedCancel, 1);
                        break;
                }
            }
        };

        listView.setMenuCreator(creator);
        otherHistoryAdapter = new OtherHistoryAdapter(new HashMap<Integer, List<LeaveHistoryResponse.DataEntity>>(), new HashMap<String, Float[]>(), new ArrayList<CheckForDayOffResponse.DataEntity>(), "", getActivity());
        listView.setAdapter(otherHistoryAdapter);

        // 4. Set OnItemLongClickListener
        listView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                int itemType = ExpandableListView.getPackedPositionType(id);
                int childPosition, groupPosition;
                if (itemType == ExpandableListView.PACKED_POSITION_TYPE_CHILD) {
                    childPosition = ExpandableListView.getPackedPositionChild(id);
                    groupPosition = ExpandableListView.getPackedPositionGroup(id);

                    return true; // true if we consumed the click, false if not

                } else if (itemType == ExpandableListView.PACKED_POSITION_TYPE_GROUP) {
                    groupPosition = ExpandableListView.getPackedPositionGroup(id);
                    return true; // true if we consumed the click, false if not

                } else {
                    // null item; we don't consume the click
                    return false;
                }
            }
        });

        // 5.Set OnGroupClickListener
        listView.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {
            @Override
            public boolean onGroupClick(ExpandableListView parent, View v, int groupPosition, long id) {

                return false;
            }
        });

        // 6.Set OnChildClickListener
        listView.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
            @Override
            public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {
                if (v instanceof SwipeMenuLayout) {
                    HashMap<Integer, List<LeaveHistoryResponse.DataEntity>> data = otherHistoryAdapter.getModels();
                    Object[] keys = data.keySet().toArray();
                    if (childPosition != data.get(keys[groupPosition]).size() - 1) {
                        SwipeMenuLayout swipeMenu = (SwipeMenuLayout) v;
                        swipeMenu.smoothOpenMenu();
                        OtherHistoryFragment.this.childView = v;
                    }
                }
                return true;
            }
        });


        // 8. Set OnMenuItemClickListener
        listView.setOnMenuItemClickListener(new SwipeMenuExpandableListView.OnMenuItemClickListenerForExpandable() {

            @Override
            public boolean onMenuItemClick(int groupPosition, int childPosition, SwipeMenu menu, int index) {
                int type = otherHistoryAdapter.getChildType(groupPosition, childPosition);
                int size = listSwipeSize.get(type);
                if (size == 2) {
                    if (index == 0) {
                        showDetailDialog(groupPosition, childPosition);
                    } else if (index == 1) {
                        ContentViewWrapper view = otherHistoryAdapter.getChildViewAndReUsable(groupPosition, childPosition, false, null, null);
                        changeChildListView(ApproveId, groupPosition, childPosition, view.view);
                    }
                } else if (size == 1) {
                    showDetailDialog(groupPosition, childPosition);
                }
                return false;
            }

        });

        swipeView.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                swipeView.setRefreshing(true);

                getHistoryDataType();
                getStaffSummary();
                checkForDayOff();
               
            }
        });

        return view;
    }

    private void setLookUpPieChart() {
        mChart1.setUsePercentValues(true);
        mChart1.setDescription("");
        mChart1.setDragDecelerationFrictionCoef(0.95f);
        mChart1.setDrawHoleEnabled(true);
        mChart1.setHoleColorTransparent(true);
        mChart1.setHoleRadius(70f);
        mChart1.setTransparentCircleRadius(61f);
        mChart1.setDrawCenterText(true);
        mChart1.setRotationEnabled(false);
        mChart1.setNoDataText("");

        Legend l1 = mChart1.getLegend();
        l1.setEnabled(false);
    }

    ///----------------------------  SERVICE ----------------------------------------------

    OnListenerFromWSManager onListenerFromWSManager = new OnListenerFromWSManager() {
        @Override
        public void onComplete(String statusCode, String message) {
            Gson gson = new Gson();
            LeaveHistoryResponse leaveHistoryFromWS = gson.fromJson(message, LeaveHistoryResponse.class);
            ResponseEntity responseEntity = leaveHistoryFromWS.getResponse();
            final List<LeaveHistoryResponse.DataEntity> models = leaveHistoryFromWS.getData();
            final HashMap<Integer, List<LeaveHistoryResponse.DataEntity>> data = new HashMap<>();
            final String serverTime = responseEntity.getServer_time();
//            for(int i = 0 ; i < leaveTypesRequest.length ; i++){
//                data.put(leaveTypesRequest[i],new ArrayList<LeaveHistoryResponse.DataEntity>());
//            }

            for (int i = 0; i < models.size(); i++) {
                String leaveType = models.get(i).getLEAVETYPEID();
                String statusType = models.get(i).getRL_STATUSID();
                Log.e("", "statusType : " + statusType);
                int leaveTypeId = 0;
                if (!(leaveType.trim().equalsIgnoreCase("") || leaveType.trim().equalsIgnoreCase("null"))) {
                    leaveTypeId = Integer.parseInt(leaveType);
                }

                if (data.containsKey(leaveTypeId)) {
                    List<LeaveHistoryResponse.DataEntity> listData = data.get(leaveTypeId);

                    if (!statusType.trim().equals("2")) {
                        listData.add(models.get(i));
                        data.put(leaveTypeId, listData);
                    }

                } else {
                    List<LeaveHistoryResponse.DataEntity> listData = new ArrayList<>();

                    if (!statusType.trim().equals("2")) {
                        listData.add(models.get(i));
                        data.put(leaveTypeId, listData);
                    }
                }
            }

            Object[] objs = data.keySet().toArray();
            for (int i = 0; i < objs.length; i++) {
                List<LeaveHistoryResponse.DataEntity> childTemp = data.get(objs[i]);
                final SimpleDateFormat outFormat = new SimpleDateFormat("yyyyMMddHHss");        //201510290830

//                Collections.sort(childTemp, new Comparator<LeaveHistoryResponse.DataEntity>() {
//                    @Override
//                    public int compare(LeaveHistoryResponse.DataEntity lhs, LeaveHistoryResponse.DataEntity rhs) {
//                        Date lhsDate = null;
//                        Date rhsDate = null;
//                        try {
//                            lhsDate = outFormat.parse(lhs.getRLD_DATE_START());
//                            rhsDate = outFormat.parse(rhs.getRLD_DATE_START());
//                        } catch (ParseException e) {
//                            e.printStackTrace();
//                        }
//
//                        if (lhsDate == null || rhsDate == null)
//                            return 0;
//
//                        return lhsDate.compareTo(rhsDate);
//                    }
//                });

                //add data null in child`
                LeaveHistoryResponse.DataEntity nullData = new LeaveHistoryResponse.DataEntity();
                SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmm", Locale.ENGLISH);
                nullData.setRL_STATUSID("7");
                try {
                    nullData.setRLD_DATE_START(sdf.format(AwareUtils.getCurTimeFromServer(getActivity())));
                } catch (Exception e) {
                    nullData.setRLD_DATE_START(sdf.format(new Date()));
                    e.printStackTrace();
                }
                childTemp.add(nullData);
                data.put((Integer) objs[i], childTemp);
            }

            otherHistoryAdapter.setServerTime(serverTime);
            otherHistoryAdapter.setLeaveModels(data);
            listView.setAdapter(otherHistoryAdapter);
            otherHistoryAdapter.notifyDataSetChanged();

            listView.setOnGroupExpandListener(onGroupExpand);
            if (swipeView.isRefreshing())
                swipeView.setRefreshing(false);

            alertWaiting.dismiss();
        }

        @Override
        public void onFailed(String statusCode, String message) {
            //f (alertWaiting.getShowsDialog())
            alertWaiting.dismiss();

            if (swipeView.isRefreshing())
                swipeView.setRefreshing(false);

        }

        @Override
        public void onFailed(String message) {
            // if (alertWaiting.getShowsDialog())
            alertWaiting.dismiss();

            if (swipeView.isRefreshing())
                swipeView.setRefreshing(false);

        }

        @Override
        public void onFailedAndForceLogout() {

            if (!AwareUtils.isForceLogout) {
                AwareUtils.forceLogOut(getActivity());
            }

        }
    };
    View.OnTouchListener listViewOnTouch = new View.OnTouchListener() {
        @Override
        public boolean onTouch(View v, MotionEvent event) {
            if (event.getAction() == MotionEvent.ACTION_DOWN) {
                listView.getParent().requestDisallowInterceptTouchEvent(false);
            } else if (event.getAction() == MotionEvent.ACTION_MOVE) {

            }
            return false;
        }
    };


    private void createGroupList() {
        groupList = new ArrayList<String>();
        groupList.add("Birthday Leave");
        groupList.add("Military Leave");
        groupList.add("Sterilization Leave");
        groupList.add("Ordination Leave");
        groupList.add("Maternity Leave");
        groupList.add("Paternity Leave");
        groupList.add("Berevement Leave");
        groupList.add("Leave without Pay");
    }

    private void getHistoryDataType() {
        //if (!alertWaiting.getShowsDialog())
        alertWaiting.show();
        LMSWSManager.getLeaveHistoryByType(getActivity(), application.getCurProfile().getSTAFF_TOKEN(), leaveTypesRequest, onListenerFromWSManager);
    }


    private void showDetailDialog(int groupPosition, int childPosition) {
        SimpleDateFormat inputSDF = new SimpleDateFormat("yyyyMMddHHmm");                            //201509140830
        SimpleDateFormat outputSDF = new SimpleDateFormat("EEE dd-MMM-yy", Locale.ENGLISH);

        final Dialog boxAddLeave = new Dialog(getActivity());
        boxAddLeave.setCanceledOnTouchOutside(false);
        boxAddLeave.getWindow().setBackgroundDrawable(new ColorDrawable(Color.argb(0, 0, 0, 0)));
        boxAddLeave.requestWindowFeature(Window.FEATURE_NO_TITLE);
        boxAddLeave.setContentView(R.layout.custom_box_sick_history);

        TextView text_sup_comment = (TextView) boxAddLeave.findViewById(R.id.text_supervisor);
        TextView text_leave_type = (TextView) boxAddLeave.findViewById(R.id.text_leave_type);
        TextView text_sent = (TextView) boxAddLeave.findViewById(R.id.text_sent);
        TextView text_comments = (TextView) boxAddLeave.findViewById(R.id.text_comments);
        TextView text_form = (TextView) boxAddLeave.findViewById(R.id.text_form);
        TextView text_to = (TextView) boxAddLeave.findViewById(R.id.text_to);
        TextView text_time_used = (TextView) boxAddLeave.findViewById(R.id.text_time_used);
        RelativeLayout box_btn_comment = (RelativeLayout) boxAddLeave.findViewById(R.id.box_btn_comment);

        //get Data
        LeaveHistoryResponse.DataEntity leaveModels =
                (LeaveHistoryResponse.DataEntity) otherHistoryAdapter.getChild(groupPosition, childPosition);
        String comment = leaveModels.getREASON();
        String sup_comment = "";

        String startDate = leaveModels.getRLD_DATE_START();
        String endDate = leaveModels.getRLD_DATE_END();
        Date parse_end_date = new Date();
        Date parse_start_date = new Date();
        Date parse_send_date = new Date();
        Date startDateCompare = null;
        Date endDateCompate = null;

        if (startDate != null) {
            try {
                parse_start_date = inputSDF.parse(startDate);
                parse_send_date = inputSDF.parse(leaveModels.getRL_DATE());
                startDateCompare = inputSDF.parse(startDate);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }

        if (endDate != null) {
            try {
                parse_end_date = inputSDF.parse(endDate);
                endDateCompate = inputSDF.parse(endDate);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }

        //default leave type
        int typeId = -1;
        try {
            typeId = Integer.parseInt(leaveModels.getLEAVETYPEID());
        } catch (Exception e) {
        }

        if (leaveModels.getRL_STATUSID().equalsIgnoreCase(LMSGeneralConstant.LEAVE_STATUS.Pending + "")) {
            sup_comment = "N/A";
        } else {
            if (leaveModels.getREMARK() != null) {
                if (leaveModels.getREMARK().equalsIgnoreCase(""))
                    sup_comment = "N/A";
                else
                    sup_comment = leaveModels.getREMARK();
            } else {
                sup_comment = "N/A";
            }
        }
        text_sup_comment.setText(sup_comment);

        String sickTime = "";
        String minuteTotal = leaveModels.getRLD_ALLTIME();
        int i_minute = Integer.parseInt(minuteTotal);
        int int_date = i_minute / 480;
        float f_hour = (i_minute % 480.0f) / 60.0f;

        if (int_date > 0) {
            sickTime += int_date;
            sickTime += int_date <= 1 ? " Day " : " Days ";
        }

        if (f_hour > 0) {
            sickTime += (f_hour % 1 == 0 ? (int) f_hour + "" : f_hour + "") + (f_hour <= 1 ? " Hour " : " Hours ");
        }

        SimpleDateFormat outDate = new SimpleDateFormat("EEE dd-MMM-yy HH:mm", Locale.ENGLISH);

        text_form.setText(outDate.format(parse_start_date));
        text_to.setText(outDate.format(parse_end_date));
        text_time_used.setText(sickTime);

        text_leave_type.setText(getLeaveType(typeId));
        text_comments.setText(comment);
        text_sent.setText(outputSDF.format(parse_send_date));


        box_btn_comment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boxAddLeave.dismiss();
            }
        });

        boxAddLeave.show();
    }

    private String getLeaveType(int leaveType_id) {
        String status_type_txt_title = "";
        int draw_title = R.drawable.star_small;
        if (leaveType_id == LMSGeneralConstant.LEAVE_TYPE.AnnualLeave) {
            status_type_txt_title = "Annual Leave";
            draw_title = R.drawable.palm_small;
        } else if (leaveType_id == LMSGeneralConstant.LEAVE_TYPE.BirthdayLeave) {
            status_type_txt_title = "Birthday Leave";
            draw_title = R.drawable.bday;
        } else if (leaveType_id == LMSGeneralConstant.LEAVE_TYPE.SickLeave) {
            status_type_txt_title = "Sick Leave";
            draw_title = R.drawable.heart_small;
        } else if (leaveType_id == LMSGeneralConstant.LEAVE_TYPE.MilitaryLeave) {
            status_type_txt_title = "Military Leave";
            draw_title = R.drawable.military;
        } else if (leaveType_id == LMSGeneralConstant.LEAVE_TYPE.SterilizationLeave) {
            status_type_txt_title = "Sterilization Leave";
            draw_title = R.drawable.sterilization;
        } else if (leaveType_id == LMSGeneralConstant.LEAVE_TYPE.OrdinationLeave) {
            status_type_txt_title = "Ordination Leave";
            draw_title = R.drawable.ordination;
        } else if (leaveType_id == LMSGeneralConstant.LEAVE_TYPE.MaternityLeave) {
            status_type_txt_title = "Maternity Leave";
            draw_title = R.drawable.maternity;
        } else if (leaveType_id == LMSGeneralConstant.LEAVE_TYPE.PaternityLeave) {
            status_type_txt_title = "Paternity Leave";
            draw_title = R.drawable.paternity;
        } else if (leaveType_id == LMSGeneralConstant.LEAVE_TYPE.BereavementLeave) {
            status_type_txt_title = "Bereavement Leave";
            draw_title = R.drawable.bereavment;
        } else if (leaveType_id == LMSGeneralConstant.LEAVE_TYPE.LeaveWithoutPay) {
            status_type_txt_title = "LeaveWithoutPay";
            draw_title = R.drawable.leave_without_pay;
        } else if (leaveType_id == LMSGeneralConstant.LEAVE_TYPE.DayOff) {
            status_type_txt_title = "DayOff";
            draw_title = R.drawable.star_small;
        } else {
            return "";
        }

        return status_type_txt_title;
    }

    private SwipeMenuItem createMenuSwapItem(String title, int drawable, int color, int txt_color) {
        SwipeMenuItem itemMenu = new SwipeMenuItem(getActivity());
        itemMenu.setBackground(new ColorDrawable(getResources().getColor(color)));
        // set item widthe
        itemMenu.setWidth(AwareUtils.dp2px(getActivity(), 90));
        // set a icon
        itemMenu.setIcon(drawable);
        itemMenu.setTitle(title);
        itemMenu.setTitleSize(12);
        itemMenu.setTitleColor(getResources().getColor(txt_color));

        return itemMenu;
    }

    private void changeChildListView(final int id, final int groupPosition, final int childPosition, final View view) {
        RelativeLayout linear_main = (RelativeLayout) childView.findViewById(R.id.linear_main);
        LinearLayout linear_sub_main = (LinearLayout) childView.findViewById(R.id.linear_sub_main);
        LinearLayout cancel = (LinearLayout) childView.findViewById(R.id.btn_cancel);
        LinearLayout btn_confirm = (LinearLayout) childView.findViewById(R.id.btn_approve);
        ImageView img_approve = (ImageView) childView.findViewById(R.id.img_approve);
        final TextView title_text_confirm = (TextView) childView.findViewById(R.id.title_text_confirm);
        final EditText editComment = (EditText) childView.findViewById(R.id.edit_comment);

        int m_height = childView.getHeight();
        int s_height = linear_sub_main.getHeight();
        if (top_editText == -1)
            top_editText = (int) AwareUtils.dpAsPixel(getActivity(), (float) ((m_height - s_height) / 2.0f) + editComment.getPaddingBottom());

        if (bottom_editText == -1)
            bottom_editText = (int) AwareUtils.dpAsPixel(getActivity(), (float) ((m_height - s_height) / 2.0f) + editComment.getPaddingTop());

        editComment.setHeight(m_height);

        if (id == CancelId) {
            btn_confirm.setBackgroundColor(getResources().getColor(R.color.bg_red));
            img_approve.setImageDrawable(getResources().getDrawable(R.drawable.denied));
            title_text_confirm.setText("Deny");

        }

        //case opened
        if (linear_sub_main.getVisibility() == View.VISIBLE) {
            linear_sub_main.setVisibility(View.GONE);
            linear_main.setVisibility(View.VISIBLE);

        }//case closed
        else {
            linear_sub_main.setVisibility(View.VISIBLE);
            linear_main.setVisibility(View.INVISIBLE);
            AwareUtils.keyboardHide(getActivity());
        }

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                changeChildListView(id, groupPosition, childPosition, view);
            }
        });

        btn_confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String commentText = editComment.getText().toString();
                if (id == ApproveId) {
                    if (!commentText.trim().equalsIgnoreCase("")) {
                        //change status
//                        Toast.makeText(getActivity(), "Deny Success", Toast.LENGTH_SHORT).show();
                        AwareUtils.keyboardHide(getActivity());
                        reason = commentText;
                        updateStatus(groupPosition, childPosition, commentText);
                    } else {
//                        take comment only for deny pending
                        Toast.makeText(getActivity(), "no comment in edit text", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });

    }

    private void updateStatus(int groupPosition, int childPosition, String commentText) {
        this.groupPosition = groupPosition;
        this.childPosition = childPosition;
        String toChangeStatus = "";
        int RL_StatusId = otherHistoryAdapter.getChildType(groupPosition, childPosition);

        Object[] keys = otherHistoryAdapter.getModels().keySet().toArray();
        LeaveHistoryResponse.DataEntity data = otherHistoryAdapter.getModels().get(keys[groupPosition]).get(childPosition);

        //type
        leaveType = data.getLEAVETYPEID();

        if (!application.getCurProfile().getEMP_ROLE()
                .equalsIgnoreCase(LMSGeneralConstant.EMP_ROLE.DIRECTOR_TEXT)) {
            if (RL_StatusId == LMSGeneralConstant.LEAVE_STATUS.Pending) {           //Pending(1) -> Cancel(2)
                toChangeStatus = "2";
            } else if (RL_StatusId == LMSGeneralConstant.LEAVE_STATUS.Approved) {      //Approve(3) -> Pending Cancel(5)
                toChangeStatus = "5";
            }
        } else {
            if (RL_StatusId == LMSGeneralConstant.LEAVE_STATUS.Pending) {               //Pending(1) -> Cancel(2)
                toChangeStatus = "2";
            } else if (RL_StatusId == LMSGeneralConstant.LEAVE_STATUS.Approved) {       //Directer auto approve to cancel(6)
                toChangeStatus = "6";                                                   //Approve -> Approve Cancel
            }
        }

        LeaveHistoryResponse.DataEntity childData = (LeaveHistoryResponse.DataEntity) otherHistoryAdapter.getChild(groupPosition, childPosition);
        String RL_ID = childData.getRL_ID();
        LMSWSManager.updateLeaveStatus(
                getActivity(),
                application.getCurProfile().getSTAFF_TOKEN(),
                RL_ID,
                toChangeStatus,
                commentText,
                onResponseUpdateStatus
        );
    }

    private OnListenerFromWSManager onResponseUpdateStatus = new OnListenerFromWSManager() {
        @Override
        public void onComplete(String statusCode, String message) {
            if (leaveType.equalsIgnoreCase("2")) {
                SaveSharedPreference sharedPreferences = new SaveSharedPreference(getActivity());
                sharedPreferences.saveHBDStatusRequest(true);
            }

            // update data
            getHistoryDataType();
            getStaffSummary();
            checkForDayOff();
        }

        @Override
        public void onFailed(String statusCode, final String message) {

            DialogUtils.showAlertDismissofRetry(getActivity(), message, new RequestDialogListener() {
                @Override
                public void responseSubmit() {
                    updateStatus(groupPosition, childPosition, reason);
                }

                @Override
                public void responseCancel() {

                }
            });

        }

        @Override
        public void onFailed(final String message) {

            DialogUtils.showAlertDismissofRetry(getActivity(), message, new RequestDialogListener() {
                @Override
                public void responseSubmit() {
                    updateStatus(groupPosition, childPosition, reason);
                }

                @Override
                public void responseCancel() {

                }
            });

        }

        @Override
        public void onFailedAndForceLogout() {
            if (!AwareUtils.isForceLogout) {
                AwareUtils.forceLogOut(getActivity());
            }
        }
    };

    private void getStaffSummary() {
        String token = application.getCurProfile().getSTAFF_TOKEN();
        LMSWSManager.getStaffSummary(getActivity(), token, onResponseStaffSummary);
    }

    private OnListenerFromWSManager onResponseBadgeUpdate = new OnListenerFromWSManager() {
        @Override
        public void onComplete(String statusCode, String message) {
            application.setIntOtherShowBadge(0);
        }

        @Override
        public void onFailed(String statusCode, String message) {

        }

        @Override
        public void onFailed(String message) {

        }

        @Override
        public void onFailedAndForceLogout() {

        }
    };

    private OnListenerFromWSManager onResponseStaffSummary = new OnListenerFromWSManager() {
        @Override
        public void onComplete(String statusCode, String message) {
            Gson gson = new Gson();
            StaffSummaryResponse staffs = gson.fromJson(message, StaffSummaryResponse.class);
            ResponseEntity responseEntity = staffs.getResponse();
            List<StaffSummaryResponse.DataEntity> dataEntity = staffs.getData();
            final String serverTime = responseEntity.getServer_time();
            String[] timeUsedData = null;
            String[] timeAllDayForLeave = null;

            HashMap<String, String[]> dayTokenDatas = new HashMap<>();
            final HashMap<String, Float[]> daysTaken = new HashMap<>();
            int i = 0;
            for (StaffSummaryResponse.DataEntity data : dataEntity) {
                Log.e("", "pos : " + (i++));
                String leaveName = data.getLEAVENAME();
                String allTimeUsed = data.getALLTIMEUSED();
                String AllDayForLeave = data.getALLDAYFORLEAVE();

                if (allTimeUsed == null) allTimeUsed = "0";
                if (AllDayForLeave == null) AllDayForLeave = "0";

                int ALLTIMEUSED = Integer.parseInt(allTimeUsed);
                int ALLDAYFORLEAVE = Integer.parseInt(AllDayForLeave);
                final int DAYTAKEN = ALLDAYFORLEAVE - ALLTIMEUSED;
                float daysToken = ALLTIMEUSED / 480.0f;
                float hourToken = (ALLTIMEUSED % 480.0f) / 60.0f;

                daysTaken.put(leaveName, new Float[]{daysToken, hourToken});
            }

            otherHistoryAdapter.setServerTime(serverTime);
            otherHistoryAdapter.setStaffData(daysTaken);
            //listView.setAdapter(otherHistoryAdapter);
            otherHistoryAdapter.notifyDataSetChanged();
            //data summary
            application.setDayTokenDatas(dayTokenDatas);
        }

        @Override
        public void onFailed(String statusCode, String message) {

        }

        @Override
        public void onFailed(String message) {

        }

        @Override
        public void onFailedAndForceLogout() {
            if (swipeView.isRefreshing())
                swipeView.setRefreshing(false);

            if (!AwareUtils.isForceLogout) {
                AwareUtils.forceLogOut(getActivity());
            }
        }
    };

    private ExpandableListView.OnGroupExpandListener onGroupExpand = new ExpandableListView.OnGroupExpandListener() {
        int previousGroup = -1;

        @Override
        public void onGroupExpand(final int groupPosition) {

            if (groupPosition != previousGroup)
                listView.collapseGroup(previousGroup);

            previousGroup = groupPosition;
        }
    };

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
//        super.setUserVisibleHint(isVisibleToUser);
        if (this.isVisible()) {
            if (!isVisibleToUser) {
                Log.e("", "isVisibleToUser : " + isVisibleToUser);
            }
        }
    }

    private void updateBadge() {
        int count_sick = application.getIntOtherShowBadge();
        String token = application.getCurProfile().getSTAFF_TOKEN();
        String staff_id = application.getCurProfile().getSTF_ID();

        LMSWSManager.badgeCountUpdate(getActivity(), token, staff_id, "OTHER", "0", onResponseBadgeUpdate);
    }

    private OnListenerFromWSManager onResponseCheckForDayOff = new OnListenerFromWSManager() {
        @Override
        public void onComplete(String statusCode, String message) {
            Gson gson = new Gson();
            CheckForDayOffResponse checkForDayOffResponse = gson.fromJson(message, CheckForDayOffResponse.class);
            List<CheckForDayOffResponse.DataEntity> dataEntity = checkForDayOffResponse.getData();
            if (dataEntity == null)
                dataEntity = new ArrayList<CheckForDayOffResponse.DataEntity>();

            final List<CheckForDayOffResponse.DataEntity> finalDataEntity = dataEntity;

            boolean isDayOff = otherHistoryAdapter.getModels().containsKey(11);
            if (!isDayOff && finalDataEntity.size() > 0) {
                HashMap<Integer, List<LeaveHistoryResponse.DataEntity>> data = otherHistoryAdapter.getModels();

                LeaveHistoryResponse.DataEntity childData = new LeaveHistoryResponse.DataEntity();
//                        childData.setLEAVETYPEID("11");
                List<LeaveHistoryResponse.DataEntity> GroupData = new ArrayList<LeaveHistoryResponse.DataEntity>();
                GroupData.add(childData);
                data.put(11, GroupData);
                otherHistoryAdapter.setLeaveModels(data);
            }

            otherHistoryAdapter.setDayOffData(finalDataEntity);
            listView.setAdapter(otherHistoryAdapter);
            otherHistoryAdapter.notifyDataSetChanged();

        }

        @Override
        public void onFailed(String statusCode, String message) {

        }

        @Override
        public void onFailed(String message) {

        }

        @Override
        public void onFailedAndForceLogout() {
            if (swipeView.isRefreshing())
                swipeView.setRefreshing(false);
            if (!AwareUtils.isForceLogout) {
                AwareUtils.forceLogOut(getActivity());
            }
        }
    };


    private void checkForDayOff() {
        String token = application.getCurProfile().getSTAFF_TOKEN();
        LMSWSManager.checkForDayOff(getActivity(), token, onResponseCheckForDayOff);
    }

    private int getLeaveTypeFromStatusName(String staffName) {
        if (staffName.equalsIgnoreCase("Annual Leave"))
            return 1;
        else if (staffName.equalsIgnoreCase("Birthday Leave"))
            return 2;
        else if (staffName.equalsIgnoreCase("Sick Leave"))
            return 3;
        else if (staffName.equalsIgnoreCase("Military Leave"))
            return 4;
        else if (staffName.equalsIgnoreCase("Sterilization Leave"))
            return 5;
        else if (staffName.equalsIgnoreCase("Ordination Leave"))
            return 6;
        else if (staffName.equalsIgnoreCase("Paternity Leave"))
            return 8;
        else if (staffName.equalsIgnoreCase("Bereavement Leave"))
            return 9;
        else if (staffName.equalsIgnoreCase("Leave Without Pay"))
            return 10;
        else if (staffName.equalsIgnoreCase("Day Off"))
            return 11;
        else
            return 0;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.img_home_img:
                getActivity().finish();
                break;
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        setLookUpPieChart();
        createGroupList();
        getHistoryDataType();
        getStaffSummary();
        checkForDayOff();
        updateBadge();
    }
}
