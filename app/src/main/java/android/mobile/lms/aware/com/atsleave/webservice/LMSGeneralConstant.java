package android.mobile.lms.aware.com.atsleave.webservice;

import android.mobile.lms.aware.com.atsleave.R;
import android.provider.BaseColumns;

import java.text.SimpleDateFormat;
import java.util.Locale;

/**
 * Created by apinun.w on 14/5/2558.
 */
public class LMSGeneralConstant {
    public static String LMSTAG = "TAG";
    public static SimpleDateFormat sdp_input = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH); //1976-10-27 00:00:00
    public static SimpleDateFormat sdp_input2 = new SimpleDateFormat("yyyyMMddHHmm", Locale.ENGLISH);
    public static SimpleDateFormat sdp_output_profile = new SimpleDateFormat("MMM dd, yyyy", Locale.ENGLISH);
    public static SimpleDateFormat sdp_output_myteam = new SimpleDateFormat("MMM dd", Locale.ENGLISH);
    public static SimpleDateFormat sdp_output_pending = new SimpleDateFormat("EE. dd-MMM-yy", Locale.ENGLISH);

    public static SimpleDateFormat sdp_output_pendinghis = new SimpleDateFormat("EEE dd-MMM-yy hh:mm a", Locale.ENGLISH);

    public class EMP_ROLE implements BaseColumns {
        public static final int SUPERVISOR = 1;
        public static final int EMPLOYEE = 2;
        public static final int DIRECTOR = 3;
        public static final String SUPERVISOR_TEXT = "SUPERVISOR";
        public static final String EMPLOYEE_TEXT = "EMPLOYEE";
        public static final String DIRECTOR_TEXT = "DIRECTOR";
    }

    public class LEAVE_STATUS implements BaseColumns {
        public static final int Pending = 1;
        public static final int Canceled = 2;
        public static final int Approved = 3;
        public static final int Rejected = 4;
        public static final int PendingCancel = 5;
        public static final int ApprovedCancel = 6;
        public static final int RejectedCancel = 7;
        public static final int DetailOnly = 100;

        public static final String string_Pending = "Pending";
        public static final String string_Canceled = "Canceled";
        public static final String string_Approved = "Approved";
        public static final String string_Rejected = "Rejected";
        public static final String string_PendingCancel = "PendingCancel";
        public static final String string_ApprovedCancel = "ApprovedCancel";
        public static final String string_RejectedCancel = "RejectedCancel";
    }

    public class LEAVE_TYPE implements BaseColumns {
        public static final int AnnualLeave = 1;
        public static final int BirthdayLeave = 2;
        public static final int SickLeave = 3;
        public static final int MilitaryLeave = 4;
        public static final int SterilizationLeave = 5;
        public static final int OrdinationLeave = 6;
        public static final int MaternityLeave = 7;
        public static final int PaternityLeave = 8;
        public static final int BereavementLeave = 9;
        public static final int LeaveWithoutPay = 10;
        public static final int DayOff = 11;

        public static final String text_AnnualLeave = "Annual Leave";
        public static final String text_BirthdayLeave = "Birthday Leave";
        public static final String text_SickLeave = "Sick Leave";
        public static final String text_MilitaryLeave = "Military Leave";
        public static final String text_SterilizationLeave = "Sterilization Leave";
        public static final String text_OrdinationLeave = "Ordination Leave";
        public static final String text_MaternityLeave = "Maternity Leave";
        public static final String text_PaternityLeave = "Paternity Leave";
        public static final String text_BereavementLeave = "Bereavement Leave";
        public static final String text_LeaveWithoutPay = "Leave Without Pay";
        public static final String text_DayOff = "DayOff";
    }

    public class DAY_OFF_STATUS implements BaseColumns {
        public static final int Pending = 1;
        public static final int Canceled = 2;
        public static final int Approved = 3;
    }

    public class BADGE_COUNT_READ implements BaseColumns {
        public static final String LEAVE_MODULE_ANNAUL = "ANNUAL";
        public static final String LEAVE_MODULE_DIRE = "DIRE";
        public static final String LEAVE_MODULE_OTHER = "OTHER";
        public static final String LEAVE_MODULE_SICK = "SICK";
        public static final String LEAVE_MODULE_SUPV = "SUPV";
    }

    //MAIN VIEW PAGE
    public static final int FIRST_PAGE = 1;
    public static final int SECOND_PAGE = 2;

    //PAGE FRAGMENT
    public static final int REQUEST_PAGE = 0;
    public static final int HOME_PAGE = 1;
    public static final int MY_TEAM_PAGE = 2;
    public static final int SICK_HISTORY_PAGE = 3;
    public static final int PERSONAL_HISTORY_PAGE = 4;
    public static final int OTHER_HISTORY_PAGE = 5;
    public static final int PENDING_REQUEST_PAGE = 6;
    public static final int TEAM_LEAVE = 7;

    public static String getNameLeaveTypeById(int leave_type_id){
        switch (leave_type_id) {
            case LMSGeneralConstant.LEAVE_TYPE.AnnualLeave:
                return LMSGeneralConstant.LEAVE_TYPE.text_AnnualLeave;

            case LMSGeneralConstant.LEAVE_TYPE.BirthdayLeave :
                return LMSGeneralConstant.LEAVE_TYPE.text_BirthdayLeave;

            case LMSGeneralConstant.LEAVE_TYPE.SickLeave :
                return LMSGeneralConstant.LEAVE_TYPE.text_SickLeave;

            case LMSGeneralConstant.LEAVE_TYPE.MilitaryLeave :
                return LMSGeneralConstant.LEAVE_TYPE.text_MilitaryLeave;

            case LMSGeneralConstant.LEAVE_TYPE.SterilizationLeave :
                return LMSGeneralConstant.LEAVE_TYPE.text_SterilizationLeave;

            case LMSGeneralConstant.LEAVE_TYPE.OrdinationLeave :
                return LMSGeneralConstant.LEAVE_TYPE.text_OrdinationLeave;

            case LMSGeneralConstant.LEAVE_TYPE.MaternityLeave :
                return LMSGeneralConstant.LEAVE_TYPE.text_MaternityLeave;

            case LMSGeneralConstant.LEAVE_TYPE.PaternityLeave :
                return LMSGeneralConstant.LEAVE_TYPE.text_PaternityLeave;

            case LMSGeneralConstant.LEAVE_TYPE.BereavementLeave :
                return LMSGeneralConstant.LEAVE_TYPE.text_BereavementLeave;

            case LMSGeneralConstant.LEAVE_TYPE.LeaveWithoutPay :
                return LMSGeneralConstant.LEAVE_TYPE.text_LeaveWithoutPay;

            case LMSGeneralConstant.LEAVE_TYPE.DayOff :
                return LMSGeneralConstant.LEAVE_TYPE.text_DayOff;
            default:
                return "";
        }
    }

    public static int getIconLeaveTypeById(int leave_type_id){
        switch (leave_type_id) {
            case LMSGeneralConstant.LEAVE_TYPE.AnnualLeave:
                return R.drawable.palm_icon;

            case LMSGeneralConstant.LEAVE_TYPE.BirthdayLeave :
                return R.drawable.present_icon;

            case LMSGeneralConstant.LEAVE_TYPE.SickLeave :
                return R.drawable.heart_icon;

            case LMSGeneralConstant.LEAVE_TYPE.MilitaryLeave :
                return R.drawable.military_icon;

            case LMSGeneralConstant.LEAVE_TYPE.SterilizationLeave :
                return R.drawable.needle_icon;

            case LMSGeneralConstant.LEAVE_TYPE.OrdinationLeave :
                return R.drawable.ordination_icon;

            case LMSGeneralConstant.LEAVE_TYPE.MaternityLeave :
                return R.drawable.maternity_icon;

            case LMSGeneralConstant.LEAVE_TYPE.PaternityLeave :
                return R.drawable.paternity_icon;

            case LMSGeneralConstant.LEAVE_TYPE.BereavementLeave :
                return R.drawable.bereavement_icon;

            case LMSGeneralConstant.LEAVE_TYPE.LeaveWithoutPay :
                return R.drawable.creditcard_icon;

            case LMSGeneralConstant.LEAVE_TYPE.DayOff :
                return R.drawable.star_icon;
            default:
                return R.drawable.star_icon;
        }
    }


    public static int getIconNormalLeaveTypeById(int leave_type_id) {
        switch (leave_type_id) {
            case LMSGeneralConstant.LEAVE_TYPE.AnnualLeave:
                return R.drawable.palm_small;

            case LMSGeneralConstant.LEAVE_TYPE.BirthdayLeave:
                return R.drawable.bday;

            case LMSGeneralConstant.LEAVE_TYPE.SickLeave:
                return R.drawable.heart_small;

            case LMSGeneralConstant.LEAVE_TYPE.MilitaryLeave:
                return R.drawable.military;

            case LMSGeneralConstant.LEAVE_TYPE.SterilizationLeave:
                return R.drawable.sterilization;

            case LMSGeneralConstant.LEAVE_TYPE.OrdinationLeave:
                return R.drawable.ordination;

            case LMSGeneralConstant.LEAVE_TYPE.MaternityLeave:
                return R.drawable.maternity;

            case LMSGeneralConstant.LEAVE_TYPE.PaternityLeave:
                return R.drawable.paternity;

            case LMSGeneralConstant.LEAVE_TYPE.BereavementLeave:
                return R.drawable.bereavment;

            case LMSGeneralConstant.LEAVE_TYPE.LeaveWithoutPay:
                return R.drawable.leave_without_pay;

            case LMSGeneralConstant.LEAVE_TYPE.DayOff:
                return R.drawable.star_small;

            default:
                return R.drawable.star_small;

        }
    }
}
