package android.mobile.lms.aware.com.atsleave.adapter;

import android.app.Activity;
import android.mobile.lms.aware.com.atsleave.R;
import android.mobile.lms.aware.com.atsleave.models.CheckForDayOffResponse;
import android.mobile.lms.aware.com.atsleave.utils.AwareUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.utils.ColorTemplate;

import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * Created by apinun.w on 7/9/2558.
 */
public class DayOffOtherAdapter extends BaseAdapter {

    Activity activity;
    List<CheckForDayOffResponse.DataEntity> data;
    String serverTime;

    public List<CheckForDayOffResponse.DataEntity> getData() {
        return data;
    }

    public void setData(List<CheckForDayOffResponse.DataEntity> data) {
        this.data = data;
    }

    public DayOffOtherAdapter(Activity activity, List<CheckForDayOffResponse.DataEntity> data, String servertime) {
        this.activity = activity;
        this.data = data;
        this.serverTime = servertime;
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        return data.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Holder holder;
        if(convertView == null){
            convertView = LayoutInflater.from(activity).inflate(R.layout.layout_piechart_dayoff,parent,false);
            holder = new Holder(convertView);
            holder.piechart = (PieChart) convertView.findViewById(R.id.chart1);

        }else{
            holder = (Holder) convertView.getTag();
        }
        setData(data.get(position), holder.piechart);
        String str_total_used = data.get(position).getTOTAL_USED();
        String str_rd_time = data.get(position).getRD_TIME();
        String str_balance = data.get(position).getBALANCE();
        int int_total_used = Integer.parseInt(str_total_used);
        int int_rd_time = Integer.parseInt(str_rd_time);
        int int_dayTaken = int_rd_time - int_total_used;
        int int_balance = Integer.parseInt(str_balance);
        int dayBalance = (int) (int_balance/480.0f);
        float hourBalance = (int_balance%480.0f)/60.0f;

        String test_balance = "";
//        if(hourBalance % 1 == 0) {
//            test_balance += dayBalance == 0 ? "" : dayBalance + " days ";
//            test_balance += (int) hourBalance + " hours";
//        }else {
//            test_balance += dayBalance == 0 ? "" : dayBalance + " days ";
//            test_balance += hourBalance + " hours";
//        }

        if((int)dayBalance > 0) {
            test_balance += (int) dayBalance;
            test_balance += (int) dayBalance <= 1 ? " D " : " D ";
        }
        if(hourBalance > 0){
            test_balance += (hourBalance % 1 == 0? (int)hourBalance+"": hourBalance+"") + (hourBalance <= 1? " Hr ":" Hrs ");
        }

        if((int) dayBalance == 0 && hourBalance == 0){
            test_balance = "0 D";
        }


        String stStart = new String(serverTime);
        serverTime = stStart.substring(0,10);
        int timeExp=0;

        String stEnd = new String(data.get(position).getRD_EXPIREDATE());
        String expTime = stEnd.substring(0,10);

        List<Date> dates = getDates(serverTime,expTime, "yyyy-MM-dd");//check betweendate
        for (Date date : dates) {
            timeExp++;
        }

        timeExp--;

        SimpleDateFormat endFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");     //2015-12-17 18:14:44
        Date endDate = null;
        try {
            endDate = endFormat.parse(stEnd);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        Calendar endCal = Calendar.getInstance();
        endCal.setTime(endDate);

        if(timeExp == 0){
            if(checkOneDate(serverTime,endCal)){
                timeExp = 1;
            }
        }

        DecimalFormat df = new DecimalFormat("#.##");

        holder.text_daytaken.setText(String.format(activity.getString(R.string.gridview_day_off_text_taken,test_balance)));
        holder.text_dayleft.setText(String.format(activity.getString(R.string.gridview_day_off_text_let, timeExp+"")));

        return convertView;
    }

    private class Holder{
        PieChart piechart;
        TextView text_daytaken;
        TextView text_dayleft;
        public Holder(View view){
            piechart = (PieChart) view.findViewById(R.id.chart1);
            text_daytaken = (TextView) view.findViewById(R.id.text_daytaken);
            text_dayleft = (TextView) view.findViewById(R.id.text_dayleft);
            piechart.setUsePercentValues(true);
            piechart.setDescription("");
            piechart.setDragDecelerationFrictionCoef(0.95f);
            piechart.setDrawHoleEnabled(true);
            piechart.setHoleColorTransparent(true);
            //mChart1.setTransparentCircleColor(getResources().getColor(R.color.bg_white));
            piechart.setHoleRadius(70f);
            piechart.setTransparentCircleRadius(61f);
            piechart.setDrawCenterText(true);
            piechart.setRotationEnabled(false);


            Legend l1 = piechart.getLegend();
            l1.setEnabled(false);
//            setData(1, 90, "1",piechart);

            view.setTag(this);
        }
    }

    //-------------------chart -----------------------------------
    private void setData(CheckForDayOffResponse.DataEntity dataEntity, PieChart piechart) {
        String text_usedTime = dataEntity.getTOTAL_USED();
        String text_totalTime = dataEntity.getRD_TIME();

        int int_usedTime = Integer.parseInt(text_usedTime);
        int int_totalTime = Integer.parseInt(text_totalTime);
        int int_diffTime = int_totalTime - int_usedTime;

        Log.e("", "int_usedTime "+int_usedTime);
        Log.e("", "int_diffTime "+int_diffTime);

        ArrayList<Entry> yVals1 = new ArrayList<Entry>();
//        float value1=  (float) int_usedTime/int_totalTime
        yVals1.add(new Entry((float) int_usedTime, 0));
        yVals1.add(new Entry((float) int_diffTime, 1));

        ArrayList<String> xVals = new ArrayList<String>();

        for (int i = 0; i < yVals1.size(); i++)
            xVals.add("");

        PieDataSet dataSet = new PieDataSet(yVals1, "");
        ArrayList<Integer> colors = new ArrayList<Integer>();

        for (int c : ColorTemplate.CUSTOM_COLORS_DAYOFF)
            colors.add(c);

        dataSet.setColors(colors);
        PieData data = new PieData(xVals, dataSet);
        data.setDrawValues(false);

        int totalTime = Integer.parseInt(dataEntity.getRD_TIME());
        int day = totalTime/480;
        float hours = (totalTime % 480.0f)/60.0f;
        String strDayTaken = "";
        if(day > 0) {
            strDayTaken += (int) day;
            strDayTaken += (int) day <= 1 ? " D " : " D ";
        }
        if(hours > 0){
            strDayTaken += (hours % 1 == 0? (int)hours+"": hours+"") + (hours <= 1? " Hr ":" Hrs ");
        }

        if(day <= 0 && hours <= 0){
            strDayTaken = "0 D";
        }

        piechart.setCenterTextColor(activity.getResources().getColor(R.color.bg_black));
        piechart.setCenterTextSize(12);
        piechart.setCenterText(strDayTaken +"\nReceived");
        piechart.setData(data);
    }

    private static List<Date> getDates(String dateString1, String dateString2,String formats)
    {
        ArrayList<Date> dates = new ArrayList<Date>();
        DateFormat df1 = new SimpleDateFormat(formats);

        Date date1 = null;
        Date date2 = null;

        try {
            date1 = df1 .parse(dateString1);
            date2 = df1 .parse(dateString2);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        Calendar cal1 = Calendar.getInstance();
        cal1.setTime(date1);


        Calendar cal2 = Calendar.getInstance();
        cal2.setTime(date2);

        while(!cal1.after(cal2))
        {
            dates.add(cal1.getTime());
            cal1.add(Calendar.DATE, 1);
        }
        return dates;
    }

    private boolean checkOneDate(String serverTime,Calendar endDate){
        Date curTimeFromServer = new Date();
        try {
            curTimeFromServer = AwareUtils.getCurTimeFromServer(activity);
        } catch (Exception e) {
            e.printStackTrace();
        }

        if(endDate.after(curTimeFromServer)){
            return false;
        }else{
            return true;
        }
    }
}
