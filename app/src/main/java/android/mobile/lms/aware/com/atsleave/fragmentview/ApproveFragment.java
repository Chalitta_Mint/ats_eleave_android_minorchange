package android.mobile.lms.aware.com.atsleave.fragmentview;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.mobile.lms.aware.com.atsleave.LMSApplication;
import android.mobile.lms.aware.com.atsleave.R;
import android.mobile.lms.aware.com.atsleave.adapter.ListApproveAdapter;
import android.mobile.lms.aware.com.atsleave.callback.OnListenerFromWSManager;
import android.mobile.lms.aware.com.atsleave.models.HolidayResponse;
import android.mobile.lms.aware.com.atsleave.models.TeamLeaveResponse;
import android.mobile.lms.aware.com.atsleave.utils.DialogUtils;
import android.mobile.lms.aware.com.atsleave.utils.OnChangePageView;
import android.mobile.lms.aware.com.atsleave.webservice.LMSGeneralConstant;
import android.mobile.lms.aware.com.atsleave.webservice.LMSWSManager;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.baoyz.swipemenulistview.SwipeMenuListView;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonParser;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

/**
 * Created by chalitta.k on 15/07/2562
 */

public class ApproveFragment extends Fragment implements View.OnClickListener {

    private String TAG = "ApproveFragment";
    private OnChangePageView onChangePageView;
    private TextView tvCalenderStart, tvCalenderEnd, txtNoData;
    private ImageButton ibSearch;
    private ImageView imgRefresh;
    private LMSApplication application;
    private int yearEnd, monthEnd, dayEnd, yearStart, monthStart, dayStart;
    private ListApproveAdapter adapter;
    private SwipeMenuListView lv_team_leave;
    private Dialog dialogWaiting;

    public static ApproveFragment initApproveFragment(OnChangePageView onchangePageView) {
        ApproveFragment approveFragment = new ApproveFragment();
        approveFragment.setOnChangePageView(onchangePageView);
        return approveFragment;
    }

    public void setOnChangePageView(OnChangePageView onChangePageView) {
        this.onChangePageView = onChangePageView;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_approve, container, false);
        initView(view);
        onClick(view);
        initGetDefault();
        return view;
    }

    private void initGetDefault() {
        showAlertDialog();
        Calendar c = Calendar.getInstance();
        int year = c.get(Calendar.YEAR);
        int monthOfYear = c.get(Calendar.MONTH);
        int dayOfMonth = c.get(Calendar.DAY_OF_MONTH);
        String date = new StringBuilder().append(dayOfMonth).append("-").append(monthOfYear + 1).append("-").append(year).toString();
        String defaultDate = dateFormat(date, "d-M-yyyy", "dd-MMM-yy");
        tvCalenderStart.setText(defaultDate);
        tvCalenderEnd.setText(defaultDate);
        tvCalenderStart.setTag(setTagDate(year, monthOfYear, dayOfMonth));
        tvCalenderEnd.setTag(setTagDate(year, monthOfYear, dayOfMonth));
        yearStart = c.get(Calendar.YEAR);
        monthStart = c.get(Calendar.MONTH);
        dayStart = c.get(Calendar.DAY_OF_MONTH);
        yearEnd = c.get(Calendar.YEAR);
        monthEnd = c.get(Calendar.MONTH);
        dayEnd = c.get(Calendar.DAY_OF_MONTH);
        getSearchLeave();
    }

    private void initView(View view) {
        dialogWaiting = DialogUtils.createDialog(getActivity());
        application = (LMSApplication) getContext().getApplicationContext();
        tvCalenderStart = (TextView) view.findViewById(R.id.tv_calendar_start);
        tvCalenderEnd = (TextView) view.findViewById(R.id.tv_calendar_end);
        ibSearch = (ImageButton) view.findViewById(R.id.ib_search);
        lv_team_leave = (SwipeMenuListView) view.findViewById(R.id.lv_team_leave);
        txtNoData = (TextView) view.findViewById(R.id.txt_no_history);
        imgRefresh = (ImageView) view.findViewById(R.id.img_refresh);
        imgRefresh.setOnClickListener(this);
        tvCalenderStart.setOnClickListener(this);
        tvCalenderEnd.setOnClickListener(this);
        ibSearch.setOnClickListener(this);
        setUpLeaveHistoryAdapter();
    }

    @Override
    public void onClick(View v) {

        if (v == tvCalenderStart) {
            setStartDateToView();
        } else if (v == tvCalenderEnd) {
            setEndDateToView();
        } else if (v == ibSearch) {
            checkEndDateField();
        } else if (v == imgRefresh) {
            initGetDefault();
            adapter.clearList();
        }
    }

    private void checkEndDateField() {
        if (!TextUtils.isEmpty(tvCalenderEnd.getText().toString())) {
            showAlertDialog();
            getSearchLeave();
        } else {
            DialogUtils.showAlertDialog(getActivity(), "Please fill your end date");
        }
    }

    //final Activity context, String token, String staffID, String leaveID, String leaveType, String leaveStatus, String requestDate, String startLeaveDate, String endLeaveDate, final OnListenerFromWSManager onResponse
    private void getSearchLeave() {
        final TextView noData = txtNoData;
        String startDate = dateFormat(tvCalenderStart.getTag().toString(), "yyyyMMd", "yyyyMMdd");
        String endDate = dateFormat(tvCalenderEnd.getTag().toString(), "yyyyMMd", "yyyyMMdd");
        String status = LMSGeneralConstant.LEAVE_STATUS.Pending + "," + LMSGeneralConstant.LEAVE_STATUS.Approved + "," + LMSGeneralConstant.LEAVE_STATUS.RejectedCancel;
        LMSWSManager.getTeamLeaveByDate((Activity) getContext(), application.getCurProfile().getSTAFF_TOKEN(), "", "", "", status, "", startDate, endDate, new OnListenerFromWSManager() {
            @Override
            public void onComplete(String statusCode, String message) {
                dismissDialog();
                Log.i(TAG, "getTeamLeaveByDate_message: " + new GsonBuilder().setPrettyPrinting().create().toJson(new JsonParser().parse(message)));
                Gson gson = new Gson();
                final TeamLeaveResponse members = gson.fromJson(message, TeamLeaveResponse.class);
                final List<TeamLeaveResponse.DataEntity> dataEntity = members.getData();
                final SimpleDateFormat outFormat = new SimpleDateFormat("yyyyMMddHHss");
                //sort by date
//                Collections.sort(dataEntity, new Comparator<TeamLeaveResponse.DataEntity>() {
//                    @Override
//                    public int compare(TeamLeaveResponse.DataEntity lhs, TeamLeaveResponse.DataEntity rhs) {
//                        Date lhsDate = null;
//                        Date rhsDate = null;
//                        try {
//                            lhsDate = outFormat.parse(lhs.getRLD_DATE_START());
//                            rhsDate = outFormat.parse(rhs.getRLD_DATE_START());
//                        } catch (ParseException e) {
//                            e.printStackTrace();
//                        }
//
//                        if (lhsDate == null || rhsDate == null)
//                            return 0;
//
//                        return lhsDate.compareTo(rhsDate);
//                    }
//                });
                //set by type
                List<Object> objs = new ArrayList<Object>();
                List<Integer> listType = new ArrayList<Integer>();
                for (int i = 0; i < dataEntity.size(); i++) {
                    objs.add(dataEntity.get(i));
                    listType.add(adapter.STATUS_PENDING_NORMAL);
                }

                if (objs.size() == 0) {
                    noData.setVisibility(View.VISIBLE);
                } else {
                    noData.setVisibility(View.GONE);
                }

                adapter.setListType(listType);
                adapter.setLeaveTeams(objs);
                Log.i("size", "" + objs.size());
                lv_team_leave.setAdapter(adapter);
                adapter.notifyDataSetChanged();
            }

            @Override
            public void onFailed(String statusCode, String message) {
                dismissDialog();
            }

            @Override
            public void onFailed(String message) {
                dismissDialog();
            }

            @Override
            public void onFailedAndForceLogout() {
                dismissDialog();
            }
        });
    }

    private String dateFormat(String inputDate, String inputFormat, String outputFormat) {
        SimpleDateFormat dateFormat = new SimpleDateFormat(outputFormat, Locale.US);
        SimpleDateFormat input = new SimpleDateFormat(inputFormat, Locale.US);
        String strDate = null;
        try {
            strDate = dateFormat.format(input.parse(inputDate));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return strDate;
    }

    private void setUpLeaveHistoryAdapter() {
        adapter = new ListApproveAdapter(getActivity(), new ArrayList<Object>(), new ArrayList<Integer>(), new ArrayList<HolidayResponse.DataEntity>());
        lv_team_leave.setAdapter(adapter);
        lv_team_leave.setExpanded(false);
    }

    private void setStartDateToView() {
        DatePickerDialog datePickerDialog = new DatePickerDialog(getActivity(), R.style.DialogTheme, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int year, int monthOfYear, int dayOfMonth) {
                //dd-mm-yyyy
                String date = new StringBuilder().append(dayOfMonth).append("-").append(monthOfYear + 1).append("-").append(year).toString();
                String startDate = dateFormat(date, "d-M-yyyy", "dd-MMM-yy");
                Calendar c = Calendar.getInstance();
                yearStart = year;
                monthStart = monthOfYear;
                dayStart = dayOfMonth;
                yearEnd = c.get(Calendar.YEAR);
                monthEnd = c.get(Calendar.MONTH);
                dayEnd = c.get(Calendar.DAY_OF_MONTH);
                tvCalenderStart.setText(startDate);
                tvCalenderEnd.setText("");
                tvCalenderStart.setTag(setTagDate(year, monthOfYear, dayOfMonth));
            }
        }, yearStart, monthStart, dayStart);
        datePickerDialog.show();
    }

    private void setEndDateToView() {

        DatePickerDialog datePickerDialog = new DatePickerDialog(getActivity(), R.style.DialogTheme, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int year, int monthOfYear, int dayOfMonth) {
                //dd-mm-yyyy
                String date = new StringBuilder().append(dayOfMonth).append("-").append(monthOfYear + 1).append("-").append(year).toString();
                String endDate = dateFormat(date, "d-M-yyyy", "dd-MMM-yy");
                yearEnd = year;
                monthEnd = monthOfYear;
                dayEnd = dayOfMonth;
                tvCalenderEnd.setText(endDate);
                tvCalenderEnd.setTag(setTagDate(year, monthOfYear, dayOfMonth));
            }
        }, yearEnd, monthEnd, dayEnd);

        if (!TextUtils.isEmpty(tvCalenderStart.getText().toString())) {
            String startDate = dateFormat(tvCalenderStart.getText().toString(), "dd-MMM-yy", "dd-MM-yyyy");
            String[] getFrom = startDate.split("-");
            int year = Integer.parseInt(getFrom[2]);
            int month = Integer.parseInt(getFrom[1]) - 1;
            int day = Integer.parseInt(getFrom[0]);
            Calendar calendar = Calendar.getInstance();
            calendar.set(year, month, day);
            datePickerDialog.getDatePicker().setMinDate(calendar.getTimeInMillis());
        }
        datePickerDialog.show();
    }

    public static StringBuilder setTagDate(int year, int monthOfYear, int dayOfMonth) {
        String monthText;
        if ((monthOfYear + 1) < 10) {
            monthText = "0" + (monthOfYear + 1);
        } else {
            monthText = String.valueOf((monthOfYear + 1));
        }
        return new StringBuilder().append(year).append(monthText).append(dayOfMonth);
    }

    private void showAlertDialog() {
        if (!dialogWaiting.isShowing()) {
            dialogWaiting.show();
        }
    }

    private void dismissDialog() {
        dialogWaiting.dismiss();
    }
}





