package android.mobile.lms.aware.com.atsleave.adapter;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.mobile.lms.aware.com.atsleave.FullHistoryActivity;
import android.mobile.lms.aware.com.atsleave.R;
import android.mobile.lms.aware.com.atsleave.customview.ExpandableHeightGridView;
import android.mobile.lms.aware.com.atsleave.models.CheckForDayOffResponse;
import android.mobile.lms.aware.com.atsleave.models.LeaveHistoryResponse;
import android.mobile.lms.aware.com.atsleave.utils.AwareUtils;
import android.mobile.lms.aware.com.atsleave.webservice.LMSGeneralConstant;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.allen.expandablelistview.BaseSwipeMenuExpandableListAdapter;
import com.baoyz.swipemenulistview2.ContentViewWrapper;

import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

/**
 * Created by apinun.w on 22/8/2558.
 */
public class OtherHistoryAdapter extends BaseSwipeMenuExpandableListAdapter {
    HashMap<Integer, List<LeaveHistoryResponse.DataEntity>> models;
    Object[] objectsKey;
    Activity activity;
    HashMap<String, Float[]> staffData;
    List<CheckForDayOffResponse.DataEntity> dayOffData;
    String serverTime;

    public OtherHistoryAdapter(HashMap<Integer, List<LeaveHistoryResponse.DataEntity>> models, HashMap<String, Float[]> staffData, List<CheckForDayOffResponse.DataEntity> dayOffData, String serverTime, Activity activity) {
        this.models = models;
        this.objectsKey = models.keySet().toArray();
        this.staffData = staffData;
        this.dayOffData = dayOffData;
        this.activity = activity;
        this.serverTime = serverTime;
    }

    public String getServerTime() {
        return serverTime;
    }

    public void setServerTime(String serverTime) {
        this.serverTime = serverTime;
    }

    public HashMap<String, Float[]> getStaffData() {
        return staffData;
    }

    public void setStaffData(HashMap<String, Float[]> staffData) {
        this.staffData = staffData;
    }

    public List<CheckForDayOffResponse.DataEntity> getDayOffData() {
        return dayOffData;
    }

    public void setDayOffData(List<CheckForDayOffResponse.DataEntity> dayOffData) {
        this.dayOffData = dayOffData;
    }

    public void setLeaveModels(HashMap<Integer, List<LeaveHistoryResponse.DataEntity>> models) {
        this.models = models;
        this.objectsKey = models.keySet().toArray();
    }

    public HashMap<Integer, List<LeaveHistoryResponse.DataEntity>> getModels() {
        return models;
    }

    private SimpleDateFormat inputSDF = new SimpleDateFormat("yyyyMMddHHmm", Locale.ENGLISH);       //    201509010830
    private SimpleDateFormat outputSDF = new SimpleDateFormat("EEE dd-MMM-yy", Locale.ENGLISH);

    /**
     * /**
     * Whether this group item swipable
     *
     * @param groupPosition
     * @return
     * @see BaseSwipeMenuExpandableListAdapter#isGroupSwipable(int)
     */


    @Override
    public boolean isGroupSwipable(int groupPosition) {
        return false;
    }

    /**
     * Whether this child item swipable
     *
     * @param groupPosition
     * @param childPosition
     * @return
     * @see BaseSwipeMenuExpandableListAdapter#isChildSwipable(int,
     * int)
     */
    @Override
    public boolean isChildSwipable(int groupPosition, int childPosition) {
        // Toast.makeText(getActivity(), "childPosition:"+childPosition, Toast.LENGTH_SHORT).show();
        return true;
    }

    @Override
    public int getChildType(int groupPosition, int childPosition) {
        int childType = -1;
        try {
            int keyInt = (int) objectsKey[groupPosition];
            int childCount = models.get(keyInt).size();
            String childTypeText = models.get(keyInt).get(childPosition).getRL_STATUSID();
            String str_startDate = models.get(keyInt).get(childPosition).getRLD_DATE_START();

            boolean isExpireLimited = AwareUtils.checkExpireDate(activity, str_startDate, "yyyyMMddHHmm"); //201508210830

            if (isExpireLimited) {
                return 7;
            }

            childType = Integer.parseInt(childTypeText);
        } catch (Exception e) {
            childType = 7;
        }

        Log.e("", "childType : " + childType);
        return childType;
    }

    @Override
    public int getChildTypeCount() {
        return 7;
    }

    @Override
    public int getGroupType(int groupPosition) {
        return groupPosition;
    }

    @Override
    public int getGroupTypeCount() {
        return 3;
    }

    class ViewHolder {
        ImageView icon_list;
        TextView name_item;
        ImageView img_sick_leave;
        TextView txt_sick_date;
        TextView txt_sick_time;
        TextView txt_sick_status;
        TextView txt_sick_my_reason;
        TextView txt_sick_sup_reson;
        RelativeLayout firstView;
        RelativeLayout secondView;
        RelativeLayout thirdView;
        TextView text_day_taken;
        ExpandableHeightGridView gridView;
        Button btn_full_history;
        LinearLayout linear_full_his;
        LinearLayout linear_null_history;

        public ViewHolder(View view) {
            icon_list = (ImageView) view.findViewById(R.id.icon_list);
            name_item = (TextView) view.findViewById(R.id.name_item);
            img_sick_leave = (ImageView) view.findViewById(R.id.img_sick_leave);
            txt_sick_date = (TextView) view.findViewById(R.id.txt_sick_date);
            txt_sick_time = (TextView) view.findViewById(R.id.txt_sick_time);
            txt_sick_status = (TextView) view.findViewById(R.id.txt_sick_status);
            txt_sick_my_reason = (TextView) view.findViewById(R.id.txt_sick_my_reason);
            txt_sick_sup_reson = (TextView) view.findViewById(R.id.txt_sick_sup_reson);
            linear_null_history = (LinearLayout) view.findViewById(R.id.linear_null_history);
            firstView = (RelativeLayout) view.findViewById(R.id.firstView);
            secondView = (RelativeLayout) view.findViewById(R.id.secondView);
            thirdView = (RelativeLayout) view.findViewById(R.id.thirdView);
            text_day_taken = (TextView) view.findViewById(R.id.text_day_taken);
            gridView = (ExpandableHeightGridView) view.findViewById(R.id.gridView);
            btn_full_history = (Button) view.findViewById(R.id.btn_full_history);
            linear_full_his = (LinearLayout) view.findViewById(R.id.linear_full_his);
            view.setTag(this);
        }
    }

    class ViewGroupHolder {
        ImageView imageView;
        TextView txtTitle;

        public ViewGroupHolder(View view) {
            imageView = (ImageView) view.findViewById(R.id.icon_list);
            txtTitle = (TextView) view.findViewById(R.id.name_item);
            view.setTag(this);
        }
    }

    @Override
    public int getGroupCount() {
        return objectsKey.length;
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        int keyInt = (int) objectsKey[groupPosition];
        int childCount = models.get(keyInt).size();
//        Log.e("", "childCount : "+childCount);
        if (childCount > 6)
            childCount = 6;
        return childCount;

//        if(childCount == 0)
//            return 0;
//        else
//            return childCount+1;
    }

    @Override
    public Object getGroup(int groupPosition) {
        int keyInt = (int) objectsKey[groupPosition];
        return models.get(keyInt);
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        int keyInt = (int) objectsKey[groupPosition];
        List<LeaveHistoryResponse.DataEntity> childData = models.get(keyInt);
        return childData.get(childPosition);
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public ContentViewWrapper getGroupViewAndReUsable(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        boolean reUseable = true;
        ViewHolder holder;
        if (convertView == null) {
            convertView = View.inflate(activity, R.layout.group_item, null);
            holder =  new ViewHolder(convertView);
            convertView.setTag(holder);
            reUseable = false;
        }else{
            holder = (ViewHolder) convertView.getTag();
        }

        String GroupName = getLeaveType((int) objectsKey[groupPosition]);
        int typeId = (int) objectsKey[groupPosition];
        holder.name_item = (TextView) convertView.findViewById(R.id.name_item);
        holder.icon_list = (ImageView) convertView.findViewById(R.id.icon_list);
        holder.name_item.setText(GroupName);

        int drawable = getDrawableFromType(String.valueOf(typeId));
        holder.icon_list.setImageDrawable(activity.getResources().getDrawable(drawable));
        return new ContentViewWrapper(convertView, reUseable);
    }

    @Override
    public ContentViewWrapper getChildViewAndReUsable(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
        boolean reUseable = true;
        ViewHolder holder;
        if (convertView == null) {
            convertView = View.inflate(activity, R.layout.layout_leave_other_listitem, null);
            holder =  new ViewHolder(convertView);
            convertView.setTag(holder);
            reUseable = false;
        }else{
            holder = (ViewHolder) convertView.getTag();
        }

        LeaveHistoryResponse.DataEntity childData = models.get(objectsKey[groupPosition]).get(childPosition);
        int countChild = getChildrenCount(groupPosition);
        final int leaveType = (int) objectsKey[groupPosition];

        if (childPosition == countChild - 1) {
            holder.firstView.setVisibility(View.GONE);

            if (leaveType == 11) {
                //days off show grid view
                holder.secondView.setVisibility(View.GONE);
                holder.thirdView.setVisibility(View.VISIBLE);

                holder.gridView.setExpanded(true);
                DayOffOtherAdapter dayOffOtherAdapter = new DayOffOtherAdapter(activity, dayOffData, serverTime);
                holder.gridView.setAdapter(dayOffOtherAdapter);

                if (countChild == 1 && dayOffData.size() > 0) {
                    holder.linear_null_history.setVisibility(View.VISIBLE);
                } else {
                    holder.linear_null_history.setVisibility(View.GONE);
                }

            } else {
                //otherwise days off show days taken only
                holder.secondView.setVisibility(View.VISIBLE);
                holder.thirdView.setVisibility(View.GONE);
                holder.linear_null_history.setVisibility(View.GONE);
                //set Data
                try {
                    Float[] dateTaken = getDataTaken(leaveType);
                    float dayTaken = dateTaken[0];
                    float hourTaken = dateTaken[1];

                    String strDayTaken = "";

                    if ((int) dayTaken > 0) {
                        strDayTaken += (int) dayTaken;
                        strDayTaken += (int) dayTaken <= 1 ? " Day " : " Days ";
                    }
                    if (hourTaken > 0) {
                        strDayTaken += (hourTaken % 1 == 0 ? (int) hourTaken + "" : hourTaken + "") + (hourTaken <= 1 ? " Hour " : " Hours ");
                    }

                    if (dayTaken <= 0 && hourTaken <= 0) {
                        strDayTaken = "0 Days";
                    }

                    DecimalFormat df = new DecimalFormat("#.##");
                    holder.text_day_taken.setText(strDayTaken);
                } catch (Exception e) {
                    holder.text_day_taken.setText("0 Days");
                }

            }

            //full history
            if (models.get(objectsKey[groupPosition]).size() > 6) {        //data 5 + griddata 1
                holder.linear_full_his.setVisibility(View.VISIBLE);
            } else {
                holder.linear_full_his.setVisibility(View.GONE);
            }
        } else {
            holder.firstView.setVisibility(View.VISIBLE);
            holder.secondView.setVisibility(View.GONE);
            holder.thirdView.setVisibility(View.GONE);
            holder.linear_full_his.setVisibility(View.GONE);
            holder.linear_null_history.setVisibility(View.GONE);
            int imageId = R.drawable.denied;
            int status_id = Integer.parseInt(childData.getRL_STATUSID());
            switch (status_id) {
                case LMSGeneralConstant.LEAVE_STATUS.Approved:
                    imageId = R.drawable.approved;
                    break;
                case LMSGeneralConstant.LEAVE_STATUS.Rejected:
                    imageId = R.drawable.denied;
                    break;
                case LMSGeneralConstant.LEAVE_STATUS.PendingCancel:
                    imageId = R.drawable.pending_cancel;
                    break;
                case LMSGeneralConstant.LEAVE_STATUS.ApprovedCancel:
                    imageId = R.drawable.approved_cancel;
                    break;
                case LMSGeneralConstant.LEAVE_STATUS.Canceled:
                    imageId = R.drawable.denied_cancel;
                    break;
                case LMSGeneralConstant.LEAVE_STATUS.Pending:
                    imageId = R.drawable.pending;
                    break;
                case LMSGeneralConstant.LEAVE_STATUS.RejectedCancel:
                    imageId = R.drawable.denied_cancel;
                    break;
            }
            Bitmap bitmap = BitmapFactory.decodeResource(activity.getResources(), imageId);
            holder.img_sick_leave.setImageBitmap(bitmap);

            //       holder.img_sick_leave.setImageBitmap();
            String startDate = childData.getRLD_DATE_START();
            String endDate = childData.getRLD_DATE_END();
            Date parse_end_date = new Date();
            Date parse_start_date = new Date();
            Date startDateCompare = null;
            Date endDateCompate = null;

            if (startDate != null) {
                try {
                    parse_start_date = inputSDF.parse(startDate);
                    startDateCompare = inputSDF.parse(startDate);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }

            if (endDate != null) {
                try {
                    parse_end_date = inputSDF.parse(endDate);
                    endDateCompate = inputSDF.parse(endDate);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }

            String toDateText = "";

            startDateCompare.setHours(0);
            startDateCompare.setMinutes(0);
            endDateCompate.setHours(0);
            endDateCompate.setMinutes(0);

            if (startDateCompare.compareTo(endDateCompate) == 0) {
                toDateText = outputSDF.format(parse_start_date);
            } else {
                toDateText = outputSDF.format(parse_start_date) + "-" + outputSDF.format(parse_end_date);
            }
            holder.txt_sick_date.setText(toDateText);


            String sickTime = "";
            String minuteTotal = childData.getRLD_ALLTIME();
            int i_minute = Integer.parseInt(minuteTotal);
            int int_date = i_minute / 480;
            float f_hour = (i_minute % 480.0f) / 60.0f;

            if (int_date > 0) {
                sickTime += int_date;
                sickTime += int_date <= 1 ? " day " : " days ";
            }

            if (f_hour > 0) {
                sickTime += (f_hour % 1 == 0 ? (int) f_hour + "" : f_hour + "") + (f_hour <= 1 ? " Hour " : " Hours ");
            }


            holder.txt_sick_time.setText(sickTime);

            holder.txt_sick_status.setText(activity.getString(R.string.title_sick_status) + getLeaveStatus(Integer.parseInt(childData.getRL_STATUSID())));

            String sup_comment = "";
            if (childData.getRL_STATUSID().equalsIgnoreCase(LMSGeneralConstant.LEAVE_STATUS.Pending + "")) {
                sup_comment = "N/A";
            } else {
                if (childData.getREMARK() != null) {
                    if (childData.getREMARK().equalsIgnoreCase(""))
                        sup_comment = "N/A";
                    else
                        sup_comment = childData.getREMARK();
                } else {
                    sup_comment = "N/A";
                }
            }
            holder.txt_sick_my_reason.setText(childData.getREASON());
            holder.txt_sick_sup_reson.setText(sup_comment);
        }

        holder.btn_full_history.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(activity, FullHistoryActivity.class);
                intent.putExtra("status_type", new int[]{leaveType});
                activity.startActivity(intent);
            }
        });

        return new ContentViewWrapper(convertView, reUseable);
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }

    private String getLeaveType(int type) {
        switch (type) {
            case LMSGeneralConstant.LEAVE_TYPE.AnnualLeave:
                return LMSGeneralConstant.LEAVE_TYPE.text_AnnualLeave;
            case LMSGeneralConstant.LEAVE_TYPE.BirthdayLeave:
                return LMSGeneralConstant.LEAVE_TYPE.text_BirthdayLeave;
            case LMSGeneralConstant.LEAVE_TYPE.SickLeave:
                return LMSGeneralConstant.LEAVE_TYPE.text_SickLeave;
            case LMSGeneralConstant.LEAVE_TYPE.MilitaryLeave:
                return LMSGeneralConstant.LEAVE_TYPE.text_MilitaryLeave;
            case LMSGeneralConstant.LEAVE_TYPE.SterilizationLeave:
                return LMSGeneralConstant.LEAVE_TYPE.text_SterilizationLeave;
            case LMSGeneralConstant.LEAVE_TYPE.OrdinationLeave:
                return LMSGeneralConstant.LEAVE_TYPE.text_OrdinationLeave;
            case LMSGeneralConstant.LEAVE_TYPE.MaternityLeave:
                return LMSGeneralConstant.LEAVE_TYPE.text_MaternityLeave;
            case LMSGeneralConstant.LEAVE_TYPE.PaternityLeave:
                return LMSGeneralConstant.LEAVE_TYPE.text_PaternityLeave;
            case LMSGeneralConstant.LEAVE_TYPE.BereavementLeave:
                return LMSGeneralConstant.LEAVE_TYPE.text_BereavementLeave;
            case LMSGeneralConstant.LEAVE_TYPE.LeaveWithoutPay:
                return LMSGeneralConstant.LEAVE_TYPE.text_LeaveWithoutPay;
            case LMSGeneralConstant.LEAVE_TYPE.DayOff:
                return LMSGeneralConstant.LEAVE_TYPE.text_DayOff;
            default:
                return "";
        }
    }

    private String getLeaveStatus(int type) {
        switch (type) {
            case LMSGeneralConstant.LEAVE_STATUS.Pending:
                return "Pending";
            case LMSGeneralConstant.LEAVE_STATUS.Approved:
                return "Approved";
            case LMSGeneralConstant.LEAVE_STATUS.ApprovedCancel:
                return "Approved Cancelation";
            case LMSGeneralConstant.LEAVE_STATUS.Canceled:
                return "";
            case LMSGeneralConstant.LEAVE_STATUS.Rejected:
                return "Rejected";
            case LMSGeneralConstant.LEAVE_STATUS.PendingCancel:
                return "Pending Cancelation";
            case LMSGeneralConstant.LEAVE_STATUS.RejectedCancel:
                return "Rejected Cancelation";
            default:
                return "";
        }
    }

    private Float[] getDataTaken(int typeId) {
        return staffData.get(getLeaveType(typeId));
    }

    private int getDrawableFromType(String groupName) {
        if (groupName.equalsIgnoreCase(String.valueOf(LMSGeneralConstant.LEAVE_TYPE.BirthdayLeave))) {
            return R.drawable.present_icon;
        } else if (groupName.equals(String.valueOf(LMSGeneralConstant.LEAVE_TYPE.MilitaryLeave))) {
            return R.drawable.military_icon;
        } else if (groupName.equals(String.valueOf(LMSGeneralConstant.LEAVE_TYPE.SterilizationLeave))) {
            return R.drawable.needle_icon;
        } else if (groupName.equals(String.valueOf(LMSGeneralConstant.LEAVE_TYPE.OrdinationLeave))) {
            return R.drawable.ordination_icon;
        } else if (groupName.equals(String.valueOf(LMSGeneralConstant.LEAVE_TYPE.MaternityLeave))) {
            return R.drawable.maternity_icon;
        } else if (groupName.equals(String.valueOf(LMSGeneralConstant.LEAVE_TYPE.PaternityLeave))) {
            return R.drawable.paternity_icon;
        } else if (groupName.equals(String.valueOf(LMSGeneralConstant.LEAVE_TYPE.BereavementLeave))) {
            return R.drawable.bereavement_icon;
        } else if (groupName.equals(String.valueOf(LMSGeneralConstant.LEAVE_TYPE.LeaveWithoutPay))) {
            return R.drawable.creditcard_icon;
        } else if (groupName.equals(String.valueOf(LMSGeneralConstant.LEAVE_TYPE.DayOff))) {
            return R.drawable.star_blue;
        } else {
            return R.drawable.icon_app;
        }
    }

}
