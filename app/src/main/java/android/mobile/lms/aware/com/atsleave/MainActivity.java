package android.mobile.lms.aware.com.atsleave;

import android.content.Intent;
import android.mobile.lms.aware.com.atsleave.adapter.FragmentAdapters;
import android.mobile.lms.aware.com.atsleave.callback.BackPressed;
import android.mobile.lms.aware.com.atsleave.callback.RequestDialogListener;
import android.mobile.lms.aware.com.atsleave.customview.MyViewPager;
import android.mobile.lms.aware.com.atsleave.fragmentview.ApproveFragment;
import android.mobile.lms.aware.com.atsleave.fragmentview.HomePageFragment;
import android.mobile.lms.aware.com.atsleave.fragmentview.MyTeamFragment;
import android.mobile.lms.aware.com.atsleave.fragmentview.PendingRequestFragment;
import android.mobile.lms.aware.com.atsleave.fragmentview.RequestPageFragment;
import android.mobile.lms.aware.com.atsleave.utils.DialogUtils;
import android.mobile.lms.aware.com.atsleave.utils.OnChangePageView;
import android.mobile.lms.aware.com.atsleave.webservice.LMSGeneralConstant;
import android.os.Bundle;
import android.util.Log;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;

import com.crashlytics.android.Crashlytics;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.viewpagerindicator.CirclePageIndicator;
import com.yalantis.ucrop.UCrop;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends CustomActivity implements OnChangePageView {

    private boolean isApplyBirthDay = false;
    private boolean isApplyDayoff = false;
    private CirclePageIndicator mIndicator;
    private int page = LMSGeneralConstant.FIRST_PAGE;
    private int subPage = LMSGeneralConstant.HOME_PAGE;
    private Animation fade_out_on_bottom;
    private Animation fade_in_on_top;
    private MyViewPager pager;
    private LMSApplication application;
    private FragmentAdapters adapter;
    private BackPressed back;
    private FloatingActionButton fabMenu;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        application = LMSApplication.getInstance();

        if (application.getCurProfile() == null) {
            DialogUtils.showAlertDialog(this, "Load data is failed, Please try again.", false, new RequestDialogListener() {
                @Override
                public void responseSubmit() {
                    finish();
                }

                @Override
                public void responseCancel() {

                }
            });
        }

        Crashlytics.setUserName(application.getCurProfile().getNAME_ENG());
        Crashlytics.setUserEmail(application.getCurProfile().getEMAIL());
        Crashlytics.setUserIdentifier(application.getCurProfile().getEMP_CODE());

        //viewflipper = (ViewFlipper) findViewById(R.id.viewFlipper);
        fabMenu = (FloatingActionButton) findViewById(R.id.fabMenu);
        fade_in_on_top = AnimationUtils.loadAnimation(MainActivity.this, R.anim.fade_in_on_top);
        fade_out_on_bottom = AnimationUtils.loadAnimation(MainActivity.this, R.anim.fade_out_on_bottom);

        pager = (MyViewPager) findViewById(R.id.viewpager1);
        adapter = new FragmentAdapters(getSupportFragmentManager(), createFragmentFirstPage());
        pager.setAdapter(adapter);
        //  pager.setOffscreenPageLimit(0);
        pager.setCurrentItem(1);
        mIndicator = (CirclePageIndicator) findViewById(R.id.indicator);
        mIndicator.setViewPager(pager);
        pager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {
                page = LMSGeneralConstant.FIRST_PAGE;
                if (pager.getChildCount() == 4) {
                    if (position == 0) {
                        subPage = LMSGeneralConstant.REQUEST_PAGE;
                    } else if (position == 1) {
                        subPage = LMSGeneralConstant.HOME_PAGE;
                    } else if (position == 2) {
                        subPage = LMSGeneralConstant.PENDING_REQUEST_PAGE;
                    } else if (position == 3) {
                        subPage = LMSGeneralConstant.MY_TEAM_PAGE;
                    }
                } else {
                    if (position == 0) {
                        subPage = LMSGeneralConstant.REQUEST_PAGE;
                    } else if (position == 1) {
                        subPage = LMSGeneralConstant.HOME_PAGE;
                    } else if (position == 2) {
                        subPage = LMSGeneralConstant.MY_TEAM_PAGE;
                    }
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    @Override
    protected void onNewIntent(Intent intent) {
        String pageHome = intent.getExtras().getString("page", "");
        if (pageHome.equals("home")) {
            pager.setCurrentItem(1, true);
        }
    }

    @Override
    public void onBackPressed() {
        if (back != null)
            back.onBack();
    }

    @Override
    public void onAttachFragment(Fragment fragment) {
        super.onAttachFragment(fragment);
        try {
            back = (BackPressed) fragment;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public boolean isApplyBirthDay() {
        return isApplyBirthDay;
    }

    public boolean isApplyDayoff() {
        return isApplyDayoff;
    }

    public void setIsApplyBirthDay(boolean isapplayBirthDay) {
        this.isApplyBirthDay = isapplayBirthDay;
    }

    public void setIsApplayDayoff(boolean isapplayDayoff) {
        this.isApplyDayoff = isapplayDayoff;
    }

    private List<Fragment> createFragmentFirstPage() {
        List<Fragment> lists = new ArrayList<>();
        lists.add(RequestPageFragment.initRequestPageFragment("RequestPageFragment", this));
        lists.add(HomePageFragment.initHomePageFragment("HomePageFragment", this));

        String emp_role = application.getCurProfile().getEMP_ROLE();

        if (emp_role.equalsIgnoreCase(LMSGeneralConstant.EMP_ROLE.SUPERVISOR_TEXT) || emp_role.equalsIgnoreCase(LMSGeneralConstant.EMP_ROLE.DIRECTOR_TEXT)) {
            lists.add(PendingRequestFragment.initPendingReqeuestFragment("PendingRequestFragment", this));
            lists.add(ApproveFragment.initApproveFragment(this));
        }

        lists.add(MyTeamFragment.initMyTeamFragment("MyTeamFragment", this));
        return lists;
    }

    @Override
    public void setOnChangePager(int page, int sub_page) {
        this.page = page;
        this.subPage = sub_page;
        String emp_role = application.getCurProfile().getEMP_ROLE();
        if (page == LMSGeneralConstant.FIRST_PAGE) {

            if (sub_page == LMSGeneralConstant.REQUEST_PAGE) {
                pager.setCurrentItem(0, true);
            } else if (sub_page == LMSGeneralConstant.MY_TEAM_PAGE) {
                if (emp_role.equalsIgnoreCase(LMSGeneralConstant.EMP_ROLE.SUPERVISOR_TEXT) || emp_role.equalsIgnoreCase(LMSGeneralConstant.EMP_ROLE.DIRECTOR_TEXT)) {
                    pager.setCurrentItem(4, true);
                } else {
                    pager.setCurrentItem(2, true);
                }
            } else if (sub_page == LMSGeneralConstant.TEAM_LEAVE) {
                if (emp_role.equalsIgnoreCase(LMSGeneralConstant.EMP_ROLE.SUPERVISOR_TEXT) || emp_role.equalsIgnoreCase(LMSGeneralConstant.EMP_ROLE.DIRECTOR_TEXT)) {
                    pager.setCurrentItem(3, true);
                } else {
                    pager.setCurrentItem(2, true);
                }
            } else if (sub_page == LMSGeneralConstant.PENDING_REQUEST_PAGE) {
                if (emp_role.equalsIgnoreCase(LMSGeneralConstant.EMP_ROLE.SUPERVISOR_TEXT) || emp_role.equalsIgnoreCase(LMSGeneralConstant.EMP_ROLE.DIRECTOR_TEXT)) {
                    pager.setCurrentItem(2, true);
                } else {
                    pager.setCurrentItem(1, true);
                }
            } else if (sub_page == LMSGeneralConstant.HOME_PAGE) {
                pager.setCurrentItem(1, true);
            }
//            }
        } else if (page == LMSGeneralConstant.SECOND_PAGE) {
            Intent intent = new Intent(MainActivity.this, HistoryActivity.class);
            if (sub_page == LMSGeneralConstant.SICK_HISTORY_PAGE) {
                intent.putExtra("page", LMSGeneralConstant.SICK_HISTORY_PAGE);
            } else if (sub_page == LMSGeneralConstant.PERSONAL_HISTORY_PAGE) {
                intent.putExtra("page", LMSGeneralConstant.PERSONAL_HISTORY_PAGE);
            } else if (sub_page == LMSGeneralConstant.OTHER_HISTORY_PAGE) {
                intent.putExtra("page", LMSGeneralConstant.OTHER_HISTORY_PAGE);
            }
            startActivity(intent);
            overridePendingTransition(R.anim.fade_in_on_bottom, R.anim.fade_out);
        }
    }

    public int getPage() {
        return page;
    }

    public int getSubPage() {
        return subPage;
    }

    public void disableViewPager() {
        pager.setPagingEnabled(false);
    }

    public void enableViewPager() {
        pager.setPagingEnabled(true);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK && requestCode == UCrop.REQUEST_CROP) {
            Log.i("onActivityResult", "requestCode: " + requestCode + " resultCode: " + resultCode);
            HomePageFragment.instance.onActivityResult(requestCode, resultCode, data);
        }
    }
}
