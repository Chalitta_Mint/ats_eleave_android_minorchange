package android.mobile.lms.aware.com.atsleave.adapter;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.mobile.lms.aware.com.atsleave.R;
import android.mobile.lms.aware.com.atsleave.models.LeaveHistory;
import android.mobile.lms.aware.com.atsleave.utils.AwareUtils;
import android.mobile.lms.aware.com.atsleave.webservice.LMSGeneralConstant;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.allen.expandablelistview.BaseSwipeMenuExpandableListAdapter;
import com.baoyz.swipemenulistview2.ContentViewWrapper;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.SortedSet;
import java.util.TreeSet;

/**
 * Created by apinun.w on 16/7/2558.
 */
public class ExpandableFullHistoryListAdapter extends BaseSwipeMenuExpandableListAdapter {
    private HashMap<String, ArrayList<LeaveHistory>> childArray;
    SortedSet<String> keys;
    Object[] groupName;
    Activity activity;
    public ExpandableFullHistoryListAdapter(Activity activity,ArrayList<String> groupList,HashMap<String,ArrayList<LeaveHistory>> listChildData){

        this.activity = activity;
        this.childArray = listChildData;
        keys = new TreeSet<String>(listChildData.keySet());
        groupName = keys.toArray();
    }

    public HashMap<String, ArrayList<LeaveHistory>> getChildArray() {
        return childArray;
    }

    public void setChildArray(HashMap<String, ArrayList<LeaveHistory>> childArray) {
        this.childArray = childArray;
        keys = new TreeSet<String>(childArray.keySet());
        groupName = keys.toArray();
    }

    private SimpleDateFormat inputSDF = new SimpleDateFormat("yyyyMMddHHmm");       //    201509010830
    private SimpleDateFormat outputSDF = new SimpleDateFormat("EEE dd-MMM-yy",Locale.ENGLISH);

    /**

     /**
     * Whether this group item swipable
     *
     * @param groupPosition
     * @return
     * @see BaseSwipeMenuExpandableListAdapter#isGroupSwipable(int)
     */

    @Override
    public boolean isGroupSwipable(int groupPosition) {
        return false;
    }

    /**
     * Whether this child item swipable
     *
     * @param groupPosition
     * @param childPosition
     * @return
     * @see BaseSwipeMenuExpandableListAdapter#isChildSwipable(int,
     *      int)
     */

    @Override
    public boolean isChildSwipable(int groupPosition, int childPosition) {
        // Toast.makeText(getActivity(), "childPosition:"+childPosition, Toast.LENGTH_SHORT).show();
        return true;
    }

    @Override
    public int getChildType(int groupPosition, int childPosition) {

//        int childType = -1;
//        String childTypeText = "";
//        try {
//            childTypeText = childArray.get(groupName[groupPosition]).get(childPosition).getRL_STATUSID();
//            String str_startDate = childArray.get(groupName[groupPosition]).get(childPosition).getRLD_DATE_START();
//
//            boolean isExpireLimited = AwareUtils.checkExpireDate((Activity)context, str_startDate, "yyyyMMddHHmm"); //201508210830
//
//            if(isExpireLimited){
//                return 7;
//            }

//
//            childType = Integer.parseInt(childTypeText);
//        }catch (Exception e){
//            childType = 7;
//        }
//
//
//        return childType;
        String str_startDate = childArray.get(groupName[groupPosition]).get(childPosition).getRLD_DATE_START();
        boolean isExpireLimited = AwareUtils.checkExpireDate(activity, str_startDate, "yyyyMMddHHmm"); //201508210830

        if(isExpireLimited){
            return 7;
        }
        return Integer.parseInt(childArray.get(groupName[groupPosition]).get(childPosition).getRL_STATUSID());
    }

    @Override
    public int getChildTypeCount() {
        return 3;
    }

    @Override
    public int getGroupType(int groupPosition) {
        return groupPosition % 3;
    }

    @Override
    public int getGroupTypeCount() {
        return 3;
    }

    class ViewHolder {
        ImageView icon_list;
        TextView name_item;
        ImageView img_sick_leave;
        TextView txt_sick_date;
        TextView txt_sick_time;
        TextView txt_sick_status;
        TextView txt_sick_my_reason;
        TextView txt_sick_sup_reson;

        public ViewHolder(View view) {
            icon_list = (ImageView) view.findViewById(R.id.icon_list);
            name_item = (TextView) view.findViewById(R.id.name_item);
            img_sick_leave = (ImageView) view.findViewById(R.id.img_sick_leave);
            img_sick_leave = (ImageView) view.findViewById(R.id.img_sick_leave);
            txt_sick_date = (TextView) view.findViewById(R.id.txt_sick_date);
            txt_sick_time = (TextView) view.findViewById(R.id.txt_sick_time);
            txt_sick_status = (TextView) view.findViewById(R.id.txt_sick_status);
            txt_sick_my_reason = (TextView) view.findViewById(R.id.txt_sick_my_reason);
            txt_sick_sup_reson = (TextView) view.findViewById(R.id.txt_sick_sup_reson);

            view.setTag(this);
        }
    }

    @Override
    public int getGroupCount() {
        return groupName.length;
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return childArray.get(groupName[groupPosition]).size();
    }

    @Override
    public Object getGroup(int groupPosition) {
        int groupIndex = Integer.parseInt((String)groupName[groupPosition]);
        return  getMonthName(groupIndex);
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        // return "The " + childPosition + "'th child in " + groupPosition + "'th group.";
        return  childArray.get(groupName[groupPosition]).get(childPosition);
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public ContentViewWrapper getGroupViewAndReUsable(int groupPosition, boolean isExpanded, View convertView,
                                                      ViewGroup parent) {

        boolean reUseable = true;
        if (convertView == null) {
            convertView = View.inflate(activity, R.layout.group_full_item, null);
            convertView.setTag(new ViewHolder(convertView));
            reUseable = false;
        }

        ViewHolder holder = (ViewHolder) convertView.getTag();
        String GroupName = (String) getGroup(groupPosition);
        TextView name_item = (TextView) convertView.findViewById(R.id.name_item);
        ImageView icon_list = (ImageView) convertView.findViewById(R.id.icon_list);
        name_item.setText(GroupName);

        if(GroupName.equals("Birthday Leave")){
            icon_list.setBackgroundResource(R.drawable.present_icon);
        }else if(GroupName.equals("Military Leave")){
            icon_list.setBackgroundResource(R.drawable.military_icon);
        }else if(GroupName.equals("Sterilization Leave")){
            icon_list.setBackgroundResource(R.drawable.needle_icon);
        }else if(GroupName.equals("Ordination Leave")){
            icon_list.setBackgroundResource(R.drawable.ordination_icon);
        }else if(GroupName.equals("Maternity Leave")){
            icon_list.setBackgroundResource(R.drawable.maternity_icon);
        }else if(GroupName.equals("Paternity Leave")){
            icon_list.setBackgroundResource(R.drawable.paternity_icon);
        }else if(GroupName.equals("Berevement Leave")){
            icon_list.setBackgroundResource(R.drawable.bereavement_icon);
        }else if(GroupName.equals("Leave without Pay")){
            icon_list.setBackgroundResource(R.drawable.creditcard_icon);
        }

        return new ContentViewWrapper(convertView, reUseable);
    }

    @Override
    public ContentViewWrapper getChildViewAndReUsable(int groupPosition, int childPosition, boolean isLastChild, View convertView,
                                                      ViewGroup parent) {
        boolean reUseable = true;
        if (convertView == null) {
            convertView = View.inflate(activity, R.layout.layout_leave_listitem, null);
            convertView.setTag(new ViewHolder(convertView));
            reUseable = false;
        }

        ViewHolder holder = (ViewHolder) convertView.getTag();
        if (null == holder) {
            holder = new ViewHolder(convertView);
        }

        int imageId = R.drawable.denied;
        int status_id = Integer.parseInt(childArray.get(groupName[groupPosition]).get(childPosition).getRL_STATUSID());

        switch (status_id){
            case LMSGeneralConstant.LEAVE_STATUS.Approved :
                imageId = R.drawable.approved;
                break;
            case LMSGeneralConstant.LEAVE_STATUS.Rejected :
                imageId = R.drawable.denied;
                break;
            case LMSGeneralConstant.LEAVE_STATUS.PendingCancel :
                imageId = R.drawable.pending_cancel;
                break;
            case LMSGeneralConstant.LEAVE_STATUS.ApprovedCancel :
                imageId = R.drawable.approved_cancel;
                break;
            case LMSGeneralConstant.LEAVE_STATUS.Canceled:
                imageId = R.drawable.denied_cancel;
                break;
            case LMSGeneralConstant.LEAVE_STATUS.Pending :
                imageId = R.drawable.pending;
                break;
            case LMSGeneralConstant.LEAVE_STATUS.RejectedCancel :
                imageId = R.drawable.denied_cancel;
                break;
        }

        Bitmap bitmap = BitmapFactory.decodeResource(activity.getResources(), imageId);
        holder.img_sick_leave.setImageBitmap(bitmap);

        //       holder.img_sick_leave.setImageBitmap();
        LeaveHistory childData = childArray.get(groupName[groupPosition]).get(childPosition);
        String startDate =childData.getRLD_DATE_START();
        String endDate = childData.getRLD_DATE_END();
        Date parse_end_date = new Date();
        Date parse_start_date = new Date();
        Date startDateCompare = null;
        Date endDateCompate = null;

        if(startDate != null) {
            try {
                parse_start_date = inputSDF.parse(startDate);
                startDateCompare = inputSDF.parse(startDate);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }

        if(endDate != null) {
            try {
                parse_end_date = inputSDF.parse(endDate);
                endDateCompate = inputSDF.parse(endDate);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }

        String toDateText = "";

        startDateCompare.setHours(0);
        startDateCompare.setMinutes(0);
        endDateCompate.setHours(0);
        endDateCompate.setMinutes(0);

        if(startDateCompare.compareTo(endDateCompate)==0) {
            toDateText = outputSDF.format(parse_start_date);
        }else{
            toDateText = outputSDF.format(parse_start_date) + "-" +outputSDF.format(parse_end_date);
        }
        holder.txt_sick_date.setText(toDateText);

        //Sick Time
        String sickTime = "";
        String minuteTotal = childData.getRLD_ALLTIME();
        int i_minute = Integer.parseInt(minuteTotal);
        int int_date = i_minute / 480;
        float f_hour = (i_minute%480.0f) / 60.0f;

        if(int_date > 0) {
            sickTime += (int) int_date;
            sickTime += (int) int_date <= 1 ? " Day " : " Days ";
        }

        if(f_hour > 0){
            sickTime += (f_hour % 1 == 0? (int)f_hour+"": f_hour+"") + (f_hour <= 1? " Hour ":" Hours ");
        }

        if(int_date <= 0 && f_hour <= 0){
            sickTime = "0 Days";
        }

        holder.txt_sick_time.setText(sickTime);

        String sup_comment = "";
        if(childData.getRL_STATUSID().equalsIgnoreCase(LMSGeneralConstant.LEAVE_STATUS.Pending + "")){
            sup_comment = "N/A";
        }else{
            if(childData.getREMARK() != null) {
                if (childData.getREMARK().equalsIgnoreCase(""))
                    sup_comment = "N/A";
                else
                    sup_comment = childData.getREMARK();
            }else{
                sup_comment = "N/A";
            }
        }

        holder.txt_sick_sup_reson.setText(sup_comment);
        holder.txt_sick_status.setText(activity.getString(R.string.title_sick_status) +
                getLeaveStatus(Integer.parseInt(childData.getRL_STATUSID())));
        holder.txt_sick_my_reason.setText(childData.getREASON());

        return new ContentViewWrapper(convertView, reUseable);
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }

    private String getLeaveStatus(int type){
        switch (type){
            case LMSGeneralConstant.LEAVE_STATUS.Pending :
                return  "Pending";
            case LMSGeneralConstant.LEAVE_STATUS.Approved :
                return  "Approved";
            case LMSGeneralConstant.LEAVE_STATUS.ApprovedCancel :
                return  "Approved Cancelation";
            case LMSGeneralConstant.LEAVE_STATUS.Canceled :
                return  "";
            case LMSGeneralConstant.LEAVE_STATUS.Rejected :
                return  "Rejected";
            case LMSGeneralConstant.LEAVE_STATUS.PendingCancel :
                return  "Pending Cancelation";
            case LMSGeneralConstant.LEAVE_STATUS.RejectedCancel :
                return  "Rejected Cancelation";
            default:
                return "";
        }
    }

    private String getMonthName(int month){
        if(month == 1){
            return "JANUARY";
        }else if(month == 2){
            return "FEBRUARY";
        }else if(month == 3){
            return "MARCH";
        }else if(month == 4){
            return "APRIL";
        }else if(month == 5){
            return "MAY";
        }else if(month == 6){
            return "JUNE";
        }else if(month == 7){
            return "JULY";
        }else if(month == 8){
            return "AUGUST";
        }else if(month == 9){
            return "SEPTEMBER";
        }else if(month == 10){
            return "OCTOBER";
        }else if(month == 11){
            return "NOVEMBER";
        }else if(month == 12){
            return "DECEMBER";
        }else {
            return "";
        }
    }
}
