package android.mobile.lms.aware.com.atsleave.models;

import java.util.List;

/**
 * Created by apinun.w on 17/8/2558.
 */
public class RequestDaysOffResponse extends BaseResponse {

    private List<DataEntity> data;

    public void setData(List<DataEntity> data) {
        this.data = data;
    }

    public List<DataEntity> getData() {
        return data;
    }


    public static class DataEntity {

        private String NT_REQ_STATUS;
        private String NT_DATE;
        private String DEVICE_TOKEN;
        private String NT_REQ_TYPE;
        private String NT_SENDER;
        private String DEVICE_TYPE;
        private String NT_RECEIVER;
        private String NT_REQ_COUNT;
        private String NT_LEAVE_TYPE_ID;

        public void setNT_REQ_STATUS(String NT_REQ_STATUS) {
            this.NT_REQ_STATUS = NT_REQ_STATUS;
        }

        public void setNT_DATE(String NT_DATE) {
            this.NT_DATE = NT_DATE;
        }

        public void setDEVICE_TOKEN(String DEVICE_TOKEN) {
            this.DEVICE_TOKEN = DEVICE_TOKEN;
        }

        public void setNT_REQ_TYPE(String NT_REQ_TYPE) {
            this.NT_REQ_TYPE = NT_REQ_TYPE;
        }

        public void setNT_SENDER(String NT_SENDER) {
            this.NT_SENDER = NT_SENDER;
        }

        public void setDEVICE_TYPE(String DEVICE_TYPE) {
            this.DEVICE_TYPE = DEVICE_TYPE;
        }

        public void setNT_RECEIVER(String NT_RECEIVER) {
            this.NT_RECEIVER = NT_RECEIVER;
        }

        public void setNT_REQ_COUNT(String NT_REQ_COUNT) {
            this.NT_REQ_COUNT = NT_REQ_COUNT;
        }

        public void setNT_LEAVE_TYPE_ID(String NT_LEAVE_TYPE_ID) {
            this.NT_LEAVE_TYPE_ID = NT_LEAVE_TYPE_ID;
        }

        public String getNT_REQ_STATUS() {
            return NT_REQ_STATUS;
        }

        public String getNT_DATE() {
            return NT_DATE;
        }

        public String getDEVICE_TOKEN() {
            return DEVICE_TOKEN;
        }

        public String getNT_REQ_TYPE() {
            return NT_REQ_TYPE;
        }

        public String getNT_SENDER() {
            return NT_SENDER;
        }

        public String getDEVICE_TYPE() {
            return DEVICE_TYPE;
        }

        public String getNT_RECEIVER() {
            return NT_RECEIVER;
        }

        public String getNT_REQ_COUNT() {
            return NT_REQ_COUNT;
        }

        public String getNT_LEAVE_TYPE_ID() {
            return NT_LEAVE_TYPE_ID;
        }
    }
}
