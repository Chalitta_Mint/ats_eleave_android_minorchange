package android.mobile.lms.aware.com.atsleave;

import android.mobile.lms.aware.com.atsleave.adapter.FragmentAdapters;
import android.mobile.lms.aware.com.atsleave.fragmentview.OtherHistoryFragment;
import android.mobile.lms.aware.com.atsleave.fragmentview.PersonalHistoryFragment;
import android.mobile.lms.aware.com.atsleave.fragmentview.SickLeaveHistoryFragment;
import android.mobile.lms.aware.com.atsleave.callback.RequestDialogListener;
import android.mobile.lms.aware.com.atsleave.utils.DialogUtils;
import android.mobile.lms.aware.com.atsleave.utils.OnChangePageView;
import android.mobile.lms.aware.com.atsleave.webservice.LMSGeneralConstant;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;

import java.util.ArrayList;
import java.util.List;

public class HistoryActivity extends CustomActivity implements OnChangePageView {

    private ViewPager viewpager;
    private LMSApplication application;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_secondviewpager);

        application = LMSApplication.getInstance();
        int page = getIntent().getIntExtra("page", LMSGeneralConstant.SICK_HISTORY_PAGE);

        if (application.getCurProfile() == null) {
            DialogUtils.showAlertDialog(this, "Load data is failed,Please try again.", false, new RequestDialogListener() {
                @Override
                public void responseSubmit() {
                    finish();
                }

                @Override
                public void responseCancel() {

                }
            });
        }

        viewpager = (ViewPager) findViewById(R.id.viewpager2);
        FragmentAdapters adapter2 = new FragmentAdapters(getSupportFragmentManager(), createFragmentSecondPage());
        viewpager.setAdapter(adapter2);
        viewpager.setOffscreenPageLimit(3);

        if (page == LMSGeneralConstant.SICK_HISTORY_PAGE) {
            viewpager.setCurrentItem(0);
        } else if (page == LMSGeneralConstant.PERSONAL_HISTORY_PAGE) {
            viewpager.setCurrentItem(1);
        } else if (page == LMSGeneralConstant.OTHER_HISTORY_PAGE) {
            viewpager.setCurrentItem(2);
        }
    }

    private List<Fragment> createFragmentSecondPage() {
        List<Fragment> lists = new ArrayList<>();
        lists.add(SickLeaveHistoryFragment.initSickLeaveFragment("SickLeaveHistoryFragment", this));
        lists.add(PersonalHistoryFragment.initPersonalFragment("PersonalHistoryFragment", this));
        lists.add(OtherHistoryFragment.initOtherFragment("OtherHistoryFragment", this));
        return lists;
    }

    @Override
    public void setOnChangePager(int page, int position) {

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
    }
}
