package android.mobile.lms.aware.com.atsleave.fragmentview;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.mobile.lms.aware.com.atsleave.LMSApplication;
import android.mobile.lms.aware.com.atsleave.R;
import android.mobile.lms.aware.com.atsleave.adapter.ListPendingAdapter;
import android.mobile.lms.aware.com.atsleave.callback.OnListenerFromWSManager;
import android.mobile.lms.aware.com.atsleave.callback.RequestDialogListener;
import android.mobile.lms.aware.com.atsleave.models.HolidayResponse;
import android.mobile.lms.aware.com.atsleave.models.ReadDayOffResponse;
import android.mobile.lms.aware.com.atsleave.models.StaffSummaryResponse;
import android.mobile.lms.aware.com.atsleave.models.TeamLeaveResponse;
import android.mobile.lms.aware.com.atsleave.utils.AwareUtils;
import android.mobile.lms.aware.com.atsleave.utils.DialogUtils;
import android.mobile.lms.aware.com.atsleave.utils.EventBus;
import android.mobile.lms.aware.com.atsleave.utils.OnChangePageView;
import android.mobile.lms.aware.com.atsleave.webservice.LMSGeneralConstant;
import android.mobile.lms.aware.com.atsleave.webservice.LMSWSManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import androidx.annotation.Nullable;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import android.util.Log;
import android.util.TypedValue;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.baoyz.swipemenulistview.SwipeMenu;
import com.baoyz.swipemenulistview.SwipeMenuCreator;
import com.baoyz.swipemenulistview.SwipeMenuItem;
import com.baoyz.swipemenulistview.SwipeMenuListView;
import com.google.gson.Gson;
import com.squareup.otto.Subscribe;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

/**
 * Created by apinun.w on 31/7/2558.
 */
public class PendingRequestFragment extends MyTeamFragment {
    private String TAG = "PendingRequest";
    private OnChangePageView onChangePageView;
    private SwipeMenuListView listview_swipe;
    private ListPendingAdapter adapter;
    private LMSApplication application;
    private static final int DetailId = 111;
    private static final int ApproveId = 222;
    private static final int CancelId = 333;
    private static final String COMMA = ",";
    private static String PENDING_REQUEST = "";
    private RelativeLayout linear_first_title;
    private SwipeRefreshLayout swipeView;
    private Context context;
    private Dialog dialogWaiting;
    private Dialog cdd;
    //static value
    private static int status_id = ApproveId;
    private static int position_data = -1;
    private static String comment = "";
    private boolean isVisibleView = true;
    private static int countAlert = 0;
    private static int top_editText = -1;
    private static int bottom_editText = -1;
    private Dialog boxAddLeave;

    public static PendingRequestFragment initPendingReqeuestFragment(String fragment, OnChangePageView onchangePageView) {
        PendingRequestFragment pendingRequestFragment = new PendingRequestFragment();
        pendingRequestFragment.setOnChangePageView(onchangePageView);
        return pendingRequestFragment;
    }

    public void setOnChangePageView(OnChangePageView onChangePageView) {
        this.onChangePageView = onChangePageView;
    }

    final SwipeMenuCreator creator = new SwipeMenuCreator() {
        @Override
        public void create(SwipeMenu menu) {
            SwipeMenuItem menuItem = null;
            switch (menu.getViewType()) {
                case ListPendingAdapter.STATUS_PENDING_NORMAL:
                    menuItem = createMenuSwapItem("Details", R.drawable.swipe_icon_detail, R.color.bg_gray, R.color.bg_white);
                    menuItem.setId(DetailId);
                    menu.addMenuItem(menuItem);
                    menuItem = createMenuSwapItem("Approve", R.drawable.swipe_icon_approve, R.color.color_confirm, R.color.bg_white);
                    menuItem.setId(ApproveId);
                    menu.addMenuItem(menuItem);
                    menuItem = createMenuSwapItem("Deny", R.drawable.swipe_icon_deny, R.color.color_deny, R.color.bg_white);
                    menuItem.setId(CancelId);
                    menu.addMenuItem(menuItem);
                    break;
                case ListPendingAdapter.STATUS_PENDING_DAYS_OFF:
                    menuItem = createMenuSwapItem("Approve", R.drawable.swipe_icon_approve, R.color.color_confirm, R.color.bg_white);
                    menuItem.setId(ApproveId);
                    menu.addMenuItem(menuItem);
                    menuItem = createMenuSwapItem("Deny", R.drawable.swipe_icon_deny, R.color.color_deny, R.color.bg_white);
                    menuItem.setId(CancelId);
                    menu.addMenuItem(menuItem);
                    break;
            }
        }
    };

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        context = getActivity();
        application = LMSApplication.getInstance();
        PENDING_REQUEST = LMSGeneralConstant.LEAVE_STATUS.Pending + COMMA + LMSGeneralConstant.LEAVE_STATUS.PendingCancel;
        dialogWaiting = DialogUtils.createDialog(getActivity());
        View view = inflater.inflate(R.layout.layout_pending_request, container, false);
        swipeView = (SwipeRefreshLayout) view.findViewById(R.id.swipeRefreshLayout);
        listview_swipe = (SwipeMenuListView) view.findViewById(R.id.listview_swipe);
        linear_first_title = (RelativeLayout) view.findViewById(R.id.linear_first_title);
        adapter = new ListPendingAdapter(getActivity(), new ArrayList<Object>(), new ArrayList<Integer>(), new ArrayList<HolidayResponse.DataEntity>(), listOnClick);
        listview_swipe.setAdapter(adapter);
        listview_swipe.setExpanded(false);

        countAlert = 1;
        getTeamLeave();

        listview_swipe.setOnItemClickListener(onItemCLick);
        listview_swipe.setOnMenuItemClickListener(swipeMenuItemClick);

        swipeView.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                swipeView.setRefreshing(true);
                countAlert = 1;
                getTeamLeave();
            }
        });

        listview_swipe.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView absListView, int i) {

            }

            @Override
            public void onScroll(AbsListView absListView, int firstVisibleItem, int visibleItemCount, int totalItemCount) {

                boolean enable = false;
                if (absListView != null && absListView.getChildCount() > 0) {
                    // check if the first item of the list is visible
                    boolean firstItemVisible = absListView.getFirstVisiblePosition() == 0;
                    // check if the top of the first item is visible
                    boolean topOfFirstItemVisible = absListView.getChildAt(0).getTop() == 0;
                    // enabling or disabling the refresh layout
                    enable = firstItemVisible && topOfFirstItemVisible;
                } else if (absListView.getChildCount() == 0) {
                    enable = true;
                }
                swipeView.setEnabled(enable);
            }
        });

        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        onChangePageView = (OnChangePageView) context;
    }

    private void changeChildListView(final int id, final int position) {

        int visiblePosition = listview_swipe.getFirstVisiblePosition();
        View view = listview_swipe.getChildAt(position - visiblePosition);
        listview_swipe.getAdapter().getView(position, view, listview_swipe);
        LinearLayout relative_main = (LinearLayout) view.findViewById(R.id.relative_main1);
        LinearLayout relative_sub = (LinearLayout) view.findViewById(R.id.relative_second);
        LinearLayout cancel = (LinearLayout) view.findViewById(R.id.btn_cancel);
        LinearLayout btn_confirm = (LinearLayout) view.findViewById(R.id.btn_approve);
        ImageView img_approve = (ImageView) view.findViewById(R.id.img_approve);
        TextView text_confirm = (TextView) view.findViewById(R.id.text_confirm);
        final EditText editComment = (EditText) view.findViewById(R.id.edit_comment);
        int m_height = view.getHeight();
        int s_height = relative_sub.getHeight();
        if (top_editText == -1)
            top_editText = (int) AwareUtils.dpAsPixel(getActivity(), (float) ((m_height - s_height) / 2.0f) + editComment.getPaddingBottom());

        if (bottom_editText == -1)
            bottom_editText = (int) AwareUtils.dpAsPixel(getActivity(), (float) ((m_height - s_height) / 2.0f) + editComment.getPaddingTop());

        editComment.setHeight(m_height);

        if (id == ApproveId) {
            btn_confirm.setBackgroundColor(getResources().getColor(R.color.color_confirm_btn));
            img_approve.setImageDrawable(getResources().getDrawable(R.drawable.approved_cancel));
            text_confirm.setText("Approve");
        } else if (id == CancelId) {
            btn_confirm.setBackgroundColor(getResources().getColor(R.color.bg_red));
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                img_approve.setImageDrawable(getResources().getDrawable(R.drawable.denied, context.getTheme()));
            } else {
                img_approve.setImageDrawable(getResources().getDrawable(R.drawable.denied));
            }
            text_confirm.setText("Deny");
        }

        //case opened
        if (relative_sub.getVisibility() == View.VISIBLE) {
            relative_sub.setVisibility(View.GONE);
            relative_main.setVisibility(View.VISIBLE);

        }//case closed
        else {
            relative_sub.setVisibility(View.VISIBLE);
            relative_main.setVisibility(View.INVISIBLE);
            AwareUtils.keyboardHide(getActivity());
        }

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                changeChildListView(id, position);
            }
        });

        btn_confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String commentText = editComment.getText().toString();
                if (id == ApproveId) {
                    // change status
                    updateLeaveStatus(position, id, commentText);
                } else if (id == CancelId) {
                    if (!commentText.trim().equalsIgnoreCase("")) {
                        // change status
                        countAlert = 1;
                        updateLeaveStatus(position, id, commentText);
                    } else {
                        // take comment only for deny pending
                        DialogUtils.showAlertDialog(getActivity(), getString(R.string.alert_text_btn_deny), false);
                    }
                }
            }
        });

    }

    private void updateLeaveStatus(int position, int statusButton, String comment) {
        showAlertDialog();
        position_data = position;
        PendingRequestFragment.comment = comment;
        status_id = statusButton;
        int type = adapter.getListType().get(position);
        String requestId = "";
        String statusId = "";
        if (type == adapter.STATUS_PENDING_NORMAL) {
            TeamLeaveResponse.DataEntity data = (TeamLeaveResponse.DataEntity) adapter.getLeaveTeams().get(position);
            requestId = data.getRL_ID();
            statusId = data.getRL_STATUSID();
        } else if (type == adapter.STATUS_PENDING_DAYS_OFF) {
            ReadDayOffResponse.DataEntity data = (ReadDayOffResponse.DataEntity) adapter.getLeaveTeams().get(position);
            requestId = data.getRD_ID();
            statusId = data.getRD_STATUSID();
        }

        String targetStatusId = "";
        if (type == adapter.STATUS_PENDING_NORMAL) {
            if (statusId.equalsIgnoreCase("1")) {
                if (statusButton == ApproveId)
                    targetStatusId = "3";
                else if (statusButton == CancelId)
                    targetStatusId = "4";

            } else if (statusId.equalsIgnoreCase("5")) {
                if (statusButton == ApproveId)
                    targetStatusId = "6";
                else if (statusButton == CancelId)
                    targetStatusId = "7";
            }

            LMSWSManager.updateLeaveStatus(
                    getActivity(),
                    application.getCurProfile().getSTAFF_TOKEN(),
                    requestId,
                    targetStatusId,
                    comment,
                    onResponseWS
            );
        } else {
            //DAY OFF
            if (statusId.equalsIgnoreCase("1")) {
                if (statusButton == ApproveId)
                    targetStatusId = "3";
                else if (statusButton == CancelId)
                    targetStatusId = "2";

            }

            countAlert = 1;
            showAlertDialog();
            LMSWSManager.changeDayOffStatus(getActivity(), application.getCurProfile().getSTAFF_TOKEN(), requestId, targetStatusId, onResponseWS);
        }
    }

    Handler seconds;

    private OnListenerFromWSManager onResponseWS = new OnListenerFromWSManager() {
        @Override
        public void onComplete(String statusCode, String message) {

            if (boxAddLeave != null) {
                if (boxAddLeave.isShowing())
                    boxAddLeave.dismiss();
            }

            cdd = new Dialog(context);
            cdd.getWindow().setBackgroundDrawable(new ColorDrawable(Color.argb(0, 0, 0, 0)));
            cdd.requestWindowFeature(Window.FEATURE_NO_TITLE);
            cdd.setContentView(R.layout.custom_alert_success);
            TextView detailAlert = (TextView) cdd.findViewById(R.id.alertText);
            LinearLayout linear_main = (LinearLayout) cdd.findViewById(R.id.linear_main);
            ImageView img_show_confirm = (ImageView) cdd.findViewById(R.id.img_show_confirm);

            if (status_id == ApproveId) {
                linear_main.setBackground(getResources().getDrawable(R.drawable.shape_alert_success));
                img_show_confirm.setImageDrawable(getResources().getDrawable(R.drawable.pending_confirm));
            } else if (status_id == CancelId) {
                linear_main.setBackground(getResources().getDrawable(R.drawable.shape_alert_deny));
                img_show_confirm.setImageDrawable(getResources().getDrawable(R.drawable.pending_remove));
            }
            detailAlert.setTextSize(TypedValue.COMPLEX_UNIT_PX, context.getResources().getDimension(R.dimen.text_normal));
            detailAlert.setText("Success");
            cdd.setCanceledOnTouchOutside(false);
            cdd.setCancelable(true);
            cdd.show();

            seconds = new Handler() {
                @Override
                public void handleMessage(Message msg) {
                    cdd.dismiss();
                }
            };

            seconds.postDelayed(runDelay, 4000);

            List<Object> leaveTeams = adapter.getLeaveTeams();
            List<Integer> listType = adapter.getListType();
            leaveTeams.remove(position_data);
            listType.remove(position_data);
            adapter.setListType(listType);
            adapter.setLeaveTeams(leaveTeams);
            listview_swipe.setAdapter(adapter);
            adapter.notifyDataSetChanged();

            dismissDialog();
            dismissRefresh();
            int emp_count = application.getIntEmpShowBadge();
            application.setIntEmpShowBadge(--emp_count);
        }

        @Override
        public void onFailed(String statusCode, String message) {
            dismissDialog();
            dismissRefresh();
            showDialogSentDataAgain();
        }

        @Override
        public void onFailed(String message) {
            dismissDialog();
            dismissRefresh();
            showDialogSentDataAgain();
        }

        @Override
        public void onFailedAndForceLogout() {
            dismissDialog();
            dismissRefresh();
            if (!AwareUtils.isForceLogout) {
                AwareUtils.forceLogOut(getActivity());
            }
        }
    };

    private void getTeamLeave() {
        showAlertDialog();
        LMSWSManager.getTeamLeave(getActivity(), application.getCurProfile().getSTAFF_TOKEN(), "", "", "", PENDING_REQUEST, "", teamLeaveListener);
    }

    Runnable runDelay = new Runnable() {
        @Override
        public void run() {
            seconds.sendMessage(new Message());
        }
    };

    private SwipeMenuItem createMenuSwapItem(String title, int drawable, int color, int txt_color) {
        SwipeMenuItem itemMenu = new SwipeMenuItem(getActivity());
        itemMenu.setBackground(new ColorDrawable(getResources().getColor(color)));
        // set item width
        itemMenu.setWidth(AwareUtils.dp2px(getActivity(), 90));
        // set a icon
        itemMenu.setIcon(drawable);
        itemMenu.setTitle(title);
        itemMenu.setTitleSize(12);
        itemMenu.setTitleColor(getResources().getColor(txt_color));

        return itemMenu;
    }

    private SwipeMenuListView.OnMenuItemClickListener swipeMenuItemClick = new SwipeMenuListView.OnMenuItemClickListener() {
        @Override
        public boolean onMenuItemClick(int position, SwipeMenu menu, int index) {
            int type = adapter.getListType().get(position);

            switch (menu.getMenuItem(index).getId()) {
                case ApproveId:
                    if (type == adapter.STATUS_PENDING_DAYS_OFF) {
                        ReadDayOffResponse.DataEntity data = (ReadDayOffResponse.DataEntity) adapter.getLeaveTeams().get(position);
                        String requestId = data.getRD_ID();
                        String statusId = data.getRD_STATUSID();
                        position_data = position;
                        updateDayOffStatus(ApproveId, statusId, requestId);
                    } else {
                        changeChildListView(ApproveId, position);
                    }

                    break;
                case CancelId:
                    if (type == adapter.STATUS_PENDING_DAYS_OFF) {
                        ReadDayOffResponse.DataEntity data = (ReadDayOffResponse.DataEntity) adapter.getLeaveTeams().get(position);
                        String requestId = data.getRD_ID();
                        String statusId = data.getRD_STATUSID();
                        position_data = position;
                        updateDayOffStatus(CancelId, statusId, requestId);
                    } else {
                        changeChildListView(CancelId, position);
                    }
                    break;
                case DetailId:
                    showPopUpDialogDetail(position);
                    break;
            }
            return true;
        }
    };

    private AdapterView.OnItemClickListener onItemCLick = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            listview_swipe.smoothOpenMenu(position);
        }
    };

    private View.OnClickListener listOnClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

        }
    };

    /***********************************
     * Detail Dialog
     ***********************************/
    private void showPopUpDialogDetail(final int position) {
        int type = adapter.getListType().get(position);
//        String requestId = "";
//        String statusId = "";
        String leaveTypeId = "";
        String date_start = "";
        String date_end = "";
        String remark = "";
        String reason = "";
        String sent = "";
        String staff_id = "";
        if (type == adapter.STATUS_PENDING_NORMAL) {
            TeamLeaveResponse.DataEntity data = (TeamLeaveResponse.DataEntity) adapter.getLeaveTeams().get(position);
//            requestId = data.getRL_ID();
//            statusId = data.getRL_STATUSID();
            leaveTypeId = data.getLEAVETYPEID();
            date_start = data.getRLD_DATE_START();
            date_end = data.getRLD_DATE_END();
            remark = data.getREMARK();
            reason = data.getREASON();
            sent = data.getRL_DATE();
            staff_id = data.getSTF_ID();
        } else if (type == adapter.STATUS_PENDING_DAYS_OFF) {
            ReadDayOffResponse.DataEntity data = (ReadDayOffResponse.DataEntity) adapter.getLeaveTeams().get(position);
//            requestId = data.getRD_ID();
//            statusId = data.getRD_STATUSID();
            leaveTypeId = "11";
            staff_id = data.getRD_REQUESTTO();
        }
        boxAddLeave = new Dialog(getActivity());
//        boxAddLeave.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));           //Background not black
        int width_of_dialog = (int) (application.getWidth_screen() * 0.85f);
        int height_of_dialog = (int) (application.getHeight_screen() * 0.8f);
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(boxAddLeave.getWindow().getAttributes());
        lp.width = width_of_dialog;
        lp.height = height_of_dialog;


        boxAddLeave.setCanceledOnTouchOutside(false);
        boxAddLeave.getWindow().setBackgroundDrawable(new ColorDrawable(Color.argb(0, 0, 0, 0)));
        boxAddLeave.requestWindowFeature(Window.FEATURE_NO_TITLE);
        boxAddLeave.setContentView(R.layout.custom_box_pending_history);

        final LinearLayout linear_form_detail = (LinearLayout) boxAddLeave.findViewById(R.id.linear_form_detail);
        final LinearLayout linear_form_confirm = (LinearLayout) boxAddLeave.findViewById(R.id.linear_form_confirm);
        ImageView btn_approve_detail = (ImageView) boxAddLeave.findViewById(R.id.btn_approve_detail);
        ImageView btn_deny_detail = (ImageView) boxAddLeave.findViewById(R.id.btn_deny_detail);
        final ImageView btn_confirm_approve = (ImageView) boxAddLeave.findViewById(R.id.btn_confirm_approve);
        LinearLayout linear_btn_confirm_cancel = (LinearLayout) boxAddLeave.findViewById(R.id.linear_btn_confirm_cancel);
        final EditText edit_confirm_comment = (EditText) boxAddLeave.findViewById(R.id.edit_confirm_comment);
        RelativeLayout box_btn_comment = (RelativeLayout) boxAddLeave.findViewById(R.id.box_btn_comment);

        TextView text_leave_type = (TextView) boxAddLeave.findViewById(R.id.text_leave_type);
        TextView text_when = (TextView) boxAddLeave.findViewById(R.id.text_when);
        TextView text_to = (TextView) boxAddLeave.findViewById(R.id.text_to);
        TextView text_comments = (TextView) boxAddLeave.findViewById(R.id.text_comments);
        TextView text_sent = (TextView) boxAddLeave.findViewById(R.id.text_sent);
        TextView text_remark = (TextView) boxAddLeave.findViewById(R.id.text_remark);
        LinearLayout ly_remark = (LinearLayout) boxAddLeave.findViewById(R.id.ly_remark);
        linear_form_confirm.setVisibility(View.INVISIBLE);
        linear_form_detail.setVisibility(View.VISIBLE);

        text_leave_type.setText(getLeaveTypeFromId(leaveTypeId));
        Date startDate = new Date();
        Date endDate = new Date();
        Date sendDate = new Date();
        boolean formatDate = true;
        try {
            startDate = LMSGeneralConstant.sdp_input2.parse(date_start);
            endDate = LMSGeneralConstant.sdp_input2.parse(date_end);
            sendDate = LMSGeneralConstant.sdp_input.parse(sent);
        } catch (ParseException e) {
            e.printStackTrace();
            formatDate = false;
        }

        if (formatDate) {
            text_when.setText(LMSGeneralConstant.sdp_output_pendinghis.format(startDate));
            text_to.setText(LMSGeneralConstant.sdp_output_pendinghis.format(endDate));
            text_sent.setText(LMSGeneralConstant.sdp_output_pendinghis.format(sendDate));
        } else {
            text_when.setText("");
            text_sent.setText("");
            text_to.setText("");
        }

        if (reason != null) {
            if (reason.equalsIgnoreCase(""))
                reason = "";
        } else {
            reason = "";
        }
        text_comments.setText(reason);

        if (remark != null) {
            if (remark.equals(""))
                ly_remark.setVisibility(View.GONE);
            else
                text_remark.setText(remark);
        } else {
            ly_remark.setVisibility(View.GONE);
        }

        btn_approve_detail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //show edit text confirm
                linear_form_confirm.setVisibility(View.VISIBLE);
                linear_form_detail.setVisibility(View.INVISIBLE);

                //change icon approve
                btn_confirm_approve.setImageDrawable(getResources().getDrawable(R.drawable.apprive_icon));
                btn_confirm_approve.setTag(0);
            }
        });

        btn_deny_detail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //show edit text deny
                linear_form_confirm.setVisibility(View.VISIBLE);
                linear_form_detail.setVisibility(View.INVISIBLE);

                //change icon deny
                btn_confirm_approve.setImageDrawable(getResources().getDrawable(R.drawable.deny_icon));
                btn_confirm_approve.setTag(1);
            }
        });

        linear_btn_confirm_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //show detail form
                linear_form_confirm.setVisibility(View.INVISIBLE);
                linear_form_detail.setVisibility(View.VISIBLE);

                edit_confirm_comment.setText("");
            }
        });

        box_btn_comment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boxAddLeave.dismiss();
            }
        });

        btn_confirm_approve.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //check deny or approve
                int status_confirm = (int) btn_confirm_approve.getTag();
                String edit_text_string = edit_confirm_comment.getText().toString();
                if (status_confirm == 0) {                    //confirm
                    //sent tvice confirm update status
                    //pending -> approve
                    countAlert = 1;
                    updateLeaveStatus(position, ApproveId, edit_text_string);

                } else if (status_confirm == 1) {              //deny
                    //sent service deny update status
                    //pending -> canceled
                    if (edit_text_string.trim().equalsIgnoreCase("")) {
                        DialogUtils.showAlertDialog(getActivity(), getString(R.string.title_alert_edit_deny), false);
                    } else {
                        updateLeaveStatus(position, CancelId, edit_text_string);
                    }
                }
            }
        });


        LMSWSManager.getStaffSummary(getActivity(), application.getCurProfile().getSTAFF_TOKEN(), staff_id, new OnListenerFromWSManager() {
            @Override
            public void onComplete(String statusCode, String message) {
                dismissDialog();
                dismissRefresh();

                Gson gson = new Gson();
                StaffSummaryResponse datas = gson.fromJson(message, StaffSummaryResponse.class);
                List<StaffSummaryResponse.DataEntity> lists = datas.getData();

                final HashMap<String, Integer> leaveModels = analysisSummary(lists);

                final TextView value_annual = (TextView) boxAddLeave.findViewById(R.id.value_annual);
                final TextView value_birthday = (TextView) boxAddLeave.findViewById(R.id.value_birthday);
                final TextView value_sick = (TextView) boxAddLeave.findViewById(R.id.value_sick);
                final TextView value_military = (TextView) boxAddLeave.findViewById(R.id.value_military);
                final TextView value_sterilization = (TextView) boxAddLeave.findViewById(R.id.value_sterilization);
                final TextView value_ordination = (TextView) boxAddLeave.findViewById(R.id.value_ordination);
                final TextView value_maternity = (TextView) boxAddLeave.findViewById(R.id.value_maternity);
                final TextView value_paternity = (TextView) boxAddLeave.findViewById(R.id.value_paternity);
                final TextView value_bereavement = (TextView) boxAddLeave.findViewById(R.id.value_bereavement);
                final TextView value_leave = (TextView) boxAddLeave.findViewById(R.id.value_leave);
                final TextView value_dayoff = (TextView) boxAddLeave.findViewById(R.id.value_dayoff);

                final RelativeLayout row_title1 = (RelativeLayout) boxAddLeave.findViewById(R.id.row_title1);
                final RelativeLayout row_title2 = (RelativeLayout) boxAddLeave.findViewById(R.id.row_title2);
                final RelativeLayout row_title3 = (RelativeLayout) boxAddLeave.findViewById(R.id.row_title3);
                final RelativeLayout row_title4 = (RelativeLayout) boxAddLeave.findViewById(R.id.row_title4);
                final RelativeLayout row_title5 = (RelativeLayout) boxAddLeave.findViewById(R.id.row_title5);
                final RelativeLayout row_title6 = (RelativeLayout) boxAddLeave.findViewById(R.id.row_title6);
                final RelativeLayout row_title7 = (RelativeLayout) boxAddLeave.findViewById(R.id.row_title7);
                final RelativeLayout row_title8 = (RelativeLayout) boxAddLeave.findViewById(R.id.row_title8);
                final RelativeLayout row_title9 = (RelativeLayout) boxAddLeave.findViewById(R.id.row_title9);
                final RelativeLayout row_title10 = (RelativeLayout) boxAddLeave.findViewById(R.id.row_title10);
                final RelativeLayout row_title11 = (RelativeLayout) boxAddLeave.findViewById(R.id.row_title11);

                final Object[] keys = leaveModels.keySet().toArray();

                HashMap<Integer, Integer> index_data = new HashMap<Integer, Integer>();
                for (Object model : keys) {
                    String leave_type = (String) model;
                    String str_time = "";
                    int i_minute = leaveModels.get(leave_type);

                    int int_date = i_minute / 480;
                    float f_hour = (float) (i_minute % 480) / 60.0f;

                    if (f_hour == 0) {
                        str_time = int_date + "D";
                    } else {
                        if (int_date == 0) {
                            if (f_hour % 1 == 0) {
                                str_time = (int) f_hour + "H";
                            } else {
                                str_time = f_hour + "H";
                            }
                        } else {
                            if (f_hour % 1 == 0) {
                                str_time = int_date + "D " + (int) f_hour + "H";
                            } else {
                                str_time = int_date + "D " + f_hour + "H";
                            }
                        }
                    }

                    if (leave_type.equalsIgnoreCase("Annual Leave")) {
                        value_annual.setText(str_time);
                        index_data.put(1, 1);
                    }
                    if (leave_type.equalsIgnoreCase("Birthday Leave")) {
                        value_birthday.setText(str_time);
                        index_data.put(2, 2);
                    }
                    if (leave_type.equalsIgnoreCase("Sick Leave")) {
                        value_sick.setText(str_time);
                        index_data.put(3, 3);
                    }
                    if (leave_type.equalsIgnoreCase("Military Leave")) {
                        value_military.setText(str_time);
                        index_data.put(4, 4);
                    }
                    if (leave_type.equalsIgnoreCase("Sterilization Leave")) {
                        value_sterilization.setText(str_time);
                        index_data.put(5, 5);
                    }
                    if (leave_type.equalsIgnoreCase("Ordination Leave")) {
                        value_ordination.setText(str_time);
                        index_data.put(6, 6);
                    }
                    if (leave_type.equalsIgnoreCase("Maternity Leave")) {
                        value_maternity.setText(str_time);
                        index_data.put(7, 7);
                    }
                    if (leave_type.equalsIgnoreCase("Paternity Leave")) {
                        value_paternity.setText(str_time);
                        index_data.put(8, 8);
                    }
                    if (leave_type.equalsIgnoreCase("Bereavement Leave")) {
                        value_bereavement.setText(str_time);
                        index_data.put(9, 9);
                    }
                    if (leave_type.equalsIgnoreCase("Leave Without Pay")) {
                        value_leave.setText(str_time);
                        index_data.put(10, 10);
                    }
                    if (leave_type.equalsIgnoreCase("Day Off")) {
                        value_dayoff.setText(str_time);
                        index_data.put(11, 11);
                    }

                }

                for (int i = 1; i <= 11; i++) {
                    int visible = View.GONE;
                    if (index_data.get(i) != null) {
                        visible = View.VISIBLE;
                    }

                    if (i == 1) {
                        row_title1.setVisibility(visible);
                    } else if (i == 2) {
                        row_title2.setVisibility(visible);
                    } else if (i == 3) {
                        row_title3.setVisibility(visible);
                    } else if (i == 4) {
                        row_title4.setVisibility(visible);
                    } else if (i == 5) {
                        row_title5.setVisibility(visible);
                    } else if (i == 6) {
                        row_title6.setVisibility(visible);
                    } else if (i == 7) {
                        row_title7.setVisibility(visible);
                    } else if (i == 8) {
                        row_title8.setVisibility(visible);
                    } else if (i == 9) {
                        row_title9.setVisibility(visible);
                    } else if (i == 10) {
                        row_title10.setVisibility(visible);
                    } else if (i == 11) {
                        row_title11.setVisibility(visible);
                    }
                }

                if (index_data.size() == 0) {
                    row_title1.setVisibility(View.INVISIBLE);
                    row_title2.setVisibility(View.INVISIBLE);
                    row_title3.setVisibility(View.INVISIBLE);
                    row_title4.setVisibility(View.INVISIBLE);
                }

                boxAddLeave.show();
            }

            @Override
            public void onFailed(String statusCode, String message) {
                dismissDialog();
                dismissRefresh();
            }

            @Override
            public void onFailed(String message) {
                dismissDialog();
                dismissRefresh();
            }

            @Override
            public void onFailedAndForceLogout() {
                dismissDialog();
                dismissRefresh();
                if (!AwareUtils.isForceLogout) {
                    AwareUtils.forceLogOut(getActivity());
                }
            }
        });

        boxAddLeave.show();
        boxAddLeave.getWindow().setAttributes(lp);
    }

    private HashMap<String, Integer> analysisSummary(List<StaffSummaryResponse.DataEntity> liststaff) {
        HashMap<String, Integer> staffs = new HashMap<>();
        for (StaffSummaryResponse.DataEntity model : liststaff) {
            int minute = 0;
            if (model.getALLTIMEUSED() != null) {
                minute = Integer.parseInt(model.getALLTIMEUSED());
                staffs.put(model.getLEAVENAME(), minute);
            }
        }
        return staffs;
    }

    /*response from pending request*/
    OnListenerFromWSManager teamLeaveListener = new OnListenerFromWSManager() {
        @Override
        public void onComplete(String statusCode, String message) {
            dismissDialog();
            dismissRefresh();
            Gson gson = new Gson();
            final TeamLeaveResponse members = gson.fromJson(message, TeamLeaveResponse.class);
            final List<TeamLeaveResponse.DataEntity> dataEntity = members.getData();

            if (dialogWaiting.isShowing())
                dialogWaiting.dismiss();

            listview_swipe.setMenuCreator(creator);

            final SimpleDateFormat outFormat = new SimpleDateFormat("yyyyMMddHHss");        //201510090830
            //sort by date
            Collections.sort(dataEntity, new Comparator<TeamLeaveResponse.DataEntity>() {
                @Override
                public int compare(TeamLeaveResponse.DataEntity lhs, TeamLeaveResponse.DataEntity rhs) {
                    Date lhsDate = null;
                    Date rhsDate = null;
                    try {
                        lhsDate = outFormat.parse(lhs.getRLD_DATE_START());
                        rhsDate = outFormat.parse(rhs.getRLD_DATE_START());
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }

                    if (lhsDate == null || rhsDate == null)
                        return 0;

                    return lhsDate.compareTo(rhsDate);
                }
            });

            //set by type
            List<Object> objs = new ArrayList<Object>();
            List<Integer> listType = new ArrayList<Integer>();
            for (int i = 0; i < dataEntity.size(); i++) {
                objs.add(dataEntity.get(i));
                listType.add(adapter.STATUS_PENDING_NORMAL);
            }

            adapter.setListType(listType);
            adapter.setLeaveTeams(objs);
            listview_swipe.setAdapter(adapter);
            adapter.notifyDataSetChanged();

            String emp_role = application.getCurProfile().getEMP_ROLE();
            if (emp_role.equalsIgnoreCase(LMSGeneralConstant.EMP_ROLE.DIRECTOR_TEXT)) {
                String token = application.getCurProfile().getSTAFF_TOKEN();
                String statusId = "1";
                showAlertDialog();
                countAlert = 1;
                LMSWSManager.readDayOff(getActivity(), token, "", "", statusId, onResponseReadDayOff);
            }
        }

        @Override
        public void onFailed(String statusCode, String message) {
            dismissDialog();
            dismissRefresh();
        }

        @Override
        public void onFailed(String message) {
            dismissDialog();
            dismissRefresh();
        }

        @Override
        public void onFailedAndForceLogout() {
            dismissDialog();
            dismissRefresh();
            if (!AwareUtils.isForceLogout) {
                AwareUtils.forceLogOut(getActivity());
            }
        }
    };

    private OnListenerFromWSManager onResponseReadDayOff = new OnListenerFromWSManager() {
        @Override
        public void onComplete(String statusCode, String message) {
            dismissDialog();
            dismissRefresh();
            Gson gson = new Gson();
            ReadDayOffResponse readDayOffResponse = gson.fromJson(message, ReadDayOffResponse.class);
            List<ReadDayOffResponse.DataEntity> dataEntity = readDayOffResponse.getData();

            final SimpleDateFormat outFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");        //RD_REQUESTDATE: "2015-10-06 16:40:02",

            //sort data from start date
            Collections.sort(dataEntity, new Comparator<ReadDayOffResponse.DataEntity>() {
                @Override
                public int compare(ReadDayOffResponse.DataEntity lhs, ReadDayOffResponse.DataEntity rhs) {
                    Date lhsDate = null;
                    Date rhsDate = null;
                    try {
                        lhsDate = outFormat.parse(lhs.getRD_REQUESTDATE());
                        rhsDate = outFormat.parse(rhs.getRD_REQUESTDATE());
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }

                    if (lhsDate == null || rhsDate == null)
                        return 0;

                    return lhsDate.compareTo(rhsDate);
                }
            });


            final List<Object> listObj_temp = adapter.getLeaveTeams();
            final List<Integer> listType_temp = adapter.getListType();
            for (int i = 0; i < dataEntity.size(); i++) {
                listObj_temp.add(dataEntity.get(i));
                listType_temp.add(adapter.STATUS_PENDING_DAYS_OFF);
            }

            adapter.setListType(listType_temp);
            adapter.setLeaveTeams(listObj_temp);
            listview_swipe.setAdapter(adapter);
            adapter.notifyDataSetChanged();

        }

        @Override
        public void onFailed(String statusCode, String message) {
            dismissDialog();
            dismissRefresh();
        }

        @Override
        public void onFailed(String message) {
            dismissDialog();
            dismissRefresh();
        }

        @Override
        public void onFailedAndForceLogout() {
            dismissDialog();
            dismissRefresh();
            if (!AwareUtils.isForceLogout) {
                AwareUtils.forceLogOut(getActivity());
            }
        }
    };

    private String getLeaveTypeFromId(String leavetype) {
        if (leavetype.equalsIgnoreCase(String.valueOf(LMSGeneralConstant.LEAVE_TYPE.AnnualLeave))) {
            return LMSGeneralConstant.LEAVE_TYPE.text_AnnualLeave;
        } else if (leavetype.equalsIgnoreCase(String.valueOf(LMSGeneralConstant.LEAVE_TYPE.BirthdayLeave))) {
            return LMSGeneralConstant.LEAVE_TYPE.text_BirthdayLeave;
        } else if (leavetype.equalsIgnoreCase(String.valueOf(LMSGeneralConstant.LEAVE_TYPE.SickLeave))) {
            return LMSGeneralConstant.LEAVE_TYPE.text_SickLeave;
        } else if (leavetype.equalsIgnoreCase(String.valueOf(LMSGeneralConstant.LEAVE_TYPE.MilitaryLeave))) {
            return LMSGeneralConstant.LEAVE_TYPE.text_MilitaryLeave;
        } else if (leavetype.equalsIgnoreCase(String.valueOf(LMSGeneralConstant.LEAVE_TYPE.SterilizationLeave))) {
            return LMSGeneralConstant.LEAVE_TYPE.text_SterilizationLeave;
        } else if (leavetype.equalsIgnoreCase(String.valueOf(LMSGeneralConstant.LEAVE_TYPE.OrdinationLeave))) {
            return LMSGeneralConstant.LEAVE_TYPE.text_OrdinationLeave;
        } else if (leavetype.equalsIgnoreCase(String.valueOf(LMSGeneralConstant.LEAVE_TYPE.MaternityLeave))) {
            return LMSGeneralConstant.LEAVE_TYPE.text_MaternityLeave;
        } else if (leavetype.equalsIgnoreCase(String.valueOf(LMSGeneralConstant.LEAVE_TYPE.PaternityLeave))) {
            return LMSGeneralConstant.LEAVE_TYPE.text_PaternityLeave;
        } else if (leavetype.equalsIgnoreCase(String.valueOf(LMSGeneralConstant.LEAVE_TYPE.BereavementLeave))) {
            return LMSGeneralConstant.LEAVE_TYPE.text_BereavementLeave;
        } else if (leavetype.equalsIgnoreCase(String.valueOf(LMSGeneralConstant.LEAVE_TYPE.LeaveWithoutPay))) {
            return LMSGeneralConstant.LEAVE_TYPE.text_LeaveWithoutPay;
        } else if (leavetype.equalsIgnoreCase(String.valueOf(LMSGeneralConstant.LEAVE_TYPE.DayOff))) {
            return LMSGeneralConstant.LEAVE_TYPE.text_DayOff;
        } else {
            return LMSGeneralConstant.LEAVE_TYPE.text_AnnualLeave;
        }
    }

    private void showDialogSentDataAgain() {

        DialogUtils.showAlertDismissofRetry(context, getString(R.string.alert_text_sent_again), new RequestDialogListener() {
            @Override
            public void responseSubmit() {
                updateLeaveStatus(position_data, status_id, comment);
            }

            @Override
            public void responseCancel() {

            }

        });
    }

    private void showAlertDialog() {
        if (!dialogWaiting.isShowing() && isVisibleView) {
            dialogWaiting.show();
        }
    }

    private void dismissDialog() {
        if (dialogWaiting.isShowing() && --countAlert <= 0) {
            dialogWaiting.dismiss();
            countAlert = 0;
        }
    }

    private void dismissRefresh() {
        if (swipeView.isRefreshing())
            swipeView.setRefreshing(false);
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (this.isVisible()) {
            if (!isVisibleToUser) {
                isVisibleView = false;
            } else {
                isVisibleView = true;
            }
        }
    }

    @Override
    public void onResume() {
        super.onResume();

        getView().setFocusableInTouchMode(true);
        getView().requestFocus();
        getView().setOnKeyListener(onKeyDown);
        EventBus.getInstance().register(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        EventBus.getInstance().unregister(this);
    }

    @Subscribe
    public void getMessage(String msg) {
        Log.e("getMessage", "TeamLeave getMessage: " + msg);
        getTeamLeave();
    }

    private View.OnKeyListener onKeyDown = new View.OnKeyListener() {
        @Override
        public boolean onKey(View v, int keyCode, KeyEvent event) {
            if (isVisibleView) {
                if (KeyEvent.ACTION_UP == event.getAction()) {
                    if (keyCode == KeyEvent.KEYCODE_BACK) {
                        onChangePageView.setOnChangePager(LMSGeneralConstant.FIRST_PAGE, LMSGeneralConstant.HOME_PAGE);
                    }
                }
                return true;
            }
            return false;
        }
    };

    private void updateDayOffStatus(int statusButton, String statusId, final String requestId) {
        String targetStatusId = "";
        String message = "";
        String posText = "";
        String negativeText = "Cancel";
        status_id = statusButton;
        if (statusId.equalsIgnoreCase("1")) {
            if (statusButton == ApproveId) {
                targetStatusId = "3";
                message = "Please confirm that you wish to approve this request";
                posText = "Approve";
            } else if (statusButton == CancelId) {
                targetStatusId = "2";
                message = "Please confirm that you wish to deny this request";
                posText = "Deny";
            }
        }

        final String finalTargetStatusId = targetStatusId;
        DialogUtils.showAlertDialogYesandNo(getActivity(), message, posText, negativeText, new RequestDialogListener() {
            @Override
            public void responseSubmit() {
                countAlert = 1;
                dialogWaiting.show();
                LMSWSManager.changeDayOffStatus(getActivity(),
                        application.getCurProfile().getSTAFF_TOKEN(),
                        requestId,
                        finalTargetStatusId,
                        onResponseWS
                );
            }

            @Override
            public void responseCancel() {

            }
        });
    }

    @Override
    public void onBack() {
        if (isVisibleView) {
            onChangePageView.setOnChangePager(LMSGeneralConstant.FIRST_PAGE, LMSGeneralConstant.HOME_PAGE);
        }
    }
}
