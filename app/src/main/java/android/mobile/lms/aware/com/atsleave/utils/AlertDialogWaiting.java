package android.mobile.lms.aware.com.atsleave.utils;

import android.app.Dialog;
import android.mobile.lms.aware.com.atsleave.R;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.fragment.app.DialogFragment;
import android.view.WindowManager;

public class AlertDialogWaiting extends DialogFragment {

    private boolean isVisibility = false;

    public AlertDialogWaiting() {
        super();

    }

    public boolean isVisibility() {
        return isVisibility;
    }

    public void setIsVisibility(boolean isVisibility) {
        this.isVisibility = isVisibility;
    }

    //
    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = new Dialog(getActivity(), R.style.Theme_Transparent);
        dialog.setContentView(R.layout.layout_waiting_dialog);
        getActivity().getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
        return dialog;
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        isVisibility = isVisibleToUser;
    }
}
