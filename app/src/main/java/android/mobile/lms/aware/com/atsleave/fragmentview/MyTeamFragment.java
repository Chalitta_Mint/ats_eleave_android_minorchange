package android.mobile.lms.aware.com.atsleave.fragmentview;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.mobile.lms.aware.com.atsleave.HistoryLeaveActivity;
import android.mobile.lms.aware.com.atsleave.LMSApplication;
import android.mobile.lms.aware.com.atsleave.MainActivity;
import android.mobile.lms.aware.com.atsleave.R;
import android.mobile.lms.aware.com.atsleave.adapter.ListMyTeamAdapter;
import android.mobile.lms.aware.com.atsleave.callback.BackPressed;
import android.mobile.lms.aware.com.atsleave.callback.OnListenerFromWSManager;
import android.mobile.lms.aware.com.atsleave.callback.RequestDialogListener;
import android.mobile.lms.aware.com.atsleave.models.MemberResponse;
import android.mobile.lms.aware.com.atsleave.models.StaffSummaryResponse;
import android.mobile.lms.aware.com.atsleave.utils.AwareUtils;
import android.mobile.lms.aware.com.atsleave.utils.DialogUtils;
import android.mobile.lms.aware.com.atsleave.utils.OnChangePageView;
import android.mobile.lms.aware.com.atsleave.webservice.LMSGeneralConstant;
import android.mobile.lms.aware.com.atsleave.webservice.LMSWSManager;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.NumberPicker;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.baoyz.swipemenulistview.SwipeMenu;
import com.baoyz.swipemenulistview.SwipeMenuCreator;
import com.baoyz.swipemenulistview.SwipeMenuItem;
import com.baoyz.swipemenulistview.SwipeMenuListView;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by apinun.w on 15/7/2558.
 */
public class MyTeamFragment extends Fragment implements View.OnClickListener, BackPressed {
    private String TAG = "MyTeamFragment";
    private static final int HISTORY_ID = 111;
    private static final int DAY_OFF_ID = 222;
    private static final int CONTACT_ID = 333;
    private OnChangePageView onChangePageView;
    private SwipeMenuListView listview_myteam;
    private LMSApplication application;
    private Dialog alertWaiting;
    private ListMyTeamAdapter adapter;
    private ImageView img_background;
    private RelativeLayout linear_title;
    private LinearLayout linear_popup;
    private Animation anim_in_from_bottom;
    private Animation anim_out_from_bottom;
    private RelativeLayout linear_call;
    private RelativeLayout linear_email;
    private LinearLayout linear_cancel;
    private ImageView icon_checkbox;
    private TextView text_title_popup;
    private LinearLayout linear_daysoff;
    private RelativeLayout linear_checkbox_detail;
    private TextView text_selected;
    private SwipeRefreshLayout swipeView;
    private TextView text_done;
    private int totalMinute = 0;
    private boolean isShowPopUp = false;
    private int request_position;
    private boolean isVisibleView = true;
    private static int countAlert = 0;
    private String remarkDayOff;

    public static MyTeamFragment initMyTeamFragment(String fragment, OnChangePageView onchangePageView) {
        MyTeamFragment myTeamFragment = new MyTeamFragment();
        myTeamFragment.setOnChangePageView(onchangePageView);
        return myTeamFragment;
    }

    public void setOnChangePageView(OnChangePageView onChangePageView) {
        this.onChangePageView = onChangePageView;
    }

    final SwipeMenuCreator creator = new SwipeMenuCreator() {
        @Override
        public void create(SwipeMenu menu) {
            SwipeMenuItem menuItem = null;
            menuItem = createMenuSwapItem(getString(R.string.title_contact), R.drawable.swipe_icon_team_main2, R.color.bg_gray, R.color.bg_white);
            menuItem.setId(CONTACT_ID);
            menu.addMenuItem(menuItem);
            menuItem = createMenuSwapItem(getString(R.string.title_view_history), R.drawable.swipe_icon_history, R.color.bg_my_team_history, R.color.bg_white);
            menuItem.setId(HISTORY_ID);
            menu.addMenuItem(menuItem);
            menuItem = createMenuSwapItem(getString(R.string.title_aware_day_off), R.drawable.swipe_icon_star2, R.color.bg_my_team_dayoff, R.color.bg_white);
            menuItem.setId(DAY_OFF_ID);
            menu.addMenuItem(menuItem);
        }
    };

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        application = (LMSApplication) getActivity().getApplication();
        alertWaiting = DialogUtils.createDialog(getActivity());
        //set view id
        View view = inflater.inflate(R.layout.layout_myteam_page, container, false);
        linear_title = (RelativeLayout) view.findViewById(R.id.linear_title);
        linear_popup = (LinearLayout) view.findViewById(R.id.linear_popup);
        img_background = (ImageView) view.findViewById(R.id.img_bg);
        text_title_popup = (TextView) view.findViewById(R.id.text_title_popup);
        linear_call = (RelativeLayout) view.findViewById(R.id.linear_call);
        linear_email = (RelativeLayout) view.findViewById(R.id.linear_email);
        linear_cancel = (LinearLayout) view.findViewById(R.id.linear_cancel);
        linear_daysoff = (LinearLayout) view.findViewById(R.id.linear_daysoff);
        text_selected = (TextView) view.findViewById(R.id.text_selected);
        swipeView = (SwipeRefreshLayout) view.findViewById(R.id.swipeRefreshLayout);
        text_done = (TextView) view.findViewById(R.id.text_done);
        listview_myteam = (SwipeMenuListView) view.findViewById(R.id.listview_myteam_page);
        listview_myteam.setTag(-1);
        linear_checkbox_detail = (RelativeLayout) view.findViewById(R.id.linear_checkbox_detail);
        icon_checkbox = (ImageView) view.findViewById(R.id.icon_checkbox);

        //Animation
        anim_in_from_bottom = AnimationUtils.loadAnimation(getActivity(), R.anim.fade_in_on_bottom);
        anim_in_from_bottom.setDuration(50);
        anim_out_from_bottom = AnimationUtils.loadAnimation(getActivity(), R.anim.fade_out_on_bottom);
        anim_out_from_bottom.setDuration(50);

        ((MainActivity) getActivity()).enableViewPager();

        linear_call.setOnClickListener(popupListener);
        linear_email.setOnClickListener(popupListener);
        linear_cancel.setOnClickListener(popupListener);

        //set background width and height approx to monitor
        img_background.setLayoutParams(new RelativeLayout.LayoutParams(
                application.getWidth_screen(), application.getHeight_screen()
                - linear_title.getMeasuredHeight()
                - AwareUtils.getNevigationBar(getActivity())
        ));

        adapter = new ListMyTeamAdapter(getActivity(), new ArrayList<MemberResponse.DataEntity>(), application.getCurProfile().getEMP_ROLE(), onClickCheckBox);
        listview_myteam.setAdapter(adapter);
        //pop up all days off
        linear_daysoff.setOnClickListener(onClickAllDayOff);
        registerForContextMenu(listview_myteam);

        icon_checkbox.setOnClickListener(this);
        text_done.setOnClickListener(this);
        listview_myteam.setOnItemClickListener(onItemClick);

        countAlert = 1;
        getMember();

        // step 2. listener item click event
        listview_myteam.setOnMenuItemClickListener(new SwipeMenuListView.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(int position, SwipeMenu menu, int index) {
                switch (menu.getMenuItem(index).getId()) {
                    case DAY_OFF_ID:
                        callDialogAward(position);
                        break;
                    case HISTORY_ID:
                        callHistoryActivity(position);
                        adapter.notifyDataSetChanged();
                        break;
                    case CONTACT_ID:
                        listview_myteam.setTag(position);
                        if (linear_popup.getVisibility() == View.VISIBLE) {
                            hidePopUp();
                        } else {
                            showPopUp();
                        }
                        break;
                }
                return true;

            }
        });

        swipeView.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                swipeView.setRefreshing(true);
                countAlert = 1;
                getMember();

            }
        });

        // test item long click
        listview_myteam.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    listview_myteam.getParent().requestDisallowInterceptTouchEvent(false);
                }
                return false;
            }
        });

        //set Enable andd Disable to using RefreshView
        listview_myteam.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView absListView, int i) {

            }

            @Override
            public void onScroll(AbsListView absListView, int firstVisibleItem, int visibleItemCount, int totalItemCount) {

                boolean enable = false;
                if (absListView != null && absListView.getChildCount() > 0) {
                    // check if the first item of the list is visible
                    boolean firstItemVisible = absListView.getFirstVisiblePosition() == 0;
                    // check if the top of the first item is visible
                    boolean topOfFirstItemVisible = absListView.getChildAt(0).getTop() == 0;
                    // enabling or disabling the refresh layout
                    enable = firstItemVisible && topOfFirstItemVisible;
                }
                swipeView.setEnabled(enable);
            }
        });

        //set DayOffs
        String emp_role = application.getCurProfile().getEMP_ROLE();

        if (emp_role.equalsIgnoreCase(LMSGeneralConstant.EMP_ROLE.SUPERVISOR_TEXT) || emp_role.equalsIgnoreCase(LMSGeneralConstant.EMP_ROLE.DIRECTOR_TEXT)) {
            icon_checkbox.setVisibility(View.VISIBLE);
        } else {
            icon_checkbox.setVisibility(View.INVISIBLE);
        }

        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        onChangePageView = (OnChangePageView) context;
    }

    private void callHistoryActivity(int position) {
        String emp_code = adapter.getMembers().get(position).getEMP_CODE();
        Log.d(TAG, "emp_code : " + emp_code);
        Intent intent = new Intent(getActivity(), HistoryLeaveActivity.class);
        intent.putExtra("staffID", emp_code);
        startActivity(intent);
    }

    public void callDialogAward(final int position) {
        final String[] days_time = {"Day", "0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10"};
        final String[] hours_time = {"Hour", "0", "1", "2", "3", "4", "5", "6", "7"};
        final String[] minute_time = {"Minute", "0", "30"};
        final Dialog boxAddLeave = new Dialog(getActivity());
        boxAddLeave.setCanceledOnTouchOutside(false);
        boxAddLeave.getWindow().setBackgroundDrawable(new ColorDrawable(Color.argb(0, 0, 0, 0)));
        boxAddLeave.requestWindowFeature(Window.FEATURE_NO_TITLE);
        boxAddLeave.setContentView(R.layout.custom_box_award);
        RelativeLayout box_btn_comment = (RelativeLayout) boxAddLeave.findViewById(R.id.box_btn_comment);
        Button btn_confirm_award = (Button) boxAddLeave.findViewById(R.id.btn_confirm_award);
        final TextView txtLimit = (TextView) boxAddLeave.findViewById(R.id.txtLimit);
        final EditText editRemark = (EditText) boxAddLeave.findViewById(R.id.editRemark);
        String strLimit = String.format(getString(R.string.txtLimit), ("0"));
        txtLimit.setText(strLimit);
        final NumberPicker np_day = (NumberPicker) boxAddLeave.findViewById(R.id.numberPicker);
        final NumberPicker np_hours = (NumberPicker) boxAddLeave.findViewById(R.id.numberPicker_hours);
        final NumberPicker np_minute = (NumberPicker) boxAddLeave.findViewById(R.id.numberPicker_minute);

        //setting number picker days
        np_day.setMaxValue(days_time.length - 1);
        np_day.setMinValue(0);
        np_day.setWrapSelectorWheel(true);
        np_day.setDisplayedValues(days_time);

        //setting number picker hours
        np_hours.setMaxValue(hours_time.length - 1);
        np_hours.setMinValue(0);
        np_hours.setDisplayedValues(hours_time);

        //setting number picker minutes
        np_minute.setMaxValue(minute_time.length - 1);
        np_minute.setMinValue(0);
        np_minute.setDisplayedValues(minute_time);

        box_btn_comment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boxAddLeave.dismiss();
            }
        });

        btn_confirm_award.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String remark = editRemark.getText().toString();
                if (remark.trim().equalsIgnoreCase("")) {
                    Animation animation = AnimationUtils.loadAnimation(getActivity(), R.anim.shake_view);
                    editRemark.setAnimation(animation);
                    editRemark.startAnimation(animation);
                    return;
                }
                remarkDayOff = remark;
                /*check date picker input*/
                String txt_date = days_time[np_day.getValue()];
                String txt_hours = hours_time[np_hours.getValue()];
                String txt_minute = minute_time[np_minute.getValue()];

                if (txt_date.equalsIgnoreCase(days_time[0])
                        || txt_hours.equalsIgnoreCase(hours_time[0])
                        || txt_minute.equalsIgnoreCase(minute_time[0])) {
                    DialogUtils.showAlertDialog(getActivity(), getString(R.string.alert_text_time), false);
//                    Toast.makeText(getActivity(),"Please select days, hours and min to continue.",Toast.LENGTH_SHORT).show();
                    return;
                }
                int int_day = Integer.parseInt(txt_date);
                int int_hour = Integer.parseInt(txt_hours);
                int int_minute = Integer.parseInt(txt_minute);
                int totalMinute = calculutDatePickerToMinute(int_day, int_hour, int_minute);

                if (totalMinute == 0) {
                    DialogUtils.showAlertDialog(getActivity(), getString(R.string.alert_text_time), false);
                    return;
                }

                request_position = position;
                MyTeamFragment.this.totalMinute = totalMinute;
                requestDayOff();
                if (boxAddLeave.isShowing()) {
                    boxAddLeave.dismiss();
                }
            }
        });

        editRemark.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String strLimit = String.format(getString(R.string.txtLimit), (count + ""));
                txtLimit.setText(strLimit);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        boxAddLeave.show();

    }

    private OnListenerFromWSManager requestResponse = new OnListenerFromWSManager() {
        @Override
        public void onComplete(String statusCode, String message) {
            dismissDialog();
            dismissDialog();

            linear_checkbox_detail.setVisibility(View.GONE);
            adapter.setIsEnableCheckBox(false);
            adapter.notifyDataSetChanged();

        }

        @Override
        public void onFailed(String statusCode, final String message) {
            dismissDialog();
            dismissDialog();

            linear_checkbox_detail.setVisibility(View.GONE);
            DialogUtils.showAlertDismissofRetry(getActivity(), "", new RequestDialogListener() {
                @Override
                public void responseSubmit() {
                    requestDayOff();
                }

                @Override
                public void responseCancel() {
                }
            });


        }

        @Override
        public void onFailed(final String message) {
            dismissDialog();
            dismissDialog();

            linear_checkbox_detail.setVisibility(View.GONE);
            DialogUtils.showAlertDismissofRetry(getActivity(), "", new RequestDialogListener() {
                @Override
                public void responseSubmit() {
                    requestDayOff();
                }

                @Override
                public void responseCancel() {
                }
            });


        }

        @Override
        public void onFailedAndForceLogout() {
            dismissDialog();
            dismissRefresh();

            if (!AwareUtils.isForceLogout) {
                AwareUtils.forceLogOut(getActivity());
            }
        }

    };

    private View.OnClickListener popupListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            int position = (int) listview_myteam.getTag();
            if (position == -1) return;
            switch (v.getId()) {
                case R.id.linear_call:
                    MemberResponse.DataEntity member = adapter.getMembers().get(position);
                    AwareUtils.callMobilePhone(getActivity(), member.getMOBILE_NUMBER());
                    hidePopUp();
                    break;
                case R.id.linear_email:
                    MemberResponse.DataEntity member_email = adapter.getMembers().get(position);
                    AwareUtils.sendEmail(getActivity(), "", member_email.getEMAIL());
                    hidePopUp();
                    break;
                case R.id.linear_cancel:
                    hidePopUp();
                    break;
            }

        }
    };

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onStart() {
        super.onStart();
        setHasOptionsMenu(true);
        getView().setFocusableInTouchMode(true);
        getView().requestFocus();
        getView().setOnKeyListener(onKeyDown);
    }


    public static void setListViewHeightBasedOnChildren(ListView listView) {
        ListAdapter listAdapter = listView.getAdapter();
        if (listAdapter == null)
            return;

        int desiredWidth = View.MeasureSpec.makeMeasureSpec(listView.getWidth(), View.MeasureSpec.UNSPECIFIED);
        int totalHeight = 0;
        View view = null;
        for (int i = 0; i < listAdapter.getCount(); i++) {
            view = listAdapter.getView(i, view, listView);
            if (i == 0)
                view.setLayoutParams(new ViewGroup.LayoutParams(desiredWidth, LinearLayout.LayoutParams.WRAP_CONTENT));

            view.measure(desiredWidth, View.MeasureSpec.UNSPECIFIED);
            totalHeight += view.getMeasuredHeight();
        }
        ViewGroup.LayoutParams params = listView.getLayoutParams();
        params.height = totalHeight + (listView.getDividerHeight() * (listAdapter.getCount() - 1));
        listView.setLayoutParams(params);
        listView.requestLayout();

    }

    private int calculutDatePickerToMinute(int day, int hour, int minute) {
        int totalMinuteOfDay = day * 8 * 60;
        int totalMinuteOfHour = hour * 60;
        int totalMinuteOfMinute = minute;

        return totalMinuteOfDay + totalMinuteOfHour + totalMinuteOfMinute;
    }

    private SwipeMenuItem createMenuSwapItem(String title, int drawable, int color, int txt_color) {
        SwipeMenuItem itemMenu = new SwipeMenuItem(getActivity());
        itemMenu.setBackground(new ColorDrawable(getResources().getColor(color)));
        // set item width
        itemMenu.setWidth(AwareUtils.dp2px(getActivity(), 90));
        // set a icon
        itemMenu.setIcon(drawable);
        itemMenu.setTitle(title);
        itemMenu.setTitleSize(12);
        itemMenu.setTitleColor(getResources().getColor(txt_color));

        return itemMenu;
    }

    /*toggle swipe menu*/
    private AdapterView.OnItemClickListener onItemClick = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            String emp_role = application.getCurProfile().getEMP_ROLE();
            if ((emp_role.equalsIgnoreCase(LMSGeneralConstant.EMP_ROLE.SUPERVISOR_TEXT) && position != 0) || emp_role.equalsIgnoreCase(LMSGeneralConstant.EMP_ROLE.DIRECTOR_TEXT)) {
                listview_myteam.smoothOpenMenu(position);
            } else {
                if (adapter.isEnableCheckBox()) {
                    return;
                }
                if (linear_popup.getVisibility() == View.VISIBLE) {
                    hidePopUp();
                } else {
                    listview_myteam.setTag(position);
                    showPopUp();
                }
            }
        }
    };

    /*back of fragment*/
    private View.OnKeyListener onKeyDown = new View.OnKeyListener() {
        @Override
        public boolean onKey(View v, int keyCode, KeyEvent event) {
            Log.i("onKey", "keyCode: " + keyCode);
            if (isVisibleView && !isShowPopUp) {
                if (KeyEvent.ACTION_UP == event.getAction()) {
                    if (keyCode == KeyEvent.KEYCODE_BACK) {
                        Log.i("keyCode", "keyCode: " + keyCode);
                        onChangePageView.setOnChangePager(LMSGeneralConstant.FIRST_PAGE, LMSGeneralConstant.PENDING_REQUEST_PAGE);
                    }
                }

                return true;
            }
            return false;
        }
    };

    public void showPopUp() {
        isShowPopUp = true;

        int position = (int) listview_myteam.getTag();
        String user_emp_role = application.getCurProfile().getEMP_ROLE();
        String member_emp_role = adapter.getMembers().get(position).getEMP_ROLE();
        String str_call = "Call or";
        String nick_name = adapter.getMembers().get(position).getNICKNAME_ENG();

        //set Look Up for popup open
        if (user_emp_role.equalsIgnoreCase(LMSGeneralConstant.EMP_ROLE.SUPERVISOR_TEXT) || user_emp_role.equalsIgnoreCase(LMSGeneralConstant.EMP_ROLE.DIRECTOR_TEXT)) {
            //show all(email and mobile number) user is supervisor
            linear_call.setVisibility(View.VISIBLE);

        } else {
            //show email only user is employee.
            if (member_emp_role.equalsIgnoreCase(LMSGeneralConstant.EMP_ROLE.SUPERVISOR_TEXT) || member_emp_role.equalsIgnoreCase(LMSGeneralConstant.EMP_ROLE.DIRECTOR_TEXT)) {
                //show all(email and mobile number) member is supervisor
                linear_call.setVisibility(View.VISIBLE);
            } else {
                //show email only member is employee
                linear_call.setVisibility(View.GONE);
                str_call = "";
            }
        }

        text_title_popup.setText(String.format(getString(R.string.title_popup), str_call, " (" + nick_name + ")"));

        //clear animation
        anim_in_from_bottom.reset();

        //linear popup visible
        linear_popup.setVisibility(View.VISIBLE);
        linear_popup.clearAnimation();
        linear_popup.startAnimation(anim_in_from_bottom);

        //listview enable and set not swipe
        listview_myteam.setEnabled(false);
        listview_myteam.setOnSwipeListener(null);
        ((MainActivity) getActivity()).disableViewPager();
    }

    public void hidePopUp() {
        isShowPopUp = false;
        //show pop up
        linear_popup.setVisibility(View.GONE);
        //anim_out_from_bottom animation
        anim_out_from_bottom.reset();
        //set listview animation.
        listview_myteam.clearAnimation();
        linear_popup.startAnimation(anim_out_from_bottom);
        listview_myteam.setEnabled(true);
        listview_myteam.setOnItemClickListener(onItemClick);

        ((MainActivity) getActivity()).enableViewPager();
    }

    /*change checkbox check data*/
    private View.OnClickListener onClickCheckBox = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            List<Boolean> checkBoxData = adapter.getCheckBoxData();
            int count = 0;
            for (int i = 0; i < checkBoxData.size(); i++) {
                if (checkBoxData.get(i) == true) {
                    count++;
                }
            }
            text_selected.setText(count + "");
        }
    };

    private HashMap<String, Integer> analysisSummary(List<StaffSummaryResponse.DataEntity> liststaff) {
        HashMap<String, Integer> staffs = new HashMap<>();
        for (StaffSummaryResponse.DataEntity model : liststaff) {
            int minute = 0;
            if (model.getALLTIMEUSED() != null) {
                minute = Integer.parseInt(model.getALLTIMEUSED());
                staffs.put(model.getLEAVENAME(), minute);
            }

        }
        return staffs;
    }

    private View.OnClickListener onClickAllDayOff = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.linear_daysoff:
                    callDialogAward(-1);
                    break;
            }
        }
    };

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.icon_checkbox:
                toggleShowCheckBox();
                break;
            case R.id.text_done:
                toggleShowCheckBox();
                break;
        }
    }

    private void toggleShowCheckBox() {

        if (adapter.isEnableCheckBox()) {                     //show check box
            text_done.setVisibility(View.GONE);
            icon_checkbox.setVisibility(View.VISIBLE);
            adapter.setIsEnableCheckBox(false);
            adapter.notifyDataSetChanged();
            linear_checkbox_detail.setVisibility(View.GONE);
        } else {
            text_done.setVisibility(View.VISIBLE);          //hide check box
            icon_checkbox.setVisibility(View.GONE);
            adapter.setIsEnableCheckBox(true);
            adapter.notifyDataSetChanged();
            linear_checkbox_detail.setVisibility(View.VISIBLE);
        }

    }

    private OnListenerFromWSManager onResponseGetMyTeam = new OnListenerFromWSManager() {
        @Override
        public void onComplete(String statusCode, String message) {
            dismissDialog();
            dismissRefresh();
            Gson gson = new Gson();
            MemberResponse memberModel = gson.fromJson(message, MemberResponse.class);
            final List<MemberResponse.DataEntity> members = memberModel.getData();

            if (members == null)
                adapter.setMembers(new ArrayList<MemberResponse.DataEntity>());
            else
                adapter.setMembers(members);

            //set show swipe view
            if (application.getCurProfile().getEMP_ROLE().equalsIgnoreCase(LMSGeneralConstant.EMP_ROLE.SUPERVISOR_TEXT) || application.getCurProfile().getEMP_ROLE().equalsIgnoreCase(LMSGeneralConstant.EMP_ROLE.DIRECTOR_TEXT)) {
                //add swipe view
                listview_myteam.setMenuCreator(creator);

            } else {
                //remove all swipe view
                listview_myteam.setMenuCreator(null);
            }

            listview_myteam.setAdapter(adapter);
        }

        @Override
        public void onFailed(String statusCode, final String message) {
            dismissDialog();
            dismissRefresh();

            DialogUtils.showAlertDismissofRetry(getActivity(), message, new RequestDialogListener() {
                @Override
                public void responseSubmit() {
                    getMember();
                }

                @Override
                public void responseCancel() {

                }
            });

        }

        @Override
        public void onFailed(final String message) {
            dismissDialog();
            dismissRefresh();

            DialogUtils.showAlertDismissofRetry(getActivity(), message, new RequestDialogListener() {
                @Override
                public void responseSubmit() {
                    getMember();
                }

                @Override
                public void responseCancel() {

                }
            });
        }

        @Override
        public void onFailedAndForceLogout() {
            dismissDialog();
            dismissRefresh();
            if (!AwareUtils.isForceLogout) {
                AwareUtils.forceLogOut(getActivity());
            }
        }
    };

    private void getMember() {
        showAlertDialog();
        LMSWSManager.getMember(getActivity(), application.getCurProfile().getEMP_ROLE(), application.getCurProfile().getSTAFF_TOKEN(), onResponseGetMyTeam);
    }

    private void requestDayOff() {
        countAlert = 1;
        showAlertDialog();
        String staff_id = "";
        String token = application.getCurProfile().getSTAFF_TOKEN();
        String request_id = "";
        String status_id = LMSGeneralConstant.DAY_OFF_STATUS.Pending + "";

        // request days off  single emp
        List<Boolean> checkBoxData = adapter.getCheckBoxData();
        if (request_position != -1) {
            staff_id = adapter.getMembers().get(request_position).getEMP_CODE();
        } else {
            for (int i = 0; i < checkBoxData.size(); i++) {
                if (checkBoxData.get(i).booleanValue() == true)
                    staff_id += adapter.getMembers().get(i).getEMP_CODE() + ",";
            }

            if (staff_id.charAt(staff_id.length() - 1) == ',') {
                staff_id = staff_id.substring(0, staff_id.length() - 1);
            }
        }

        LMSWSManager.requestDayOff(
                getActivity(),
                token,
                staff_id,
                request_id,
                status_id,
                totalMinute + "",
                remarkDayOff,
                requestResponse);
    }

    private void showAlertDialog() {
        if (!alertWaiting.isShowing() && isVisibleView) {
            alertWaiting.show();
        }
    }

    private void dismissDialog() {
        if (alertWaiting.isShowing() && --countAlert <= 0) {
            alertWaiting.dismiss();
            countAlert = 0;
        }
    }

    private void dismissRefresh() {
        if (swipeView.isRefreshing())
            swipeView.setRefreshing(false);
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (this.isVisible()) {
            if (!isVisibleToUser) {
                isVisibleView = false;
            } else {
                ((MainActivity) getActivity()).enableViewPager();
                isVisibleView = true;
            }
        }
    }

    @Override
    public void onBack() {
        if (isVisibleView && !isShowPopUp) {
            onChangePageView.setOnChangePager(LMSGeneralConstant.FIRST_PAGE, LMSGeneralConstant.PENDING_REQUEST_PAGE);
        }
    }
}
