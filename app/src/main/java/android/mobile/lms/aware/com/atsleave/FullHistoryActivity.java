package android.mobile.lms.aware.com.atsleave;

import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.mobile.lms.aware.com.atsleave.adapter.ExpandableFullHistoryListAdapter;
import android.mobile.lms.aware.com.atsleave.models.LeaveHistoryResponse;
import android.mobile.lms.aware.com.atsleave.webservice.LMSGeneralConstant;
import android.mobile.lms.aware.com.atsleave.callback.OnListenerFromWSManager;
import android.mobile.lms.aware.com.atsleave.models.LeaveHistory;
import android.mobile.lms.aware.com.atsleave.utils.AlertDialogWaiting;
import android.mobile.lms.aware.com.atsleave.utils.AwareUtils;
import android.mobile.lms.aware.com.atsleave.utils.DialogUtils;
import android.mobile.lms.aware.com.atsleave.utils.OnChangePageView;
import android.mobile.lms.aware.com.atsleave.webservice.LMSWSManager;
import android.os.Bundle;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.allen.expandablelistview.SwipeMenuExpandableCreator;
import com.allen.expandablelistview.SwipeMenuExpandableListView;
import com.baoyz.swipemenulistview2.ContentViewWrapper;
import com.baoyz.swipemenulistview2.SwipeMenuItem;
import com.baoyz.swipemenulistview2.SwipeMenuLayout;
import com.google.gson.Gson;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.SortedSet;
import java.util.TreeSet;

/**
 * Created by apinun.w on 15/7/2558.
 */

public class FullHistoryActivity extends CustomActivity {
    private static final int DetailId = 111;
    private static final int ApprovedId = 222;
    private static final int CancelId = 333;
    private OnChangePageView onchangePageView;
    private AlertDialogWaiting alertdialog;
    private View childView;
    ArrayList<String> groupList;
    private HashMap<Integer, Integer> listSwipeSize = new HashMap<>();
    HashMap<String, ArrayList<LeaveHistory>> childArrayList = new HashMap<>();
    //----------------For swap------------
    private LMSApplication application;
    private AlertDialogWaiting alertWaiting = new AlertDialogWaiting();
    private String TAG = "OtherLeaveHistoryFragment";
    private static int countAlert = 0;
    //------------------------------------
    private ExpandableFullHistoryListAdapter mAdapter;
    private com.allen.expandablelistview.SwipeMenuExpandableListView listView;
    private ImageView img_home_img;
    private int[] leaveTypesRequest = null;
    private SwipeRefreshLayout swipeView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_full_history_page);
        leaveTypesRequest = getIntent().getIntArrayExtra("status_type");

        application = (LMSApplication) getApplication();
        img_home_img = (ImageView) findViewById(R.id.img_home_img);
        swipeView = (SwipeRefreshLayout) findViewById(R.id.swipeRefreshLayout);
        img_home_img.setVisibility(View.INVISIBLE);


        createGroupList();
        updateSickLeave();

        listView = (SwipeMenuExpandableListView) findViewById(R.id.listView);
        mAdapter = new ExpandableFullHistoryListAdapter(FullHistoryActivity.this, groupList, new HashMap<String, ArrayList<LeaveHistory>>());
        listView.setAdapter(mAdapter);

        swipeView.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                swipeView.setRefreshing(true);
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        countAlert = 2;
                        updateSickLeave();
                    }
                });
            }
        });

// 4. Set OnItemLongClickListener
        listView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                int itemType = ExpandableListView.getPackedPositionType(id);
                int childPosition, groupPosition;
                if (itemType == ExpandableListView.PACKED_POSITION_TYPE_CHILD) {
                    childPosition = ExpandableListView.getPackedPositionChild(id);
                    groupPosition = ExpandableListView.getPackedPositionGroup(id);

                    // do your per-item callback here
//                    Toast.makeText(getApplication(),
//                            "long click on group " + groupPosition + " child " + childPosition, Toast.LENGTH_SHORT)
//                            .show();

                    return true; // true if we consumed the click, false if not

                } else if (itemType == ExpandableListView.PACKED_POSITION_TYPE_GROUP) {
                    groupPosition = ExpandableListView.getPackedPositionGroup(id);
                    // do your per-group callback here
                   /* Toast.makeText(DifferentMenuActivity.this, "long click on group " + groupPosition,
                            Toast.LENGTH_SHORT).show();*/
                    return true; // true if we consumed the click, false if not

                } else {
                    // null item; we don't consume the click
                    return false;
                }
            }
        });

        // 5.Set OnGroupClickListener
        listView.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {
            @Override
            public boolean onGroupClick(ExpandableListView parent, View v, int groupPosition, long id) {
                int countChild = mAdapter.getChildrenCount(groupPosition);
                if (countChild == 0) {
                    Toast.makeText(getApplication(), "Not data", Toast.LENGTH_SHORT).show();
                }
                return false;
            }
        });

        // 6.Set OnChildClickListener
        listView.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
            @Override
            public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {
                if (v instanceof SwipeMenuLayout) {
                    SwipeMenuLayout swipeMenu = (SwipeMenuLayout) v;
                    swipeMenu.smoothOpenMenu();
                }

                FullHistoryActivity.this.childView = v;

                return true;
            }
        });

        // 7. Create and set a SwipeMenuExpandableCreator
        // define the group and child creator function
        SwipeMenuExpandableCreator creator = new SwipeMenuExpandableCreator() {
            @Override
            public void createGroup(com.baoyz.swipemenulistview2.SwipeMenu menu) {

            }

            @Override
            public void createChild(com.baoyz.swipemenulistview2.SwipeMenu menu) {
// Create different menus depending on the view type
                SwipeMenuItem menuItem = null;
                switch (menu.getViewType()) {
                    case LMSGeneralConstant.LEAVE_STATUS.Pending:
                        menuItem = createMenuSwapItem("Detail", R.drawable.swipe_icon_detail, R.color.bg_gray, R.color.bg_white);
                        menuItem.setId(DetailId);
                        menu.addMenuItem(menuItem);
                        menuItem = createMenuSwapItem("Cancel", R.drawable.swipe_menu_cancel, R.color.bg_white, R.color.bg_black);
                        menuItem.setId(CancelId);
                        menu.addMenuItem(menuItem);
                        listSwipeSize.put(LMSGeneralConstant.LEAVE_STATUS.Pending, 2);
                        break;
                    case LMSGeneralConstant.LEAVE_STATUS.Canceled:
                        menuItem = createMenuSwapItem("Cancel", R.drawable.btn_close, R.color.bg_white, R.color.bg_black);
                        menuItem.setId(CancelId);
                        menu.addMenuItem(menuItem);
                        listSwipeSize.put(LMSGeneralConstant.LEAVE_STATUS.Canceled, 1);
                        break;
                    case LMSGeneralConstant.LEAVE_STATUS.Approved:
                        menuItem = createMenuSwapItem("Detail", R.drawable.swipe_icon_detail, R.color.bg_gray, R.color.bg_white);
                        menuItem.setId(DetailId);
                        menu.addMenuItem(menuItem);
                        menuItem = createMenuSwapItem("Request to Cancel", R.drawable.swipe_close_white, R.color.bg_black, R.color.bg_white);
                        menuItem.setId(CancelId);
                        menu.addMenuItem(menuItem);
                        listSwipeSize.put(LMSGeneralConstant.LEAVE_STATUS.Approved, 2);
                        break;
                    case LMSGeneralConstant.LEAVE_STATUS.Rejected:
                        menuItem = createMenuSwapItem("Detail", R.drawable.swipe_icon_detail, R.color.bg_gray, R.color.bg_white);
                        menuItem.setId(DetailId);
                        menu.addMenuItem(menuItem);
                        listSwipeSize.put(LMSGeneralConstant.LEAVE_STATUS.Rejected, 1);
                        break;
                    case LMSGeneralConstant.LEAVE_STATUS.PendingCancel:
                        menuItem = createMenuSwapItem("Detail", R.drawable.swipe_icon_detail, R.color.bg_gray, R.color.bg_white);
                        menuItem.setId(DetailId);
                        menu.addMenuItem(menuItem);
                        listSwipeSize.put(LMSGeneralConstant.LEAVE_STATUS.PendingCancel, 1);
                        break;
                    case LMSGeneralConstant.LEAVE_STATUS.ApprovedCancel:
                        menuItem = createMenuSwapItem("Detail", R.drawable.swipe_icon_detail, R.color.bg_gray, R.color.bg_white);
                        menuItem.setId(DetailId);
                        menu.addMenuItem(menuItem);
                        listSwipeSize.put(LMSGeneralConstant.LEAVE_STATUS.ApprovedCancel, 1);
                        break;
                    case LMSGeneralConstant.LEAVE_STATUS.RejectedCancel:
                        menuItem = createMenuSwapItem("Detail", R.drawable.swipe_icon_detail, R.color.bg_gray, R.color.bg_white);
                        menuItem.setId(DetailId);
                        menu.addMenuItem(menuItem);
                        listSwipeSize.put(LMSGeneralConstant.LEAVE_STATUS.RejectedCancel, 1);
                        break;
                }
            }

        };
        listView.setMenuCreator(creator);

        // 8. Set OnMenuItemClickListener
        listView.setOnMenuItemClickListener(new SwipeMenuExpandableListView.OnMenuItemClickListenerForExpandable() {
            @Override
            public boolean onMenuItemClick(int groupPosition, int childPosition, com.baoyz.swipemenulistview2.SwipeMenu menu, int index) {
                int type = mAdapter.getChildType(groupPosition, childPosition);
                int size = listSwipeSize.get(type);
                if (size == 2) {
                    if (index == 0) {
                        showDetailDialog(groupPosition, childPosition);
                    } else if (index == 1) {
                        //change View
                        ContentViewWrapper view = mAdapter.getChildViewAndReUsable(groupPosition, childPosition, false, null, null);
                        changeChildListView(ApprovedId, groupPosition, childPosition, view.view);
                    }
                } else if (size == 1) {
                    showDetailDialog(groupPosition, childPosition);
                } else {

                }
//                Toast.makeText(getActivity(), s, Toast.LENGTH_SHORT).show();
                return false;
            }

//            @Override
//            public boolean onMenuItemClick(int groupPosition, int childPosition, SwipeMenu menu, int index) {
//
//            }
        });


    }

    ///----------------------------  SERVICE ----------------------------------------------

    private void showDetailDialog(int groupPosition, int childPosition) {
        SimpleDateFormat inputSDF = new SimpleDateFormat("yyyyMMddHHmm");                            //201509140830
        SimpleDateFormat outputSDF = new SimpleDateFormat("EEE dd-MMM-yy", Locale.ENGLISH);

        final Dialog boxAddLeave = new Dialog(this);
        boxAddLeave.setCanceledOnTouchOutside(false);
        boxAddLeave.getWindow().setBackgroundDrawable(new ColorDrawable(Color.argb(0, 0, 0, 0)));
        boxAddLeave.requestWindowFeature(Window.FEATURE_NO_TITLE);
        boxAddLeave.setContentView(R.layout.custom_box_sick_history);

        TextView text_sup_comment = (TextView) boxAddLeave.findViewById(R.id.text_supervisor);
        TextView text_leave_type = (TextView) boxAddLeave.findViewById(R.id.text_leave_type);
        TextView text_sent = (TextView) boxAddLeave.findViewById(R.id.text_sent);
        TextView text_comments = (TextView) boxAddLeave.findViewById(R.id.text_comments);
        TextView text_form = (TextView) boxAddLeave.findViewById(R.id.text_form);
        TextView text_to = (TextView) boxAddLeave.findViewById(R.id.text_to);
        TextView text_time_used = (TextView) boxAddLeave.findViewById(R.id.text_time_used);
        RelativeLayout box_btn_comment = (RelativeLayout) boxAddLeave.findViewById(R.id.box_btn_comment);

        //get Data
        LeaveHistory leaveModels =
                (LeaveHistory) mAdapter.getChild(groupPosition, childPosition);
        String comment = leaveModels.getREASON();
        String sup_comment = "";

        String startDate = leaveModels.getRLD_DATE_START();
        String endDate = leaveModels.getRLD_DATE_END();
        Date parse_end_date = new Date();
        Date parse_start_date = new Date();
        Date parse_send_date = new Date();
        Date startDateCompare = null;
        Date endDateCompate = null;

        if (startDate != null) {
            try {
                parse_start_date = inputSDF.parse(startDate);
                parse_send_date = inputSDF.parse(leaveModels.getRL_DATE());
                startDateCompare = inputSDF.parse(startDate);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }

        if (endDate != null) {
            try {
                parse_end_date = inputSDF.parse(endDate);
                endDateCompate = inputSDF.parse(endDate);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }

        //default leave type
        int typeId = -1;
        try {
            typeId = Integer.parseInt(leaveModels.getLEAVETYPEID());
        } catch (Exception e) {
        }

//        String toDateText = "";
//
//        startDateCompare.setHours(0);
//        startDateCompare.setMinutes(0);
//        endDateCompate.setHours(0);
//        endDateCompate.setMinutes(0);
//
//        if(startDateCompare.compareTo(endDateCompate)==0) {
//            toDateText = outputSDF.format(parse_start_date);
//        }else{
//            toDateText = outputSDF.format(parse_start_date) + "-" +outputSDF.format(parse_end_date);
//        }


        if (leaveModels.getRL_STATUSID().equalsIgnoreCase(LMSGeneralConstant.LEAVE_STATUS.Pending + "")) {
            sup_comment = "N/A";
        } else {
            if (leaveModels.getREMARK() != null) {
                if (leaveModels.getREMARK().equalsIgnoreCase(""))
                    sup_comment = "N/A";
                else
                    sup_comment = leaveModels.getREMARK();
            } else {
                sup_comment = "N/A";
            }
        }
        text_sup_comment.setText(sup_comment);

        String sickTime = "";
        String minuteTotal = leaveModels.getRLD_ALLTIME();
        int i_minute = Integer.parseInt(minuteTotal);
        int int_date = i_minute / 480;
        float f_hour = (i_minute % 480.0f) / 60.0f;

        if (int_date > 0) {
            sickTime += int_date;
            sickTime += int_date <= 1 ? " Day " : " Days ";
        }

        if (f_hour > 0) {
            sickTime += (f_hour % 1 == 0 ? (int) f_hour + "" : f_hour + "") + (f_hour <= 1 ? " Hour " : " Hours ");
        }

        SimpleDateFormat outDate = new SimpleDateFormat("EEE dd-MMM-yy HH:mm", Locale.ENGLISH);

        text_form.setText(outDate.format(parse_start_date));
        text_to.setText(outDate.format(parse_end_date));
        text_time_used.setText(sickTime);

        text_leave_type.setText(getLeaveType(typeId));
        text_comments.setText(comment);
        text_sent.setText(outputSDF.format(parse_send_date));


        box_btn_comment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boxAddLeave.dismiss();
            }
        });

        boxAddLeave.show();
    }

    private void createGroupList() {
        groupList = new ArrayList<String>();
        groupList.add("JANUARY");
        groupList.add("FEBRUARY");
        groupList.add("MARCH");
        groupList.add("APRIL");
        groupList.add("MAY");
        groupList.add("JUNE");
        groupList.add("JULY");
        groupList.add("AUGUST");
        groupList.add("SEPTEMBER");
        groupList.add("OCTOBER");
        groupList.add("NOVEMBER");
        groupList.add("DECEMBER");
    }

    private String getLeaveType(int type) {
        switch (type) {
            case LMSGeneralConstant.LEAVE_TYPE.AnnualLeave:
                return LMSGeneralConstant.LEAVE_TYPE.text_AnnualLeave;
            case LMSGeneralConstant.LEAVE_TYPE.BirthdayLeave:
                return LMSGeneralConstant.LEAVE_TYPE.text_BirthdayLeave;
            case LMSGeneralConstant.LEAVE_TYPE.SickLeave:
                return LMSGeneralConstant.LEAVE_TYPE.text_SickLeave;
            case LMSGeneralConstant.LEAVE_TYPE.MilitaryLeave:
                return LMSGeneralConstant.LEAVE_TYPE.text_MilitaryLeave;
            case LMSGeneralConstant.LEAVE_TYPE.SterilizationLeave:
                return LMSGeneralConstant.LEAVE_TYPE.text_SterilizationLeave;
            case LMSGeneralConstant.LEAVE_TYPE.OrdinationLeave:
                return LMSGeneralConstant.LEAVE_TYPE.text_OrdinationLeave;
            case LMSGeneralConstant.LEAVE_TYPE.MaternityLeave:
                return LMSGeneralConstant.LEAVE_TYPE.text_MaternityLeave;
            case LMSGeneralConstant.LEAVE_TYPE.PaternityLeave:
                return LMSGeneralConstant.LEAVE_TYPE.text_PaternityLeave;
            case LMSGeneralConstant.LEAVE_TYPE.BereavementLeave:
                return LMSGeneralConstant.LEAVE_TYPE.text_BereavementLeave;
            case LMSGeneralConstant.LEAVE_TYPE.LeaveWithoutPay:
                return LMSGeneralConstant.LEAVE_TYPE.text_LeaveWithoutPay;
            case LMSGeneralConstant.LEAVE_TYPE.DayOff:
                return LMSGeneralConstant.LEAVE_TYPE.text_DayOff;
            default:
                return "";
        }
    }

    private void createChildList(List<LeaveHistoryResponse.DataEntity> models) {
        childArrayList = new HashMap<>();
        ArrayList<LeaveHistory> JANUARY_LIST = new ArrayList<LeaveHistory>();
        ArrayList<LeaveHistory> FEBRUARY_LIST = new ArrayList<LeaveHistory>();
        ArrayList<LeaveHistory> MARCH_LIST = new ArrayList<LeaveHistory>();
        ArrayList<LeaveHistory> APRIL_LIST = new ArrayList<LeaveHistory>();
        ArrayList<LeaveHistory> MAY_LIST = new ArrayList<LeaveHistory>();
        ArrayList<LeaveHistory> JUNE_LIST = new ArrayList<LeaveHistory>();
        ArrayList<LeaveHistory> JULY_LIST = new ArrayList<LeaveHistory>();
        ArrayList<LeaveHistory> AUGUST_LIST = new ArrayList<LeaveHistory>();
        ArrayList<LeaveHistory> SEPTEMBER_LIST = new ArrayList<LeaveHistory>();
        ArrayList<LeaveHistory> OCTOBER_LIST = new ArrayList<LeaveHistory>();
        ArrayList<LeaveHistory> NOVEMBER_LIST = new ArrayList<LeaveHistory>();
        ArrayList<LeaveHistory> DECEMBER_LIST = new ArrayList<LeaveHistory>();


        for (LeaveHistoryResponse.DataEntity model : models) {


            String str = new String(model.getRLD_DATE_START());
            String strMonth = str.substring(4, 6);

            if (strMonth.equals("01")) {
                JANUARY_LIST.add(new LeaveHistory(model.getRLD_DATE_START(), model.getRL_STATUSID(), model.getNAME_ENG(), model.getRL_ID(), model.getNICKNAME_ENG(), model.getLEAVETYPEID(), model.getSURNAME_THAI(), model.getNICKNAME_THAI(), model.getSTF_ID(), model.getNAME_THAI(),
                        model.getRLD_DATE_END(), model.getSURNAME_ENG(), model.getTRANSACTION_BY(), model.getRL_DATE(), model.getREASON(), model.getRL_UPDATEDATE(), model.getREMARK(), model.getRLD_ALLTIME()));
            } else if (strMonth.equals("02")) {
                FEBRUARY_LIST.add(new LeaveHistory(model.getRLD_DATE_START(), model.getRL_STATUSID(), model.getNAME_ENG(), model.getRL_ID(), model.getNICKNAME_ENG(), model.getLEAVETYPEID(), model.getSURNAME_THAI(), model.getNICKNAME_THAI(), model.getSTF_ID(), model.getNAME_THAI(),
                        model.getRLD_DATE_END(), model.getSURNAME_ENG(), model.getTRANSACTION_BY(), model.getRL_DATE(), model.getREASON(), model.getRL_UPDATEDATE(), model.getREMARK(), model.getRLD_ALLTIME()));
            } else if (strMonth.equals("03")) {
                MARCH_LIST.add(new LeaveHistory(model.getRLD_DATE_START(), model.getRL_STATUSID(), model.getNAME_ENG(), model.getRL_ID(), model.getNICKNAME_ENG(), model.getLEAVETYPEID(), model.getSURNAME_THAI(), model.getNICKNAME_THAI(), model.getSTF_ID(), model.getNAME_THAI(),
                        model.getRLD_DATE_END(), model.getSURNAME_ENG(), model.getTRANSACTION_BY(), model.getRL_DATE(), model.getREASON(), model.getRL_UPDATEDATE(), model.getREMARK(), model.getRLD_ALLTIME()));
            } else if (strMonth.equals("04")) {
                APRIL_LIST.add(new LeaveHistory(model.getRLD_DATE_START(), model.getRL_STATUSID(), model.getNAME_ENG(), model.getRL_ID(), model.getNICKNAME_ENG(), model.getLEAVETYPEID(), model.getSURNAME_THAI(), model.getNICKNAME_THAI(), model.getSTF_ID(), model.getNAME_THAI(),
                        model.getRLD_DATE_END(), model.getSURNAME_ENG(), model.getTRANSACTION_BY(), model.getRL_DATE(), model.getREASON(), model.getRL_UPDATEDATE(), model.getREMARK(), model.getRLD_ALLTIME()));
            } else if (strMonth.equals("05")) {
                MAY_LIST.add(new LeaveHistory(model.getRLD_DATE_START(), model.getRL_STATUSID(), model.getNAME_ENG(), model.getRL_ID(), model.getNICKNAME_ENG(), model.getLEAVETYPEID(), model.getSURNAME_THAI(), model.getNICKNAME_THAI(), model.getSTF_ID(), model.getNAME_THAI(),
                        model.getRLD_DATE_END(), model.getSURNAME_ENG(), model.getTRANSACTION_BY(), model.getRL_DATE(), model.getREASON(), model.getRL_UPDATEDATE(), model.getREMARK(), model.getRLD_ALLTIME()));
            } else if (strMonth.equals("06")) {
                JUNE_LIST.add(new LeaveHistory(model.getRLD_DATE_START(), model.getRL_STATUSID(), model.getNAME_ENG(), model.getRL_ID(), model.getNICKNAME_ENG(), model.getLEAVETYPEID(), model.getSURNAME_THAI(), model.getNICKNAME_THAI(), model.getSTF_ID(), model.getNAME_THAI(),
                        model.getRLD_DATE_END(), model.getSURNAME_ENG(), model.getTRANSACTION_BY(), model.getRL_DATE(), model.getREASON(), model.getRL_UPDATEDATE(), model.getREMARK(), model.getRLD_ALLTIME()));
            } else if (strMonth.equals("07")) {
                JULY_LIST.add(new LeaveHistory(model.getRLD_DATE_START(), model.getRL_STATUSID(), model.getNAME_ENG(), model.getRL_ID(), model.getNICKNAME_ENG(), model.getLEAVETYPEID(), model.getSURNAME_THAI(), model.getNICKNAME_THAI(), model.getSTF_ID(), model.getNAME_THAI(),
                        model.getRLD_DATE_END(), model.getSURNAME_ENG(), model.getTRANSACTION_BY(), model.getRL_DATE(), model.getREASON(), model.getRL_UPDATEDATE(), model.getREMARK(), model.getRLD_ALLTIME()));
            } else if (strMonth.equals("08")) {
                AUGUST_LIST.add(new LeaveHistory(model.getRLD_DATE_START(), model.getRL_STATUSID(), model.getNAME_ENG(), model.getRL_ID(), model.getNICKNAME_ENG(), model.getLEAVETYPEID(), model.getSURNAME_THAI(), model.getNICKNAME_THAI(), model.getSTF_ID(), model.getNAME_THAI(),
                        model.getRLD_DATE_END(), model.getSURNAME_ENG(), model.getTRANSACTION_BY(), model.getRL_DATE(), model.getREASON(), model.getRL_UPDATEDATE(), model.getREMARK(), model.getRLD_ALLTIME()));
            } else if (strMonth.equals("09")) {
                SEPTEMBER_LIST.add(new LeaveHistory(model.getRLD_DATE_START(), model.getRL_STATUSID(), model.getNAME_ENG(), model.getRL_ID(), model.getNICKNAME_ENG(), model.getLEAVETYPEID(), model.getSURNAME_THAI(), model.getNICKNAME_THAI(), model.getSTF_ID(), model.getNAME_THAI(),
                        model.getRLD_DATE_END(), model.getSURNAME_ENG(), model.getTRANSACTION_BY(), model.getRL_DATE(), model.getREASON(), model.getRL_UPDATEDATE(), model.getREMARK(), model.getRLD_ALLTIME()));
            } else if (strMonth.equals("10")) {
                OCTOBER_LIST.add(new LeaveHistory(model.getRLD_DATE_START(), model.getRL_STATUSID(), model.getNAME_ENG(), model.getRL_ID(), model.getNICKNAME_ENG(), model.getLEAVETYPEID(), model.getSURNAME_THAI(), model.getNICKNAME_THAI(), model.getSTF_ID(), model.getNAME_THAI(),
                        model.getRLD_DATE_END(), model.getSURNAME_ENG(), model.getTRANSACTION_BY(), model.getRL_DATE(), model.getREASON(), model.getRL_UPDATEDATE(), model.getREMARK(), model.getRLD_ALLTIME()));
            } else if (strMonth.equals("11")) {
                NOVEMBER_LIST.add(new LeaveHistory(model.getRLD_DATE_START(), model.getRL_STATUSID(), model.getNAME_ENG(), model.getRL_ID(), model.getNICKNAME_ENG(), model.getLEAVETYPEID(), model.getSURNAME_THAI(), model.getNICKNAME_THAI(), model.getSTF_ID(), model.getNAME_THAI(),
                        model.getRLD_DATE_END(), model.getSURNAME_ENG(), model.getTRANSACTION_BY(), model.getRL_DATE(), model.getREASON(), model.getRL_UPDATEDATE(), model.getREMARK(), model.getRLD_ALLTIME()));
            } else if (strMonth.equals("12")) {
                DECEMBER_LIST.add(new LeaveHistory(model.getRLD_DATE_START(), model.getRL_STATUSID(), model.getNAME_ENG(), model.getRL_ID(), model.getNICKNAME_ENG(), model.getLEAVETYPEID(), model.getSURNAME_THAI(), model.getNICKNAME_THAI(), model.getSTF_ID(), model.getNAME_THAI(),
                        model.getRLD_DATE_END(), model.getSURNAME_ENG(), model.getTRANSACTION_BY(), model.getRL_DATE(), model.getREASON(), model.getRL_UPDATEDATE(), model.getREMARK(), model.getRLD_ALLTIME()));
            }

        }

        childArrayList.put("01", JANUARY_LIST);
        childArrayList.put("02", FEBRUARY_LIST);
        childArrayList.put("03", MARCH_LIST);
        childArrayList.put("04", APRIL_LIST);
        childArrayList.put("05", MAY_LIST);
        childArrayList.put("06", JUNE_LIST);
        childArrayList.put("07", JULY_LIST);
        childArrayList.put("08", AUGUST_LIST);
        childArrayList.put("09", SEPTEMBER_LIST);
        childArrayList.put("10", OCTOBER_LIST);
        childArrayList.put("11", NOVEMBER_LIST);
        childArrayList.put("12", DECEMBER_LIST);

        //remove month is not data.
        Object[] objs = childArrayList.keySet().toArray();
        SortedSet<String> keys = new TreeSet<String>(childArrayList.keySet());


        for (String key : keys) {
            boolean isHaveData = true;
            ArrayList<LeaveHistory> childData = childArrayList.get(key);
            if (childData != null) {
                if (childData.size() <= 0)
                    isHaveData = false;
            } else {
                isHaveData = false;
            }

            if (!isHaveData) {
                childArrayList.remove(key);
            }
        }


        FullHistoryActivity.this.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mAdapter.setChildArray(childArrayList);
                listView.setAdapter(mAdapter);
                mAdapter.notifyDataSetChanged();
                dismissRefresh();
//                    setLookView(models);
            }
        });
    }

    OnListenerFromWSManager onListenerFromWSManager = new OnListenerFromWSManager() {
        @Override
        public void onComplete(String statusCode, String message) {

            Gson gson = new Gson();
            LeaveHistoryResponse leaveHistoryFromWS = gson.fromJson(message, LeaveHistoryResponse.class);
            final List<LeaveHistoryResponse.DataEntity> models = leaveHistoryFromWS.getData();

            //remove cancel id
            for (int i = 0; i < models.size(); i++) {
                LeaveHistoryResponse.DataEntity leaveModel = models.get(i);
                int status_id = Integer.parseInt(leaveModel.getRL_STATUSID());
                if (status_id == LMSGeneralConstant.LEAVE_STATUS.Canceled) {
                    models.remove(i);
                    i--;
                }
            }

            final SimpleDateFormat outFormat = new SimpleDateFormat("yyyyMMddHHss");        //201510290830

            //sort data from start date
            Collections.sort(models, new Comparator<LeaveHistoryResponse.DataEntity>() {
                @Override
                public int compare(LeaveHistoryResponse.DataEntity lhs, LeaveHistoryResponse.DataEntity rhs) {
                    Date lhsDate = null;
                    Date rhsDate = null;
                    try {
                        lhsDate = outFormat.parse(lhs.getRLD_DATE_START());
                        rhsDate = outFormat.parse(rhs.getRLD_DATE_START());
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }

                    if (lhsDate == null || rhsDate == null)
                        return 0;

                    return lhsDate.compareTo(rhsDate);
                }
            });


            createChildList(models);

            alertWaiting.dismiss();
        }

        @Override
        public void onFailed(String statusCode, String message) {
            alertWaiting.dismiss();
        }

        @Override
        public void onFailed(String message) {
            alertWaiting.dismiss();
        }

        @Override
        public void onFailedAndForceLogout() {
            alertWaiting.dismiss();
        }
    };

    View.OnTouchListener listViewOnTouch = new View.OnTouchListener() {
        @Override
        public boolean onTouch(View v, MotionEvent event) {
            if (event.getAction() == MotionEvent.ACTION_DOWN) {
                listView.getParent().requestDisallowInterceptTouchEvent(false);
            } else if (event.getAction() == MotionEvent.ACTION_MOVE) {

            }
            return false;
        }
    };


    private void updateSickLeave() {
        alertWaiting.show(FullHistoryActivity.this.getSupportFragmentManager(), TAG);
        LMSWSManager.getLeaveHistoryByType(
                FullHistoryActivity.this,
                application.getCurProfile().getSTAFF_TOKEN(), leaveTypesRequest,
                onListenerFromWSManager);
    }

//    View.OnClickListener l = new View.OnClickListener() {
//        @Override
//        public void onClick(View v) {
//            // TODO Auto-generated method stub
//
//            mAdapter.notifyDataSetChanged(true);
//        }
//    };


    private SwipeMenuItem createMenuSwapItem(String title, int drawable, int color, int txt_color) {
        SwipeMenuItem itemMenu = new SwipeMenuItem(getApplication());
        itemMenu.setBackground(new ColorDrawable(getResources().getColor(color)));
        // set item widthe
        itemMenu.setWidth(AwareUtils.dp2px(getApplication(), 90));
        // set a icon
        itemMenu.setIcon(drawable);
        itemMenu.setTitle(title);
        itemMenu.setTitleSize(12);
        itemMenu.setTitleColor(getResources().getColor(txt_color));

        return itemMenu;
    }

    private void changeChildListView(final int id, final int groupPosition, final int childPosition, final View view) {
        RelativeLayout linear_main = (RelativeLayout) childView.findViewById(R.id.linear_main);
        LinearLayout linear_sub_main = (LinearLayout) childView.findViewById(R.id.linear_sub_main);
        LinearLayout cancel = (LinearLayout) childView.findViewById(R.id.btn_cancel);
        LinearLayout btn_confirm = (LinearLayout) childView.findViewById(R.id.btn_approve);
        ImageView img_approve = (ImageView) childView.findViewById(R.id.img_approve);
        final TextView title_text_confirm = (TextView) childView.findViewById(R.id.title_text_confirm);
        final EditText editComment = (EditText) childView.findViewById(R.id.edit_comment);

        int m_height = childView.getHeight();
        int s_height = linear_sub_main.getHeight();
//        if(top_editText == -1)
//            top_editText = (int) AwareUtils.dpAsPixel(getActivity(), (float) ((m_height - s_height) / 2.0f) + editComment.getPaddingBottom());
//
//        if(bottom_editText == -1)
//            bottom_editText = (int) AwareUtils.dpAsPixel(getActivity(), (float) ((m_height - s_height) / 2.0f) + editComment.getPaddingTop());
//        editComment.setPadding(
//                0,
//                top_editText,
//                0,
//                bottom_editText);
        editComment.setHeight(m_height);
        if (id == CancelId) {
            btn_confirm.setBackgroundColor(getResources().getColor(R.color.bg_red));
            img_approve.setImageDrawable(getResources().getDrawable(R.drawable.denied));
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    title_text_confirm.setText("Deny");
                }
            });
        }

        //case opened
        if (linear_sub_main.getVisibility() == View.VISIBLE) {
            linear_sub_main.setVisibility(View.GONE);
            linear_main.setVisibility(View.VISIBLE);

        }//case closed
        else {
            linear_sub_main.setVisibility(View.VISIBLE);
            linear_main.setVisibility(View.INVISIBLE);
//            editComment.requestFocus();
//            keyboardHide();
        }

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                changeChildListView(id, groupPosition, childPosition, view);
            }
        });

        btn_confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String commentText = editComment.getText().toString();
                if (id == ApprovedId) {
                    if (!commentText.trim().equalsIgnoreCase("")) {
                        //change status
                        Toast.makeText(getApplication(), "Deny Success", Toast.LENGTH_SHORT).show();
                        updateStatus(groupPosition, childPosition);
                    } else {
//                        take comment only for deny pending
//                        Toast.makeText(getApplication(), "no comment in edit text", Toast.LENGTH_SHORT).show();
                        DialogUtils.showAlertDialog(FullHistoryActivity.this, getString(R.string.alert_text_btn_deny), false);
                    }
                }
            }
        });

    }

    private void updateStatus(int groupPosition, int childPosition) {
        String toChangeStatus = "";
        int RL_StatusId = mAdapter.getChildType(groupPosition, childPosition);
//        int RL_StatusId = Integer.parseInt(sickLeaveAdapter.getLeaveModels().get(position).getRL_STATUSID());
        if (!application.getCurProfile().getEMP_ROLE()
                .equalsIgnoreCase(LMSGeneralConstant.EMP_ROLE.DIRECTOR_TEXT)) {
            if (RL_StatusId == LMSGeneralConstant.LEAVE_STATUS.Pending) {           //Pending(1) -> Cancel(2)
                toChangeStatus = "2";
            } else if (RL_StatusId == LMSGeneralConstant.LEAVE_STATUS.Approved) {      //Approve(3) -> Pending Cancel(5)
                toChangeStatus = "5";
            }
        } else {
            if (RL_StatusId == LMSGeneralConstant.LEAVE_STATUS.Pending) {               //Pending(1) -> Cancel(2)
                toChangeStatus = "2";
            } else if (RL_StatusId == LMSGeneralConstant.LEAVE_STATUS.Approved) {       //Directer auto approve to cancel(6)
                toChangeStatus = "6";                                                   //Approve -> Approve Cancel
            }
        }

        LeaveHistory childData = (LeaveHistory) mAdapter.getChild(groupPosition, childPosition);
        String RL_ID = childData.getRL_ID();
        LMSWSManager.updateLeaveStatus(
                this,
                application.getCurProfile().getSTAFF_TOKEN(),
                RL_ID,
                toChangeStatus,
                "",
                onResponse
        );
    }

    private OnListenerFromWSManager onResponse = new OnListenerFromWSManager() {
        @Override
        public void onComplete(String statusCode, String message) {
            // update data
            updateSickLeave();
        }

        @Override
        public void onFailed(String statusCode, final String message) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    DialogUtils.showAlertDialog(FullHistoryActivity.this, message, false);
                }
            });
        }

        @Override
        public void onFailed(final String message) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    DialogUtils.showAlertDialog(FullHistoryActivity.this, message, false);
                }
            });
        }

        @Override
        public void onFailedAndForceLogout() {
        }
    };

    private void dismissRefresh() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (swipeView.isRefreshing())
                    swipeView.setRefreshing(false);
            }
        });
    }
}
