package android.mobile.lms.aware.com.atsleave.constant;

/**
 * Created by apinun.w on 13/5/2558.
 */
public enum RequestMethod {
    POST,
    GET
}
