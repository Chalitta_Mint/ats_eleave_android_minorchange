package android.mobile.lms.aware.com.atsleave.utils;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.graphics.Typeface;
import android.mobile.lms.aware.com.atsleave.R;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;

import android.mobile.lms.aware.com.atsleave.widget.OnWheelChangedListener;
import android.mobile.lms.aware.com.atsleave.widget.WheelView;
import android.mobile.lms.aware.com.atsleave.widget.adapters.ArrayWheelAdapter;
import android.mobile.lms.aware.com.atsleave.widget.adapters.NumericWheelAdapter;

@SuppressLint("InflateParams") public class DateView {

    private static final String MINUTE_FIXED[] = {"0","30"};
    private static final String AMPM_FIXED[] = {"AM","PM"};

    public  static int LANGUAGE_EN = 11;
    public  static int LANGUAGE_TH = 22;
    public  String[] DATE_NAME_WEEK_TH;
    public  String[] MONTH_NAME_TH;
    public  String[] DATE_NAME_WEEK_EN;
    public  String[] MONTH_NAME_EN;
    private WheelView wheel_date;
    private WheelView wheel_hour;
    private WheelView wheel_minute;
    private WheelView wheel_hourview;
    private View view;
    private Context mContext;
    private Activity activity;
    Typeface tf;
    private NumberPicker hourAdapter;
//  private NumberPicker minuteAdapter,minuteAdapter2;
    private NumberMinuteFixed minuteAdapterFixed;
    private AmPmFixed ampmAdapterFixed;

    //static
    private static LinkedList<String> linkedlist;
    private static LinkedList<String> LinkedDate;
    private static HashMap<String, Integer> HashDateIndex;
    private static Calendar tempCarlendar;

    private LinearLayout  box_date;
    public static void setLinkedlist(LinkedList<String> linkedlist) {
        DateView.linkedlist = linkedlist;
    }

    public static void setLinkedDate(LinkedList<String> linkedDate) {
        LinkedDate = linkedDate;
    }

    public static void setHashDateIndex(HashMap<String, Integer> hashDateIndex) {
        HashDateIndex = hashDateIndex;
    }

    public static void setTempCarlendar(Calendar tempCarlendar) {
        DateView.tempCarlendar = tempCarlendar;
    }

    public DateView(Context context) {

        DATE_NAME_WEEK_TH = context.getResources().getStringArray(R.array.DaysOfWeek);    ;
        MONTH_NAME_TH = context.getResources().getStringArray(R.array.MonthName);
        DATE_NAME_WEEK_EN = context.getResources().getStringArray(R.array.DaysOfWeek_EN);
        MONTH_NAME_EN = context.getResources().getStringArray(R.array.MonthName_EN);

        //if (linkedlist == null)
        linkedlist = new LinkedList<String>();
        //if (LinkedDate == null)
        LinkedDate = new LinkedList<String>();
        //if (HashDateIndex == null)
        HashDateIndex = new HashMap<String, Integer>();
        //if (tempCarlendar == null)
        tempCarlendar = Calendar.getInstance();

        this.mContext = context;
        tf = Typeface.createFromAsset(mContext.getAssets(),
                "HelveticaNeue.ttf");
        activity = (Activity) context;
        view = activity.getLayoutInflater().inflate(R.layout.layout_wheeltime,
                null);
        wheel_date = (WheelView) view.findViewById(R.id.wheel_date);
        wheel_hour = (WheelView) view.findViewById(R.id.wheel_hour);
        wheel_minute = (WheelView) view.findViewById(R.id.wheel_minute);
        wheel_hourview = (WheelView) view.findViewById(R.id.wheel_hourview);//New 060858
         box_date = (LinearLayout)view.findViewById(R.id.box_date);//New 060858

        hourAdapter = new NumberPicker(mContext, 0, 23, 0, "%02d");
//        minuteAdapter = new NumberPicker(mContext, 30, 30, 00, "%02d");
        minuteAdapterFixed = new NumberMinuteFixed(mContext,MINUTE_FIXED,0);
        wheel_hour.setViewAdapter(hourAdapter);
        wheel_minute.setViewAdapter(minuteAdapterFixed);
        ampmAdapterFixed = new AmPmFixed(mContext,AMPM_FIXED,0);
        wheel_hourview.setViewAdapter(ampmAdapterFixed);
//		if(LinkedDate.size() == 0)
        addDateView(0);

        // time now
        wheel_date.setCurrentItem(tempCarlendar.get(Calendar.DAY_OF_YEAR) - 1);
        wheel_hour.setCurrentItem(tempCarlendar.get(Calendar.HOUR_OF_DAY));
//        wheel_minute.setCurrentItem(tempCarlendar.get(Calendar.MINUTE));
        wheel_minute.setCurrentItem(0);
        wheel_hourview.setCurrentItem(tempCarlendar.get(Calendar.AM_PM));//New 060858
        wheel_date.addChangingListener(onWheelChange);
        wheel_hour.addChangingListener(onWheelChange);

        wheel_hourview.setVisibility(View.GONE);

    }

    public void showAllDay(String status) {// By ton

        if(status.equals("SHOW")){

            wheel_hour.setVisibility(View.VISIBLE);
            wheel_minute.setVisibility(View.VISIBLE);
            //wheel_hourview.setVisibility(View.VISIBLE);
            box_date.setWeightSum(4);
        }else{

            wheel_hour.setVisibility(View.GONE);
            wheel_minute.setVisibility(View.GONE);
            box_date.setWeightSum(2);
            //wheel_hourview.setVisibility(View.GONE);
        }

    }


    private OnWheelChangedListener onWheelChange = new OnWheelChangedListener() {

        @Override
        public void onChanged(WheelView wheel, int oldValue, int newValue) {
            if (newValue >= wheel_date.getViewAdapter().getItemsCount() - 1) {
                addDateView(1);
            }
        }
    };


    public int getDate() {
        String text = LinkedDate.get(wheel_date.getCurrentItem());
        String[] splite_data = text.split(",");
        int date = Integer.parseInt(splite_data[0]);
        return date;
    }

    public int getMonth() {
        String text = LinkedDate.get(wheel_date.getCurrentItem());
        String[] splite_data = text.split(",");
        int month = Integer.parseInt(splite_data[1]);
        return month;
    }

    public int getYear() {
        String text = LinkedDate.get(wheel_date.getCurrentItem());
        String[] splite_data = text.split(",");
        int year = Integer.parseInt(splite_data[2]);
        return year;
    }


    public String getAMPM() {
        return AMPM_FIXED[wheel_hourview.getCurrentItem()];
    }

    public int getHour() {
        return wheel_hour.getCurrentItem();
    }

    public int getMinute() {
        return Integer.parseInt(MINUTE_FIXED[wheel_minute.getCurrentItem()]);
    }

    public void setDate(int date, int month, int year, int hourOfDay, int minute) {
        String key = "" + date + month + year;
        int current = 0;
        try {
            if(HashDateIndex.containsKey(key)) {
                current = HashDateIndex.get(key);
            }else{
                return;
            }
        } catch (IllegalStateException e) {
            e.printStackTrace();
        }

        String[] data = convertLinkedListToStringArray(linkedlist);

        Calendar selectCalendar = Calendar.getInstance();
        selectCalendar.set(year, month, date, hourOfDay, minute);

        Calendar currentDate = Calendar.getInstance();
        SimpleDateFormat dateFormat = new SimpleDateFormat("ddMMyyyyHHmm");


        int day=currentDate.get(Calendar.DAY_OF_YEAR);
        int hour=currentDate.get(Calendar.HOUR_OF_DAY);
        int min=currentDate.get(Calendar.MINUTE);

//		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        Date dateTimeSelect = selectCalendar.getTime();
        Date dateNow = currentDate.getTime();
        Date dateSelect  = selectCalendar.getTime();
        currentDate.add(Calendar.DATE, 30);
        Date currDate = currentDate.getTime();

        if(dateSelect.after(currDate) || dateTimeSelect.before(dateNow)){
            wheel_date.setViewAdapter(new DateViewAdapter(mContext, data, current));
            wheel_date.setCurrentItem(current);
            wheel_hour.setCurrentItem(hourOfDay);
            wheel_minute.setCurrentItem(minute == 0? 0:1);
        }else{
            wheel_date.setViewAdapter(new DateViewAdapter(mContext, data, current));
            wheel_date.setCurrentItem(current);
            wheel_hour.setCurrentItem(hourOfDay);
            wheel_minute.setCurrentItem(minute == 0? 0:1);

        }

    }


    @SuppressWarnings("unused")
    private void addDateView(int add) {
        //----------------------------------
        Date today = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy");//all time
        String yearConfig = sdf.format(today);
        //----------------------------------

        tempCarlendar.add(Calendar.YEAR, add);
        Calendar finalInYear = Calendar.getInstance();
        finalInYear.set(Integer.parseInt(yearConfig), 11, 31, 23, 59, 0);

        Calendar increaseCalendar = Calendar.getInstance();
        increaseCalendar.set(tempCarlendar.get(Calendar.YEAR), 0, 1, 0, 0, 0);
        //increaseCalendar.set(tempCarlendar.get(Calendar.YEAR), 11, 31, 23,59, 0);
        for (int i = 0; increaseCalendar.getTimeInMillis() < finalInYear
                .getTimeInMillis(); i++) {

            int date_week_index = increaseCalendar.get(Calendar.DAY_OF_WEEK);
            int month_index = increaseCalendar.get(Calendar.MONTH);

            // set language
            //set cut Sat & Sun by ton
            if(date_week_index!=1 && date_week_index!=7) {

                linkedlist.add(DATE_NAME_WEEK_EN[date_week_index - 1] + " "
                        + increaseCalendar.get(Calendar.DATE) + " "
                        + MONTH_NAME_EN[month_index]);

                String key = "" + increaseCalendar.get(Calendar.DATE)
                        + increaseCalendar.get(Calendar.MONTH)
                        + increaseCalendar.get(Calendar.YEAR);
                String value = increaseCalendar.get(Calendar.DATE) + ","
                        + increaseCalendar.get(Calendar.MONTH) + ","
                        + increaseCalendar.get(Calendar.YEAR);

                LinkedDate.add(value);
                HashDateIndex.put(key, LinkedDate.size() - 1);
            }else{
                Log.e("", ""+date_week_index);
            }
                increaseCalendar.add(Calendar.DATE, 1);

        }

        String[] dateData = convertLinkedListToStringArray(linkedlist);

        wheel_date.setViewAdapter(new DateViewAdapter(mContext, dateData, 0));
    }

    private String[] convertLinkedListToStringArray(
            LinkedList<String> linkedlist) {
        String[] stringArray = new String[linkedlist.size()];
        int size = linkedlist.size();

        for (int i = 0; i < size; i++) {
            stringArray[i] = linkedlist.get(i);
        }

        return stringArray;
    }

    public View getView() {
        return view;
    }

    private class DateViewAdapter extends ArrayWheelAdapter<String> {
        // Index of current item
        int currentItem;
        // Index of item to be highlighted
        int currentValue;

        /**
         * Constructor
         */
        public DateViewAdapter(Context context, String[] items, int current) {
            super(context, items);
            this.currentValue = current;
            setTextSize(13);
        }

        @Override
        protected void configureTextView(TextView view) {
            super.configureTextView(view);
            if (currentItem == currentValue) {
              //  view.setTextColor(0xFF0000F0);
            }
            view.setTypeface(tf);
//            view.setText((int) context.getResources().getDimension(R.dimen.text_small));
        }

        @Override
        public View getItem(int index, View cachedView, ViewGroup parent) {
            currentItem = index;
            return super.getItem(index, cachedView, parent);
        }
    }

    private class NumberPicker extends NumericWheelAdapter {
        // Index of current item
        int currentItem;
        // Index of item to be highlighted
        int currentValue;

        /**
         * Constructor
         */
        public NumberPicker(Context context, int minValue, int maxValue,
                            int current, String format) {
            super(context, minValue, maxValue, format);
            this.currentValue = current;
            setTextSize(13);
        }

        @Override
        protected void configureTextView(TextView view) {
            super.configureTextView(view);
            if (currentItem == currentValue) {
                //view.setTextColor(0xFF0000F0);
            }
            view.setTypeface(tf);
        }

        @Override
        public View getItem(int index, View cachedView, ViewGroup parent) {
            currentItem = index;
            return super.getItem(index, cachedView, parent);
        }
    }

    private class NumberMinuteFixed extends ArrayWheelAdapter<String> {
        // Index of current item
        int currentItem;
        // Index of item to be highlighted
        int currentValue;

        /**
         * Constructor
         *
         * @param context the current context
         * @param items   the items
         */
        public NumberMinuteFixed(Context context, String[] items,int current) {
            super(context, items);
            this.currentValue = current;
            setTextSize(13);
        }

        @Override
        protected void configureTextView(TextView view) {
            super.configureTextView(view);
            if (currentItem == currentValue) {
              //  view.setTextColor(0xFF0000F0);
            }
            view.setTypeface(tf);
        }

        @Override
        public View getItem(int index, View convertView, ViewGroup parent) {
            currentItem = index;
            return super.getItem(index, convertView, parent);
        }
    }

    //-------------------------------------------------------------------------
    private class AmPmFixed extends ArrayWheelAdapter<String> {
        // Index of current item
        int currentItem;
        // Index of item to be highlighted
        int currentValue;

        /**
         * Constructor
         *
         * @param context the current context
         * @param items   the items
         */
        public AmPmFixed(Context context, String[] items,int current) {
            super(context, items);
            this.currentValue = current;
            /*setTextSize((int) mContext.getResources().getDimension(
                    R.dimen.text_size_small));*/
            setTextSize(13);
        }

        @Override
        protected void configureTextView(TextView view) {
            super.configureTextView(view);
            if (currentItem == currentValue) {
                //view.setTextColor(0xFF0000F0);
            }else{
                //view.setTextColor(0xffdcdcdc);
            }
            view.setTypeface(tf);
        }

        @Override
        public View getItem(int index, View convertView, ViewGroup parent) {
            currentItem = index;
            return super.getItem(index, convertView, parent);
        }
    }
}
