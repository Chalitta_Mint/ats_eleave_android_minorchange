package android.mobile.lms.aware.com.atsleave.service;

/**
 * Created by worawit.b on 4/21/2016.
 */
public class QuickstartPreferences {
    public static final String REGISTRATION_COMPLETE = "REGISTRATION_COMPLETE";
    public static final String SENT_TOKEN_TO_SERVER = "SENT_TOKEN_TO_SERVER";
}
