package android.mobile.lms.aware.com.atsleave.fragmentview;

import android.content.Intent;
import android.mobile.lms.aware.com.atsleave.MainActivity;
import android.mobile.lms.aware.com.atsleave.R;
import android.net.Uri;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.MediaController;
import android.widget.TextView;
import android.widget.VideoView;

/**
 * Created by damrongsak.c on 20-Oct-15.
 */
public class FirstTutorialsFragment extends Fragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.activity_tutorials_first, container, false);

        String path1 = "https://ats-eleave.aware.co.th/data/tutorials/request_leave_ios.mp4";//Production
        Uri uri = Uri.parse(path1);

        VideoView video = (VideoView) v.findViewById(R.id.videoView);
        video.setVideoURI(uri);
        video.setMediaController(new MediaController(getActivity()));
        video.requestFocus();
        video.start();

        TextView btn_close = (TextView) v.findViewById(R.id.btn_close);


        btn_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), MainActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
            }
        });

        return v;
    }

    public static FirstTutorialsFragment newInstance(String text) {

        FirstTutorialsFragment f = new FirstTutorialsFragment();
        Bundle b = new Bundle();
        b.putString("msg", text);

        f.setArguments(b);

        return f;
    }
}
