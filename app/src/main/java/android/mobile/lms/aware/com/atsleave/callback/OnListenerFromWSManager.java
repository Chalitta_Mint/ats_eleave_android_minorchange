package android.mobile.lms.aware.com.atsleave.callback;

/**
 * Created by apinun.w on 15/5/2558.
 */
public interface OnListenerFromWSManager {
    void onComplete(String statusCode, String message);

    void onFailed(String statusCode, String message);

    void onFailed(String message);

    void onFailedAndForceLogout();
}
