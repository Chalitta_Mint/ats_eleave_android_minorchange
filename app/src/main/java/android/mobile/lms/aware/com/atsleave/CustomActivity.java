package android.mobile.lms.aware.com.atsleave;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.mobile.lms.aware.com.atsleave.constant.LMSGeneralConstant;
import android.mobile.lms.aware.com.atsleave.callback.RequestDialogListener;
import android.mobile.lms.aware.com.atsleave.utils.DialogUtils;
import android.mobile.lms.aware.com.atsleave.utils.EventBus;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

/**
 * Created by apinun.w on 12/5/2558.
 */
public class CustomActivity extends AppCompatActivity {
    protected LMSApplication application;
    private BroadcastReceiver mReceiver;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    protected void onResume() {
        super.onResume();
        IntentFilter filter = new IntentFilter();
        filter.addAction(LMSGeneralConstant.ACTION);
        mReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                String message = intent.getExtras().getString("message");
                showDialog(message);
            }
        };
        registerReceiver(mReceiver, filter);
    }

    @Override
    protected void onPause() {
        super.onPause();
        unregisterReceiver(mReceiver);
    }

    public void showDialog(final String msg) {
        DialogUtils.showAlertDialog(this, msg, new RequestDialogListener() {
            @Override
            public void responseSubmit() {
                EventBus.getInstance().post(msg);
            }

            @Override
            public void responseCancel() {

            }
        });
    }
}
