package android.mobile.lms.aware.com.atsleave.adapter;

import android.content.Context;
import android.mobile.lms.aware.com.atsleave.R;
import android.mobile.lms.aware.com.atsleave.customview.RoundImageView;
import android.mobile.lms.aware.com.atsleave.models.MemberResponse;
import android.mobile.lms.aware.com.atsleave.webservice.LMSGeneralConstant;
import android.mobile.lms.aware.com.atsleave.webservice.LMSRestClient;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by apinun.w on 17/7/2558.
 */
public class ListMyTeamAdapter extends BaseAdapter {
    Context context;
    List<MemberResponse.DataEntity> members;
    String USER_EMP_ROLE;
    View.OnClickListener onClick;
    private boolean isEnableSwipe = false;
    private boolean isEnableCheckBox = false;
    private List<Boolean> checkBoxData;

    public boolean isEnableCheckBox() {
        return isEnableCheckBox;
    }

    public void setIsEnableCheckBox(boolean isEnableCheckBox) {
        this.isEnableCheckBox = isEnableCheckBox;
        if (isEnableCheckBox == false) {
            checkBoxData = createCheckBoxData(members.size());
        }
    }

    public List<Boolean> getCheckBoxData() {
        return checkBoxData;
    }

    public void setCheckBoxData(List<Boolean> checkBoxData) {
        this.checkBoxData = checkBoxData;
    }

    public ListMyTeamAdapter(Context context, List<MemberResponse.DataEntity> members, String USER_EMP_ROLE, View.OnClickListener onClick) {
        this.context = context;
        this.members = members;
        this.USER_EMP_ROLE = USER_EMP_ROLE;
        this.onClick = onClick;
        checkBoxData = createCheckBoxData(members.size());
    }

    public List<MemberResponse.DataEntity> getMembers() {
        return members;
    }

    public void setMembers(List<MemberResponse.DataEntity> members) {
        this.members = members;
        checkBoxData = createCheckBoxData(members.size());
    }

    public String getEMP_ROLE() {
        return USER_EMP_ROLE;
    }

    public void setEMP_ROLE(String EMP_ROLE) {
        this.USER_EMP_ROLE = EMP_ROLE;
    }

    @Override
    public int getCount() {
        return members.size();
    }

    @Override
    public Object getItem(int position) {
        return members.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        Holder holder = null;
        if (convertView == null) {
            holder = new Holder();
            convertView = LayoutInflater.from(context).inflate(R.layout.layout_listitem_myteam, parent, false);
//            holder.linear_right = (Button) convertView.findViewById(R.id.linear_right);
            holder.img_arrow_icon = (ImageView) convertView.findViewById(R.id.img_arrow_icon);
            holder.roundimage = (RoundImageView) convertView.findViewById(R.id.round_img_emp);
            holder.checkBox = (CheckBox) convertView.findViewById(R.id.checkBox);
            holder.txt_name = (TextView) convertView.findViewById(R.id.txt_name);
            holder.txt_position = (TextView) convertView.findViewById(R.id.txt_position);
            holder.txt_id = (TextView) convertView.findViewById(R.id.txt_id);
            holder.txt_mobile_number = (TextView) convertView.findViewById(R.id.txt_mobile_number);
            holder.txt_birthday = (TextView) convertView.findViewById(R.id.txt_birthday);
            holder.txt_email = (TextView) convertView.findViewById(R.id.txt_email);
            holder.icon_mobile = (ImageView) convertView.findViewById(R.id.icon_mobile);
            holder.icon_email = (ImageView) convertView.findViewById(R.id.icon_email);
            convertView.setTag(holder);
        } else {
            holder = (Holder) convertView.getTag();
        }

        holder.checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                checkBoxData.set(position, isChecked);
                onClick.onClick(buttonView);
            }
        });

        if (members.get(position).getPROFILE_IMAGE_PATH() != null) {
            ImageLoader.getInstance().displayImage(LMSRestClient.getImageURL() + members.get(position).getPROFILE_IMAGE_PATH(), holder.roundimage);
        } else {
            holder.roundimage.setImageResource(R.drawable.img_person);
        }

        holder.txt_name.setText(members.get(position).getNAME_ENG() + " (" + members.get(position).getNICKNAME_ENG() + ") " + members.get(position).getSURNAME_ENG());
        holder.txt_id.setText("ID#" + members.get(position).getEMP_CODE());
        holder.txt_mobile_number.setText(members.get(position).getMOBILE_NUMBER());
        Date birthday_date = null;
        try {
            birthday_date = LMSGeneralConstant.sdp_input.parse(members.get(position).getBIRTH_DATE());
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (USER_EMP_ROLE.equalsIgnoreCase(LMSGeneralConstant.EMP_ROLE.SUPERVISOR_TEXT) || USER_EMP_ROLE.equalsIgnoreCase(LMSGeneralConstant.EMP_ROLE.DIRECTOR_TEXT)) {
            holder.img_arrow_icon.setVisibility(View.VISIBLE);
        } else {
            holder.img_arrow_icon.setVisibility(View.INVISIBLE);
        }

//        holder.img_arrow_icon.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                v.setTag(position);
//                onClick.onClick(v);
//            }
//        });
//
        if (isEnableCheckBox) {
            // if(USER_EMP_ROLE.equalsIgnoreCase(LMSGeneralConstant.EMP_ROLE.SUPERVISOR_TEXT) && (members.get(position).getEMP_ROLE().equalsIgnoreCase(LMSGeneralConstant.EMP_ROLE.DIRECTOR_TEXT)||members.get(position).getEMP_ROLE().equalsIgnoreCase(LMSGeneralConstant.EMP_ROLE.SUPERVISOR_TEXT))){
            if (position == 0 && members.get(position).getEMP_ROLE().equalsIgnoreCase(LMSGeneralConstant.EMP_ROLE.DIRECTOR_TEXT)) {
                holder.checkBox.setVisibility(View.GONE);
            } else {
                holder.checkBox.setVisibility(View.VISIBLE);
                holder.checkBox.setChecked(checkBoxData.get(position));
            }

        } else {
            holder.checkBox.setVisibility(View.GONE);
        }

        if (birthday_date != null) {
            holder.txt_birthday.setText(LMSGeneralConstant.sdp_output_myteam.format(birthday_date));
        } else {
            holder.txt_birthday.setText("");
        }

        holder.txt_position.setText(members.get(position).getPOSITION() + " - " + members.get(position).getASSIGN_ROLE());
        holder.txt_email.setText(members.get(position).getEMAIL());

        if (USER_EMP_ROLE.equalsIgnoreCase(LMSGeneralConstant.EMP_ROLE.SUPERVISOR_TEXT) || USER_EMP_ROLE.equalsIgnoreCase(LMSGeneralConstant.EMP_ROLE.DIRECTOR_TEXT)) {
            holder.txt_mobile_number.setVisibility(View.VISIBLE);
            holder.icon_mobile.setVisibility(View.VISIBLE);
        } else {
            if (members.get(position).getEMP_ROLE().equalsIgnoreCase(LMSGeneralConstant.EMP_ROLE.SUPERVISOR_TEXT)
                    || members.get(position).getEMP_ROLE().equalsIgnoreCase(LMSGeneralConstant.EMP_ROLE.DIRECTOR_TEXT)) {
                if (USER_EMP_ROLE.equalsIgnoreCase(LMSGeneralConstant.EMP_ROLE.EMPLOYEE_TEXT)) {
                    if (position == 0) {
                        holder.txt_mobile_number.setVisibility(View.VISIBLE);
                        holder.icon_mobile.setVisibility(View.VISIBLE);
                    } else {
                        holder.txt_mobile_number.setVisibility(View.GONE);
                        holder.icon_mobile.setVisibility(View.GONE);
                    }
                } else {
                    holder.txt_mobile_number.setVisibility(View.VISIBLE);
                    holder.icon_mobile.setVisibility(View.VISIBLE);
                }

            } else {
                holder.txt_mobile_number.setVisibility(View.GONE);
                holder.icon_mobile.setVisibility(View.GONE);
            }
        }

//
//        Animation anim = AnimationUtils.loadAnimation(
//                context, R.anim.slide_in_on_right
//        );
//        anim.setDuration(300 + (position * 10));
//        convertView.startAnimation(anim);

        return convertView;
    }

    private class Holder {
        RoundImageView roundimage;
        CheckBox checkBox;
        TextView txt_name;
        TextView txt_position;
        TextView txt_id;
        TextView txt_mobile_number;
        TextView txt_birthday;
        TextView txt_email;
        ImageView img_arrow_icon;
        ImageView icon_mobile;
        ImageView icon_email;
        TextView txt_assignrole;
    }


    @Override
    public int getItemViewType(int position) {
        if (members.get(position).getEMP_ROLE().equalsIgnoreCase(LMSGeneralConstant.EMP_ROLE.SUPERVISOR_TEXT)) {
            return LMSGeneralConstant.EMP_ROLE.SUPERVISOR;
        } else if (members.get(position).getEMP_ROLE().equalsIgnoreCase(LMSGeneralConstant.EMP_ROLE.DIRECTOR_TEXT)) {
            return LMSGeneralConstant.EMP_ROLE.DIRECTOR;
        } else if (members.get(position).getEMP_ROLE().equalsIgnoreCase(LMSGeneralConstant.EMP_ROLE.EMPLOYEE_TEXT)) {
            return LMSGeneralConstant.EMP_ROLE.EMPLOYEE;
        } else {
            return LMSGeneralConstant.EMP_ROLE.EMPLOYEE;
        }
    }

    private List<Boolean> createCheckBoxData(int size) {
        List<Boolean> checkBoxData = new ArrayList<>();
        for (int i = 0; i < size; i++) {
            checkBoxData.add(false);
        }
        return checkBoxData;
    }
}

