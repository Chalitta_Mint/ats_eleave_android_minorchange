package android.mobile.lms.aware.com.atsleave.models;

public class LeaveHistory {

    private String RLD_DATE_START;
    private String RL_STATUSID;
    private String NAME_ENG;
    private String RL_ID;
    private String NICKNAME_ENG;
    private String LEAVETYPEID;
    private String SURNAME_THAI;
    private String NICKNAME_THAI;
    private String STF_ID;
    private String NAME_THAI;
    private String RLD_DATE_END;
    private String SURNAME_ENG;
    private String TRANSACTION_BY;
    private String RL_DATE;
    private String REASON;
    private String RL_UPDATEDATE;
    private String REMARK;
    private String RLD_ALLTIME;
    
    public LeaveHistory(String RLD_DATE_START,String RL_STATUSID,String NAME_ENG,String RL_ID,String NICKNAME_ENG,String LEAVETYPEID,String SURNAME_THAI,String NICKNAME_THAI,String STF_ID,String NAME_THAI,
                        String RLD_DATE_END,String SURNAME_ENG,String TRANSACTION_BY,String RL_DATE,String REASON,String RL_UPDATEDATE,String REMARK,String RLD_ALLTIME) {
        // TODO Auto-generated constructor stub

            this.RLD_DATE_START = RLD_DATE_START;
            this.RL_STATUSID = RL_STATUSID;
            this.NAME_ENG = NAME_ENG;
            this.RL_ID = RL_ID;
            this.NICKNAME_ENG = NICKNAME_ENG;
            this.LEAVETYPEID = LEAVETYPEID;
            this.SURNAME_THAI = SURNAME_THAI;
            this.NICKNAME_THAI = NICKNAME_THAI;
            this.STF_ID = STF_ID;
            this.NAME_THAI = NAME_THAI;
            this.RLD_DATE_END = RLD_DATE_END;
            this.SURNAME_ENG = SURNAME_ENG;
            this.TRANSACTION_BY = TRANSACTION_BY;
            this.RL_DATE = RL_DATE;
            this.REASON = REASON;
            this.RL_UPDATEDATE = RL_UPDATEDATE;
            this.REMARK = REMARK;
            this.RLD_ALLTIME = RLD_ALLTIME;

    }

    public String getRLD_DATE_START() {
        return RLD_DATE_START;
    }

    public String getRL_STATUSID() {
        return RL_STATUSID;
    }

    public String getNAME_ENG() {
        return NAME_ENG;
    }

    public String getRL_ID() {
        return RL_ID;
    }

    public String getNICKNAME_ENG() {
        return NICKNAME_ENG;
    }

    public String getLEAVETYPEID() {
        return LEAVETYPEID;
    }

    public String getSURNAME_THAI() {
        return SURNAME_THAI;
    }

    public String getNICKNAME_THAI() {
        return NICKNAME_THAI;
    }

    public String getSTF_ID() {
        return STF_ID;
    }

    public String getNAME_THAI() {
        return NAME_THAI;
    }

    public String getRLD_DATE_END() {
        return RLD_DATE_END;
    }

    public String getSURNAME_ENG() {
        return SURNAME_ENG;
    }

    public String getTRANSACTION_BY() {
        return TRANSACTION_BY;
    }

    public String getRL_DATE() {
        return RL_DATE;
    }

    public String getREASON() {
        return REASON;
    }

    public String getRL_UPDATEDATE() {
        return RL_UPDATEDATE;
    }

    public String getREMARK() {
        return REMARK;
    }

    public String getRLD_ALLTIME() {
        return RLD_ALLTIME;
    }


}
