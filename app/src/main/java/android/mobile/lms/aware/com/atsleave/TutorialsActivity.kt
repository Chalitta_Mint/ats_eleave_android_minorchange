package android.mobile.lms.aware.com.atsleave

import android.mobile.lms.aware.com.atsleave.constant.SaveSharedPreference
import android.mobile.lms.aware.com.atsleave.fragmentview.FirstTutorialsFragment
import android.mobile.lms.aware.com.atsleave.fragmentview.FourTutorialsFragment
import android.mobile.lms.aware.com.atsleave.fragmentview.SecondTutorialsFragment
import android.mobile.lms.aware.com.atsleave.fragmentview.ThirdTutorialsFragment
import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import androidx.lifecycle.Lifecycle
import androidx.viewpager.widget.ViewPager
import androidx.viewpager2.adapter.FragmentStateAdapter
import androidx.viewpager2.widget.ViewPager2

class TutorialsActivity : FragmentActivity() {
    var application: LMSApplication? = null
    var pageShow1: Fragment? = null
    var pageShow2: Fragment? = null
    var saveSharedPreference: SaveSharedPreference? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_tutorials)
        saveSharedPreference = SaveSharedPreference(this)
        application = getApplication() as LMSApplication
        saveSharedPreference!!.saveViewTutorials(false)
        val pager = findViewById<View>(R.id.viewpager) as ViewPager2
        pager.adapter = MyPagerAdapter(supportFragmentManager, lifecycle)
        if (application!!.curProfile.emP_ROLE == "EMPLOYEE") {
            pageShow1 = FirstTutorialsFragment.newInstance("FirstFragment,Instance 1")
            pageShow2 = SecondTutorialsFragment.newInstance("SecondFragment, Instance 2")
        } else {
            pageShow1 = ThirdTutorialsFragment.newInstance("ThirdFragment, Instance 3")
            pageShow2 = FourTutorialsFragment.newInstance("FourFragment, Instance 4")
        }
    }

    private inner class MyPagerAdapter(fragmentManager: FragmentManager, lifecycle: Lifecycle) : FragmentStateAdapter(fragmentManager, lifecycle) {
        override fun getItemCount(): Int {
            return 2
        }

        override fun createFragment(position: Int): Fragment {
            return when (position) {
                0 -> pageShow1!!
                1 -> pageShow2!!
                else -> FirstTutorialsFragment.newInstance("FirstFragment, Default")
            }
        }
    }
}