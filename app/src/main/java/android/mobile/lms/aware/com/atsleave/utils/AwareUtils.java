package android.mobile.lms.aware.com.atsleave.utils;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.drawable.BitmapDrawable;
import android.media.ExifInterface;
import android.mobile.lms.aware.com.atsleave.LMSApplication;
import android.mobile.lms.aware.com.atsleave.LoginActivity;
import android.mobile.lms.aware.com.atsleave.R;
import android.mobile.lms.aware.com.atsleave.constant.SaveSharedPreference;
import android.mobile.lms.aware.com.atsleave.callback.RequestDialogListener;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.telephony.TelephonyManager;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;

import com.prolificinteractive.materialcalendarview.CalendarDay;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

import me.leolin.shortcutbadger.ShortcutBadger;

/**
 * Created by apinun.w on 16/7/2558.
 */
public class AwareUtils {

    private static SaveSharedPreference saveSharedPreference;

    public static void blurImage(Context context, Bitmap bkg, View view) {
        long startMs = System.currentTimeMillis();
        float radius = 20;

        Bitmap overlay = Bitmap.createBitmap((int) (view.getMeasuredWidth()),
                (int) (view.getMeasuredHeight()), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(overlay);
        canvas.translate(-view.getLeft(), -view.getTop());
        canvas.drawBitmap(bkg, 0, 0, null);
        // overlay = FastBlur.doBlur(overlay, (int)radius, true);
        view.setBackgroundDrawable(new BitmapDrawable(context.getResources(), overlay));
        // statusText.setText(System.currentTimeMillis() - startMs + "ms");
    }

    public static int dp2px(Context context, int dp) {
        return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, context.getResources().getDisplayMetrics());
    }

    public static long getDifferenceDate(Date startDate, Date endDate) {
//        Calendar start = Calendar.getInstance();
//        Calendar end = Calendar.getInstance();
//        start.set(2010, 7, 23);
//        end.set(2010, 8, 26);
//        Date startDate = start.getTime();
//        Date endDate = end.getTime();
        long startTime = startDate.getTime();
        long endTime = endDate.getTime();
        long diffTime = endTime - startTime;
        long diffDays = diffTime / (1000 * 60 * 60 * 24);
        DateFormat dateFormat = DateFormat.getDateInstance();
//        Log.e("", "The difference between " +
//                dateFormat.format(startDate) + " and " +
//                dateFormat.format(endDate) + " is " +
//                diffDays + " days.");
        return diffDays;
    }

    public static float getDifferenceHours(Date startDate, Date endDate) {
//        Calendar start = Calendar.getInstance();
//        Calendar end = Calendar.getInstance();
//        start.set(2010, 8, 23,8,30);
//        end.set(2010, 8, 23,12,00);
//        startDate = start.getTime();
//        endDate = end.getTime();
        long startTime = startDate.getTime();
        long endTime = endDate.getTime();
        long diffTime = endTime - startTime;
        long diffSeconds = diffTime / 1000 % 60;
        long diffMinutes = diffTime / (60 * 1000) % 60;
        long diffHours = diffTime / (60 * 60 * 1000) % 24;
        long diffDays = diffTime / (24 * 60 * 60 * 1000);
        DateFormat dateFormat = DateFormat.getDateInstance();
//        Log.e("", "The difference between " +
//                dateFormat.format(startDate) + " and " +
//                dateFormat.format(endDate) + " is " + diffHours + " Hours "+
//                diffMinute + " minutes.");
        diffDays = diffDays == 0 ? 1 : diffDays;
        Date startBreak = new Date(startTime);
        startBreak.setHours(12);
        int countBreakTime = 0;

        while (startDate.before(endDate)) {
            if (startDate.getHours() >= 13) {
                countBreakTime++;
            }
            startDate.setDate(startDate.getDate() + 1);
        }

        return diffHours + countBreakTime + (diffMinutes / 60);
    }

    public static void keyboardHide(Activity act) {
//      Check if no view has focus:
        View view = act.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) act.getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    public static String[] CalculateDateWorkingFromHour(float hours) {

        float days = 0;

        if (hours > 9) {
            if (hours > 9) {
                hours -= 15.5;
                days++;
            }

            while (hours > 17.5) {
                hours -= 24;
                days++;
            }

            // Last day
            if (hours <= 17.5) {
                hours -= 8.5;
            }
        }

        if (hours == 9) {
            hours = 0;
            days++;
        } else {
            hours--;
        }

        return new String[]{((int) days) + "", hours + ""};
    }

    public static String[] CalculateDateWorkingFromMinute(int minutes) {
        float hours = minutes / 60.0f;
        float days = 0;

        if (hours > 8) {

            while (hours > 8.0) {
                hours -= 8.0;
                days++;
            }
        }

        if (hours == 8) {
            days++;
            hours = 0;
        } else {
            //hours < 8
            //not doing
        }

        return new String[]{((int) days) + "", hours + ""};
    }

    public static String CalculateDayWorkingFromMinute(int minutes) {
        float hours = minutes / 60.0f;
        float days = 0;

        if (hours > 8) {

            while (hours > 8.0) {
                hours -= 8.0;
                days++;
            }
        }

        if (hours == 8) {
            days++;
            hours = 0;
        } else {
            //hours < 8
            //not doing
        }

        return ((int) days) + "" + (hours / 8.0f) + "";
    }

    public static int getNevigationBar(Context context) {
        int result = 0;
        int resourceId = context.getResources().getIdentifier("status_bar_height", "dimen", "android");
        if (resourceId > 0) {
            result = context.getResources().getDimensionPixelSize(resourceId);
        }
        return result;
    }

    public static void callMobilePhone(Context context, String mobileNumber) {
        String number = mobileNumber;
        Intent intent = new Intent(Intent.ACTION_CALL);
        intent.setData(Uri.parse("tel:" + number));
        context.startActivity(intent);
    }

    public static void sendEmail(Context context, String email_from, String email_to) {
        String[] recipients = {email_to};
        Intent email = new Intent(Intent.ACTION_SEND, Uri.parse("mailto:"));
        // prompts email clients only
        email.setType("message/rfc822");
        email.putExtra(Intent.EXTRA_EMAIL, recipients);
//        i.putExtra(Intent.EXTRA_SUBJECT, "subject of email");
//        i.putExtra(Intent.EXTRA_TEXT   , "body of email");

        try {
            // the user can choose the email client
            context.startActivity(Intent.createChooser(email, "Choose an email client from..."));

        } catch (android.content.ActivityNotFoundException ex) {
            Toast.makeText(context, "No email client installed.", Toast.LENGTH_LONG).show();
        }
    }

    public static String getIMEI(Context context) {
        TelephonyManager mngr = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
        mngr.getDeviceId();
        String IMEI = mngr.getDeviceId();
        if (IMEI == null) {
            IMEI = "123456";
        }
        return IMEI;
    }

    //###################### GET DEVICE #################################
    public static String getDeviceToken(Context context) {
        TelephonyManager tManager = (TelephonyManager) context.getSystemService(context.TELEPHONY_SERVICE);
        String unique = tManager.getDeviceId();
        return unique;
    }

    public static float convertDpToPixel(float dp, Context context) {
        Resources resources = context.getResources();
        DisplayMetrics metrics = resources.getDisplayMetrics();
        float px = dp * (metrics.densityDpi / 160f);
        return px;
    }

    public static int dpAsPixel(Context context, float sizeInDp) {
        float scale = context.getResources().getDisplayMetrics().density;
        int dpAsPixels = (int) (sizeInDp * scale + 0.5f);

        return dpAsPixels;
    }

    /*
 * isOnline - Check if there is a NetworkConnection
 * @return boolean
 */
    public static boolean isOnline(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        if (netInfo != null && netInfo.isConnected()) {
            return true;
        } else {
            return false;
        }
    }


    public static boolean checkExpireDate(Activity activity, String str_startDate, String formatDate) {
        SimpleDateFormat format = new SimpleDateFormat(formatDate);
        Date startDate = new Date();
        Date curDate = new Date();
        try {
            startDate = format.parse(str_startDate);
            curDate = getCurTimeFromServer(activity);
            Log.e("checkExpireDate", "curDate =" + curDate.toString());
            Log.e("checkExpireDate", "startDate =" + str_startDate.toString());
        } catch (Exception e) {
            e.printStackTrace();
            return true;
        }
        long time = startDate.getTime() - 1800000;
        startDate.setTime(time);
        if (curDate.before(startDate)) {
            return false;
        } else {
            return true;
        }
    }

    public static Date getCurTimeFromServer(Activity activity) throws Exception {
        LMSApplication application = LMSApplication.getInstance();
        String str_cur_time = application.getServerTime();

        SimpleDateFormat sd = new SimpleDateFormat("yyyy-MM-dd'T'kk:mm:ssZ", Locale.US);
        sd.setTimeZone(TimeZone.getTimeZone("Etc/UTC"));
        Date date = sd.parse(str_cur_time);
        Log.i("getCurTimeFromServer", "CurTime = " + date.toString());
        return date;
    }

    private static String mCurrentPhotoPath;

    public static File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );

        mCurrentPhotoPath = "file:" + image.getAbsolutePath();
        return image;
    }

    public static String getRealPathFromURI(Context context, Uri contentURI) {

        String result = "";
        try {
            String[] filePathColumn = {MediaStore.Images.Media.DATA};
            Cursor cursor = context.getContentResolver().query(contentURI, filePathColumn, null, null, null);
            if (cursor.moveToFirst()) {
                int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                result = cursor.getString(columnIndex);
            }
            cursor.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    public static void copy(File src, File dst) throws IOException {
        InputStream in = new FileInputStream(src);
        OutputStream out = new FileOutputStream(dst);

        // Transfer bytes from in to out
        byte[] buf = new byte[1024];
        int len;
        while ((len = in.read(buf)) > 0) {
            out.write(buf, 0, len);
        }
        in.close();
        out.close();
    }

    public static boolean isForceLogout = false;

    public static void forceLogOut(final Activity activity) {
        isForceLogout = true;

        DialogUtils.showAlertDialog(activity, activity.getString(R.string.alert_text_log_out), false, new RequestDialogListener() {
            @Override
            public void responseSubmit() {
                isForceLogout = false;
                saveSharedPreference = new SaveSharedPreference(activity);
                saveSharedPreference.saveToken("");
                saveSharedPreference.saveJSON_LOGIN("");
                saveSharedPreference.saveHBDStatus(false);

                saveSharedPreference.saveHBDStatusRequest(true);
                saveSharedPreference.saveHBDStatusMessage(true);
                saveSharedPreference.saveDayoffStatusMessage(true);
                //ShortcutBadger.applyCount(activity,0);
                ShortcutBadger.with(activity).count(0);

                Intent logoutIntent = new Intent(activity, LoginActivity.class);
                activity.startActivity(logoutIntent);
                activity.finish();

            }

            @Override
            public void responseCancel() {
                isForceLogout = false;
            }
        });

    }

    public static boolean convertBitmapToFile(Bitmap bm, String mCurrentPhotoPath) {
        if (bm != null) {
            OutputStream os = null;
            try {
                os = new FileOutputStream(new File(mCurrentPhotoPath));
                bm.compress(Bitmap.CompressFormat.PNG, 100, os);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
                return false;
            }

        } else {
            return false;
        }

        return true;
    }

    public static Bitmap rotateBitmapCorrect(String realPath) {
        Bitmap bitmap = null;
        try {
            bitmap = BitmapFactory.decodeFile(realPath);
            if (bitmap == null)
                return null;
            ExifInterface exif = new ExifInterface(realPath);
            int orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, 1);
            Log.d("EXIF", "Exif: " + orientation);
            Matrix matrix = new Matrix();
            if (orientation == 6) {
                matrix.postRotate(90);
            } else if (orientation == 3) {
                matrix.postRotate(180);
            } else if (orientation == 8) {
                matrix.postRotate(270);
            }
            bitmap = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true); // rotating bitmap
        } catch (Exception e) {

        }

        return bitmap;
    }

    public static int checkOrientation(String image) {
        ExifInterface exif = null;
        int degree = 0;
        try {
            exif = new ExifInterface(image);
        } catch (IOException ex) {
        }
        if (exif != null) {
            int orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, -1);
            if (orientation != -1) {
                switch (orientation) {
                    case ExifInterface.ORIENTATION_ROTATE_90:
                        degree = 90;
                        break;
                    case ExifInterface.ORIENTATION_ROTATE_180:
                        degree = 180;
                        break;
                    case ExifInterface.ORIENTATION_ROTATE_270:
                        degree = 270;
                        break;
                    default:
                        degree = 0;
                        break;
                }
            }
        }
        return degree;
    }

    public static String notUseSuffix(File file) {
        String name = file.getName();
        int pos = name.lastIndexOf(".");
        if (pos > 0) {
            name = name.substring(0, pos);
        }

        return name;
    }

    public static String pad(int num) {
        if (num < 10) {
            return "0" + num;
        }
        return num + "";
    }

    public static String getDateFormat(CalendarDay date) {
        return date.getYear() + "-" + pad(date.getMonth() + 1) + "-" + pad(date.getDay());
    }

    public static String getYearRequest() {
        LMSApplication application = LMSApplication.getInstance();
        String str_cur_time = application.getServerTime();
        String curYear = str_cur_time.substring(0, 4);
        int prevYear = Integer.parseInt(curYear) - 1;
        int nextYear = Integer.parseInt(curYear) + 1;
        return prevYear + "," + curYear + "," + nextYear;
    }

    public static String getCurrentNextYearRequest() {
        LMSApplication application = LMSApplication.getInstance();
        String str_cur_time = application.getServerTime();
        String curYear = str_cur_time.substring(0, 4);
        int nextYear = Integer.parseInt(curYear) + 1;
        return curYear + "," + nextYear;
    }

    public static String getCurrentYearRequest() {
        LMSApplication application = LMSApplication.getInstance();
        String str_cur_time = application.getServerTime();
        return str_cur_time.substring(0, 4);
    }
}
