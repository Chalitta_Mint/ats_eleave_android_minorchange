package android.mobile.lms.aware.com.atsleave.models;

/**
 * Created by apinun.w on 18/8/2558.
 */
public class BaseResponse {

    private ResponseEntity Response;

    public void setResponse(ResponseEntity Response) {
        this.Response = Response;
    }

    public ResponseEntity getResponse() {
        return Response;
    }

}
