package android.mobile.lms.aware.com.atsleave;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.graphics.Bitmap;
import android.mobile.lms.aware.com.atsleave.constant.SaveSharedPreference;
import android.mobile.lms.aware.com.atsleave.models.LoginResponse;
import android.os.Bundle;
import android.os.Handler;
import android.view.Display;
import android.view.WindowManager;

import com.crashlytics.android.Crashlytics;
import com.google.gson.Gson;
import com.nostra13.universalimageloader.cache.disc.impl.UnlimitedDiscCache;
import com.nostra13.universalimageloader.cache.disc.naming.HashCodeFileNameGenerator;
import com.nostra13.universalimageloader.cache.memory.impl.LruMemoryCache;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.assist.QueueProcessingType;
import com.nostra13.universalimageloader.core.display.SimpleBitmapDisplayer;
import com.nostra13.universalimageloader.core.download.BaseImageDownloader;
import com.nostra13.universalimageloader.utils.StorageUtils;

import java.io.File;
import java.util.HashMap;

import io.fabric.sdk.android.Fabric;

/**
 * Created by apinun.w on 14/5/2558.
 */
public class LMSApplication extends Application implements Application.ActivityLifecycleCallbacks {
    private static LMSApplication instance;
    private DisplayImageOptions options;
    private int width_screen = 0;
    private HashMap<String, String[]> dayTokenDatas;
    private String serverTime = "";
    private boolean isInterestingActivityVisible = false;

    private int intSickLeaveShowBadge = 0;
    private int intAnnualShowBadge = 0;
    private int intOtherShowBadge = 0;
    private int intEmpShowBadge = 0;

    private String str_holidays;
    private int height_screen = 0;
    private SaveSharedPreference sharedPreference;

    public static LMSApplication getInstance() {
        return instance;
    }

    public String getServerTime() {
        return serverTime;
    }

    public void setServerTime(String serverTime) {
        this.serverTime = serverTime;
    }

    public String getStr_holidays() {
        return str_holidays;
    }

    public void setStr_holidays(String str_holidays) {
        this.str_holidays = str_holidays;
    }

    public int getIntEmpShowBadge() {
        return intEmpShowBadge;
    }

    public void setIntEmpShowBadge(int intEmpShowBadge) {
        this.intEmpShowBadge = intEmpShowBadge;
    }

    public int getIntSickLeaveShowBadge() {
        return intSickLeaveShowBadge;
    }

    public void setIntSickLeaveShowBadge(int intSickLeaveShowBadge) {
        this.intSickLeaveShowBadge = intSickLeaveShowBadge;
    }

    public int getIntAnnualShowBadge() {
        return intAnnualShowBadge;
    }

    public void setIntAnnualShowBadge(int intAnnualShowBadge) {
        this.intAnnualShowBadge = intAnnualShowBadge;
    }

    public int getIntOtherShowBadge() {
        return intOtherShowBadge;
    }

    public void setIntOtherShowBadge(int intOtherShowBadge) {
        this.intOtherShowBadge = intOtherShowBadge;
    }

    public HashMap<String, String[]> getDayTokenDatas() {
        return dayTokenDatas;
    }

    public void setDayTokenDatas(HashMap<String, String[]> dayTokenDatas) {
        this.dayTokenDatas = dayTokenDatas;
    }

    public int getWidth_screen() {
        return width_screen;
    }

    public int getHeight_screen() {
        return height_screen;
    }

    public DisplayImageOptions getOptions() {
        return options;
    }

    public LoginResponse.DataEntity getCurProfile() {
        sharedPreference = new SaveSharedPreference(this);
        String jsonData = sharedPreference.getLOGIN_JSON();
        if (!jsonData.equalsIgnoreCase("")) {
            Gson gson = new Gson();
            LoginResponse profileData = gson.fromJson(jsonData, LoginResponse.class);
            return profileData.getData();
        }
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Fabric.with(this, new Crashlytics());
        instance = this;

        registerActivityLifecycleCallbacks(this);

        File cacheDir = StorageUtils.getCacheDirectory(this);

        DisplayImageOptions options = new DisplayImageOptions.Builder()
                .showImageOnLoading(R.drawable.img_person) // resource or drawable
                .showImageForEmptyUri(R.drawable.img_person) // resource or drawable
                .showImageOnFail(R.drawable.img_person) // resource or drawable
                .resetViewBeforeLoading(false)  // default
                .delayBeforeLoading(0)
                .cacheInMemory(true) // default
                .cacheOnDisk(true) // default
                .considerExifParams(false) // default
                .imageScaleType(ImageScaleType.IN_SAMPLE_POWER_OF_2) // default
                .bitmapConfig(Bitmap.Config.ARGB_8888) // default
                .displayer(new SimpleBitmapDisplayer()) // default
                .handler(new Handler()) // default
                .build();

        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(this)
                .memoryCacheExtraOptions(480, 800) // default = device screen dimensions
                .diskCacheExtraOptions(480, 800, null)
//                .taskExecutor(...).taskExecutorForCachedImages(...)
                .threadPoolSize(3) // default
                .threadPriority(Thread.NORM_PRIORITY - 2) // default
                .tasksProcessingOrder(QueueProcessingType.FIFO) // default
                .denyCacheImageMultipleSizesInMemory()
                .memoryCache(new LruMemoryCache(2 * 1024 * 1024))
                .memoryCacheSize(2 * 1024 * 1024)
                .memoryCacheSizePercentage(13) // default
                .diskCache(new UnlimitedDiscCache(cacheDir)) // default
                .diskCacheSize(50 * 1024 * 1024)
                .diskCacheFileCount(100)
                .diskCacheFileNameGenerator(new HashCodeFileNameGenerator()) // default
                .imageDownloader(new BaseImageDownloader(this)) // default
                .defaultDisplayImageOptions(DisplayImageOptions.createSimple()) // default
                .writeDebugLogs()
                .defaultDisplayImageOptions(options)
                .build();
        ImageLoader.getInstance().init(config);

        getScreenSize();

    }


    private void getScreenSize() {
        Display display = ((WindowManager) getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay();
        width_screen = display.getWidth();  // deprecated
        height_screen = display.getHeight();  // deprecated
    }

    public boolean isInterestingActivityVisible() {
        return isInterestingActivityVisible;
    }

    @Override
    public void onActivityCreated(Activity activity, Bundle savedInstanceState) {
        isInterestingActivityVisible = true;
    }

    @Override
    public void onActivityStarted(Activity activity) {
        isInterestingActivityVisible = true;
    }

    @Override
    public void onActivityResumed(Activity activity) {
        isInterestingActivityVisible = true;
    }

    @Override
    public void onActivityPaused(Activity activity) {
        isInterestingActivityVisible = false;
    }

    @Override
    public void onActivityStopped(Activity activity) {

    }

    @Override
    public void onActivitySaveInstanceState(Activity activity, Bundle outState) {

    }

    @Override
    public void onActivityDestroyed(Activity activity) {

    }
}
