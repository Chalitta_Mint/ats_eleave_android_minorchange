package android.mobile.lms.aware.com.atsleave.models;

import java.util.List;

/**
 * Created by apinun.w on 24/9/2558.
 */
public class ProfileResponse extends BaseResponse {

    private List<DataEntity> data;

    public void setData(List<DataEntity> data) {
        this.data = data;
    }

    public List<DataEntity> getData() {
        return data;
    }

    public static class DataEntity {

        private String JOIN_DATE;
        private String OTHER_EMAIL;
        private String MOBILE_NUMBER;
        private String JOB_CATEGORY;
        private String PROBATION;
        private String REMARKS;
        private String EMAIL;
        private String PROFILE_IMAGE_PATH;
        private String SUP_CODE;
        private String POSITION;
        private String STF_USER_AD;
        private String SURNAME_THAI;
        private String NICKNAME_THAI;
        private String SURNAME_ENG;
        private String BIRTH_DATE;
        private String GENDER;
        private String OFFICIAL_DATE;
        private String ASSIGN_ROLE;
        private String UPDATEBY;
        private String RESIGN_DATE;
        private String STAFF_TOKEN;
        private String HASPUBLICHOLIDAY;
        private String TOKEN_EXPIRE;
        private String NAME_ENG;
        private String PUBLICHOLIDAYID;
        private String NICKNAME_ENG;
        private String LAST_WORK_DATE;
        private String CREATEDATE;
        private String SUPERVISOR;
        private String EMP_CODE;
        private String HIRE_STATUS;
        private String COMPANY;
        private String CREATEBY;
        private String NAME_THAI;
        private String STF_ID;
        private String COVER_IMAGE_PATH;
        private String EMP_ROLE;
        private String TEAMID;
        private String UPDATEDATE;
        private String RESIGN_REASON;

        public void setJOIN_DATE(String JOIN_DATE) {
            this.JOIN_DATE = JOIN_DATE;
        }

        public void setOTHER_EMAIL(String OTHER_EMAIL) {
            this.OTHER_EMAIL = OTHER_EMAIL;
        }

        public void setMOBILE_NUMBER(String MOBILE_NUMBER) {
            this.MOBILE_NUMBER = MOBILE_NUMBER;
        }

        public void setJOB_CATEGORY(String JOB_CATEGORY) {
            this.JOB_CATEGORY = JOB_CATEGORY;
        }

        public void setPROBATION(String PROBATION) {
            this.PROBATION = PROBATION;
        }

        public void setREMARKS(String REMARKS) {
            this.REMARKS = REMARKS;
        }

        public void setEMAIL(String EMAIL) {
            this.EMAIL = EMAIL;
        }

        public void setPROFILE_IMAGE_PATH(String PROFILE_IMAGE_PATH) {
            this.PROFILE_IMAGE_PATH = PROFILE_IMAGE_PATH;
        }

        public void setSUP_CODE(String SUP_CODE) {
            this.SUP_CODE = SUP_CODE;
        }

        public void setPOSITION(String POSITION) {
            this.POSITION = POSITION;
        }

        public void setSTF_USER_AD(String STF_USER_AD) {
            this.STF_USER_AD = STF_USER_AD;
        }

        public void setSURNAME_THAI(String SURNAME_THAI) {
            this.SURNAME_THAI = SURNAME_THAI;
        }

        public void setNICKNAME_THAI(String NICKNAME_THAI) {
            this.NICKNAME_THAI = NICKNAME_THAI;
        }

        public void setSURNAME_ENG(String SURNAME_ENG) {
            this.SURNAME_ENG = SURNAME_ENG;
        }

        public void setBIRTH_DATE(String BIRTH_DATE) {
            this.BIRTH_DATE = BIRTH_DATE;
        }

        public void setGENDER(String GENDER) {
            this.GENDER = GENDER;
        }

        public void setOFFICIAL_DATE(String OFFICIAL_DATE) {
            this.OFFICIAL_DATE = OFFICIAL_DATE;
        }

        public void setASSIGN_ROLE(String ASSIGN_ROLE) {
            this.ASSIGN_ROLE = ASSIGN_ROLE;
        }

        public void setUPDATEBY(String UPDATEBY) {
            this.UPDATEBY = UPDATEBY;
        }

        public void setRESIGN_DATE(String RESIGN_DATE) {
            this.RESIGN_DATE = RESIGN_DATE;
        }

        public void setSTAFF_TOKEN(String STAFF_TOKEN) {
            this.STAFF_TOKEN = STAFF_TOKEN;
        }

        public void setHASPUBLICHOLIDAY(String HASPUBLICHOLIDAY) {
            this.HASPUBLICHOLIDAY = HASPUBLICHOLIDAY;
        }

        public void setTOKEN_EXPIRE(String TOKEN_EXPIRE) {
            this.TOKEN_EXPIRE = TOKEN_EXPIRE;
        }

        public void setNAME_ENG(String NAME_ENG) {
            this.NAME_ENG = NAME_ENG;
        }

        public void setPUBLICHOLIDAYID(String PUBLICHOLIDAYID) {
            this.PUBLICHOLIDAYID = PUBLICHOLIDAYID;
        }

        public void setNICKNAME_ENG(String NICKNAME_ENG) {
            this.NICKNAME_ENG = NICKNAME_ENG;
        }

        public void setLAST_WORK_DATE(String LAST_WORK_DATE) {
            this.LAST_WORK_DATE = LAST_WORK_DATE;
        }

        public void setCREATEDATE(String CREATEDATE) {
            this.CREATEDATE = CREATEDATE;
        }

        public void setSUPERVISOR(String SUPERVISOR) {
            this.SUPERVISOR = SUPERVISOR;
        }

        public void setEMP_CODE(String EMP_CODE) {
            this.EMP_CODE = EMP_CODE;
        }

        public void setHIRE_STATUS(String HIRE_STATUS) {
            this.HIRE_STATUS = HIRE_STATUS;
        }

        public void setCOMPANY(String COMPANY) {
            this.COMPANY = COMPANY;
        }

        public void setCREATEBY(String CREATEBY) {
            this.CREATEBY = CREATEBY;
        }

        public void setNAME_THAI(String NAME_THAI) {
            this.NAME_THAI = NAME_THAI;
        }

        public void setSTF_ID(String STF_ID) {
            this.STF_ID = STF_ID;
        }

        public void setCOVER_IMAGE_PATH(String COVER_IMAGE_PATH) {
            this.COVER_IMAGE_PATH = COVER_IMAGE_PATH;
        }

        public void setEMP_ROLE(String EMP_ROLE) {
            this.EMP_ROLE = EMP_ROLE;
        }

        public void setTEAMID(String TEAMID) {
            this.TEAMID = TEAMID;
        }

        public void setUPDATEDATE(String UPDATEDATE) {
            this.UPDATEDATE = UPDATEDATE;
        }

        public void setRESIGN_REASON(String RESIGN_REASON) {
            this.RESIGN_REASON = RESIGN_REASON;
        }

        public String getJOIN_DATE() {
            return JOIN_DATE;
        }

        public String getOTHER_EMAIL() {
            return OTHER_EMAIL;
        }

        public String getMOBILE_NUMBER() {
            return MOBILE_NUMBER;
        }

        public String getJOB_CATEGORY() {
            return JOB_CATEGORY;
        }

        public String getPROBATION() {
            return PROBATION;
        }

        public String getREMARKS() {
            return REMARKS;
        }

        public String getEMAIL() {
            return EMAIL;
        }

        public String getPROFILE_IMAGE_PATH() {
            return PROFILE_IMAGE_PATH;
        }

        public String getSUP_CODE() {
            return SUP_CODE;
        }

        public String getPOSITION() {
            return POSITION;
        }

        public String getSTF_USER_AD() {
            return STF_USER_AD;
        }

        public String getSURNAME_THAI() {
            return SURNAME_THAI;
        }

        public String getNICKNAME_THAI() {
            return NICKNAME_THAI;
        }

        public String getSURNAME_ENG() {
            return SURNAME_ENG;
        }

        public String getBIRTH_DATE() {
            return BIRTH_DATE;
        }

        public String getGENDER() {
            return GENDER;
        }

        public String getOFFICIAL_DATE() {
            return OFFICIAL_DATE;
        }

        public String getASSIGN_ROLE() {
            return ASSIGN_ROLE;
        }

        public String getUPDATEBY() {
            return UPDATEBY;
        }

        public String getRESIGN_DATE() {
            return RESIGN_DATE;
        }

        public String getSTAFF_TOKEN() {
            return STAFF_TOKEN;
        }

        public String getHASPUBLICHOLIDAY() {
            return HASPUBLICHOLIDAY;
        }

        public String getTOKEN_EXPIRE() {
            return TOKEN_EXPIRE;
        }

        public String getNAME_ENG() {
            return NAME_ENG;
        }

        public String getPUBLICHOLIDAYID() {
            return PUBLICHOLIDAYID;
        }

        public String getNICKNAME_ENG() {
            return NICKNAME_ENG;
        }

        public String getLAST_WORK_DATE() {
            return LAST_WORK_DATE;
        }

        public String getCREATEDATE() {
            return CREATEDATE;
        }

        public String getSUPERVISOR() {
            return SUPERVISOR;
        }

        public String getEMP_CODE() {
            return EMP_CODE;
        }

        public String getHIRE_STATUS() {
            return HIRE_STATUS;
        }

        public String getCOMPANY() {
            return COMPANY;
        }

        public String getCREATEBY() {
            return CREATEBY;
        }

        public String getNAME_THAI() {
            return NAME_THAI;
        }

        public String getSTF_ID() {
            return STF_ID;
        }

        public String getCOVER_IMAGE_PATH() {
            return COVER_IMAGE_PATH;
        }

        public String getEMP_ROLE() {
            return EMP_ROLE;
        }

        public String getTEAMID() {
            return TEAMID;
        }

        public String getUPDATEDATE() {
            return UPDATEDATE;
        }

        public String getRESIGN_REASON() {
            return RESIGN_REASON;
        }
    }
}
