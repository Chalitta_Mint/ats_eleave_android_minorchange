package android.mobile.lms.aware.com.atsleave.webservice;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.mobile.lms.aware.com.atsleave.BuildConfig;
import android.mobile.lms.aware.com.atsleave.constant.RequestMethod;
import android.mobile.lms.aware.com.atsleave.callback.OnListenerFromWS;
import android.mobile.lms.aware.com.atsleave.models.LMSServiceModel;
import android.os.AsyncTask;
import android.util.Base64;
import android.util.Log;

import com.google.gson.GsonBuilder;
import com.google.gson.JsonParser;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.Map;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocketFactory;

public class LMSRestClient extends AsyncTask<Void, Void, String> {

    private Context mContext;
    private RequestMethod method;
    private LMSServiceModel lmsModel;
    private OnListenerFromWS onresponse;
    private String error = "";

    public static String getServiceURL() {
        if (BuildConfig.ServerID == 0) {
            return "https://ats-eleave.aware.co.th/WebserviceApi/";  //UAT
        } else if (BuildConfig.ServerID == 1) {
            return "http://172.16.51.234:88/ats-lms-test/current/public/WebserviceApi/";  //TEST
        } else if (BuildConfig.ServerID == 2) {
            return "http://172.16.51.234:88/ats-lms/current/public/webServiceApi/";  //DEV
        } else {
            return "http://172.16.51.234:88/ats-lms-test/current/public/WebserviceApi";  //TEST
        }
    }

    public static String getImageURL() {
        if (BuildConfig.ServerID == 0) {
            return "https://ats-eleave.aware.co.th/";  //UAT
        } else if (BuildConfig.ServerID == 1) {
            return "http://172.16.51.234:88/ats-lms-test/current/public/";  //TEST
        } else if (BuildConfig.ServerID == 2) {
            return "http://172.16.51.234:88/ats-lms/current/public/";  //DEV
        } else {
            return "http://172.16.51.234:88/ats-lms-test/current/public/";  //TEST
        }
    }

    public LMSRestClient(Activity activity, RequestMethod method, LMSServiceModel lmsModel, OnListenerFromWS onresponse) {
        this.mContext = activity;
        this.method = method;
        this.lmsModel = lmsModel;
        this.onresponse = onresponse;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected String doInBackground(Void... params) {
        error = "";
        try {
            return sendPost(lmsModel);
        } catch (Exception e) {
            error = e.getMessage();
            e.printStackTrace();
            return null;
        }
    }

    private String sendPost(LMSServiceModel lmsModel) throws Exception {
        String url = getServiceURL();
        URL obj = new URL(url);
        URLConnection con;
        if ("https".equals(obj.getProtocol())) {
            SSLContext sslcontext = SSLContext.getInstance("TLSv1");
            sslcontext.init(null, null, null);
            SSLSocketFactory NoSSLv3Factory = new NoSSLv3SocketFactory(sslcontext.getSocketFactory());
            HttpsURLConnection.setDefaultSSLSocketFactory(NoSSLv3Factory);
            con = obj.openConnection();
            ((HttpsURLConnection) con).setRequestMethod(method.name());
        } else {
            con = obj.openConnection();
            ((HttpURLConnection) con).setRequestMethod(method.name());
        }
        con.setConnectTimeout(20000);

        String combinedParams = "";
        if (!lmsModel.getParams().isEmpty()) {
            if (lmsModel.getRealImagePath() != null)
                lmsModel.addParams("pic", encodeImageToString(lmsModel.getRealImagePath()));

            for (Map.Entry<String, String> entry : lmsModel.getParams().entrySet()) {
                String paramString = entry.getKey() + "=" + URLEncoder.encode(entry.getValue(), "UTF-8");
                if (combinedParams.length() > 1) {
                    combinedParams += "&" + paramString;
                } else {
                    combinedParams += paramString;
                }
            }
        }

        con.setDoOutput(true);
        con.connect();
        DataOutputStream wr = new DataOutputStream(con.getOutputStream());
        wr.writeBytes(combinedParams);
        wr.flush();
        wr.close();

        BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
        String inputLine;
        StringBuilder response = new StringBuilder();

        while ((inputLine = in.readLine()) != null) {
            response.append(inputLine);
        }
        in.close();

        return response.toString();
    }

    @Override
    protected void onPostExecute(String result) {
        Log.i("Complete", "Service " + lmsModel.getParams().get("requestType"));
        try {
            Log.i("Complete", "Response :" + new GsonBuilder().setPrettyPrinting().create().toJson(new JsonParser().parse(result)));
        } catch (Exception e) {
            Log.i("Error", e.getMessage());
        }

        if (result != null) {
            onresponse.onComplete(result);
        } else {
            onresponse.onFailed(error);
        }
    }

    private String encodeImageToString(String imgPath) {
        BitmapFactory.Options options = null;
        options = new BitmapFactory.Options();
        options.inSampleSize = 1;
        Bitmap bitmap = BitmapFactory.decodeFile(imgPath, options);
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        // Must compress the Image to reduce image size to make upload easy
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, stream);
        byte[] byte_arr = stream.toByteArray();
        // Encode Image to String
        return Base64.encodeToString(byte_arr, 0);
    }
}
