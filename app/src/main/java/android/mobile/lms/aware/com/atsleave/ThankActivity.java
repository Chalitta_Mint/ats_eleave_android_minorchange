package android.mobile.lms.aware.com.atsleave;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;


public class ThankActivity extends CustomActivity {
    String salulation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_request_thank);
        salulation = getIntent().getStringExtra("salulation");//Get
        TextView text_detail = (TextView) findViewById(R.id.text_detail);
        ImageView btn_home = (ImageView) findViewById(R.id.btn_home);
        text_detail.setText(salulation);
        btn_home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                finish();
            }
        });
    }
}