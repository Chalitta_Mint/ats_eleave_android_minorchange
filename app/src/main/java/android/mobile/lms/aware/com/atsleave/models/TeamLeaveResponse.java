package android.mobile.lms.aware.com.atsleave.models;

import java.util.List;

/**
 * Created by apinun.w on 4/8/2558.
 */
public class TeamLeaveResponse extends BaseResponse {

    private List<DataEntity> data;

    public void setData(List<DataEntity> data) {
        this.data = data;
    }

    public List<DataEntity> getData() {
        return data;
    }

    public static class DataEntity {

        private String RLD_DATE_START;
        private String RL_STATUSID;
        private String NAME_ENG;
        private String RL_ID;
        private String NICKNAME_ENG;
        private String LEAVETYPEID;
        private String SURNAME_THAI;
        private String NICKNAME_THAI;
        private String STF_ID;
        private String NAME_THAI;
        private String RLD_DATE_END;
        private String SURNAME_ENG;
        private String TRANSACTION_BY;
        private String RL_DATE;
        private String RL_DAYOFF_REF_ID;
        private String REASON;
        private String RL_UPDATEDATE;
        private String REMARK;
        private String RLD_ALLTIME;

        public void setRLD_DATE_START(String RLD_DATE_START) {
            this.RLD_DATE_START = RLD_DATE_START;
        }

        public void setRL_STATUSID(String RL_STATUSID) {
            this.RL_STATUSID = RL_STATUSID;
        }

        public void setNAME_ENG(String NAME_ENG) {
            this.NAME_ENG = NAME_ENG;
        }

        public void setRL_ID(String RL_ID) {
            this.RL_ID = RL_ID;
        }

        public void setNICKNAME_ENG(String NICKNAME_ENG) {
            this.NICKNAME_ENG = NICKNAME_ENG;
        }

        public void setLEAVETYPEID(String LEAVETYPEID) {
            this.LEAVETYPEID = LEAVETYPEID;
        }

        public void setSURNAME_THAI(String SURNAME_THAI) {
            this.SURNAME_THAI = SURNAME_THAI;
        }

        public void setNICKNAME_THAI(String NICKNAME_THAI) {
            this.NICKNAME_THAI = NICKNAME_THAI;
        }

        public void setSTF_ID(String STF_ID) {
            this.STF_ID = STF_ID;
        }

        public void setNAME_THAI(String NAME_THAI) {
            this.NAME_THAI = NAME_THAI;
        }

        public void setRLD_DATE_END(String RLD_DATE_END) {
            this.RLD_DATE_END = RLD_DATE_END;
        }

        public void setSURNAME_ENG(String SURNAME_ENG) {
            this.SURNAME_ENG = SURNAME_ENG;
        }

        public void setTRANSACTION_BY(String TRANSACTION_BY) {
            this.TRANSACTION_BY = TRANSACTION_BY;
        }

        public void setRL_DATE(String RL_DATE) {
            this.RL_DATE = RL_DATE;
        }

        public void setRL_DAYOFF_REF_ID(String RL_DAYOFF_REF_ID) {
            this.RL_DAYOFF_REF_ID = RL_DAYOFF_REF_ID;
        }

        public void setREASON(String REASON) {
            this.REASON = REASON;
        }

        public void setRL_UPDATEDATE(String RL_UPDATEDATE) {
            this.RL_UPDATEDATE = RL_UPDATEDATE;
        }

        public void setREMARK(String REMARK) {
            this.REMARK = REMARK;
        }

        public void setRLD_ALLTIME(String RLD_ALLTIME) {
            this.RLD_ALLTIME = RLD_ALLTIME;
        }

        public String getRLD_DATE_START() {
            return RLD_DATE_START;
        }

        public String getRL_STATUSID() {
            return RL_STATUSID;
        }

        public String getNAME_ENG() {
            return NAME_ENG;
        }

        public String getRL_ID() {
            return RL_ID;
        }

        public String getNICKNAME_ENG() {
            return NICKNAME_ENG;
        }

        public String getLEAVETYPEID() {
            return LEAVETYPEID;
        }

        public String getSURNAME_THAI() {
            return SURNAME_THAI;
        }

        public String getNICKNAME_THAI() {
            return NICKNAME_THAI;
        }

        public String getSTF_ID() {
            return STF_ID;
        }

        public String getNAME_THAI() {
            return NAME_THAI;
        }

        public String getRLD_DATE_END() {
            return RLD_DATE_END;
        }

        public String getSURNAME_ENG() {
            return SURNAME_ENG;
        }

        public String getTRANSACTION_BY() {
            return TRANSACTION_BY;
        }

        public String getRL_DATE() {
            return RL_DATE;
        }

        public String getRL_DAYOFF_REF_ID() {
            return RL_DAYOFF_REF_ID;
        }

        public String getREASON() {
            return REASON;
        }

        public String getRL_UPDATEDATE() {
            return RL_UPDATEDATE;
        }

        public String getREMARK() {
            return REMARK;
        }

        public String getRLD_ALLTIME() {
            return RLD_ALLTIME;
        }
    }
}
