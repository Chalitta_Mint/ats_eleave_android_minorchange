package android.mobile.lms.aware.com.atsleave.models;

/**
 * Created by apinun.w on 23/9/2558.
 */
public class ImageUploadResponse extends BaseResponse {

    private DataEntity data;

    public void setData(DataEntity data) {
        this.data = data;
    }

    public DataEntity getData() {
        return data;
    }


    public static class DataEntity {

        private String imgUrl;

        public void setImgUrl(String imgUrl) {
            this.imgUrl = imgUrl;
        }

        public String getImgUrl() {
            return imgUrl;
        }
    }
}
