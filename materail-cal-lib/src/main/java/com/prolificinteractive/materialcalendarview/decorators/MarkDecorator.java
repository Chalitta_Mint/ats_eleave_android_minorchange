package com.prolificinteractive.materialcalendarview.decorators;

import android.content.Context;
import android.graphics.Point;
import android.view.Display;
import android.view.WindowManager;

import com.prolificinteractive.materialcalendarview.CalendarDay;
import com.prolificinteractive.materialcalendarview.DayViewDecorator;
import com.prolificinteractive.materialcalendarview.DayViewFacade;
import com.prolificinteractive.materialcalendarview.spans.DotSpan;

import java.util.Collection;
import java.util.HashSet;

/**
 * Decorate several days with a dot
 */
public class MarkDecorator implements DayViewDecorator {

    private int color;

    private HashSet<CalendarDay> dates;
    private Context context;

    public MarkDecorator(Context context,int color, Collection<CalendarDay> dates) {
        this.color = color;
        this.dates = new HashSet<>(dates);
        this.context=context;
    }

    @Override
    public boolean shouldDecorate(CalendarDay day) {
        return dates.contains(day);
    }

    @Override
    public void decorate(DayViewFacade view) {
        int ci=callCircle();
        view.addSpan(new DotSpan(ci, color,"MARK"));
    }

    public int callCircle(){

        WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);

        int width = size.x;
        int height = size.y;

        int ws = 0;
        if(width>=720 ){
            ws=35;
        }else if(width>=480 ){
            ws=30;
        }else {
            ws=35;
        }

        return ws;
    }
}
